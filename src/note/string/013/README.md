# [Longest Substring Without Repeating Characters][title]

## Description

Given a roman numeral, convert it to an integer.

Input is guaranteed to be within the range from 1 to 3999.

**Tags:** Math, String


## 思路

題意是計算不帶重複字符的最長子字符串的長度，

```java
class Solution {
    public int lengthOfLongestSubstring(String s) {
    }
}
```


## 结语

如果你同我一样热爱数据结构、算法、LeetCode，可以关注我GitHub上的LeetCode题解：[awesome-java-leetcode][ajl]



[title]: https://leetcode.com/problems/roman-to-integer
[ajl]: https://github.com/Blankj/awesome-java-leetcode
