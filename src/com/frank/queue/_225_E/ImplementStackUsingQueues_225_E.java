package com.frank.queue._225_E;


import java.util.LinkedList;
import java.util.Queue;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/28
 *     title : 225. Implement Stack using Queues
 * </pre>
 */
public class ImplementStackUsingQueues_225_E {

    /***
     Implement a last-in-first-out (LIFO) stack using only two queues.
     The implemented stack should support all the functions of a normal stack (push, top, pop, and empty).

     Implement the MyStack class:

     void push(int x) Pushes element x to the top of the stack.
     int pop() Removes the element on the top of the stack and returns it.
     int top() Returns the element on the top of the stack.
     boolean empty() Returns true if the stack is empty, false otherwise.
     Notes:

     You must use only standard operations of a queue, which means that only push to back, peek/pop from front, size and is empty operations are valid.
     Depending on your language, the queue may not be supported natively. You may simulate a queue using a list or deque (double-ended queue) as long as you use only a queue's standard operations.
     Example 1:

     Input
     ["MyStack", "push", "push", "top", "pop", "empty"]
     [[], [1], [2], [], [], []]
     Output
     [null, null, null, 2, 2, false]

     Explanation
     MyStack myStack = new MyStack();
     myStack.push(1);
     myStack.push(2);
     myStack.top(); // return 2
     myStack.pop(); // return 2
     myStack.empty(); // return False
     Constraints:

     1 <= x <= 9
     At most 100 calls will be made to push, pop, top, and empty.
     All the calls to pop and top are valid.
     Follow-up: Can you implement the stack using only one queue?

     Related Topics
     Stack
     Design
     Queue
     */

    /*
    棧和隊列剛好是相反的，棧是先進後出的結構，而隊列是先進先出的隊列。而它們插入元素都是從尾部進行插入的。
    所以，我們在push一個元素的時候，需要想辦法把這個元素放到隊列的頭部去，這樣pop的時候，就是pop出這個元素，滿足了後進先出。
     */

    static class MyStack {
        private final Queue<Integer> queue;

        public MyStack() {
            queue = new LinkedList<>();
        }

        public void push(int x) {
            queue.offer(x);
            System.out.println("before queue now:" + queue);
            System.out.println("before queue size:" + queue.size());
            for (int i=0; i< queue.size() - 1;i++) {
                queue.add(queue.poll());
            }
            System.out.println("after queue now:" + queue);
        }

        public int pop() {
            return queue.poll();
        }

        public int top() {
            return queue.peek();
        }

        public boolean empty() {
            return queue.isEmpty();
        }
    }

    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push(1); // queue is: [1] myStack.push(2);
        myStack.push(2);
        myStack.push(6);
        myStack.push(9);

        System.out.println("myStack.top:" + myStack.top());
        System.out.println("myStack.pop:" + myStack.pop());
        System.out.println("myStack.top:" + myStack.top());
        System.out.println("myStack.pop:" + myStack.pop());
        System.out.println("myStack.top:" + myStack.top());

    }
}
