package com.frank.queue._232_E;

import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/28
 *     title : 232. Implement Queue using Stacks
 * </pre>
 */
public class ImplementQueueUsingStacks_232_E {

    /***
     Implement a first in first out (FIFO) queue using only two stacks.
     The implemented queue should support all the functions of a normal queue (push, peek, pop, and empty).

     Implement the MyQueue class:

     void push(int x) Pushes element x to the back of the queue.
     int pop() Removes the element from the front of the queue and returns it.
     int peek() Returns the element at the front of the queue.
     boolean empty() Returns true if the queue is empty, false otherwise.

     Notes:

     You must use only standard operations of a stack, which means only push to top, peek/pop from top, size,
     and is empty operations are valid.
     Depending on your language, the stack may not be supported natively.
     You may simulate a stack using a list or deque (double-ended queue) as long as you use only a stack's standard operations.


     Example 1:

     Input
     ["MyQueue", "push", "push", "peek", "pop", "empty"]
     [[], [1], [2], [], [], []]
     Output
     [null, null, null, 1, 1, false]

     Explanation
     MyQueue myQueue = new MyQueue();
     myQueue.push(1); // queue is: [1]
     myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)
     myQueue.peek(); // return 1
     myQueue.pop(); // return 1, queue is [2]
     myQueue.empty(); // return false


     Constraints:

     1 <= x <= 9
     At most 100 calls will be made to push, pop, peek, and empty.
     All the calls to pop and peek are valid.


     Follow-up: Can you implement the queue such that each operation is amortized O(1) time complexity?
     In other words, performing n operations will take overall O(n) time even
     if one of those operations may take longer.
     */

    /*
    s1棧用來輸入，s2棧用來輸出
    重點在於不能每次pop或者peek的時候都將s1的內容彈出壓入到s2中
    只有等到輸出棧s2為空時，再將s1的內容彈出壓入到s2中，才能保證整體數據是先進先出的

    push時間複雜度O(1)，pop時間複雜度為O(n) ，因為pop的時候，輸出棧為空，則把輸入棧所有的元素加入輸出棧。空間複雜度O(n)
     */

    static class MyQueue {
        private final Stack<Integer> s1;
        private final Stack<Integer> s2;

        public MyQueue() {
            s1 = new Stack<>();
            s2 = new Stack<>();
        }

        public void push(int x) {
            s1.push(x);
        }

        public int pop() {
            peek();
            return s2.pop();
        }

        public int peek() {
            if (s2.isEmpty()) {
                while(!s1.isEmpty()) {
                    s2.push(s1.pop());
                }
            }
            return s2.peek();
        }

        public boolean empty() {
            return s1.isEmpty() && s2.isEmpty();
        }

        public int size() {
            return Math.max(s1.size(), s2.size());
        }
    }

    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.push(1); // queue is: [1] myQueue.push(2);
        myQueue.push(2);
        myQueue.push(3);
        myQueue.push(4);

        System.out.println("myQueue.pop:" + myQueue.pop());
        System.out.println("myQueue.size:" + myQueue.size());
        System.out.println("myQueue.peek:" + myQueue.peek());
        System.out.println("myQueue.size:" + myQueue.size());
    }
}
