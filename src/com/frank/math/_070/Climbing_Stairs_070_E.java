package com.frank.math._070;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/10/12
 *     desc  : 70. Climbing Stairs
 * </pre>
 */
public class Climbing_Stairs_070_E {

    /***
     You are climbing a staircase. It takes n steps to reach the top.

     Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

     Example 1:
     Input: n = 2
     Output: 2
     Explanation: There are two ways to climb to the top.
     1. 1 step + 1 step
     2. 2 steps

     Example 2:
     Input: n = 3
     Output: 3
     Explanation: There are three ways to climb to the top.
     1. 1 step + 1 step + 1 step
     2. 1 step + 2 steps
     3. 2 steps + 1 step
     Constraints:

     1 <= n <= 45
     Related Topics
     Math
     Dynamic Programming
     Memoization
     */

    //題目:描述有一個要爬n階才能到頂端的階梯，每次只能爬1階或2階，試求有多少種不同的方法可以走到頂端。

    //思路：使用動態規劃的方法進行求解，狀態轉移方程可以看做：steps[n] = steps[n-1]+steps[n-2];
    //狀態：step[i]，表示到達第i個臺階的不同方式。

    // Time: O(2^n) ; Space: O(n)
    public static int climbStairs(int n) {
        if (n <= 2)
            return n;

        return climbStairs(n - 1) + climbStairs(n - 2);
    }

    /*
      解法:動態規劃
      1.狀態轉移方程可以看做：steps[n] = steps[n-1]+steps[n-2];
      Time: O(n) ; Space: O(n)
     */
    public int climbStairs_dp(int n) {
        // 如果台階數小於等於2，則直接返回對應的解
        if (n <= 2) {
            return n;
        }

        // 創建一個陣列用於存儲每個台階對應的爬法數目
        int[] dp = new int[n + 1];

        // 初始化陣列的前兩個元素
        dp[1] = 1; // 爬1個台階只有一種方法
        dp[2] = 2; // 爬2個台階有兩種方法

        // 使用動態規劃逐步計算出每個台階對應的爬法數目
        for (int i = 3; i <= n; i++) {
            // 每個台階的爬法數目等於前兩個台階爬法數目之和
            dp[i] = dp[i - 1] + dp[i - 2];
        }

        // 返回最後一個台階的爬法數目
        return dp[n];
    }

    //  Time: O(n) ; Space: O(1)
    public static int climbStairs_dp_optimization(int n) {
        // 如果台階數小於等於2，則直接返回對應的解
        if (n <= 2) {
            return n;
        }

        // 初始化前兩個台階的爬法數目
        int prev1 = 1; // 爬1個台階只有一種方法
        int prev2 = 2; // 爬2個台階有兩種方法

        // 使用動態規劃逐步計算出每個台階對應的爬法數目
        for (int i=3; i<=n; i++) {
            // 當前台階的爬法數目等於前兩個台階爬法數目之和
            int current = prev1 + prev2;
            // 更新前兩個台階的爬法數目
            prev1 = prev2;
            prev2 = current;
        }
        return prev2;
    }


    public static void main(String[] args) {
        int result = climbStairs_dp_optimization(10);
        System.out.println("result:" + result);
    }
}
