package com.frank.math._171;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/02/26
 *     title  : 171. Excel Sheet Column Number
 * </pre>
 */
public class Solution {

	/**
	 Given a column title as appear in an Excel sheet, return its corresponding column number.
	 For example:
	    A -> 1
	    B -> 2
	    C -> 3
	    ...
	    Z -> 26
	    AA -> 27
	    AB -> 28 
	    ...
	    
		Example 1:
			Input: "A"
			Output: 1
		
		Example 2:
			Input: "AB"
			Output: 28
			
		Example 3:
			Input: "ZY"
			Output: 701
	 */

	/*
	 解題：
	將每個字元轉換為 ASCII 碼，並且減掉 64 使 A~Z 變為 1~26 。
	在加入下一字元前，須考慮每一次進位就要乘上前一個字元經過幾次 26 次。
	 */
	public int titleToNumber(String s) {
        int result = 0;
        for (char letter : s.toCharArray()) {
            result = result * 26 + letter - 'A' + 1;
        }
        return result;
    }
	
	public static void main(String[] args) {
		Solution solution = new Solution();
		System.out.println("G :" +solution.titleToNumber("G"));
		System.out.println("AB :" +solution.titleToNumber("AB"));
		System.out.println("ZY :" +solution.titleToNumber("ZY"));
	}
	
}
