package com.frank.math._013_E;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/16
 *     title  : 13. Multiply Strings
 * </pre>
 */
public class RomanToInteger_13_E {

    /***
     Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

     Symbol       Value
     I             1
     V             5
     X             10
     L             50
     C             100
     D             500
     M             1000
     For example, 2 is written as II in Roman numeral, just two ones added together. 12 is written as XII,
     which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.

     Roman numerals are usually written largest to smallest from left to right. However,
     the numeral for four is not IIII. Instead, the number four is written as IV.
     Because the one is before the five we subtract it making four. The same principle applies to the number nine,
     which is written as IX. There are six instances where subtraction is used:

     I can be placed before V (5) and X (10) to make 4 and 9.
     X can be placed before L (50) and C (100) to make 40 and 90.
     C can be placed before D (500) and M (1000) to make 400 and 900.
     Given a roman numeral, convert it to an integer.



     Example 1:

     Input: s = "III"
     Output: 3
     Explanation: III = 3.
     Example 2:

     Input: s = "LVIII"
     Output: 58
     Explanation: L = 50, V= 5, III = 3.
     Example 3:

     Input: s = "MCMXCIV"
     Output: 1994
     Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.


     Constraints:

     1 <= s.length <= 15
     s contains only the characters ('I', 'V', 'X', 'L', 'C', 'D', 'M').
     It is guaranteed that s is a valid roman numeral in the range [1, 3999].
     */

    //題意：給一個羅馬數字符號，將之轉為整數，這個數字一定落在1 到 3999 之間。

    /* 解法：
     1.羅馬數字排列的規律:由左至右是從大到小排列的，也就是說左邊的羅馬數字通常會大於右邊的羅馬數字
     2.依序遍尋字元，若左邊大於等於(>=)右邊，就相加(+)，若左邊小於(<)右邊，就相減(-)
     Time Complexity: O(n)
     Space Complexity: O(1)
     */
    public int romanToInt(String s) {
        if (s == null || s.length() ==0) return 0;

        int ans=0;
        int n = s.length();
        for (int i=0; i<n; i++) {
            int value = getValue(s.charAt(i));
            if (i < n-1 && value < getValue(s.charAt(i+1))) {
                ans -= value;
            } else {
                ans += value;
            }
        }
        return ans;
    }

    private int getValue(char ch) {
        switch(ch) {
            case 'I': return 1;
            case 'V': return 5;
            case 'X': return 10;
            case 'L': return 50;
            case 'C': return 100;
            case 'D': return 500;
            case 'M': return 1000;
            default: return 0;
        }
    }

    public int romanToInt2(String s) {
        int sum =0;
        if (s.contains("IV")) {sum -=2;}
        if (s.contains("IX")) {sum -=2;}
        if (s.contains("XL")) {sum -=20;}
        if (s.contains("XC")) {sum -=20;}
        if (s.contains("CD")) {sum -=200;}
        if (s.contains("CM")) {sum -=200;}

        for (int i=0;i<s.length();i++) {
            char c=s.charAt(i);
            if (c=='I') sum += 1;
            if (c=='V') sum += 5;
            if (c=='X') sum += 10;
            if (c=='L') sum += 50;
            if (c=='C') sum += 100;
            if (c=='D') sum += 500;
            if (c=='M') sum += 1000;
        }
        return sum;
    }


    public static void main(String[] args) {
        RomanToInteger_13_E solution = new RomanToInteger_13_E();
        System.out.println(solution.romanToInt("MCMXCIV"));
    }
}
