package com.frank.math._202;

import java.util.Arrays;
import java.util.HashSet;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/03/10
 *     title : 202. Happy Number
 * </pre>
 */
public class HappyNumber_202_E {

	/**
	 Write an algorithm to determine if a number is "happy".

	A happy number is a number defined by the following process: 
	Starting with any positive integer, replace the number by the sum of the squares of its digits, 
	and repeat the process until the number equals 1 (where it will stay), or it loops endlessly 
	in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.
	
	Example: 
		Input: 19
		Output: true
		Explanation: 
		1^2 + 9^2 = 82
		8^2 + 2^2 = 68
		6^2 + 8^2 = 100
		1^2 + 0^2 + 0^2 = 1
	 */
	///題意：給定一整數值，判斷是否為快樂數。
	//快樂數定義是，整數中的每個位數平方後相加，其結果再做同樣的操作，直到結果為1，就是快樂數，若一直循環不是1的話，就不是快樂數。

	/*
	 解題：
	 1.使用一個map儲存計算過的數字，
	 2.若目前的數字已經計算過，表示無窮迴圈出現，return false，持續計算到1出現true
	 ime Complexity: O(lonN)
     Space Complexity: O(logN)
	 */
	public boolean isHappy(int n) {
        HashSet<Integer> set =new HashSet<>();
        while(n != 1) {
        	int temp = 0;
        	while (n!=0) {
        		int digit = n%10;
        		temp+= digit * digit;//取平方
        		n/=10;//取十位數
			}
            if (!set.add(temp)) {
            	return false;
			}
            n = temp;
        }
        return true;
    }

	public static void main(String[] args) {
		HappyNumber_202_E solution = new HappyNumber_202_E();
		System.out.println("is Happy number :" +solution.isHappy(329));
	}
}
