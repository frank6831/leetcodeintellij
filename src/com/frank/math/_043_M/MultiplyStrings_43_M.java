package com.frank.math._043_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/16
 *     title  : 43. Multiply Strings
 * </pre>
 */
public class MultiplyStrings_43_M {

    /***
     Given two non-negative integers num1 and num2 represented as strings,
     return the product of num1 and num2, also represented as a string.

     Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.


     Example 1:
     Input: num1 = "2", num2 = "3"
     Output: "6"

     Example 2:
     Input: num1 = "123", num2 = "456"
     Output: "56088"

     Constraints:

     1 <= num1.length, num2.length <= 200
     num1 and num2 consist of digits only.
     Both num1 and num2 do not contain any leading zero, except the number 0 itself.
     */

    //題意：求兩個字符串數字的相乘

    /* 解法：
    1.使用雙層迴圈，依序取值，兩相乘後與進位值和上一層相乘結果相加。
    2.更新 高位：pos[i + j]與 低位：pos[i + j + 1]，方便從最低位數開始計算。
    3.最後建立StringBuilder,將原array一一取出加進去，過濾掉最高位是不是 0

     Time Complexity: O(m*n), m，n 是兩個字符串的長度;
     Space Complexity: O(m+n), m，n 是兩個字符串的長度;
     */
    public String multiply(String num1, String num2) {
        int m = num1.length();
        int n = num2.length();
        //m 位數與 n 位數相乘，結果最大為 m+n 位
        int[] pos = new int[m+n];

        for (int i= m-1;i>=0;i--) {
            for (int j= n-1; j>=0;j--) {
                //相乘的结果
                int mul = (num1.charAt(i)-'0') * (num2.charAt(j)-'0');

                int p1 = i+j;//高位
                int p2 = i+j+1;//低位

                //加上 pos[i+j+1] 之前已经累加的结果
                int sum = mul + pos[p2];
                System.out.println("mul:"+ mul + ", i:" + i+ ", j:" + j);
                System.out.println("p1:" + p1+ ", p2:" + p2 + ", sum:"+ sum);
                //更新 pos[i + j]
                pos[p1] += sum/10;
                //更新 pos[i + j + 1]
                pos[p2] = sum % 10;
                System.out.println("result:" + Arrays.toString(pos));
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int p:pos) {
            //判断最高位是不是 0
            if (!(sb.length() ==0 && p==0)) sb.append(p);
        }
        return sb.length() ==0 ? "0" : sb.toString();
    }

    public static void main(String[] args) {
        MultiplyStrings_43_M solution = new MultiplyStrings_43_M();
        System.out.println(solution.multiply("2", "3"));
    }
}
