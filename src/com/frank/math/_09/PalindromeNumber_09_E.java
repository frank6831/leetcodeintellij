package com.frank.math._09;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/12
 *     title  : 9. Palindrome Number
 * </pre>
 */
public class PalindromeNumber_09_E {

    /***
     Given an integer x, return true if x is a palindrome, and false otherwise.

     Example 1:
     Input: x = 121
     Output: true
     Explanation: 121 reads as 121 from left to right and from right to left.

     Example 2:
     Input: x = -121
     Output: false
     Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.

     Example 3:
     Input: x = 10
     Output: false
     Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
     */

    //題意：給一個數字，判斷是否迴文數字

    /*
     解法一：先判斷負數，return false, 將數字轉為字串，用two pointer,前後依序比對是否為回文。
     Time Complexity: O(n)
     Space Complexity: O(n)
     */
    public boolean isPalindromeToString(int x) {
        if (x <0) {
            return false;
        }

        char[] nums = String.valueOf(x).toCharArray();
        int start = 0;
        int end = nums.length-1;
        while(start < end) {
            if(nums[start] != nums[end]) return false;
            start++;
            end--;
        }
        return true;
    }

    /*
    Intuitive Arithematic Solution
    解法一：將數字反轉，判斷反轉後的結果是否等於原數值。
    Time Complexity: O(n)
    Space Complexity: O(1)
    */
    public boolean isPalindromeReverse(int num) {

        if(num < 0) return false;

        int reversed = 0;
        int real = num;

        while(num != 0){
            reversed = reversed * 10 + num % 10;
            num = num / 10;
        }
        return reversed == real;
    }

    public static void main(String[] args) {
        PalindromeNumber_09_E solution = new PalindromeNumber_09_E();
        System.out.println(solution.isPalindromeReverse(1234));
    }
}
