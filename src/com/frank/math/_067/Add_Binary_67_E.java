package com.frank.math._067;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/30
 *     title  : 67. Add Binary
 * </pre>
 */
public class Add_Binary_67_E {

    /***
     Given two binary strings a and b, return their sum as a binary string.

     Example 1:
     Input: a = "11", b = "1"
     Output: "100"

     Example 2:
     Input: a = "1010", b = "1011"
     Output: "10101"


     Constraints:

     1 <= a.length, b.length <= 104
     a and b consist only of '0' or '1' characters.
     Each string does not contain leading zeros except for the zero itself.
     */

    //題意：給定兩個二元字串，回傳他們的總合。

    /* 解法：
     用兩個指針分別指向a和b的末尾，然後
     1.每次取出一個字符，轉為數字，若無法取出字符則按0處理。
     2.定義進位carry，初始化為0，將三者加起來，對2取餘即為當前位的數字，對2取商即為當前進位的值
     3.最後判斷carry，如果為1的話，要在結果最前面加上一個1。

     (1) ‘0’ 和 ‘1’ 轉成數字: 計算時候加上進位的情形很多， 轉成數字比較方便，可以簡單用這個式子: a[i] - '0'

     Time Complexity: BigO(max(M, N)), M & N is the length of string a, b;
     Space Complexity: BigO(max(M, N)), which is the size of "res" object
     */
	public String addBinary(String a, String b) {
        StringBuilder result = new StringBuilder();
        int i = a.length() - 1;
        int j = b.length() - 1;
        int carry = 0;

        //11,1
        // 由低位到高位遍歷兩個二進制字符串，直到其中一個字符串遍歷完
        while (i >=0 || j >=0) {
            // 取出當前位的數字，如果超出字符串範圍則取0
            int num1 = i >= 0 ? a.charAt(i) - '0' : 0;
            int num2 = j >= 0 ? b.charAt(j) - '0' : 0;
            System.out.println("num1:"+ num1+", num2:"+num2);
            // 當前位相加，加上進位
            int sum = num1 + num2 + carry;
            System.out.println("sum:"+ sum);
            // 將相加結果加入結果字符串
            result.insert(0, sum % 2);

            // 更新進位
            carry = sum / 2;
            System.out.println("carry:"+ carry+", result:"+result);
            // 更新指針
            i--;
            j--;
        }
        if (carry !=0)  {
            // 如果最高位有進位，則加入到結果字符串中
            result.insert(0, 1);
        }
        return result.toString();
    }
	

    public static void main(String[] args) {
        Add_Binary_67_E solution = new Add_Binary_67_E();
        String a = "23";
        //int value = a.charAt(0) - '0';
        int i = 0;
        //int sum =  a.charAt(i--) - '0';
        //System.out.println("sum:"+ a.charAt(i--));
        //System.out.println("i:"+ i);
        System.out.println(solution.addBinary("11", "1"));
    }
}
