package com.frank.math._137;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/25
 *     desc  : single number II
 * </pre>
 */
public class Solution {

	/**
	 * Given a non-empty array of integers, every element appears three times except
	 * for one, which appears exactly once. Find that single one.
	 * 
	 * Note: Your algorithm should have a linear runtime complexity. Could you
	 * implement it without using extra memory?
	 * 
	 * Example 1: Input: [2,2,3,2] Output: 3
	 * 
	 * Example 2: Input: [0,1,0,1,0,1,99] Output: 99
	 */

	/*
	 建立一個32位的數字，來統計每一位上1出現的個數，我們知道如果某一位上為1的話，
	 那麼如果該整數出現了三次，對3去餘為0，我們把每個數的對應位都加起來對3取餘，
	 最終剩下來的那個數就是單獨的數字
	 */
	public int singleNumber2(int[] nums) {
		 int res = 0;
	        for (int i = 0; i < 32; ++i) {
	            int sum = 0;
	            //System.out.println(nums[j]);
	            for (int j = 0; j < nums.length; ++j) {
	            	System.out.println("nums[" +j + "] >> " + i+ " :" + (nums[j] >> i));
	                sum += (nums[j] >> i) & 1;
	                System.out.println("nums[" +j + "] >> " + i+ "  &1:" +( (nums[j] >> i) & 1));
	            }
	            res |= (sum % 3) << i;
	        }
	        return res;
	}
	
	
	/**
	 * 思路
	 * https://ithelp.ithome.com.tw/articles/10187791
	 * @param nums
	 * @return
	 */
	public int singleNumber(int[] nums) {
		int ones = 0, twos = 0, threes = 0;
		for (int i = 0; i < nums.length; i++)  {
			twos |= ones & nums[i];
			ones ^= nums[i];
			threes = ones & twos;
			ones &= ~threes;
			twos &= ~threes;
		}
		return ones;
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		int sample[] = { 4, 1, 4, 1, 4, 1, 99 };
		System.out.println(" 4 >> 1 = " + (4 >>2));
		//System.out.println(solution.singleNumber2(sample));
		
		int i = 5;
		 
        System.out.println("變數值...");
        System.out.println(" i = " + i);
 
        System.out.println("位移運算...");
        System.out.println(" i << 1 = " + (i >> 1));
        System.out.println(" i << 2 = " + (i >> 2));
        System.out.println(" i << 3 = " + (i >> 3));
	}
}
