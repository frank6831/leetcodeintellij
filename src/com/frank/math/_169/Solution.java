package com.frank.math._169;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/26
 *     title  : 169. Majority Element
 * </pre>
 */
public class Solution {

	/**
	 * Given an array of size n, find the majority element. The majority element is
	 * the element that appears more than ⌊ n/2 ⌋ times. You may assume that the
	 * array is non-empty and the majority element always exist in the array.
	 * 
	 * Example 1:
	 * Input: [3,2,3] Output: 3 
	 * 
	 * Example 2:
	 * Input: [2,2,1,1,1,2,2] Output: 2
	 */

	/*
	 * 思路1：将数组排序，则中间的那个元素一定是多数元素
	 * 時間複雜度為:O(nlogn)
	 * 空間複雜度亦為:O(1) or O(n)
	 */
	public int majorityElementSolution1(int[] nums) {
		Arrays.sort(nums);
		return nums[nums.length / 2];
	}

	/*
	 * 思路2：利用HashMap来记录每个元素的出现次数
	 * 時間複雜度為O(n)，空間複雜度亦為O(n)
	 */
	public int majorityElementSolution2(int[] nums) {
		// 初始化一個HashMap，用於記錄每個元素出現的次數
		Map<Integer, Integer> counts = new HashMap<>();

		// 遍歷數組，計算每個元素出現的次數
		for (int num : nums) {
			// 如果該元素已經在Map中存在，則將其出現次數加1；否則將其添加到Map中，出現次數初始化為1
			counts.put(num, counts.getOrDefault(num, 0) + 1);
		}

		// 遍歷Map，找到出現次數超過一半的元素
		for (Map.Entry<Integer, Integer> entry : counts.entrySet()) {
			if (entry.getValue() > nums.length / 2) {
				// 返回出現次數超過一半的元素
				return entry.getKey();
			}
		}

		// 如果沒有找到出現次數超過一半的元素，返回-1
		return -1;
	}

	/*
	 * 思路3：Moore voting algorithm--每找出两个不同的element，
	 * 就成对删除即count--，最终剩下的一定就是所求的。
	 * 刪去一個數列中的兩個不同的數字，不會影響該數列的majority element。

	1.取出第一個數放到一個暫存的變數(res)，將計數器(cnt)設定為1，代表這個數出現1次。
	2.取出下一個數nums[i]，如果和res相等，則將計數器+1；
	  如果和res不同，且計數器>0時，將計數器-1；(代表取這兩個數成對移除)
	  如果和res不同但是計數器=0時，將res更改為nums[i]並將計數器+1
	  (代表res已經用完了，現在還沒被移除的是nums[i])
	3.反覆進行步驟2直到陣列結尾，剩下的res即為答案。(因為兩兩移除到最後一定是非majority element的先被移光)

	 * 时间复杂度：O(n)
	 * 空間複雜度: O(1)
	 */
	public int majorityElementSolutionMoore(int[] nums) {
		// 初始化候選元素和計數器
		int candidate = nums[0];
		int count = 1;

		// Boyer-Moore Voting Algorithm
		// 遍歷數組，找出可能的候選元素
		for (int i = 1; i < nums.length; ++i) {
			// 如果計數器為0，則更新候選元素
			if (count == 0) {
				candidate = nums[i];
				count = 1;
			} else if (nums[i] == candidate) {
				// 如果當前元素與候選元素相同，則計數器加1
				count++;
			} else {
				// 如果當前元素與候選元素不同，則計數器減1
				count--;
			}
		}
		// 最終的候選元素就是出現次數超過一半的元素
		return candidate;
	}

	public int majorityElementMoore2(int[] nums) {
		int res = 0, cnt = 0;
		for (int num : nums) {
			if (cnt == 0) {res = num; ++cnt;}
			else if (num == res) ++cnt;
			else --cnt;
		}
		return res;
	}


	public int majorityElementSolutionMoore3(int[] nums) {
		if (nums == null || nums.length == 0) return 0;
		int res = nums[0];
		int count = 1;
		int size = nums.length;
		for (int i=1; i< size; i++) {
			if (res == nums[i]) {
				count++;
			} else if (count > 0) {
				count--;
			} else {
				res = nums[i];
				count++;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		int sample[] = { 2, 2, 1, 1, 1, 2, 2,1,1};
		System.out.println(solution.majorityElementSolutionMoore3(sample));
	}
}
