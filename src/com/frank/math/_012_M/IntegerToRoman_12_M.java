package com.frank.math._012_M;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/16
 *     title  : 13. Multiply Strings
 * </pre>
 */
public class IntegerToRoman_12_M {

    /***
     Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

     Symbol       Value
     I             1
     V             5
     X             10
     L             50
     C             100
     D             500
     M             1000
     For example, 2 is written as II in Roman numeral, just two one's added together.
     12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.

     Roman numerals are usually written largest to smallest from left to right.
     However, the numeral for four is not IIII. Instead, the number four is written as IV.
     Because the one is before the five we subtract it making four.
     The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

     I can be placed before V (5) and X (10) to make 4 and 9.
     X can be placed before L (50) and C (100) to make 40 and 90.
     C can be placed before D (500) and M (1000) to make 400 and 900.
     Given an integer, convert it to a roman numeral.



     Example 1:

     Input: num = 3
     Output: "III"
     Explanation: 3 is represented as 3 ones.
     Example 2:

     Input: num = 58
     Output: "LVIII"
     Explanation: L = 50, V = 5, III = 3.
     Example 3:

     Input: num = 1994
     Output: "MCMXCIV"
     Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.


     Constraints:

     1 <= num <= 3999
     */

    //題意：給定一整數值，而我們要做的便是將整數值轉換成羅馬數字

    /* 解法：
     1.建立數值陣列與符號陣列，按數值從大到小排列，一一對應。
     2.輸入的整數值大於最大的特定數字，則將整數值減去最大的特定數字，繼續比較次大的特定數字 …… 一直重複操作到輸入值歸零。
     Time Complexity: O(1)
     Space Complexity: O(1)
     */
    public String intToRoman(int num) {
        int[] values = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        String[] symbols = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        StringBuilder sb = new StringBuilder();
        for (int i=0;i< values.length;i++) {
            int value = values[i];
            String symbol = symbols[i];
            System.out.println("value:" + value + ", symbol:" +symbol + ", num:" + num);//MCMXCIV
            while (num >= value) {
                num -= value;
                sb.append(symbol);
            }
            if (num == 0)
                break;;
        }
        return sb.toString();
    }

    public String intToRomanTwo(int num) {
        int[] values = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        String[] M = {"","M","MM","MMM"};
        //空, 1000, 2000, 3000
        String[] C = {"","C","CC","CCC","CD", "D","DC", "DCC", "DCCC","CM"};
        //空, 100, 200, 300, 400, 500, 600, 700, 800, 900
        String[] X = {"","X","XX","XXX","XL", "L","LX", "LXX", "LXXX", "XC"};
        //空, 10, 20, 30, 40, 50, 60, 70, 80, 90
        String[] I = {"","I","II","III","IV", "V","VI", "VII", "VIII", "IX"};
        //空, 1, 2, 3, 4, 5, 6, 7, 8, 9

        return M[num/1000] + C[(num/100)%10] + X[(num/10)%10] + I[num%10];
        // M[千位]+ C[百位]+ C[十位]+ C[個位]
    }

    public static void main(String[] args) {
        IntegerToRoman_12_M solution = new IntegerToRoman_12_M();
        System.out.println(solution.intToRoman(3));//MCMXCIV
    }
}
