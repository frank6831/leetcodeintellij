package com.frank.math._136;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/02/22
 *     desc  : 136. singleNumber
 * </pre>
 */
public class Solution {

	/**
	 Given a non-empty array of integers, every element appears twice except for one. Find that single one.

	Note:
	Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
	
	Example 1:
	Input: [2,2,1]
	Output: 1
	
	Example 2:
	Input: [4,1,2,1,2]
	Output: 4
	 */
	
	/**
	 * 思路
	 * 使用xor, 相同兩個數xor後為0
	 * 
	 * @param nums
	 * @return
	 */
	public int singleNumber(int[] nums) {
			int result = nums[0];
			for (int i = 1 ; i < nums.length; i++)  {
				result ^= nums[i];
			}
			return result;
	}
	
	public int lengthOfLastWord(String s) {
		if (s == null || s.length() == 0) {
		return 0;
		}
		String[] arrs = s.split(" ");
		return arrs.length==0 ? 0 : arrs[arrs.length - 1].length();
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		int sample[] = { 4,1,2,1,2,4,8 };
		System.out.println(solution.singleNumber(sample));
		
		//System.out.println(solution.lengthOfLastWord("Hello world abc"));
	}
}
