package com.frank.hashtab._204_E;


/**
 * <pre>
 *     author: frank
 *     time  : 2019/8/27
 *     title : 204. Count Primes
 * </pre>
 */
public class Solution {
    
	/**
     Count the number of prime numbers less than a non-negative number, n.

     Example:

     Input: 10
     Output: 4
     Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
	*/
	// 質數的定義是大於一的整數，除了1跟自己本身以外，沒有其他因數。
	// 題目要求找出小於input數字內的質數個數
	/*
	  思路：
	  先用2去篩，即把2留下，把2的倍數剔除掉；再用下一個質數，也就是3篩，把3留下，把3的倍數剔除掉；
	  接下去用下一個質數5篩，把5留下，把5的倍數剔除掉；不斷重複下去......。
	 */
    public int countPrimes(int n) {
        boolean[] notPrime = new boolean[n];
        int primeCount = 0;
        for (int i = 2; i < n; i++) {
            if (!notPrime[i]) {
                primeCount++;
                for (int j = 2; j*i < n ;j++) {
                    notPrime[j*i] = true;
                }
            }
        }
        return primeCount;
    }

    //只要是2的倍數都是非質數，所以直接略過不考慮，如此內迴圈的執行次數至少就減少了一半。
    public int countPrimes2(int n) {
        if (n < 3) {
            return 0;
        }
        int count = 1;                        // 先算入整數2
        boolean[] nonPrimes = new boolean[n];
        for (int i = 3; i < n; i += 2) {      // 不考慮2的倍數

            if (nonPrimes[i]) {
                continue;
            }
            count++;
            for (long j = (long) i * i; j < n; j += i) {
                nonPrimes[(int) j] = true;
            }
        }
        return count;
    }


    // fast
    public int countPrimes3(int n) {
        if (n < 3) {
            return 0;
        }
        int count = n / 2;
        boolean[] nonPrimes = new boolean[n];
        for (int i = 3; i * i < n; i += 2) {
            if (nonPrimes[i]) {
                continue;
            }
            for (long j = (long) i * i; j < n; j += 2 * i) {
                if (!nonPrimes[(int) j]) {
                    --count;
                    nonPrimes[(int) j] = true;
                }
            }
        }
        return count;
    }

    public static void main(String[] args) {
    	Solution solution = new Solution();

        long time = System.nanoTime();
        int result = solution.countPrimes(10000);
        System.out.println("result:" + result);
        System.out.println(System.nanoTime() - time);

        time = System.nanoTime();
        System.out.println("result:" + solution.countPrimes2(10000));
        System.out.println(System.nanoTime() - time);

        time = System.nanoTime();
        System.out.println("result:" + solution.countPrimes3(10000));
        System.out.println(System.nanoTime() - time);
    }
}
