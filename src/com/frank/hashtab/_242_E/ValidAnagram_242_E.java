package com.frank.hashtab._242_E;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/16
 *     title : 242. Valid Anagram
 * </pre>
 */
public class ValidAnagram_242_E {
    
	/**
	 Given two strings s and t , write a function to determine if t is an anagram of s.

	 Example 1:
	 Input: s = "anagram", t = "nagaram"
	 Output: true

	 Example 2:
	 Input: s = "rat", t = "car"
	 Output: false
	 Note:
	 You may assume the string contains only lowercase alphabets.

	 Follow up:
	 What if the inputs contain unicode characters? How would you adapt your solution to such case?
	*/
	// 題目意思：給兩個字串s與t，回傳t是否為s的重組字, 符合anagram兩組字，代表組成這兩個單字的字母元素與個數會是一樣的

	/*
	 思路：
	 1.建立一個長度為26的整數數組counts，用來儲存每個字母出現的次數
	 2.遍歷字串s，將對應的計數器加1；再遍歷字串t，將對應的計數器減1。
	 3.若所有的計數器都為0，則s和t是字母重排，返回true;否則，返回false
	 */
	public boolean isAnagram(String s, String t) {
		if (s.length() != t.length()) {
			return false;
		}

		int[] ch = new int[128];

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			int value =c;
			System.out.println("c:" + c + ", value:" + value);
			ch[s.charAt(i)]++;
		}

		for (int i = 0; i < t.length(); i++) {
			ch[t.charAt(i)]--;
			if (ch[t.charAt(i)] < 0) {
				return false;
			}
		}

		return true;
	}

	/*
	 處理unicode字符輸入
	 */
	private boolean isAnagramUnicode(String s, String t) {
		if (s.length() != t.length()) return false;

		Map<Character, Integer> map = new HashMap<>();

		for (int i=0; i< s.length();i++) {
			char ch = s.charAt(i);
			map.put(ch, map.getOrDefault(ch,0) + 1);
		}

		for (int i=0; i< t.length();i++) {
			char ch = t.charAt(i);
			if (!map.containsKey(ch)) {
				return false;
			}

			int count = map.get(ch);
			if (count == 1) {
				map.remove(ch);
			} else {
				map.put(ch, count -1);
			}
		}
		return true;
	}

    public static void main(String[] args) {
		ValidAnagram_242_E solution = new ValidAnagram_242_E();

        boolean result = solution.isAnagram("anagram", "nagaram");
        System.out.println("result:" + result);
    }
}
