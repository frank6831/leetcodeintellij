package com.frank.hashtab._205_E;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/9/12
 *     title : 205. Isomorphic Strings
 * </pre>
 */
public class Solution {
    
	/**
     Given two strings s and t, determine if they are isomorphic.

     Two strings are isomorphic if the characters in s can be replaced to get t.

     All occurrences of a character must be replaced with another character
     while preserving the order of characters. No two characters may map to
     the same character but a character may map to itself.

     Example 1:
     Input: s = "egg", t = "add"
     Output: true

     Example 2:
     Input: s = "foo", t = "bar"
     Output: false

     Example 3:
     Input: s = "paper", t = "title"
     Output: true
     Note:
     You may assume both s and t have the same length.
	*/
    /* 題目:給定兩個字串s和t，判斷它們是否是同構的。如果字串s可以通過字元替換的方式得到字串t，則稱s和t是同構的。
       字元的每一次出現都必須被其對應字元所替換，同時還需要保證原始順序不發生改變。兩個字元不能對映到同一個字元，但是字元可以對映到其本身。

       @什麼樣的兩個字符串是“同構”的呢？題目中給了我們幾個例子，比如字符串"egg"和"add"就是同構的，因為將"e"替換成"a"後，
       兩個"g"同時替換成兩個"d"後，這兩個字符串就變成相同的了，所以他們是"同構"的。
    */

	/*
	  思路：使用兩個HashMap，分別存儲字符串s和字符串t的每一位字符，再使用兩個新的字符串str和str2，
	  分別存儲其對應字符的結構，如果字符已經存在，則取出之前存的下標添加到str或str2中，如果是新出現，就把當前的下標添加到str或str2中。
	  最後得到的str和str2就是s、t所表示的結構，如果str和str2相等，則返回true，否則返回false。

	  時間複雜度：因為for循環中使用了HashMap的containsKey方法，而此方法最好的情況就是要判斷的key正好是第一位，則containsKey方法的時間複雜度是O(1)，
	  最壞的情況是要判斷的key在最後一位，則containsKey方法的時間複雜度是O(n)。
	  因此，此解法的時間複雜度最好情況是O(n)，最壞情況是O(n^2)。

	  空間複雜度：O(n)
	 */
	//
    private boolean isIsomorphic(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        StringBuilder str = new StringBuilder();
        for (int i=0; i<s.length(); i++) {
            if (map.containsKey(s.charAt(i))) {
                str.append(map.get(s.charAt(i)));
            } else {
                map.put(s.charAt(i), i);
                System.out.println("put:" + map.put(s.charAt(i), i));
                str.append(i);
            }
        }
        System.out.println("str:" + str);

        HashMap<Character, Integer> map2 = new HashMap<>();
        StringBuilder str2 = new StringBuilder();
        for (int j=0; j<t.length(); j++) {
            if (map2.containsKey(t.charAt(j))) {
                str2.append(map2.get(t.charAt(j)));
            } else {
                map2.put(t.charAt(j), j);
                str2.append(j);
            }
        }
        System.out.println("str2:" + str2);
        return str.toString().equals(str2.toString());
    }


    /*
    方法2：使用兩個HashMap，同樣是將兩個字符串的各個字符放入map中，不過由第一種解法的分開放變成同時放，
    如果在進行put的時候，有兩種情況：當前字符在map中不存在的時候，返回null；
    當前字符在map中存在的時候，返回此字符作為key所關聯的之前的value。
    如果兩邊的字符進行put操作而不相等，即說明此字符在兩個map中的其中一個已經出現過一次，但是另外一個沒出現過，也就表示兩字符串的結構是不同的。
     */

    public boolean isIsomorphic2(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        Map<Character, Integer> map2 = new HashMap<>();
        int size = s.length();
        for (int i = 0; i< size; i++) {
            if (map.put(s.charAt(i), i) != map2.put(t.charAt(i), i)) {
                return false;
            }
        }
        return true;
    }

    /*
    用數組來替代，因為組成字符串的字符類型有限，ASCII碼只有256個字符,所以我們預先定義好兩個大小為256的整型數組，
    創建後兩數組中的元素初始值都是0。此時，我們使用for循環對字符串s進行遍歷，
    當前字符所表示的十進制值當做是數組的索引，如果該索引分別對應的值不相等，則說明兩字符串不是同結構，
    否則，就將循環內的指針i作為其索引對應的值。其中i是從0開始的，但是數組初始化後其內所有元素都是默認值0，所以需要加1來區別默認值0。
     */
    public boolean isIsomorphic4(String s, String t) {
        int[] m1 = new int[256];
        int[] m2 = new int[256];
        for (int i = 0; i < s.length(); i++) {
            if (m1[s.charAt(i)] != m2[t.charAt(i)]) {
                return false;
            }
            m1[s.charAt(i)] = i + 1;
            m2[t.charAt(i)] = i + 1;
        }
        return true;
    }

    public static void main(String[] args) {
    	Solution solution = new Solution();
        boolean result = solution.isIsomorphic4("foo", "bar");
        System.out.println("result:" + result);
    }
}
