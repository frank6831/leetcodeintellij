package com.frank.hashtab._146_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/2
 *     title : 146. LRU Cache
 * </pre>
 */
public class LRUCache_146_M {
    
	/**
	 Design a data structure that follows the constraints of a Least Recently Used (LRU) cache.

	 Implement the LRUCache class:

	 LRUCache(int capacity) Initialize the LRU cache with positive size capacity.
	 int get(int key) Return the value of the key if the key exists, otherwise return -1.
	 void put(int key, int value) Update the value of the key if the key exists.
	 Otherwise, add the key-value pair to the cache. If the number of keys exceeds the capacity from this operation,
	 evict the least recently used key.
	 The functions get and put must each run in O(1) average time complexity.



	 Example 1:

	 Input
	 ["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
	 [[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
	 Output
	 [null, null, null, 1, null, -1, null, -1, 3, 4]

	 Explanation
	 LRUCache lRUCache = new LRUCache(2);
	 lRUCache.put(1, 1); // cache is {1=1}
	 lRUCache.put(2, 2); // cache is {1=1, 2=2}
	 lRUCache.get(1);    // return 1
	 lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
	 lRUCache.get(2);    // returns -1 (not found)
	 lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
	 lRUCache.get(1);    // return -1 (not found)
	 lRUCache.get(3);    // return 3
	 lRUCache.get(4);    // return 4


	 Constraints:

	 1 <= capacity <= 3000
	 0 <= key <= 104
	 0 <= value <= 105
	 At most 2 * 105 calls will be made to get and put.
	*/
	// 題目意思：實現get與put的操作，前者傳入 key 並回傳對應的value ，後者則是新增 key與對應的 value 到 cache 中，這兩者操作需要在 O(1) 完成。
	// LRU 全名為 least recently used ，如果 cache 容量滿了，最久沒使用的資料將被刪除，以空出容量將新資放入 cache 中。
	/*
	 思路：

	 */
	static class LRUCache {

		class Node {
			int key;
			int value;
			Node next;
			Node prev;
			public Node(int key, int value) {
				this.key = key;
				this.value = value;
			}
		}

		private HashMap<Integer, Node> map;
		private int capacity;
		//最老的元素
		private Node head;
		//最新的元素
		private Node tail;

		public LRUCache(int capacity) {
			map = new HashMap<>();
			this.capacity = capacity;
			head = null;
			tail = null;
		}

		public int get(int key) {
			if (!map.containsKey(key)) {
				return -1;
			}

			Node n = map.get(key);
			remove(n);
			setHead(n);
			return n.value;
		}

		public void put(int key, int value) {
			if (map.containsKey(key)) { //(1,1) -> (1, 100)
				Node n = map.get(key);
				n.value = value;
				remove(n);
				setHead(n);
			} else {
				Node created = new Node(key, value);
				// reach the capacity, move the oldest item.
				if (map.size() > capacity) {
					map.remove(tail.key);
					remove(tail);
					setHead(created);
				} else {
					// insert the entry into list and update mapping.
					setHead(created);
				}
				map.put(key, created);
			}
		}

		private void remove(Node n) {
			if (n.prev != null) {
				n.prev.next = n.next;
			} else {
				head = n.next;
			}

			if (n.next != null) {
				n.next.prev = n.prev;
			} else {
				tail = n.prev;
			}
		}

		private void setHead(Node n) {
			n.next = head;
			n.prev = null;
			if (head != null) head.prev = n;

			head = n;

			if (tail == null) tail = head;
		}
	}

    public static void main(String[] args) {
		LRUCache_146_M solution = new LRUCache_146_M();

    }
}
