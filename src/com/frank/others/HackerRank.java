package com.frank.others;

import java.util.*;

public class HackerRank {


    public static int reductionCost(List<Integer> num) {
        int n = num.size();
        // 如果數組只剩下一個元素，則成本為0
        if (n == 1) {
            return 0;
        }
        // 如果數組只剩下兩個元素，則成本為它們的和
        if (n == 2) {
            return num.get(0) + num.get(1);
        }
        // 將數組按非遞減順序排序
        Collections.sort(num);
        // 計算每一步的成本
        int cost = num.get(0) + num.get(1);
        // 將合併後的元素添加到數組末尾，並刪除原來的兩個元素
        num.add(num.get(0) + num.get(1));
        num.remove(0);
        num.remove(0);
        // 遞歸求解
        return cost + reductionCost(num);
    }

    public static int reductionCost2(List<Integer> num) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(num);
        int cost = 0;
        while (queue.size() > 1) {
            int sum = queue.poll() + queue.poll();
            cost += sum;
            queue.offer(sum);
        }
        return cost;
    }

    public static int reductionCost(int[] num) {
        int n = num.length;
        if (n <= 1) {
            return 0;
        }
        Arrays.sort(num);
        int cost = 0;
        int i = 0;
        int j = 1;
        while (j < n) {
            cost += num[i] + num[j];
            num[j] = num[i] + num[j];
            i++;
            j++;
        }
        return cost;
    }

    public static int reductionCost3(List<Integer> num) {
        int cost = 0;
        while (num.size() > 1) {
            // 找出最小的兩個元素
            Collections.sort(num);
            System.out.println("Frank sort num:" + num);
            int a = num.get(0);
            int b = num.get(1);

            // 刪除最小的兩個元素
            num.remove(0);
            num.remove(0);

            // 將它們的和加到最後
            int sum = a + b;
            num.add(sum);

            // 計算此步驟的花費
            cost += sum;
        }
        return cost;
    }

    public static int minOperations(int n) {
        if(n <= 1)
            return n;
        //找出最高位
        int pos = 0;
        for(int i = 0; i < 32; ++i)
            if(((n >> i) & 1) == 1)
                pos = i;
        int x = (1 << (pos+1)) - 1;
        int y = n - (1<<pos);
        return x - minOperations(y);
    }

    public static void main(String[] args) {
        int[] room = {1,2,3,4};
        List<Integer> list = Arrays.asList(4,6,8);
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        int value = reductionCost3(list1);
        System.out.println("value:" + value);


        long output = minOperations(13);
        //System.out.println("output:" + output);
    }
}
