package com.frank.others;

import java.util.*;

public class Interview {


    /**
     題目：找出一個正整數的二進制表示中，最長的連續 0 序列的長度，且此序列的前後端必須是 1。
     1.使用一個變量 longestGap 來存儲最長的 0 序列的長度，另一個變量 currentGap 來存儲目前的 0 序列的長度。
     2.使用一個循環遍歷 N 的二進制表示字串，計算每個 0 序列的長度。當遇到 1 時，檢查目前的 0 序列的長度是否大於最長的 0 序列的長度，
     如果是，就更新最長的 0 序列的長度。然後，將目前的 0 序列的長度 currentGap 重置為 0。
     */
    public int solution(int N) {
        // Implement your solution here
        String binary = Integer.toBinaryString(N);
        System.out.println("binary:" + binary);
        //1000010001
        int longestGap = 0;
        int currentGap = 0;

        for (int i=0; i< binary.length();i++) {
            System.out.println("currentGap:" + currentGap + ", longestGap:" + longestGap );
            if (binary.charAt(i) == '0') {
                currentGap++;
            } else {
                //遇到1時
                if (currentGap > longestGap) {
                    longestGap = currentGap;
                }
                currentGap = 0;
            }
        }
        return longestGap;
    }

    /**
     * 寫一個java function, 輸入字串 "I am a good boy!"， 輸出是I ma a doog yob!，標點符號不反轉
     * @param str
     */
    public String reverseString(String str) {
        // 將字符串轉換為字符數組
        char[] charArray = str.toCharArray();
        // 定義一個StringBuilder對象，用於保存反轉後的字符串
        StringBuilder sb = new StringBuilder();
        // 定義一個StringBuilder對象，用於暫存單詞
        StringBuilder word = new StringBuilder();
        // 遍歷字符數組
        // "I am a good boy!"
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == ' ' || i == charArray.length - 1) {
                // 如果是空格，或者已經到了字符數組的末尾，則反轉單詞並加入StringBuilder對象中
                sb.append(word.reverse());
                sb.append(charArray[i]);
                // 清空暫存的單詞
                word.setLength(0);
            } else if (Character.isLetter(charArray[i])) {
                // 如果是字母，則將該字母加入暫存的單詞中
                word.append(charArray[i]);
            } else {
                // 如果是標點符號，則直接加入StringBuilder對象中
                sb.append(charArray[i]);
            }
        }
        // 打印反轉後的字符串
        return sb.toString();
    }


    /***
     問題：
     將一個整數陣列向右旋轉K次的問題。旋轉陣列的意思是每個元素都向右移動一個索引位置，
     並將陣列的最後一個元素移動到第一個位置。例如，將陣列A = [3, 8, 9, 7, 6] 旋轉一次會得到 [6, 3, 8, 9, 7]。
     要求將陣列A旋轉K次，即將A中的每個元素向右移動K個索引位置。你需要撰寫一個函數，接受一個由N個整數構成的陣列A和一個整數K作為輸入，並返回旋轉後的陣列A。

     答案：
     解題思路是建立一個新的大小和A相同的陣列rotated，然後將A中的元素向右移動K次，並填充到rotated中。
     每個元素的新索引位置可以通過計算 (i + K) % N 獲得，其中i是元素在原始陣列中的索引，N是陣列大小。這樣可以處理K大於N的情況，因為它會繞到陣列的開頭。
     需要注意的是還有兩個邊界條件：如果陣列為空或K是N的倍數，則陣列不會改變，可以直接返回原始陣列A。

     */

    public int[] solution(int[] A, int K) {
        // Implement your solution here
        int n = A.length;

        int[] rotate = new int[n];
        // Handle edge cases
        if (n ==0 || K % n ==0) return A;

        // Rotate the array K times
        for (int i=0; i<n; i++) {
            int newIndex = (i+K)%n;
            System.out.println("i:" + i + ", value:" + A[i] + ", newIndex:" + newIndex);
            rotate[newIndex] = A[i];
        }
        return rotate;
    }


    /**
     問題：寫一個函數，傳入一個由N個整數構成的非空數組A，返回數組A中第一個不重複的數字。如果數組A中沒有不重複的數字，則返回-1。

     解答：
     需遍歷整個陣列至少一次才能確定有哪些數字只出現過一次。
     1.使用了HashMap來計算每個數字在陣列中出現的次數，這個操作的時間複雜度為O(N)。
     2.接著再次遍歷整個陣列，找到第一個只出現過一次的數字。
     Time Complexity: O(n)
     Space Complexity: O(n)
     */
    public int firstUnique(int[] A) {

        if (A == null || A.length ==0) return -1;
        Map<Integer, Integer> countMap = new HashMap<>();

        for (int num:A) {
            if (countMap.containsKey(num)) {
                countMap.put(num, countMap.get(num) + 1);
            } else {
                countMap.put(num, 1);
            }
        }

        for (int num:A) {
            if (countMap.get(num) == 1) {
                return num;
            }
        }
        return -1;
    }

    /**
     問題：給定一個字符串 S，返回其中某個字符的索引（從 0 開始計數），使得該字符左邊的部分與其右邊的部分是反轉關係。如果不存在這樣的索引，則返回 -1。

     解答：
     用StringBuilder reverse後，進行比對，若相等，表示整個字串是對稱的
     但須注意length需是若是偶數，回傳-1,否則會傳(s.length-1)/2
     */
    public int strSymmetryPoint(String s) {
        if (s == null || s.length() ==0) return -1;
        if (s.length() == 1) return 0;

        StringBuilder builder = new StringBuilder();
        builder.append(s).reverse();
        System.out.println("builder:"+ builder);
        if (s.equals(builder.toString())) {
            if (s.length() % 2 == 0) {
                return -1;
            } else {
                return (s.length() - 1) / 2;
            }
        } else {
            return -1;
        }
    }

    /**
     雙指針方法來解決這個問題。定義兩個指針i和j，分別指向字符串 S 的開始和結尾。
     當 S[i] 和 S[j] 相等時，分別移動指針 i 和 j；如果它們不相等，返回 -1。
     如果整個字符串 S 是反轉的，則返回 S 的中間字符的索引。
     */
    public int strSymmetryPoint2(String S) {
        if (S.isEmpty()) {
            return 0;
        }
        int i = 0;
        int j = S.length() - 1;
        while (i < j) {
            if (S.charAt(i) == S.charAt(j)) {
                i++;
                j--;
            } else {
                return -1;
            }
        }
        return S.length() % 2 == 0 ? -1 : i;
    }


    public int room(int[] A) {
        int n = A.length;
        int maxGuests = 0;
        int roomsNeeded = 0;
        int currentGuests = 0;

        for (int i = 0; i < n; i++) {
            maxGuests = Math.max(maxGuests, A[i]);
            currentGuests++;
            if (currentGuests == maxGuests) {
                roomsNeeded++;
                currentGuests = 0;
                maxGuests = 0;
            }
        }
        if (currentGuests > 0) {
            roomsNeeded++;
        }
        return roomsNeeded;
    }

    public int room2(int[] A) {
        int N = A.length;
        int rooms = 0;
        int currentRoomSize = 0;

        for (int i = 0; i < N; i++) {
            if (currentRoomSize + 1 > A[i]) {
                rooms++;
                currentRoomSize = 0;
            }
            currentRoomSize++;
        }

        return rooms + 1;
    }


    private static final int[][] directions = new int[][] {
            {1, 0}, {-1, 0}, {0, 1}, {0, -1}
    };

    public int getRemainingSpace(int[] A, String[] B, String[] C) {
        int width = A[0];
        int height = A[1];
        int N = A[2];

        // Initialize map with all empty spaces
        boolean[][] map = new boolean[height][width];
        for (int i = 0; i < height; i++) {
            Arrays.fill(map[i], true);
        }

        // Fill map with obsidian
        for (String s : B) {
            String[] coords = s.split(",");
            int x = Integer.parseInt(coords[0]) - 1;
            int y = Integer.parseInt(coords[1]) - 1;
            map[y][x] = false;
        }

        // Fill map with lava at time 0
        for (String s : C) {
            String[] coords = s.split(",");
            int x = Integer.parseInt(coords[0]) - 1;
            int y = Integer.parseInt(coords[1]) - 1;
            map[y][x] = false;
        }

        // Simulate N seconds
        for (int sec = 1; sec <= N; sec++) {
            boolean[][] newMap = new boolean[height][width];
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    // Lava flows in four directions
                    if (map[i][j] && ((i > 0 && !map[i-1][j]) ||
                            (i < height-1 && !map[i+1][j]) ||
                            (j > 0 && !map[i][j-1]) ||
                            (j < width-1 && !map[i][j+1]))) {
                        newMap[i][j] = false;  // Lava fills this space
                    } else {
                        newMap[i][j] = map[i][j];  // Copy previous state
                    }
                }
            }
            map = newMap;
        }

        // Count remaining spaces
        int count = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (map[i][j]) {
                    count++;
                }
            }
        }

        return count;
    }


    public static int solution22(int x, int y, int z) {
        int k = x + y; // 定義k為紙張和棒棒總數
        int popsicles = 0; // 定義popsicles為可換到的棒棒數量
        while (k >= z) { // 當總數大於等於換z支棒棒時
            int exchanged = k / z; // 換z支棒棒可獲得的冰棒數量
            popsicles += exchanged; // 加入獲得的冰棒數量
            k = exchanged + k % z; // 剩下的紙張和棒棒總數
        }
        return popsicles;
    }

    public int solution111(int[] A) {
        int n = A.length;
        if (n == 0) {
            return 0;
        }
        // sort the guests by their preference in ascending order
        Arrays.sort(A);
        int rooms = 1;
        int currentCapacity = A[0];
        // iterate through the guests and assign them to a room if possible
        for (int i = 1; i < n; i++) {
            if (A[i] <= currentCapacity) {
                currentCapacity -= A[i] - 1;
            } else {
                rooms++;
                currentCapacity = A[i];
            }
        }
        return rooms;
    }


    private static final int INF = 1000000000;

    public int[] calculateAwardMoney(int[] A, int[] B) {
        int n = (int) Math.sqrt(A.length * 2);
        int[][] dist = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dist[i], INF);
            dist[i][i] = 0;
        }
        for (int i = 0; i < A.length; i += 3) {
            int u = A[i] - 1, v = A[i + 1] - 1, w = A[i + 2];
            dist[u][v] = dist[v][u] = w;
        }
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    dist[i][j] = Math.min(dist[i][j], dist[i][k] + dist[k][j]);
                }
            }
        }
        int[] ans = new int[B.length / 2];
        for (int i = 0; i < B.length; i += 2) {
            int u = B[i] - 1, v = B[i + 1] - 1;
            ans[i / 2] = 100000000 / dist[u][v];
        }
        return ans;
    }

    public static void main(String[] args) {
        Interview interview = new Interview();
        String str = "I am a good boy!";
        int[] room = {1,1,1,1,1};
        int value = interview.solution111(room);
        System.out.println("value:" + value);

        int[] A = {1,8,3};
        String[] B = {"5,0"};
        String[] C = {"7,0"};


        /*int[] A = {8,4,2};
        String[] B = {"0,3", "3,6"};
        String[] C = {"1,3","2,5","2,1"};*/

        int result = interview.getRemainingSpace(A,B,C);
        System.out.println("result:" + result);

        int[] test1 = {3,4,2,3,7,4,3,9,3,4,1,6,4,6,4,7,2,1,9,8,9,9,5,2,2,10,2};
        int[] test2 =  {4,9,6,5,7,9,3,8,5,2};
        //int[] test = interview.calculateAwardMoney(test1,test2);
        //System.out.println("test:" + Arrays.toString(test));
    }
}
