package com.frank.array._053_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/11/12
 *     desc  : 53.Maximum Subarray
 * </pre>
 */

public class MaximumSubarray_53_M {
    /**
     Given an integer array nums, find the subarray with the largest sum, and return its sum.


     Example 1:

     Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
     Output: 6
     Explanation: The subarray [4,-1,2,1] has the largest sum 6.
     Example 2:

     Input: nums = [1]
     Output: 1
     Explanation: The subarray [1] has the largest sum 1.
     Example 3:

     Input: nums = [5,4,-1,7,8]
     Output: 23
     Explanation: The subarray [5,4,-1,7,8] has the largest sum 23.


     Constraints:

     1 <= nums.length <= 105
     -104 <= nums[i] <= 104


     Follow up: If you have figured out the O(n) solution, try coding another solution using the divide
     and conquer approach, which is more subtle.

     */
    /*
     題意：求數組中子數組的最大和
     */


	/*
	題意是求數組中子數組的最大和，這種最優問題一般第一時間想到的就是動態規劃，
	我們可以這樣想，當部分序列和大於零的話就一直加下一個元素即可，
	並和當前最大值進行比較，如果出現部分序列小於零的情況，
	那肯定就是從當前元素算起。其轉移方程就是
	dp[i] = nums[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);，
	由於我們不需要保留dp狀態，故可以優化空間複雜度為1，
	即'dp = nums[i] + (dp > 0 ? dp : 0);'。

	Time Complexity: O(n)
    Space Complexity: O(n)
	 */

    public int maxSubArrayDp(int[] nums) {
        if (nums == null || nums.length == 0) return 0;

        int res = nums[0];
        int[] dp = new int[nums.length];
        dp[0] = nums[0]; // 初始化最大和為第一個元素的最大和
        //{1,-2,4,3,1,-2,5};
        for (int i = 1; i < nums.length; i++) {
            dp[i] = nums[i] + (Math.max(dp[i - 1], 0));
            res = Math.max(res, dp[i]);
        }
        return res;
    }
	
	/*
	 本題為最優解問題，可利用動態規劃思路求解。從頭到尾遍歷每一個數組元素，
	 如前面元素的和為正，則加上本元素的值繼續搜索；如何前面元素的和為負，
	 則此元素開始新的和計數。整個過程中要注意更新和的最大值。
	 */
    // -2,1,-3,4,-1,2,1,-5,4
	public int maxSubArray2(int[] nums) {
        if(nums == null) return 0;
        int sum = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0 ; i < nums.length; i++){
            if (sum + nums[i] > nums[i]) {
                sum += nums[i];
            } else {
                sum = nums[i];
            }
            // sum = -2, 1 , -2, 4, 3, 5,  6, 1, 5,
            if (sum > max) {
                max = sum;// -2, 1, 4, 5, 6,
            }
        }
        return max;       
    }
	

    public static void main(String[] args) {
        MaximumSubarray_53_M solution = new MaximumSubarray_53_M();
        //int[] sample = {-2,1,-3,4,-1,2,1,-5,4};
        int[] sample = {1,-2,4,3,1,-2,5};
        System.out.println(solution.maxSubArrayDp(sample));
    }
}
