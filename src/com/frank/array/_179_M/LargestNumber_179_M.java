package com.frank.array._179_M;

import java.util.Arrays;
import java.util.Comparator;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/4/1
 *     title  : 179. Largest Number
 * </pre>
 */
public class LargestNumber_179_M {
    /**
     Given a list of non-negative integers nums, arrange them such that they form the largest number and return it.
     Since the result may be very large, so you need to return a string instead of an integer.


     Example 1:
     Input: nums = [10,2]
     Output: "210"

     Example 2:
     Input: nums = [3,30,34,5,9]
     Output: "9534330"

     Constraints:

     1 <= nums.length <= 100
     0 <= nums[i] <= 109

     */
    /*
     題意：給定一個整數陣列的情況下，找到一個數字串，使得串聯後的數字串是所有可能中的最大數字
     */

    /*
      解法:
      1.將整數陣列轉換為字串陣列，以便比較。
      2.自定義比較器，根據組合數字的字典順序降序排列。
      3.使用自定義比較器對字串陣列進行排序，並將排序後的字串陣列組合成最大數字。
      Time Complexity:  O(nlogn)
      Space Complexity: O(n)
     */
    public String largestNumber(int[] nums) {
        // 將整數陣列轉換為字串陣列，以便比較
        String[] strNums = new String[nums.length];
        for (int i=0; i<nums.length;i++) {
            strNums[i] = String.valueOf(nums[i]);
        }

        // 自定義比較器，根據組合數字的字典順序降序排列
        Comparator<String> comparator = (a, b) -> {
            String s1 = a+b;
            String s2 = b+a;
            return s2.compareTo(s1);
        };

        Arrays.sort(strNums, comparator);
        // 如果排序後的第一個數字是 0，則整個結果必定是 0
        if (strNums[0].equals("0")) {
            return "0";
        }
        // 否則，將排序後的字串陣列組合成最大數字
        StringBuilder sb = new StringBuilder();
        for (String str: strNums) {
            sb.append(str);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        LargestNumber_179_M solution = new LargestNumber_179_M();
        int[] nums1 = {1,2,2,3,3,3};
        int[] nums2 = {3,30,34,5,9};

        String largestNumber = solution.largestNumber(nums2);
        System.out.println("largestNumber:" + largestNumber);
    }
}
