package com.frank.array._123_H;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/9/2
 *     title : 123. Best Time to Buy and Sell Stock III
 * </pre>
 */
public class Solution {

	/**
     Say you have an array for which the ith element is the price of a given stock on day i.

     Design an algorithm to find the maximum profit. You may complete at most two transactions.

     Note: You may not engage in multiple transactions at the same time
     (i.e., you must sell the stock before you buy again).

     Example 1:
     Input: [3,3,5,0,0,3,1,4]
     Output: 6
     Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
     Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.

     Example 2:
     Input: [1,2,3,4,5]
     Output: 4
     Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
     Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
     engaging multiple transactions at the same time. You must sell before buying again.

     Example 3:
     Input: [7,6,4,3,1]
     Output: 0
     Explanation: In this case, no transaction is done, i.e. max profit = 0.

     Related topic: Array, Dynamic programming
	*/
	// 題目:允許兩次買賣，但同一時間只允許持有一支股票。也就意味著這兩次買賣在時間跨度上不能有重疊

    /*
	  思路：既然不能有重疊可以將整個序列以任意坐標i為分割點，分割成兩部分：
      prices[0:n-1] => prices[0:i] + prices[i:n-1]

      對於這個特定分割來說，最大收益為兩段的最大收益之和。每一段的最大收益當然可以用I的解法來做。
      而III的解一定是對所有0<=i<=n-1的分割的最大收益中取一個最大值。為了增加計算效率，
      考慮採用dp來做bookkeeping。目標是對每個坐標i：

      1. 計算A[0:i]的收益最大值：用minPrice記錄i左邊的最低價格，用maxLeftProfit記錄左側最大收益
      2. 計算A[i:n-1]的收益最大值：用maxPrices記錄i右邊的最高價格，用maxRightProfit記錄右側最大收益。
      3. 最後這兩個收益之和便是以i為分割的最大收益。將序列從左向右掃一遍可以獲取1，從右向左掃一遍可以獲取2。相加後取最大值即為答案。
	 */

    private int maxProfit(int[] prices) {
        if(prices.length == 0 ) return 0;
        int n = prices.length;

        int[] leftProfit = new int[n];

        // 先取第一個數當作目前最低價格
        int maxLeftProfit = 0, minPrice = prices[0];
        for (int i=1; i<n; i++) {
            // 若陣列[i]的價格比最低價格還低，則最低價格變成陣列[i]
            if (prices[i] < minPrice) {
                minPrice = prices[i];
            } else {
                //否則 陣列[i] - 最低價格 = 得到收益，先紀錄為目前最大收益
                maxLeftProfit = Math.max(maxLeftProfit, prices[i] - minPrice);
            }
            leftProfit[i] = maxLeftProfit;
        }

        int ret = leftProfit[n-1];
        int maxRightProfit = 0, maxPrice = prices[n-1];
        for (int i=n-2; i>=0; i--) {
            if (prices[i]>maxPrice) {
                maxPrice = prices[i];
            } else {
                maxRightProfit = Math.max(maxRightProfit, maxPrice - prices[i]);
            }
            ret = Math.max(ret, maxRightProfit + leftProfit[i]);
        }
        return ret;
    }


    public static void main(String[] args) {
    	Solution solution = new Solution();
        int[] sample = {3,3,5,0,0,3,1,4};

        int result = solution.maxProfit(sample);
        System.out.println("result:" + result);
    }
}
