package com.frank.array._268_E;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/17
 *     title  : 268. Missing Number
 * </pre>
 */
public class Solution {
    
	/**
	Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, 
	find the one that is missing from the array.

	Example 1:
	Input: [3,0,1]
	Output: 2
	
	Example 2:
	Input: [9,6,4,2,3,5,7,0,1]
	Output: 8
	
	Note:
	Your algorithm should run in linear runtime complexity. 
	Could you implement it using only constant extra space complexity?
	*/
	//題意：給定一個陣列,裡面本來應該有0~n的數各一,找出缺少的那個數

	/*
	 * 解法一：最直觀的一個方法是用等差數列的求和公式求出0到n之間所有的數字之和，
	 * 然後再遍歷數組算出給定數字的累積和，然後做減法，差值就是丟失的那個數字
	 */
	public int missingNumber(int[] nums) {
		int size = nums.length;
        int sum = 0;
		for (int num:nums) {
			sum+=num;
		}
		return (size *(size+1))/2 - sum;
    }
	
	/*
	 * 解法二：思路是既然0到n之間少了一個數，我們將這個少了一個數的數組合0到n之間
	 * 完整的數組Xor一下，那麼相同的數字都變為0了，剩下的就是少了的那個數字了
	 */
	public int missingNumberXor(int[] nums) {
        int res = 0;
        for (int i = 0; i < nums.length; ++i) {
            res ^= (i + 1) ^ nums[i];
        }
        return res;
    }
	
    public static void main(String[] args) {
    		Solution solution = new Solution();
        int sample[] = {3,0,1,2,4,6,5,8};
        int result = solution.missingNumberXor(sample);
        System.out.println("result:" + result);
    }
}
