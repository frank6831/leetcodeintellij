package com.frank.array._54_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/1/16
 *     title  : 54. Spiral Matrix
 * </pre>
 */
public class SpiralMatrix_54_M {
    /**
     Given an m x n matrix, return all elements of the matrix in spiral order.


     Example 1:


     Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
     Output: [1,2,3,6,9,8,7,4,5]
     Example 2:


     Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
     Output: [1,2,3,4,8,12,11,10,9,5,6,7]


     Constraints:

     m == matrix.length
     n == matrix[i].length
     1 <= m, n <= 10
     -100 <= matrix[i][j] <= 100
     */
    /*
     題意：將一個矩陣以螺旋順序返回其所有元素
     */

    /*
      解法：使用了模擬法
      1.用四個不同的for迴圈來遍歷矩陣的上、右、下、左四個邊

       矩陣中有 m 行和 n 列，則矩陣中的元素總數為 m * n。
      Time Complexity: O(m * n)
      Space Complexity: O(m * n)
     */
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> res = new ArrayList<>();
        if (matrix.length == 0) {
            return res;
        }
        int top = 0;//列，定義上邊界
        int bottom = matrix.length -1;//定義下邊界
        int left = 0; //行，定義左邊界
        int right = matrix[0].length -1; // 定義右邊界

        while (top <= bottom && left <= right) {
            // Traverse right, 在頂部從左到右遍歷上行
            for (int j= left; j<=right;j++) { //1,2,3
                res.add(matrix[top][j]);
            }
            top++;// 上邊界向下移動

            // Traverse down, 在右側從上到下遍歷右列
            for (int j=top;j<=bottom;j++) {
                res.add(matrix[j][right]);
            }
            right--; // 右邊界向左移動

            // 確保還有下行可遍歷
            if (top <= bottom) {
                // Traverse left, 在底部從右到左遍歷下行
                for (int j=right; j>=left;j--) {
                    res.add(matrix[bottom][j]);
                }
            }
            bottom--; // 下邊界向上移動

            System.out.println("top:"+ top + ",bottom:" +bottom);
            System.out.println("left:" +left + ", right:" + right);
            // 確保還有左列可遍歷
            if (left <= right) {
                // Traverse up, // 在左側從下到上遍歷左列
                for (int j=bottom;j>= top;j--) {
                    res.add(matrix[j][left]);
                }
            }
            left++;// 左邊界向右移動
        }
        return res;
    }

    public static void main(String[] args) {
        SpiralMatrix_54_M solution = new SpiralMatrix_54_M();
        int[][] array = {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};

        System.out.println("array length:" +array.length);
        //System.out.println("array[0] length:" +Arrays.toString(array[0]));
        //System.out.println("array[0] length:" +array[0][1]);

        List<Integer> result = solution.spiralOrder(array);

        System.out.println("result:" +result);
    }
}
