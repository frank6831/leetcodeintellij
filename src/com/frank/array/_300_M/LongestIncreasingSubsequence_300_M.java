package com.frank.array._300_M;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/4/11
 *     title  : 300. Longest Increasing Subsequence
 * </pre>
 */
public class LongestIncreasingSubsequence_300_M {
    /**
     Given an integer array nums, return the length of the longest strictly increasing
     subsequence.

     Example 1:
     Input: nums = [10,9,2,5,3,7,101,18]
     Output: 4
     Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.

     Example 2:
     Input: nums = [0,1,0,3,2,3]
     Output: 4

     Example 3:
     Input: nums = [7,7,7,7,7,7,7]
     Output: 1


     Constraints:

     1 <= nums.length <= 2500
     -104 <= nums[i] <= 104


     Follow up: Can you come up with an algorithm that runs in O(n log(n)) time complexity?
     */
    /*
     題意：給定一個整數數組中找出最長的遞增子序列的長度
     */

    /*
      解法:ArrayList+Binary search
      1.初始化空的動態數組 lis。
      2.對於每個原始數組中的元素 num：
        (1)使用二分查找找到 num 應該插入的位置left。
        (2)如果 left == lis.size()，則將num加到lis末尾；否則，將num插入到lis的left位置。
      3.返回 lis 的長度，即為最長上升子序列的長度。

      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public int lengthOfLIS(int[] nums) {
        List<Integer> list = new ArrayList<>();
        for (int num:nums) {
            int left =0, right = list.size();
            while (left < right) {
                int mid = left + (right -left)/2;
                if (list.get(mid) <num) {
                    left = mid +1;
                } else {
                    right = mid;
                }
            }
            System.out.println("left:" + left+", right:" + right + ", num:" + num + ", lis.size():" + list.size());
            if (left == list.size()) {
                list.add(num);
            } else {
                list.set(left, num);
            }
            System.out.println("list:" + list);
        }
        return list.size();
    }















    public int lengthOfLIS2(int[] nums) {
        List<Integer> list = new ArrayList<>();

        for (int num:nums) {
            int left = 0, right = list.size();
            while (left < right) {
                int mid = left + (right-left)/2;
                if (list.get(mid) < num) {
                    left = mid+1;
                } else {
                    right = mid;
                }
            }
            if (left == list.size()) {
                list.add(num);
            } else  {
                list.set(left, num);
            }
        }
        return list.size();
    }

    public static void main(String[] args) {
        LongestIncreasingSubsequence_300_M solution = new LongestIncreasingSubsequence_300_M();
        int[] nums1 = {1,2,2,3,3,3};
        int[] nums2 = {10,9,2,5,3,7,101,18};
        int lengthOfLIS = solution.lengthOfLIS2(nums2);
        System.out.println("lengthOfLIS:" + lengthOfLIS);
    }
}
