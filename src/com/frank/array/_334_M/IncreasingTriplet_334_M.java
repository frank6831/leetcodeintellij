package com.frank.array._334_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/2/26
 *     title  : 334.  Increasing Triplet Subsequence
 * </pre>
 */
public class IncreasingTriplet_334_M {
    /**
     Given an integer array nums, return true if there exists a triple of indices (i, j, k) such that
     i < j < k and nums[i] < nums[j] < nums[k]. If no such indices exists, return false.



     Example 1:

     Input: nums = [1,2,3,4,5]
     Output: true
     Explanation: Any triplet where i < j < k is valid.
     Example 2:

     Input: nums = [5,4,3,2,1]
     Output: false
     Explanation: No triplet exists.
     Example 3:

     Input: nums = [2,1,5,0,4,6]
     Output: true
     Explanation: The triplet (3, 4, 5) is valid because nums[3] == 0 < nums[4] == 4 < nums[5] == 6.


     Constraints:

     1 <= nums.length <= 5 * 105
     -231 <= nums[i] <= 231 - 1


     Follow up: Could you implement a solution that runs in O(n) time complexity and O(1) space complexity?
     */
    /*
     題意：給定一個整數陣列 nums，返回 True 如果可以在陣列中找到三個索引 (i, j, k)，同時符合i<j<k 且nums[i] < nums[j] < nums[k]，如果找不到返回 False
     */

    /*
      解法：Greedy
      1. 持續紀錄左邊出現的最小值與次小值，只要出現比這兩個值更大值，就代表找到
      2. 重點在於第二小的數目，當第二小的數目有被更新(之前有出現比它小的數目)，就能確定只要大於它，就能找到三個漸增的數字
      Time Complexity: O(n)
      Space Complexity: O(1)
     */

    public boolean increasingTriplet(int[] nums) {
        if (nums == null || nums.length < 3) {
            return false;
        }

        int min = Integer.MAX_VALUE;
        int secMin = Integer.MAX_VALUE;

        for (int num:nums) {
            if (num <= min) {
                min = num;
            } else if (num <= secMin) {
                secMin = num;
            } else {
                return true;
            }
        }
        return false;
    }


    public static void main(String[] args) {
        IncreasingTriplet_334_M solution = new IncreasingTriplet_334_M();
        int[] array = {2,1,5,0,4,6};
        int[] array2 = {5,4,3,2,1};
        boolean result = solution.increasingTriplet(array2);
        System.out.println("result:" + result);
    }
}
