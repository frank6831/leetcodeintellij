package com.frank.array._495_E;


/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/10
 *     title  : 495. Teemo Attacking
 * </pre>
 */
public class TeemoAttacking_495_E {
    /**
     Our hero Teemo is attacking an enemy Ashe with poison attacks! When Teemo attacks Ashe,
     Ashe gets poisoned for a exactly duration seconds. More formally,
     an attack at second t will mean Ashe is poisoned during the inclusive time interval [t, t + duration - 1].
     If Teemo attacks again before the poison effect ends, the timer for it is reset,
     and the poison effect will end duration seconds after the new attack.

     You are given a non-decreasing integer array timeSeries, where timeSeries[i] denotes that
     Teemo attacks Ashe at second timeSeries[i], and an integer duration.

     Return the total number of seconds that Ashe is poisoned.



     Example 1:

     Input: timeSeries = [1,4], duration = 2
     Output: 4
     Explanation: Teemo's attacks on Ashe go as follows:
     - At second 1, Teemo attacks, and Ashe is poisoned for seconds 1 and 2.
     - At second 4, Teemo attacks, and Ashe is poisoned for seconds 4 and 5.
     Ashe is poisoned for seconds 1, 2, 4, and 5, which is 4 seconds in total.
     Example 2:

     Input: timeSeries = [1,2], duration = 2
     Output: 3
     Explanation: Teemo's attacks on Ashe go as follows:
     - At second 1, Teemo attacks, and Ashe is poisoned for seconds 1 and 2.
     - At second 2 however, Teemo attacks again and resets the poison timer. Ashe is poisoned for seconds 2 and 3.
     Ashe is poisoned for seconds 1, 2, and 3, which is 3 seconds in total.


     Constraints:

     1 <= timeSeries.length <= 104
     0 <= timeSeries[i], duration <= 107
     timeSeries is sorted in non-decreasing order.
     */
    /*
     題意：給定一個陣列(sorted)表示在第幾秒攻擊,且給定毒會持續幾秒，回傳艾希共中毒幾秒
     */

    /*
      解法一：
      1.遍尋數據，開始時間為第一個攻擊的時間
      2.若兩次攻擊時間差大於duration，則只需加上duration秒
      3.否則加上兩次攻擊時間差即可
      4.跳出迴圈後，要加上最後一次攻擊的毒性時間
      Time Complexity: O(n)
      Space Complexity: O(1) //只需要記錄未中毒的開始時間即可
     */
    public int findPoisonedDuration(int[] timeSeries, int duration) {
        int total = 0;
        int n = timeSeries.length;

        //若timeSeries為空則直接回傳0
        if (n ==0) {
            return total;
        }
        //開始時間為第一個攻擊的時間
        int start = timeSeries[0];
        for (int i=1; i< timeSeries.length; i++) {
            int end = timeSeries[i];
            //若兩次攻擊時間差大於duration，則只需加上duration秒
            if (end - start >= duration) {
                total +=duration;
            } else {
                //否則加上兩次攻擊時間差即可
                total += end - start;
            }
            start = end;
        }
        //加上最後一次攻擊的毒性時間
        total += duration;
        return total;
    }

    public static void main(String[] args) {
        TeemoAttacking_495_E solution = new TeemoAttacking_495_E();
        int[] nums = {1,2,3,4,5};

        int totalDuration = solution.findPoisonedDuration(nums, 5);
        System.out.println("totalDuration:" + totalDuration);
    }
}
