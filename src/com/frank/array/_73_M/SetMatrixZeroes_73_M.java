package com.frank.array._73_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/2/19
 *     title  : 73. Set Matrix Zeroes
 * </pre>
 */
public class SetMatrixZeroes_73_M {
    /**
     Given an m x n integer matrix, if an element is 0, set its entire row and column to 0's.

     You must do it in place.



     Example 1:
     Input: matrix = [[1,1,1],[1,0,1],[1,1,1]]
     Output: [[1,0,1],[0,0,0],[1,0,1]]

     Example 2:
     Input: matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
     Output: [[0,0,0,0],[0,4,5,0],[0,3,1,0]]


     Constraints:

     m == matrix.length
     n == matrix[0].length
     1 <= m, n <= 200
     -231 <= matrix[i][j] <= 231 - 1


     Follow up:

     A straightforward solution using O(mn) space is probably a bad idea.
     A simple improvement uses O(m + n) space, but still not the best solution.
     Could you devise a constant space solution?
     */
    /*
     題意：給定一個矩陣，然後找出所有含有 0 的地方，把該位置所在行所在列的元素全部變成 0。
     */

    /*
      解法：Greedy
       1.掃描矩陣的第一行和第一列，標記哪些行和列需要被置零。
       2.再次遍歷矩陣，根據標記數組來進行置零操作。最後根據標記數組中的情況，決定是否將第一行和第一列置零。
      Time Complexity: O(n)
      Space Complexity: O(1)
     */

    public void setZeroes(int[][] matrix) {
        int rows = matrix.length; //紀錄第一列是否有零
        int cols = matrix[0].length;//紀錄第一行是否有零
        System.out.println("rows:" + rows + ", cols:" + cols);//r:3, c:4
        boolean rowZero = false;
        boolean colZero = false;

        // 检查第一列是否有零
        for (int j=0;j<cols;j++) {
            if (matrix[0][j] == 0) {
                rowZero = true;
                break;
            }
        }

        // 检查第一行是否有零
        for (int i=0;i<rows;i++) {
            if (matrix[i][0] == 0) {
                colZero = true;
                break;
            }
        }

        //使用第一列和第一行標記其他行和列是否需要置零
        for (int i=1;i<rows;i++) {
            for(int j=1;j<cols;j++) {
                if (matrix[i][j] ==0) {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }

        // 根據第一列和第一行的標記進行置零操作
        for (int i=1;i<rows;i++) {
            for (int j=1;j<cols;j++) {
                if (matrix[i][0] ==0 || matrix[0][j] ==0) {
                    matrix[i][j] = 0;
                }
            }
        }


        // 如果第一行有零，将第一行置零
        if (rowZero) {
            for (int i=0;i<rows;i++) {
                matrix[i][0] = 0;
            }
        }

        // 如果第一列有零，将第一列置零
        if (colZero) {
            for (int j=0;j<cols;j++) {
                matrix[0][j] = 0;
            }
        }
    }

    public static String defangIPaddr(String address) {
        return address.replace(".", "[.]");
    }


    public static int[] runningSum(int[] nums) {
        int[] result = new int[nums.length];

        for (int i=0;i<nums.length; i++) {
            int value = getSumValue(nums, i);
            result[i] = value;
        }
        return result;
    }


    private static int getSumValue(int[] nums, int index) {
        int sum = 0;
        for (int i=0;i<=index;i++) {
            sum+= nums[i];
        }
        return sum;
    }


    public static int[] runningSum2(int[] nums) {
        for(int i =1 ;i<nums.length; ++i){
            nums[i] += nums[i-1];
        }
        return nums ;
    }

    public static void main(String[] args) {
        SetMatrixZeroes_73_M solution = new SetMatrixZeroes_73_M();
        int[][] array = {{1,1,1},{1,0,1},{1,1,1}};
        int[][] array2 = {{0,1,2,0},{3,4,0,2},{1,3,1,5}};
        solution.setZeroes(array2);
        for (int i=0; i< array2.length;i++) {
            //System.out.println("array["+ i +"]:" + Arrays.toString(array2[i]));
        }

        int[] array3 = {3,1,2,10,1};
        int[] result2 = runningSum2(array3);
        System.out.println("result:" + Arrays.toString(result2));
    }
}
