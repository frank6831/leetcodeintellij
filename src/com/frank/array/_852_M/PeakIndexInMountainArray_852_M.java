package com.frank.array._852_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/11/7
 *     title  : 852. Peak Index in a Mountain Array
 * </pre>
 */
public class PeakIndexInMountainArray_852_M {
    /**
     An array arr is a mountain if the following properties hold:

     arr.length >= 3
     There exists some i with 0 < i < arr.length - 1 such that:
     arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
     arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
     Given a mountain array arr, return the index i such that arr[0] < arr[1] < ... < arr[i - 1] < arr[i] > arr[i + 1] > ... > arr[arr.length - 1].

     You must solve it in O(log(arr.length)) time complexity.



     Example 1:
     Input: arr = [0,1,0]
     Output: 1

     Example 2:
     Input: arr = [0,2,1,0]
     Output: 1

     Example 3:
     Input: arr = [0,10,5,2]
     Output: 1


     Constraints:
     3 <= arr.length <= 105
     0 <= arr[i] <= 106
     arr is guaranteed to be a mountain array.
     */
    /*
     題意：定義了一種山形的數組，說是有一個最高點，然後向兩邊各自降低，讓我們找出山峰的位置
     */

    /*
      解法:Binary Search
      1.如果左邊的比中間大，就往左邊找

      Time Complexity:  O(logN)
      Space Complexity: O(n)
     */
    public int peakIndexInMountainArray(int[] nums) {
        int left = 0;
        int right = nums.length-1;

        while (left < right) {
            int mid = left + (right - left) /2;
            // 縮減區間為[mid+1,right]
            if (nums[mid] < nums[mid+1]) {
                left = mid +1;
            } else {
                // 縮減區間為[left,mid]
                right = mid;
            }
        }
        //left=right時退出循環，返回left或right是相同的
        return left;
    }

    public static void main(String[] args) {
        PeakIndexInMountainArray_852_M solution = new PeakIndexInMountainArray_852_M();
        int[] nums1 = {0,10,5,2};
        int[] nums2 = {24,69,100,99,79,78,67,36,26,19};
        int peakElement = solution.peakIndexInMountainArray(nums2);
        System.out.println("peakElement:" + peakElement);
    }
}
