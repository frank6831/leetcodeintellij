package com.frank.array._088_E;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/11/12
 *     desc  : 88. Merge Sorted Array
 * </pre>
 */

public class MergeSortedArray_88_E {
    /**
     Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1
     as one sorted array.

     Note:

     The number of elements initialized in nums1 and nums2 are m and n respectively.
     You may assume that nums1 has enough space
     (size that is greater or equal to m + n) to hold additional elements from nums2.

     Example:
     Input:
     nums1 = [1,2,3,0,0,0], m = 3
     nums2 = [2,5,6],       n = 3

     Output: [1,2,2,3,5,6]
     */

    /*
      解法：
      1.直接將兩元素合併後，再排序
      Time Complexity: O(m+n) * log(n+m)
      Space Complexity: O(1)
     */

    public void mergeSort(int[] nums1, int m, int[] nums2, int n) {
        //System.arraycopy(nums2,0, nums1, m ,n);
        for (int i=0; i != n;i++) {
            nums1[m+i] = nums2[i];
        }
        Arrays.sort(nums1);
    }



    // https://www.javazhiyin.com/22093.html
    /*
        給定兩個有序整數數組A和B，把B併入A成為一個有序數組。
        注意：
        可以假設A有足夠的空間（大小大於等於m + n）來保存來自B的額外元素。 A和B的初始元素個數分別為m和n。
        Time Complexity: O(m+n)
        Space Complexity: O(m+n)
     */
	private void merge(int[] nums1, int m, int[] nums2, int n) {
	    int len = m + n -1;
        while (m > 0 && n > 0){
            if (nums1[m-1] > nums2[n-1]){
                nums1[len] = nums1[m-1];
                m--;
            } else {
                nums1[len] = nums2[n-1];
                n--;
            }
            len--;
        }

        while (n > 0){
            nums1[m+n-1] = nums2[n-1];
            n--;
        }
    }

    /*
    這題可以分成兩個步驟就會變得很簡單，先把nums2的值放到nums1裡面
    ex. nums1 = [1,2,6,null,null] , nums2 = [4,5] --> [1,2,6,4,5]
    然後將nums1裡面的元素重新排序 [1,2,4,5,6]
     */
    private void mergeBubble(int[] nums1, int m, int[] nums2, int n) {
        int index = 0;
        //將nums2裡面的值放在nums1
        for(int i = m ; i < m+n ; i++){
            nums1[i] = nums2[index];
            index++;
        }
        Arrays.sort(nums1);
    }


    public static void main(String[] args) {
        MergeSortedArray_88_E solution = new MergeSortedArray_88_E();
        int[] sample = {2,3,4,0,0,0};
        int[] sample2 = {2,5,6};
        solution.mergeBubble(sample, 3, sample2, 3);
        System.out.println(Arrays.toString(sample));
    }
}
