package com.frank.array._56_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/4
 *     title  : 56.Merge Intervals
 * </pre>
 */
public class MergeIntervals_56_M {
    /**
     Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals,
     and return an array of the non-overlapping intervals that cover all the intervals in the input.


     Example 1:
     Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
     Output: [[1,6],[8,10],[15,18]]
     Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].

     Example 2:
     Input: intervals = [[1,4],[4,5]]
     Output: [[1,5]]
     Explanation: Intervals [1,4] and [4,5] are considered overlapping.


     Constraints:

     1 <= intervals.length <= 104
     intervals[i].length == 2
     0 <= starti <= endi <= 104
     */
    /*
     題意：給定一個列表，將有重疊部分的合併。例如[ [ 1 3 ] [ 2 6 ] ] 合併成 [ 1 6 ] 。
     */

    /*
      解法：
      1.先將列表由小到大排序
      2.若遇到重疊的intervals，就把兩者merge起來，merge完的interval的時間必定會是(a.start, max(a.end, b.end))
      3.若無重疊，那就把目前的 interval 留在答案中，繼續往下一個 interval 看
      Time Complexity: O(nlong(n))
      Space Complexity: O(n)
     */

    public int[][] merge(int[][] intervals) {
        if (intervals == null || intervals.length == 0) return intervals;
        List<int[]> res = new ArrayList<>();

        Arrays.sort(intervals, Comparator.comparingInt(a -> a[0]));
        int[] curr = intervals[0];

        for (int i=1;i< intervals.length;i++) {
            // 若curr的結束時間 < 下一個item的的起始時間，表示時間沒有重疊，直接加到result list裡
            if (curr[1] < intervals[i][0]) {
                res.add(curr);
                curr = intervals[i];
            } else {
                // 因已排序過，更新結束時間就好：取兩者結束時間的最大值
                curr[1] = Math.max(curr[1], intervals[i][1]);
            }
        }
        //要把最後一個加進去
        res.add(curr);
        return res.toArray(new int[0][]);
    }

    private int[][] mergeWhileVersion(int[][] intervals) {
        if (intervals == null || intervals.length ==0) return intervals;
        List<int[]> res = new ArrayList<>();
        // 對起點終點進行排序
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);

        int i =0;
        while (i < intervals.length) {
            int left = intervals[i][0];
            int right = intervals[i][1];

            // 如果有重疊，循環判斷哪個起點滿足條件
            // 當前終點大於下一個數組的起點的時候，比較當前終點和下一個終點的大小，取為right
            while (i < intervals.length -1 && intervals[i+1][0] <= right) {
                i++;
                right = Math.max(right, intervals[i][1]);
            }
            // 將現在的區間放進res裡面
            res.add(new int[]{left, right});
            // 接著判斷下一個區間
            i++;
        }
        return res.toArray(new int[0][]);
    }

    public static void main(String[] args) {
        MergeIntervals_56_M solution = new MergeIntervals_56_M();
        int[][] array = {{8, 10}, {2, 6}, {1, 3} ,{15, 18}};
        int[][] array2 = {{1, 4}, {4, 5}};

        int[][] array3 = {{1, 4}, {2, 8}, {3,6}};
        int[][] array4 = {{1, 5}, {2, 3}};

        int[][] result = solution.merge(array);
        for (int i=0; i<result.length; i++) {
            for(int j=0; j<result[i].length; j++) {//這此運用到取得第i列的元素個數
                //System.out.println("result["+i+"]["+j+"]="+result[i][j]);
            }
        }
    }
}
