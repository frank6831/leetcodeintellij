package com.frank.array._287_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/10/30
 *     title  : 287. Find the Duplicate Number
 * </pre>
 */
public class FindTheDuplicateNumber_287_M {
    /**
     Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.

     There is only one repeated number in nums, return this repeated number.

     You must solve the problem without modifying the array nums and uses only constant extra space.



     Example 1:

     Input: nums = [1,3,4,2,2]
     Output: 2
     Example 2:

     Input: nums = [3,1,3,4,2]
     Output: 3


     Constraints:

     1 <= n <= 105
     nums.length == n + 1
     1 <= nums[i] <= n
     All the integers in nums appear only once except for precisely one integer which appears two or more times.


     Follow up:

     How can we prove that at least one duplicate number must exist in nums?
     Can you solve the problem in linear runtime complexity?
     */
    /*
     題意：給一個n+1個整數的陣列，數字都在1到n之間(包括1和n)，可知至少存在一個重複的整數。假設只有一個重複的整數，找出這個重複的數。
     */

    /*
      解法：利用另一個array紀錄每個數字的count


      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public int findDuplicate(int[] nums) {
        int size = nums.length;
        int[] candidate = new int[size + 1];
        for (int value : nums) {
            if (candidate[value] == 0) {
                candidate[value] = value;
            } else {
                return value;
            }
        }
        return 0;
    }


    /*
      解法：binary search
      原數組不是有序，但是我們知道重複的那個數字肯定是1到n中的某一個，而 1,2...,n 就是一個有序序列。 因此我們可以對 1,2...,n 進行二分查找。

      1.初始時查找區間為整個數組，取數組的中間位mid，遍歷一遍，查看小於等於mid的數字個數，
      2.若個數大於mid，說明重複的數字在[1,mid]之間，反之在[mid+1,n]區間。繼續二分查找。

      Time Complexity:  O(nlogn)
      Space Complexity: O(1)
     */
    public int findDuplicate_binary_search(int[] nums) {
        int l = 1;
        int r = nums.length;

        while(l < r) {
            int mid = l + (r - l) / 2;
            System.out.println("l:" + l + ", r:" +r);
            System.out.println("mid:" + mid);
            int count = 0;
            for (int num : nums) {
                if (num <= mid) {
                    count++;
                }
            }
            System.out.println("count:" + count);
            if (count > mid) {
                r = mid;
            } else {
                l = mid+1;
            }
        }
        return l;
    }



    public static void main(String[] args) {
        FindTheDuplicateNumber_287_M solution = new FindTheDuplicateNumber_287_M();
        int[] nums1 = {1,3,4,2,2};
        int[] nums2 = {2,1,3,4,2};
        int result = solution.findDuplicate_binary_search(nums2);
        System.out.println("result:" + result);
    }
}
