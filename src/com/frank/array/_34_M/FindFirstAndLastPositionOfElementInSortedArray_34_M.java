package com.frank.array._34_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/1/4
 *     title  : 34. Find First and Last Position of Element in Sorted Array
 * </pre>
 */
public class FindFirstAndLastPositionOfElementInSortedArray_34_M {
    /**
     Given an array of integers nums sorted in non-decreasing order,
     find the starting and ending position of a given target value.

     If target is not found in the array, return [-1, -1].

     You must write an algorithm with O(log n) runtime complexity.



     Example 1:
     Input: nums = [5,7,7,8,8,10], target = 8
     Output: [3,4]

     Example 2:
     Input: nums = [5,7,7,8,8,10], target = 6
     Output: [-1,-1]

     Example 3:
     Input: nums = [], target = 0
     Output: [-1,-1]

     Constraints:

     0 <= nums.length <= 105
     -109 <= nums[i] <= 109
     nums is a non-decreasing array.
     -109 <= target <= 109
     */
    /*
     題意：找出目標值的第一次出現和最後一次出現的位置，同樣要求 log ( n ) 下完成。
     */

    /*
      解法:Binary Search
      1.先用Binary Search 找出target，然後再往target 的左右分別進行Binary Search
      2.分邊找出起始位置


      Time Complexity:  O(logN)
      Space Complexity: O(n)
     */
    public int[] searchRange(int[] nums, int target) {
        if (nums == null || nums.length ==0) return new int[]{-1,-1};

        // 查找第一個出現的位置
        int left = findLeft(nums, target);
        int right = findRight(nums, target);
        return new int[]{left, right};
    }

    private int findLeft(int[] nums, int target) {
        int left = 0;
        int right = nums.length -1;

        // 找分界线
        while (left <= right) {
            int mid = left + (right-left) /2;
            if (nums[mid] >= target) {
                right = mid -1;
            } else {
                left = mid+1;
            }
        }

        if (nums[left] == target) {
            return left;
        } else if (nums[right] == target) {
            return right;
        } else {
            return -1;
        }
    }

    private int findRight(int[] nums, int target) {
        int left=0;
        int right = nums.length -1;

        while (left+1 < right) {
            int mid = left + (right-left) /2;
            if (nums[mid] <= target) {
                left = mid+1;
            } else {
                right = mid-1;
            }
        }

        if (nums[right] == target) {
            return right;
        } if (nums[left] == target) {
            return left;
        } else {
            return -1;
        }
    }


    public static void main(String[] args) {
        FindFirstAndLastPositionOfElementInSortedArray_34_M solution = new FindFirstAndLastPositionOfElementInSortedArray_34_M();
        int[] nums1 = {5,7,7,8,8,10};
        int[] result = solution.searchRange(nums1, 8);
        System.out.println("result:" + Arrays.toString(result));
    }
}
