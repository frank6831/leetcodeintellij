package com.frank.array._1_E;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/2
 *     title  : 1. Two Sum
 * </pre>
 */
public class TwoSum_1_E {
    /**
     Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

     You may assume that each input would have exactly one solution, and you may not use the same element twice.

     You can return the answer in any order.



     Example 1:

     Input: nums = [2,7,11,15], target = 9
     Output: [0,1]
     Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
     Example 2:

     Input: nums = [3,2,4], target = 6
     Output: [1,2]
     Example 3:

     Input: nums = [3,3], target = 6
     Output: [0,1]


     Constraints:

     2 <= nums.length <= 104
     -109 <= nums[i] <= 109
     -109 <= target <= 109
     Only one valid answer exists.


     Follow-up: Can you come up with an algorithm that is less than O(n2) time complexity?
     */
    /*
     題意：給定整數數組中查找兩個數字，其總和等於給定目標數字的問題
     */


    /*
      解答：
      1.使用HashMap來存儲數組中每個數字對應的索引
      2.遍歷數組，若存在一個數字與目標數字的差值已在HashMap中，則此兩個數字的總和等於目標數字
      3.若沒找到兩個數字的總和等於目標數字，返回null。
      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    public int[] twoSum(int[] nums, int target) {
        // 使用HashMap來存儲數組中每個數字對應的索引
        Map<Integer, Integer> map = new HashMap<>();

        // 遍歷數組中的每個數字，如果存在一個數字與目標數字的差值已經在哈希表中，那麼這兩個數字的總和等於目標數字
        for (int i=0; i< nums.length;i++) {
            int result =  target - nums[i];
            if (map.containsKey(result)) {
                return new int[]{map.get(result),i};
            } else {
                map.put(nums[i], i);
            }
        }
        return new int[]{};
    }

    public static void main(String[] args) {
        TwoSum_1_E solution = new TwoSum_1_E();
        int[] nums = {3,3};

        int[] result = solution.twoSum(nums, 6);
        System.out.println("result:" + Arrays.toString(result));
    }
}
