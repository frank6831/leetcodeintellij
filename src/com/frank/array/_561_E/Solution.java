package com.frank.array._561_E;

import java.util.Arrays;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/8/20
 *     title : 561. Array Partition I
 * </pre>
 */
public class Solution {
    
	/**
     Given an array of 2n integers, your task is to group these integers into n pairs of integer,
     say (a1, b1), (a2, b2), ..., (an, bn) which makes sum of min(ai, bi) for all i from 1 to n as large as possible.

     Example 1:
     Input: [1,4,3,2]
     Output: 4

     Explanation: n is 2, and the maximum sum of pairs is 4 = min(1, 2) + min(3, 4).
     Note:
     n is a positive integer, which is in the range of [1, 10000].
     All the integers in the array will be in the range of [-10000, 10000].
	*/
	// 題目要求我們計算每兩個數中最小數之和的最大值。以題目中的數組為例，{1,4,2,3}，四個數組成任意兩對組合
	/*
	  思路：這道題目給了我們一個數組有2n integers， 需要我們把這個數組分成n對，然後從每一對裏面拿小的那個數字，把所有的加起來，返回這個sum。
	  並且要使這個sum 盡量最大。如何讓sum 最大化呢，我們想一下，如果是兩個數字，一個很小，一個很大，這樣的話，取一個小的數字，就浪費了那個大的數字。
	  所以我們要使每一對的兩個數字盡可能接近。我們先把nums sort 一下，讓它從小到大排列，接著每次把index： 0， 2， 4...偶數位的數字加起來就可以了
	  時間複雜度是O(n log(n))，空間複雜度是O(1)。
	 */
    public int arrayPairSum(int[] nums) {
        Arrays.sort(nums);
        int total = 0;
        for (int i=0; i<nums.length; i+=2) {
            total += nums[i];
        }
        return total;
    }
	
    public static void main(String[] args) {
    	Solution solution = new Solution();
        int[] sample = {1, 4, 3, 2};
        
        int result = solution.arrayPairSum(sample);
        System.out.println("result:" + result);
    }
}
