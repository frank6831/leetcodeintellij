package com.frank.array._45_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/2/16
 *     title  : 45. Jump Game II
 * </pre>
 */
public class JumpGame_II_45_M {
    /**
     You are given a 0-indexed array of integers nums of length n. You are initially positioned at nums[0].

     Each element nums[i] represents the maximum length of a forward jump from index i.
     In other words, if you are at nums[i], you can jump to any nums[i + j] where:

     0 <= j <= nums[i] and
     i + j < n
     Return the minimum number of jumps to reach nums[n - 1]. The test cases are generated such that you can reach nums[n - 1].



     Example 1:
     Input: nums = [2,3,1,1,4]
     Output: 2
     Explanation: The minimum number of jumps to reach the last index is 2. Jump 1 step from index 0 to 1, then 3 steps to the last index.

     Example 2:
     Input: nums = [2,3,0,1,4]
     Output: 2


     Constraints:

     1 <= nums.length <= 104
     0 <= nums[i] <= 1000
     It's guaranteed that you can reach nums[n - 1].
     */
    /*
     題意：給定一個非負整數數組，陣列中的每個元素代表在該位置可以跳躍的最大長度。目標是使用最少跳躍次數到達陣列的最後一個位置。
     要求找到跳躍方案，使得能夠最快到達數組的末尾，返回最少的跳躍次數。
     */

    /*
      解法：Greedy
      1.在遍歷陣列的過程中，記錄當前能夠到達的最遠位置 maxReach，以及上一次跳躍結束的位置 lastJumpEnd。
      2.當遍歷到上一次跳躍結束的位置時，進行一次跳躍，並更新lastJumpEnd為當前能夠到達的最遠位置。最終返回跳躍次數jumps。

      Time Complexity: O(n)
      Space Complexity: O(1)
     */

    public int jump(int[] nums) {
        int jumps = 0; // 記錄跳躍次數
        int maxReach = 0; // 記錄當前能夠到達的最遠位置
        int lastJumpEnd = 0;// 記錄上一次跳躍結束的位置

        for (int i=0; i< nums.length-1;i++) { // 不需要計算最後一個位置
            maxReach = Math.max(maxReach, i+nums[i]);  // 更新當前能夠到達的最遠位置
            if (i == lastJumpEnd) {// 如果到達了上一次跳躍的最遠位置
                jumps++;// 進行一次跳躍
                lastJumpEnd = maxReach;// 更新上一次跳躍結束的位置為當前能夠到達的最遠位置
            }
        }
        return jumps;
    }


    public static void main(String[] args) {
        JumpGame_II_45_M solution = new JumpGame_II_45_M();
        int[] array = {2,3,1,1,4};
        int[] array2 = {3, 2, 1, 0, 4};
        int result = solution.jump(array2);
        System.out.println("result:" + result);
    }
}
