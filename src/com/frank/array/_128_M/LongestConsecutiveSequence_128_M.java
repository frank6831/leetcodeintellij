package com.frank.array._128_M;

import java.util.HashSet;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/3/25
 *     title  : 128. Longest Consecutive Sequence
 * </pre>
 */
public class LongestConsecutiveSequence_128_M {
    /**
     Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.
     You must write an algorithm that runs in O(n) time.



     Example 1:
     Input: nums = [100,4,200,1,3,2]
     Output: 4
     Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.

     Example 2:
     Input: nums = [0,3,7,2,5,8,4,6,0,1]
     Output: 9


     Constraints:
     0 <= nums.length <= 105
     -109 <= nums[i] <= 109
     */
    /*
     題意：找到最長的連續數字
     */

    /*
      解法:HashSet
     1.使用 HashSet 存儲所有數字。
     2.遍歷每個數字，檢查是否為序列的開頭，若是，則從該數字開始往後尋找連續的數字，同時更新最長連續序列的長度。

      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public int longestConsecutive(int[] nums) {
        HashSet<Integer> set = new HashSet<>();
        for (int num:nums) {
            set.add(num);
        }

        int longestStreak = 0;
        for (int num:nums) {
            // 如果當前數字是序列的開頭（沒有比它小的數字在 HashSet 中）
            if (!set.contains(num-1)) {
                int currentNum = num;
                int currentStreak = 1;
                // 從當前數字開始往後尋找連續數字
                while (set.contains(currentNum+1)) {
                    currentNum++;
                    currentStreak++;
                }
                // 更新最長連續序列的長度
                longestStreak = Math.max(longestStreak, currentStreak);
            }
        }
        return longestStreak;
    }


    public static void main(String[] args) {
        LongestConsecutiveSequence_128_M solution = new LongestConsecutiveSequence_128_M();
        int[] nums1 = {1,2,2,3,3,3};
        int[] nums2 = {100,4,200,1,3,2};
        int longestStreak = solution.longestConsecutive(nums2);
        System.out.println("longestStreak:" + longestStreak);
    }
}
