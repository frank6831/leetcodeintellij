package com.frank.array._75_M;

import com.frank.linkedlist.ListNode;
import com.frank.tree.TreeNode;

import java.awt.List;
import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/2/16
 *     title  : 75. Sort Colors
 * </pre>
 */
public class SortColors_75_M {
    /**
     Given an array nums with n objects colored red, white, or blue, sort them in-place so that objects of the same color are adjacent, with the colors in the order red, white, and blue.

     We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.

     You must solve this problem without using the library's sort function.



     Example 1:

     Input: nums = [2,0,2,1,1,0]
     Output: [0,0,1,1,2,2]
     Example 2:

     Input: nums = [2,0,1]
     Output: [0,1,2]


     Constraints:

     n == nums.length
     1 <= n <= 300
     nums[i] is either 0, 1, or 2.


     Follow up: Could you come up with a one-pass algorithm using only constant extra space?
     */
    /*
     題意：將陣列排序，使得所有的0都排在1和2的前面，所有的1都排在2的前面，不允許使用額外的空間。
     */

    /*
      解法：Two pointer(使用三個指針遍歷數組)
      1.當當前指針遇到0時，將其與左指針位置的元素交換
      2.遇到2時，將其與右指針位置的元素交換，遇到1時，當前指針向右移動。
      3.遍歷一遍後，數組中的元素就按照題目要求排列好了

      Time Complexity: O(n)
      Space Complexity: O(1)
     */

    public void sortColors(int[] nums) {
        int left = 0; // 定義左指針
        int right = nums.length - 1; // 定義右指針
        int current = 0; // 定義當前指針

        // 遍歷數組
        while (current <= right) {
            if (nums[current] == 0) { // 如果當前指針指向的元素為0
                swap(nums, left, current); // 將當前指針指向的元素與左指針指向的元素交換
                left++; // 同時移動左指針和當前指針
                current++;
            } else if (nums[current] == 2) { // 如果當前指針指向的元素為2
                swap(nums, current, right); // 將當前指針指向的元素與右指針指向的元素交換
                right--; // 同時移動右指針，但當前指針不動，因為交換後的右指針位置可能是0
            } else { // 如果當前指針指向的元素為1
                current++; // 當前指針向右移動，不用交換元素
            }
        }
    }


    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }


    public static void main(String[] args) {
        SortColors_75_M solution = new SortColors_75_M();
        int[] array = {2,0,2,1,1,0};
        solution.sortColors(array);
        //System.out.println("result:" + Arrays.toString(array));

        int[] array2 = {7,1,5,3,6,4};
        int result = solution.maxProfit(array2);
        //System.out.println("result:" + result);

        int step = solution.climbStairs(4);
        //System.out.println("step:" + step);

        //String match = solution.addBinary("11", "1");
        //System.out.println("match:" + match);

        int[] array3 = {4,1,2,1,2};
        //boolean hasDuplicated = solution.containsDuplicate(array3);
        //System.out.println("hasDuplicated:" + hasDuplicated);

        String[] strings = {"flower","flow","flight"};
        String longest  = solution.longestCommonPrefix2(strings);


        boolean miss = solution.isPalindrome(10);
        System.out.println("miss:" + miss);
    }

    public boolean isPalindrome(int x) {
        if (x < 0) return false;

        String str = String.valueOf(x);

        int l = 0;
        int r = str.length() -1;

        while (l <=r) {
            if (str.charAt(l) != str.charAt(r)) {
                return false;
            }
            l++;
            r--;
        }
        return true;
    }

    public int missingNumber(int[] nums) {

        int n = nums.length;
        int sum = 0;
        for (int num:nums) {
            sum+=num;
        }
        return (n*(n+1))/2 - sum;
    }


    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }

        String candidate = strs[0];

        for (int i=0;i<candidate.length();i++) {
            char c = candidate.charAt(i);
            for (int j=1;j<strs.length;j++) {
                if (i == strs[j].length() || strs[j].charAt(i) != c) {
                    System.out.println("i:" + i);
                    return strs[0].substring(0,i);
                }
            }
        }

        // flow start
        return strs[0];
    }


    public String longestCommonPrefix2(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        String prefix = strs[0];

        for (int i=1;i<strs.length;i++) {
            while (!strs[i].startsWith(prefix)) {
                prefix = prefix.substring(0, prefix.length()-1);
            }
        }

        return prefix;
    }


    private String backHelper(String str) {
        StringBuilder sb = new StringBuilder();
        for (char c:str.toCharArray()) {
            if (c !='#') {
                sb.append(c);
            } else if (sb.length() !=0) {
                sb.deleteCharAt(sb.length()-1);
            }
        }
        return sb.toString();
    }


    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int value : nums) {
            if (set.contains(value)) return true;
            set.add(value);
        }
        return false;
    }

    public ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        ListNode next;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

    public boolean canConstruct(String ransomNote, String magazine) {
        int[] array = new int[26];
        for (int i=0; i<magazine.length();i++) {
            char c = magazine.charAt(i);
            array[c-'a']++;
        }


        for(int i=0; i<ransomNote.length();i++) {
            char c = ransomNote.charAt(i);
            if ((--array[c-'a']) < 0) {
                return false;
            }
        }
        return true;
    }

    public int climbStairs(int n) {
        if (n<=2) return n;

        return climbStairs(n-1) + climbStairs(n-2);
    }

    public boolean hasCycle(ListNode head) {
        if (head == null) return false;

        ListNode fast = head;
        ListNode slow = head;

        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                return true;
            }
        }
        return false;
    }

    public boolean isBalanced(TreeNode root) {

        if (root == null) return false;
        int heightDiff = Math.abs(height(root.left) - height(root.right));

        return heightDiff <=1 && isBalanced(root.left) && isBalanced(root.right);
    }

    private int height(TreeNode root) {
        if (root == null) return 0;
        return Math.max(height(root.left), height(root.right)) + 1;
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {

        if (root == null) return null;

        if (root.val > p.val && root.val > q.val) {
            return lowestCommonAncestor(root.left, p, q);
        } else if (root.val < p.val && root.val <q.val) {
            return lowestCommonAncestor(root.right, p, q);
        } else {
            return root;
        }
    }

    public TreeNode invertTree(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) {
            return root;
        }

        TreeNode tmp = root.right;
        root.right = root.left;
        root.left = tmp;

        invertTree(root.left);
        invertTree(root.right);
        return root;
    }


    public int maxProfit(int[] prices) {
        int result = 0, buy = Integer.MAX_VALUE;
        for (int price:prices) {
            // 取最小值
            buy = Math.min(buy, price);

            //取當前價格減掉最小值後，就是得到最大的獲利
            result = Math.max(result, price - buy);
        }

        return result;
    }
}
