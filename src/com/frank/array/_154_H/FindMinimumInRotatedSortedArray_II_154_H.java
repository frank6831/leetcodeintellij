package com.frank.array._154_H;


/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/9
 *     title  : 153. Find Minimum in Rotated Sorted Array
 * </pre>
 */
public class FindMinimumInRotatedSortedArray_II_154_H {
    /**
     Suppose an array of length n sorted in ascending order is rotated between 1 and n times.
     For example, the array nums = [0,1,4,4,5,6,7] might become:

     [4,5,6,7,0,1,4] if it was rotated 4 times.
     [0,1,4,4,5,6,7] if it was rotated 7 times.
     Notice that rotating an array [a[0], a[1], a[2], ..., a[n-1]] 1 time results in the array [a[n-1], a[0], a[1], a[2], ..., a[n-2]].

     Given the sorted rotated array nums that may contain duplicates, return the minimum element of this array.

     You must decrease the overall operation steps as much as possible.



     Example 1:

     Input: nums = [1,3,5]
     Output: 1
     Example 2:

     Input: nums = [2,2,2,0,1]
     Output: 0


     Constraints:

     n == nums.length
     1 <= n <= 5000
     -5000 <= nums[i] <= 5000
     nums is sorted and rotated between 1 and n times.


     Follow up: This problem is similar to Find Minimum in Rotated Sorted Array,
     but nums may contain duplicates. Would this affect the runtime complexity? How and why?
     */
    /*
     題意：尋找旋轉有序數組(數組的值會重複)的最小值
     */


    /*
      解法一：Binary search
      1.數組是被旋轉過的，所以最小值一定會在最大值的左邊
      2.先找到中間的數mid，若mid比最右邊的數還大，最小值一定在mid右邊，否則最小值在mid左邊或者mid本身
      3.需多判斷如果mid的值==right的值的case, 執行right--
      3.不斷縮小搜索範圍，直到左右指針相遇，剩下的數即為最小值
      Time Complexity: O(log n)
      Space Complexity: O(1)
     */
    public int findMin(int[] nums) {
        int left = 0;
        int right = nums.length -1;

        while (left < right) {
            int mid = left + (right - left )/2;
            if (nums[mid] > nums[right]) {  // 如果中間的數比最右邊的數大，則最小值應該在mid右邊
                left = mid + 1;
            } else if (nums[mid] < nums[right]) {  // 否則最小值應該在mid左邊或mid本身
                right = mid;
            } else {
                right--; //遇到相同元素時，表示最小值一定是偏左的，所以可以收縮右邊界
            }
        }
        return nums[left];
    }

    public static void main(String[] args) {
        FindMinimumInRotatedSortedArray_II_154_H solution = new FindMinimumInRotatedSortedArray_II_154_H();
        int[] nums = {3,1,3,3,3};
        //4,5,6,7,0,1,2
        // 5,6,7,0,1,2,4
        // 0,1,2,4,5,6,7
        // 6,7,0,1,2,4,5
        //2,4,5,6,7,0,1
        //1,2,4,5,6,7,0

        int minValue = solution.findMin(nums);
        System.out.println("minValue:" + minValue);
    }
}
