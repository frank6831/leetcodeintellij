package com.frank.array._2540_E;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/7
 *     title  : 2540. Minimum Common Value
 * </pre>
 */
public class MinimumCommonValue_2540_E {
    /**
     Given two integer arrays nums1 and nums2, sorted in non-decreasing order,
     return the minimum integer common to both arrays.
     If there is no common integer amongst nums1 and nums2, return -1.

     Note that an integer is said to be common to nums1 and nums2
     if both arrays have at least one occurrence of that integer.



     Example 1:

     Input: nums1 = [1,2,3], nums2 = [2,4]
     Output: 2
     Explanation: The smallest element common to both arrays is 2, so we return 2.

     Example 2:
     Input: nums1 = [1,2,3,6], nums2 = [2,3,4,5]
     Output: 2
     Explanation: There are two common elements in the array 2 and 3 out of which 2 is the smallest, so 2 is returned.
     */
    /*
     題意：給定兩個非遞減排序的整數數組nums1和nums2，返回兩個數組中最小的共同整數。如果nums1和nums2中沒有共同整數，則返回-1。
     */

    /*
      解法：自己寫

      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public int getCommon(int[] nums1, int[] nums2) {
        HashSet<Integer> set1 = new HashSet<>();
        List<Integer> res = new ArrayList<>();

        for (int num:nums1) {
            set1.add(num);
        }

        for (int num:nums2) {
            if (set1.contains(num)) {
                res.add(num);
            }
        }

        if (res.isEmpty()) {
            return -1;
        } else {
            return res.get(0);
        }
    }

    /*
      解法：Two pointer
      1.初始化指針i,j
      2.建立while迴圈，若nums1[i]等於nums2[j]，則找到了共同元素
      3.若nums1[i]大於nums2[j]，則移動指針j, 否則i++;
      4.重複2~3行為，直到找到，若沒找到，回傳-1
      Time Complexity:  O(n)
      Space Complexity: O(1)
     */
    public int getCommonTwoPointer(int[] nums1, int[] nums2) {
        int n1 = nums1.length;
        int n2 = nums2.length;
        int i=0, j=0;// 初始化指針i,j
        while (i<n1 && j<n2) {
            if (nums1[i] == nums2[j]) { // 如果nums1[i]等於nums2[j]，則找到了共同元素
                return nums1[i];
            } else if (nums1[i] > nums2[j]) { // 如果nums1[i]大於nums2[j]，則移動指針j
                j++;
            } else {
                i++;
            }
        }
        return -1;// 沒有找到共同元素，返回-1
    }

    public static void main(String[] args) {
        MinimumCommonValue_2540_E solution = new MinimumCommonValue_2540_E();
        int[] nums1 = {1,2,3,6};
        int[] nums2 = {2,3,4,5};
        int[] nums3 = {1,2,3,3};
        int[] nums4 = {1,1,2,2};

        int result = solution.getCommonTwoPointer(nums1, nums2);
        System.out.println("result:" + result);
    }
}
