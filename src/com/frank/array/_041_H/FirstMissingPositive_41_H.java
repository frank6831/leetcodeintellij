package com.frank.array._041_H;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/24
 *     title : 41. First Missing Positive
 * </pre>
 */
public class FirstMissingPositive_41_H {
    //複習日期:
	/**
	 Given an unsorted integer array, find the smallest missing positive integer.

	 Example 1:
	 Input: [1,2,0]
	 Output: 3

	 Example 2:
	 Input: [3,4,-1,1]
	 Output: 2

	 Example 3:
	 Input: [7,8,9,11,12]
	 Output: 1
	 Note:

	 Your algorithm should run in O(n) time and uses constant extra space.
	 */

	/*
	 題意: 給一串數字，找出缺失的最小正數。限制了時間複雜度為 O(n)，空間複雜度為 O(1)。
	 思路: 思路是把1放在數組第一個位置nums[0]，2放在第二個位置nums[1]，即需要把nums[i]放在nums[nums[i] - 1]上，
	 那麼我們遍歷整個數組，如果nums[i] != i + 1, 而nums[i]為整數且不大於n，另外nums[i]不等於nums[nums[i] - 1]的話，
	 我們將兩者位置調換，如果不滿足上述條件直接跳過，最後我們再遍歷一遍數組，如果對應位置上的數不正確則返回正確的數
	 */
	// Time Complexity : O(n), Space Complexity : O(1)
	public int firstMissingPositive(int[] nums) {
		if (nums == null || nums.length == 0)
			return 1;
		//第i位存放i+1的數值
		for (int i = 0; i < nums.length;i++) {
			if (nums[i] > 0){//nums[i]為正數，放在i+1位置
				//如果交換的資料還是大於0且<i+1，則放在合適的位置,且資料不相等，避免死迴圈
				//這個while是關鍵，其他都是沒有難度的

				System.out.println("nums[i] :" + nums[i] + ", nums[nums[i] -1]:" + nums[nums[i] -1] + ", i:" + i);
				while (nums[i] > 0 && nums[i] < i+1 && nums[i] != nums[nums[i] -1]) {
					int temp = nums[nums[i]-1];//交換資料
					nums[nums[i]-1] = nums[i];
					nums[i] = temp;
					System.out.println("Array :" + Arrays.toString(nums));
				}
			}
		}

		System.out.println("After Array :" + Arrays.toString(nums));
		//迴圈尋找不符合要求的資料，返回
		for (int i = 0; i < nums.length;i++) {
			if (nums[i] != i+1){
				return i+1;
			}
		}
		//如果都符合要求，則返回長度+1的值
		return nums.length + 1;
	}

    public static void main(String[] args) {
		FirstMissingPositive_41_H solution = new FirstMissingPositive_41_H();
		int[] sample = {4, 3, 0, 1};
        System.out.println("First missing number :" + solution.firstMissingPositive(sample));
    }
}
