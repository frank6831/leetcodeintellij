package com.frank.array._150_M;

import java.util.Arrays;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/18
 *     title  : 150. Evaluate Reverse Polish Notation
 * </pre>
 */
public class EvaluateReversePolishNotation_150_M {
    /**
     You are given an array of strings tokens that represents an arithmetic
     expression in a Reverse Polish Notation.

     Evaluate the expression. Return an integer that represents the value of the expression.

     Note that:

     The valid operators are '+', '-', '*', and '/'.
     Each operand may be an integer or another expression.
     The division between two integers always truncates toward zero.
     There will not be any division by zero.
     The input represents a valid arithmetic expression in a reverse polish notation.
     The answer and all the intermediate calculations can be represented in a 32-bit integer.


     Example 1:

     Input: tokens = ["2","1","+","3","*"]
     Output: 9
     Explanation: ((2 + 1) * 3) = 9
     Example 2:

     Input: tokens = ["4","13","5","/","+"]
     Output: 6
     Explanation: (4 + (13 / 5)) = 6
     Example 3:

     Input: tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
     Output: 22
     Explanation: ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
     = ((10 * (6 / (12 * -11))) + 17) + 5
     = ((10 * (6 / -132)) + 17) + 5
     = ((10 * 0) + 17) + 5
     = (0 + 17) + 5
     = 17 + 5
     = 22


     Constraints:

     1 <= tokens.length <= 104
     tokens[i] is either an operator: "+", "-", "*", or "/", or an integer in the range [-200, 200].
     */
    /*
     題意：給定一個數組，返回一個新數組，對於每一個位置上的數是其他位置上數的乘積，並且限定了時間複雜度O(n)，並且不能用除法。
          題目輸入是一串字串陣列，陣列是全用字串表示的『數值』（可能包含負數）和『運算子』（加減乘除），
          表示方法為『逆波蘭表示法』（Reverse Polish Notation, RPN）。
          RPN 的意義在於，運算子永遠在運算元的後方，比方說：
          3 + 4 會寫成 3 4 +
          (4 - 3) * 5 會寫成 4 3 - 5 *
          這樣的好處在於不需要引入括號符號。
     */

    /*
      解法：
      使用stack來處理。每次遇到數值時，將其推入堆疊中；若遇到運算符號，則彈出最頂部兩個數值進行運算。
      到最後，stack只剩最後一個數值，即是最終結果。
      Time Complexity: O(n)
      Space Complexity: O(1)
     */
    public static int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        String op = "+-*/";

        for (String s:tokens) {
            if (!op.contains(s)) {
                stack.push(Integer.parseInt(s));
            } else {
                int b = stack.pop();
                int a = stack.pop();
                if (s.equals("+")) {
                    stack.push(a + b);
                } else if (s.equals("-")) {
                    stack.push(a - b);
                } else if (s.equals("*")) {
                    stack.push(a * b);
                } else {
                    stack.push(a / b);
                }
            }
        }
        return stack.pop();
    }


    public int evalRPNSwitchCase(String[] tokens) {
        if (tokens.length == 0) return 0;
        Stack<Integer> s = new Stack<>();
        int a, b;
        for (String t : tokens) {
            switch (t) {
                case "+":
                    a = s.pop();
                    b = s.pop();
                    s.push(b + a);
                    break;
                case "-":
                    a = s.pop();
                    b = s.pop();
                    s.push(b - a);
                    break;
                case "*":
                    a = s.pop();
                    b = s.pop();
                    s.push(b * a);
                    break;
                case "/":
                    a = s.pop();
                    b = s.pop();
                    s.push(b / a);
                    break;
                default:
                    s.push(Integer.parseInt(t));
                    break;
            }
        }
        return s.pop();
    }

    public static void main(String[] args) {
        String[] array = {"2","1","+","3","*"};

        int result = evalRPN(array);
        System.out.println("result:"+ result);
    }
}
