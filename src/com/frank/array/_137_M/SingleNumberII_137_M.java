package com.frank.array._137_M;


/**
 * <pre>
 *     author: frank
 *     time  : 2024/4/9
 *     title  : 137. Single Number II
 * </pre>
 */
public class SingleNumberII_137_M {
    /**
     Given an integer array nums where every element appears three times except for one, which appears exactly once. Find the single element and return it.

     You must implement a solution with a linear runtime complexity and use only constant extra space.



     Example 1:

     Input: nums = [2,2,3,2]
     Output: 3
     Example 2:

     Input: nums = [0,1,0,1,0,1,99]
     Output: 99


     Constraints:

     1 <= nums.length <= 3 * 104
     -231 <= nums[i] <= 231 - 1
     Each element in nums appears exactly three times except for one element which appears once.
     */
    /*
     題意：給一個數字陣列，裡面有些數字會出現三次，有些出現一次，請我們找出只出現一次的數字。
     */

    /*
      解法:

      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public int singleNumber(int[] nums) {
        int ones=0, twos=0,threes=0;

        for (int num:nums) {
            // twos表示出現兩次的位，ones表示出現一次的位
            twos = twos | (ones & num);

            // 將ones更新為出現一次的位，出現兩次的位則在下一次迴圈時清零
            ones ^= num;
            System.out.println("ones:" + ones + ",twos:" + twos + ", num:" + num);
            // threes表示出現三次的位
            threes = ones & twos;
            System.out.println("threes:" + threes);
            // 將出現三次的位清零
            ones &= ~threes;
            twos &= ~threes;
        }
        // 最終ones即為只出現一次的數字
        return ones;
    }


    public static void main(String[] args) {
        SingleNumberII_137_M solution = new SingleNumberII_137_M();
        int[] nums2 = {1,1,2, 1};
        int number = solution.singleNumber(nums2);
        System.out.println("number:" + number);


        int value = 1 & 2;
        int valueOr = 1 | 2;
        //System.out.println("value &:" + value);
        //System.out.println("valueOr &:" + valueOr);
    }
}
