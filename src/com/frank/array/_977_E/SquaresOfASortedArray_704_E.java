package com.frank.array._977_E;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/13
 *     title  : 977. Squares of a Sorted Array
 * </pre>
 */
public class SquaresOfASortedArray_704_E {
    /**
     Given an integer array nums sorted in non-decreasing order,
     return an array of the squares of each number sorted in non-decreasing order.


     Example 1:
     Input: nums = [-4,-1,0,3,10]
     Output: [0,1,9,16,100]
     Explanation: After squaring, the array becomes [16,1,0,9,100].
     After sorting, it becomes [0,1,9,16,100].

     Example 2:
     Input: nums = [-7,-3,2,3,11]
     Output: [4,9,9,49,121]

     Constraints:

     1 <= nums.length <= 104
     -104 <= nums[i] <= 104
     nums is sorted in non-decreasing order.

     Follow up: Squaring each element and sorting the new array is very trivial,
     could you find an O(n) solution using a different approach?
     */
    /*
     題意：一個由小到大排列的整數陣列，寫一個函式回傳每個元素的平方，並且也是由小到大排列
     */


    /*
      解法一：暴力解
      把每個數平方後，再把結果sort

      Time Complexity: O(nlongn)
      Space Complexity: O(n)
     */
    public static int[] sortedSquares(int[] nums) {
        if (nums == null || nums.length == 0) return null;
        int n = nums.length;
        int[] result = new int[n];

        for (int i=0; i<n;i++ ) {
            result[i] = nums[i] * nums[i];
        }
        Arrays.sort(result);
        return result;
    }

    /*
      Follow up: Use O(n) solution
      解法二：使用Two pointer
      拿陣列最右邊的元素去和最左邊的數，兩者以絕對值做比較，誰的平方大誰就擺在最右邊


      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    public static int[] sortedSquaresTwoPointer(int[] nums) {
        if (nums == null || nums.length == 0) return null;

        int n  = nums.length;
        int[] result = new int[n];
        int left = 0;
        int right = n-1;
        int square = 0;

        for (int i = n-1;i>=0; i--) {
            if (Math.abs(nums[left]) < Math.abs(nums[right])) {
                square = nums[right];
                right -= 1;
            } else {
                square = nums[left];
                left += 1;
            }
            result[i] = square*square;
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {-8,-4,3,5,9,12};

        int[] result = sortedSquaresTwoPointer(nums);
        System.out.println("result:" + Arrays.toString(result));
    }
}
