package com.frank.array._134_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/3/4
 *     title  : 134. Gas Station
 * </pre>
 */
public class GasStation_134_M {
    /**
     There are n gas stations along a circular route, where the amount of gas at the ith station is gas[i].

     You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from the ith station to its next (i + 1)th station.
     You begin the journey with an empty tank at one of the gas stations.

     Given two integer arrays gas and cost, return the starting gas station's index
     if you can travel around the circuit once in the clockwise direction, otherwise return -1.
     If there exists a solution, it is guaranteed to be unique



     Example 1:

     Input: gas = [1,2,3,4,5], cost = [3,4,5,1,2]
     Output: 3
     Explanation:
     Start at station 3 (index 3) and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
     Travel to station 4. Your tank = 4 - 1 + 5 = 8
     Travel to station 0. Your tank = 8 - 2 + 1 = 7
     Travel to station 1. Your tank = 7 - 3 + 2 = 6
     Travel to station 2. Your tank = 6 - 4 + 3 = 5
     Travel to station 3. The cost is 5. Your gas is just enough to travel back to station 3.
     Therefore, return 3 as the starting index.

     Example 2:
     Input: gas = [2,3,4], cost = [3,4,3]
     Output: -1
     Explanation:
     You can't start at station 0 or 1, as there is not enough gas to travel to the next station.
     Let's start at station 2 and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
     Travel to station 0. Your tank = 4 - 3 + 2 = 3
     Travel to station 1. Your tank = 3 - 3 + 3 = 3
     You cannot travel back to station 2, as it requires 4 unit of gas but you only have 3.
     Therefore, you can't travel around the circuit once no matter where you start.


     Constraints:

     n == gas.length == cost.length
     1 <= n <= 105
     0 <= gas[i], cost[i] <= 104
     */
    /*
     題意：模擬汽車在加油站之間行駛的情境。每個加油站提供一定數量的汽油(gas)，但需要消耗一定的汽油(cost)才能到達下一個加油站。
          目標是找到一個起點，使得汽車可以繞一圈並返回起點，並且始終有足夠的汽油供應。

          簡單來說，問題要求找到一個起點，使得汽車可以以最大的效率繞一圈回到起點。
     */

    /*解法:
      1.初始化總加油量totalGas和總花費totalCost變量，用來記錄所有加油站的加油量和到下一個加油站的花費
      2.遍歷所有加油站，同時計算當前剩餘的加油量 currentGas，並根據情況更新起點位置 start。
      3.如果當前剩餘加油量小於0，表示無法到達下一個加油站，將起點設置為下一個加油站的位置。
      4.如果總加油量小於總花費，返回-1；否則返回起始加油站的索引位置。

      Time Complexity:  O(n)
      Space Complexity: O(1)
     */
    public int canCompleteCircuit1(int[] gas, int[] cost) {
        int totalGas = 0; // 初始化總加油量
        int totalCost = 0; // 初始化總花費
        int currentGas = 0; // 初始化目前剩餘加油量
        int start = 0; // 初始化起始加油站點

        for (int i=0;i<gas.length;i++) {
            // 總加油量加上當前加油站的加油量
            totalGas += gas[i];
            // 總花費加上當前加油站到下一個加油站的花費
            totalCost += cost[i];
            // 當前剩餘加油量加上當前加油站的加油量減去到下一個加油站的花費
            currentGas += gas[i] - cost[i];
            // 如果當前剩餘加油量小於0，表示無法到達下一個加油站，將起點設置為下一個加油站
            if (currentGas <0) {
                currentGas = 0;// 重新計算剩餘加油量
                start = i+1;// 起點設置為下一個加油站
            }
        }

        if (totalGas < totalCost) {
            return -1;
        } else {
            return start;
        }
    }


    /*
      解法:
      1.初始化變量 total、tank 和 start 分別表示總剩餘汽油量、目前的汽油量和起始加油站位置。
      2.遍歷加油站，計算到達每個加油站的汽油盈餘，同時更新總剩餘汽油量和目前的汽油量。
      3.若目前的汽油量為負值，表示無法到達下個加油站，將起點設置為下一個加油站，並重置目前的汽油量。
      4.最後根據總剩餘汽油量判斷是否能繞一圈，若總剩餘汽油量小於0，則返回-1；否則返回起始加油站位置。

      Time Complexity:  O(logN)
      Space Complexity: O(n)
     */
    public int canCompleteCircuit2(int[] gas, int[] cost) {
        int n = gas.length;
        int total = 0; // 總剩餘汽油量
        int tank = 0; // 目前的汽油量
        int start = 0; // 起始加油站位置

        for (int i = 0; i < n; i++) {
            int diff = gas[i] - cost[i]; // 到達下一個加油站的汽油盈餘
            total += diff; // 更新總剩餘汽油量
            tank += diff; // 更新目前的汽油量

            // 如果目前的汽油量為負值，表示無法到達下一個加油站
            if (tank < 0) {
                start = i + 1; // 將起點設置為下一個加油站
                tank = 0; // 重置目前的汽油量
            }
        }

        // 如果總剩餘汽油量小於0，表示無法繞一圈
        if (total < 0) {
            return -1;
        } else {
            return start; // 返回起始加油站位置
        }
    }


    /*

    1.首先計算總加油量 totalGas 和總花費 totalCost。
    2.如果總加油量小於總花費，直接返回-1，因為無法完成一圈行程。
    3.初始化當前剩餘的加油量 currGas 和起點位置 start。
    4.更新當前剩餘的加油量，同時根據情況更新起點位置。
    5.最後返回起始加油站的索引位置。
     */
    public int canCompleteCircuit3(int[] gas, int[] cost) {
        // 初始化總加油量和總花費
        int totalGas = 0, totalCost = 0;
        // 遍歷所有加油站，計算總加油量和總花費
        for (int i = 0 ; i < gas.length ; ++i) {
            totalGas += gas[i];
            totalCost += cost[i];
        }

        // 如果總加油量小於總花費，直接返回-1，無法完成一圈行程
        if (totalGas < totalCost) return -1;

        // 初始化當前剩餘的加油量和起點位置
        int currGas = 0, start = 0;
        // 遍歷所有加油站
        for (int i = 0 ; i < gas.length ; ++i) {
            // 更新當前剩餘的加油量
            currGas += gas[i] - cost[i];
            // 如果當前剩餘加油量小於0，表示無法到達下一個加油站，更新起點位置
            if (currGas < 0) {
                currGas = 0;
                start = i + 1;
            }
        }

        // 返回起始加油站的索引位置
        return start;
    }


    public static void main(String[] args) {
        GasStation_134_M solution = new GasStation_134_M();
        int[] nums1 = {1,2,3,4,5};
        int[] nums2 = {3,4,5,1,2};
        int gasResult = solution.canCompleteCircuit3(nums1, nums2);
        System.out.println("gasResult:" + gasResult);
    }
}
