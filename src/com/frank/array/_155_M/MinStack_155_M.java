package com.frank.array._155_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/19
 *     title  : 155. Min Stack
 * </pre>
 */
public class MinStack_155_M {
    /**
     Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

     Implement the MinStack class:

     MinStack() initializes the stack object.
     void push(int val) pushes the element val onto the stack.
     void pop() removes the element on the top of the stack.
     int top() gets the top element of the stack.
     int getMin() retrieves the minimum element in the stack.
     You must implement a solution with O(1) time complexity for each function.



     Example 1:

     Input
     ["MinStack","push","push","push","getMin","pop","top","getMin"]
     [[],[-2],[0],[-3],[],[],[],[]]

     Output
     [null,null,null,null,-3,null,0,-2]

     Explanation
     MinStack minStack = new MinStack();
     minStack.push(-2);
     minStack.push(0);
     minStack.push(-3);
     minStack.getMin(); // return -3
     minStack.pop();
     minStack.top();    // return 0
     minStack.getMin(); // return -2


     Constraints:

     -231 <= val <= 231 - 1
     Methods pop, top and getMin operations will always be called on non-empty stacks.
     At most 3 * 104 calls will be made to push, pop, top, and getMin.
     */
    /*
     題意：設計具有 push, pop, top 操作，並能在常數時間 O(n) 內查詢到最小元素的堆疊
     */

    /*
      解法：
      1.用min_val來記錄當前最小值，初始化為整型最大值，push時比較若小於等於min_val，則將min_val push，並將min_val更新為當前數字。
      2.pop時，先將stack top元素移出，再判斷該元素是否和min_val相等，若相等將min_val更新為新的top元素，再將新top元素移出stack即可

      Time Complexity: O(n)
      Space Complexity: O(1)
     */
     static class MinStack {
        private Stack<Integer> stack;
        int min = Integer.MAX_VALUE;
        public MinStack() {
            stack = new Stack<>();
        }

        public void push(int x) {
            if (x <= min) {
                //把當前最小值也放進去
                stack.push(min);
                min = x;
            }
            stack.push(x);
        }

        public void pop() {
            //如果弹出的值是最小值，那么将下一个元素更新为最小值
            if (stack.pop() == min) min = stack.pop();
        }

        public int top() {
            return stack.peek();
        }

        public int getMin() {
            return min;
        }

    }



    static class RandomizedSet {
        private Map<Integer, Integer> valToIndex;
        private List<Integer> values;
        private Random rand;

        /** Initialize your data structure here. */
        public RandomizedSet() {
            valToIndex = new HashMap<>();
            values = new ArrayList<>();
            rand = new Random();
        }

        /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
        public boolean insert(int val) {
            if (valToIndex.containsKey(val)) {
                return false;
            }
            values.add(val);
            valToIndex.put(val, values.size() - 1);
            return true;
        }

        /** Removes a value from the set. Returns true if the set contained the specified element. */
        public boolean remove(int val) {
            if (!valToIndex.containsKey(val)) {
                return false;
            }
            int index = valToIndex.get(val);
            int lastVal = values.get(values.size() - 1);
            values.set(index, lastVal);
            valToIndex.put(lastVal, index);
            values.remove(values.size() - 1);
            valToIndex.remove(val);
            return true;
        }

        /** Get a random element from the set. */
        public int getRandom() {
            int index = rand.nextInt(values.size());
            return values.get(index);
        }
    }

    public static void main(String[] args) {
        MinStack obj = new MinStack();
        obj.push(-2);
        obj.push(0);
        obj.push(-3);
        int min = obj.getMin();
        //System.out.println("min:"+ min);
        obj.pop();
        int top = obj.top();
        //System.out.println("top:"+ top);
        int min2 = obj.getMin();
        //System.out.println("min2:"+ min2);

        RandomizedSet randomizedSet = new RandomizedSet();
        boolean result =randomizedSet.insert(1);
        int value = 0;
        System.out.println("result:"+ result);
        result = randomizedSet.remove(2); // Returns false as 2 does not exist in the set.
        result = randomizedSet.insert(2); // Inserts 2 to the set, returns true. Set now contains [1,2].
        value = randomizedSet.getRandom(); // getRandom() should return either 1 or 2 randomly.
        System.out.println("value 1:"+ value);
        result = randomizedSet.remove(1); // Removes 1 from the set, returns true. Set now contains [2].
        result = randomizedSet.insert(2); // 2 was already in the set, so return false.
        value = randomizedSet.getRandom();
        System.out.println("value 2:"+ value);

    }
}
