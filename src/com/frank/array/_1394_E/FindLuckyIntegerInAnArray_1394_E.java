package com.frank.array._1394_E;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/11/3
 *     title  : 1394. Find Lucky Integer in an Array
 * </pre>
 */
public class FindLuckyIntegerInAnArray_1394_E {
    /**
     Given an array of integers arr, a lucky integer is an integer that has a frequency in the array equal to its value.
     Return the largest lucky integer in the array. If there is no lucky integer return -1.



     Example 1:

     Input: arr = [2,2,3,4]
     Output: 2
     Explanation: The only lucky number in the array is 2 because frequency[2] == 2.

     Example 2:
     Input: arr = [1,2,2,3,3,3]
     Output: 3
     Explanation: 1, 2 and 3 are all lucky numbers, return the largest of them.

     Example 3:
     Input: arr = [2,2,2,3,3]
     Output: -1
     Explanation: There are no lucky numbers in the array.


     Constraints:

     1 <= arr.length <= 500
     1 <= arr[i] <= 500
     */
    /*
     題意：給一組數組，找出頻率等於數值的數，返回最大的那個，若無則返回-1。
     */

    /*
      解法:HashMap
      1.用HashMap紀錄每個數字出現次數
      2.跑迴圈找出頻率等於數值的最大的數

      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public int findLucky(int[] arr) {
        // key:array value,  value: count
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int num:arr) {
            if (map.containsKey(num)) {
                int value = map.get(num);
                map.put(num, ++value);
            } else {
                map.put(num, 1);
            }
        }
        int luckyValue = -1;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();

            if (key == value && luckyValue <= key) {
                luckyValue = key;
            }
        }
        return luckyValue;
    }



    public static void main(String[] args) {
        FindLuckyIntegerInAnArray_1394_E solution = new FindLuckyIntegerInAnArray_1394_E();
        int[] nums1 = {1,2,2,3,3,3};
        int[] nums2 = {2,2,3,4};
        int luckyValue = solution.findLucky(nums2);
        System.out.println("luckyValue:" + luckyValue);
    }
}
