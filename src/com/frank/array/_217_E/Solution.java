package com.frank.array._217_E;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/16
 *     title  : 217. Contains Duplicate
 * </pre>
 */
public class Solution {
    
	/**
	Given an array of integers, find if the array contains any duplicates.

	Your function should return true if any value appears at least twice in the array, 
	and it should return false if every element is distinct.

	Example 1:
	Input: [1,2,3,1]
	Output: true
	
	Example 2:
	Input: [1,2,3,4]
	Output: false
	
	Example 3:
	Input: [1,1,1,3,3,4,3,2,4,2]
	Output: true
	*/
	
	/*
	 * 思路：用一個集合記錄之前遇到過的數字，如果新的數字已經在集合中出現過了，則說明有重複。
	 */
	public boolean containsDuplicate(int[] nums) {
		Set<Integer> set = new HashSet<>();
		for (int num : nums) {
			if (set.contains(num)) return true;
			set.add(num);
		}
		return false;
	}
	

    public static void main(String[] args) {
		Solution s = new Solution();
        int sample[] = {1,2,3,4,7,5,6};
		boolean isDuplicated = s.containsDuplicate(sample);
        //solution.rotate(sample, k);
        System.out.println("isDuplicated:" + isDuplicated);
    }
}
