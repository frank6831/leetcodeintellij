package com.frank.array._033_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/24
 *     title : 33. Search in Rotated Sorted Array
 * </pre>
 */
public class SearchInRotatedSortedArray_33_E {
	/**
	 Suppose an array sorted in ascrighting order is rotated at some pivot unknown to you beforehand.

	 (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

	 You are given a target value to search. If found in the array return its index,
	 otherwise return -1.

	 You may assume no duplicate exists in the array.

	 Your algorithm's runtime complexity must be in the order of O(log n).

	 Example 1:
	 Input: nums = [4,5,6,7,0,1,2], target = 0
	 Output: 4

	 Example 2:
	 Input: nums = [4,5,6,7,0,1,2], target = 3
	 Output: -1
	 */
	
	/*
	 題意: 用二分搜尋法，但難在字串有rotate過
	 具體來說，假設數組是nums，每次左邊緣為left，右邊緣為right，還有中間位置是mid。在每次迭代中，分三種情況：
	（1）如果target==nums[mid]，那麼mid就是我們要的結果，直接返回；
	（2）如果nums[left]<=nums[mid]，那麼說明從low到mid一定是有序的，那麼我們只需要判斷target是不是在left到mid之間，
		如果是則把右邊緣移到mid-1，否則target就在另一半，即把左邊緣移到mid+1。
	（3）如果nums[left]>nums[mid]，那麼說明從mid到right一定是有序的，那麼我們只需要判斷target是不是在mid到right之間，
		如果是則把左邊緣移到mid +1，否則就target在另一半，即把右邊緣移到mid-1。
	 */
	// Time Complexity : O(logn), Space Complexity : O(1)
	private int search(int[] nums, int target) {
		if (nums == null || nums.length == 0) {
			return -1;
		}

		int left = 0;
		int right = nums.length -1;
		int mid;
		while (left <= right) {
			mid = (left + right) /2;
			if (nums[mid]== target) return mid;

			if (nums[mid] < nums[right]) {
				// 右邊已排序
				if (nums[mid] < target && nums[right] >= target) {
					//表示target在此一半
					left = mid + 1;
				} else {
					//否則target在另一半
					right = mid - 1;
				}
			} else {
				// 左邊已排序
				if (nums[mid] > target && nums[left] <= target) {
					//表示target在此一半
					right = mid - 1;
				} else {
					left = mid + 1;
				}
			}
		}
		return -1;
	}

    public static void main(String[] args) {
		SearchInRotatedSortedArray_33_E solution = new SearchInRotatedSortedArray_33_E();
		int[] sample = {5, 6, 7, 0, 1, 2, 4};
        System.out.println("Max area :" + solution.search(sample, 4));
    }
}
