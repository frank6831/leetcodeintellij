package com.frank.array._033_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/13
 *     title : 33. Search in Rotated Sorted Array
 * </pre>
 */
public class Sulotion_2 {
    /**
     Suppose an array sorted in ascrighting order is rotated at some pivot unknown to you beforehand.

     (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

     You are given a target value to search. If found in the array return its index,
     otherwise return -1.

     You may assume no duplicate exists in the array.

     Your algorithm's runtime complexity must be in the order of O(log n).

     Example 1:
     Input: nums = [4,5,6,7,0,1,2], target = 0
     Output: 4

     Example 2:
     Input: nums = [4,5,6,7,0,1,2], target = 3
     Output: -1
     */

    //思路：用Binary search才能達到O(log n)，翻轉過的陣列

    private int search(int[] nums, int target) {
        //
        if (nums == null || nums.length==0) {
            return -1;
        }

        int left = 0;
        int right = nums.length - 1;
        int mid;
        while (left <= right) {
            mid = (left + right) / 2;
            System.out.println("mid:" + mid);
            if (nums[mid] == target) {
                return mid;
            }

            if (nums[mid] < nums[right]) {
                //右邊已排序
                if (nums[mid] < target && nums[right] >= target) {
                    //表示target在右半邊
                    left = mid + 1;
                } else {
                    //表示target在左半邊
                    right = mid - 1;
                }
            } else {
                // 左邊已排序
                if (nums[mid] > target && nums[left] <= target) {
                    //表示target在左半邊
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        Sulotion_2 solution = new Sulotion_2();
        int[] sample = {5, 6, 7, 0, 1, 2, 4};
        System.out.println("Max position :" + solution.search(sample, 4));
    }
}
