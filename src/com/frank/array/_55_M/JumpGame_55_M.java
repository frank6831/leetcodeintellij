package com.frank.array._55_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/2/15
 *     title  : 55. Jump Game
 * </pre>
 */
public class JumpGame_55_M {
    /**
     You are given an integer array nums. You are initially positioned at the array's first index,
     and each element in the array represents your maximum jump length at that position.

     Return true if you can reach the last index, or false otherwise.



     Example 1:
     Input: nums = [2,3,1,1,4]
     Output: true
     Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.

     Example 2:
     Input: nums = [3,2,1,0,4]
     Output: false
     Explanation: You will always arrive at index 3 no matter what. Its maximum jump length is 0,
     which makes it impossible to reach the last index.


     Constraints:

     1 <= nums.length <= 104
     0 <= nums[i] <= 105
     */
    /*
     題意：給一非負整數陣列，你的起始位置從陣列的開頭開始，每個元素代表在陣列中你在這格可以跳躍的最大距離，判斷能不能跳到最後一格
     */

    /*
      解法：Greedy
      1.在遍歷的過程，若最遠可以到達的位置大於等於數組中的最後一個位置，說明最後一個位置可達，可以直接返回True作為答案。
      2.反之，若在遍歷結束後，最後一個位置仍然不可達，我們就回傳 False 作為答案。
      3.用一個變數記錄目前能夠到達的最大的索引，並逐個遍歷數組中的元素去更新這個索引，遍歷完成判斷這個索引是否大於數組長度-1即可。

      題目關鍵點在於：不用拘泥於每次究竟跳幾步，而是看覆蓋範圍，覆蓋範圍內一定是可以跳過來的，不用管是怎麼跳的

      Time Complexity: O(n)
      Space Complexity: O(1)
     */

    public boolean canJump(int[] nums) {
        int max = 0; //能覆蓋的最遠距離
        for (int i=0;i<nums.length;i++) {
            if (i > max) return false;  //如果目前位置無法到達，則傳回false
            int curr = i + nums[i];
            System.out.println("max:" + max + ", curr:" + curr);
            max = Math.max(max, curr);

        }
        System.out.println("Final max:" + max);
        return max >= nums.length -1;
    }


    public static void main(String[] args) {
        JumpGame_55_M solution = new JumpGame_55_M();
        int[] array = {2,3,1,1,4};
        int[] array2 = {3, 2, 1, 0, 4};
        boolean result = solution.canJump(array);
        System.out.println("result:" + result);
    }
}
