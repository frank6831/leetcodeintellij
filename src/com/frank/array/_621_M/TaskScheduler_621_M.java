package com.frank.array._621_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/3/3
 *     title  : 621. Task Scheduler
 * </pre>
 */
public class TaskScheduler_621_M {
    /**
     You are given an array of CPU tasks, each represented by letters A to Z, and a cooling time, n.
     Each cycle or interval allows the completion of one task. Tasks can be completed in any order,
     but there's a constraint: identical tasks must be separated by at least n intervals due to cooling time.

     ​Return the minimum number of intervals required to complete all tasks.



     Example 1:
     Input: tasks = ["A","A","A","B","B","B"], n = 2
     Output: 8

     Explanation: A possible sequence is: A -> B -> idle -> A -> B -> idle -> A -> B.

     After completing task A, you must wait two cycles before doing A again. The same applies to task B.
     In the 3rd interval, neither A nor B can be done, so you idle. By the 4th cycle, you can do A again as 2 intervals have passed.

     Example 2:
     Input: tasks = ["A","C","A","B","D","B"], n = 1
     Output: 6

     Explanation: A possible sequence is: A -> B -> C -> D -> A -> B.
     With a cooling interval of 1, you can repeat a task after just one other task.

     Example 3:
     Input: tasks = ["A","A","A", "B","B","B"], n = 3
     Output: 10

     Explanation: A possible sequence is: A -> B -> idle -> idle -> A -> B -> idle -> idle -> A -> B.

     There are only two types of tasks, A and B, which need to be separated by 3 intervals. This leads to idling twice between repetitions of these tasks.


     Constraints:

     1 <= tasks.length <= 104
     tasks[i] is an uppercase English letter.
     0 <= n <= 100
     */
    /*
     題意：輸入字串陣列tasks，代表一個cpu需要做的工作，其中每個字母代表一個不同的工作。每單位時間，cpu可以選擇完成任一項工作或是閒置。
          還有個整數n，代表兩個相同種類的工作需要間隔的時間。求cpu完成所有任務的最短耗時。
     */

    /*
      解法：
      1. 紀錄每種任務出現的次數
      2. 找出出現次數最多的任務數量以及出現次數最多的任務次數。
      3. 根據出現次數最多的任務數量和冷卻時間計算總的時間。
      4. 返回總時間，如果總時間小於任務數量，則返回任務數量。
      Time Complexity: O(n)
      Space Complexity: O(1)
     */

    public int leastInterval(char[] tasks, int n) {
        int[] count = new int[26];
        // 紀錄每種任務出現的次數
        for (char c:tasks) {
            count[c-'A']++;
        }

        // 獲取出現次數最多的任務次數
        int maxCount = 0;
        // 統計出現次數最多的任務的數量
        int maxCountSame = 0;

        for (int num:count) {
            if (num > maxCount) {
                maxCount = num;
                maxCountSame = 1;
            } else if (num == maxCount) {
                maxCountSame++;
            }
        }
        // 根據公式計算總時間
        int res = (maxCount -1)*(n+1) + maxCountSame;
        // 返回結果，如果總時間比任務數量小，則返回任務數量
        return Math.max(res, tasks.length);
    }


    /*
        公式解釋如下
        當安排任務時，我們需要考慮兩種情況：
        1.當間隔時間n較大時可以安排其他任務來填滿這些間隔。這樣可以最大化地利用CPU的空閒時間。
          例如，如果有一個任務需要間隔 n 個單位時間才能再次執行，我們可以在這n個單位時間內安排其他任務。
        2.當間隔時間n較小時，任務的間隔時間被限制，無法安排其他任務填充。在這種情況下，我們需要確保 CPU 不會閒置，盡可能地按照要求安排任務執行。
        根據上述思路，我們可以得到一個公式來計算總的安排時間：
        總時間 = (maxCount - 1) * (n + 1) + maxTasksCount

        其中，
        maxCount 是出現次數最多的任務的出現次數。
        maxTasksCount 是出現次數最多的任務的數量。
        這個公式的意思是，首先，我們安排出現次數最多的任務，然後在這些任務的間隔中插入其他任務，確保CPU不會閒置。
        因此，我們將(maxCount - 1)乘以(n + 1)，表示將出現次數最多的任務之間的間隔填滿。
        然後，我們再加上出現次數最多的任務的數量maxTasksCount，確保所有任務都得到了安排。

        最後，如果總時間小於任務的數量，表示任務的間隔時間較小，CPU 不會閒置，因此總時間就是任務的數量。
     */


    public static void main(String[] args) {
        TaskScheduler_621_M solution = new TaskScheduler_621_M();
        char[] array = {'A','A','A', 'B','B','B'};
        int result = solution.leastInterval(array,3);
        System.out.println("result:" + result);
    }
}
