package com.frank.array._2248_E;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/7
 *     title  : 2248. Intersection of Multiple Arrays
 * </pre>
 */
public class IntersectionofMultipleArrays_2248_E {
    /**
     Given a 2D integer array nums where nums[i] is a non-empty array of distinct positive integers,
     return the list of integers that are present in each array of nums sorted in ascending order.


     Example 1:

     Input: nums = [[3,1,2,4,5],[1,2,3,4],[3,4,5,6]]
     Output: [3,4]
     Explanation:
     The only integers present in each of nums[0] = [3,1,2,4,5], nums[1] = [1,2,3,4],
     and nums[2] = [3,4,5,6] are 3 and 4, so we return [3,4].
     Example 2:

     Input: nums = [[1,2,3],[4,5,6]]
     Output: []
     Explanation:
     There does not exist any integer present both in nums[0] and nums[1], so we return an empty list [].


     Constraints:

     1 <= nums.length <= 1000
     1 <= sum(nums[i].length) <= 1000
     1 <= nums[i][j] <= 1000
     All the values of nums[i] are unique.
     */
    /*
     題意：給定一個二維整數陣列nums，其中 nums[i]為一個非空的相異正整數陣列，回傳一列表其由nums中每個陣列都有出現的數字所組成，並且按照著升序排列。
     */


    /*
      解法：
      1.統計每種元素在所有陣列中的總出現次數。
      2.從1開始掃到1000，看有沒有元素總出現次數恰好為nums.length次
      3.若有則代表該元素出現於每一個陣列中，需將其加入答案之中。
      4.最後掃完之後便可以得到所求列表，且按升序排列(因為我們由小元素看到大元素)。


      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public List<Integer> intersection(int[][] nums) {
        int[] cnt = new int[1001];
        int n = nums.length;

        for (int[] num1:nums) {
            for (int num:num1) {
                cnt[num]++;
            }
        }
        //System.out.println("array:" + Arrays.toString(cnt));
        List<Integer> res = new ArrayList<>();
        for (int i=0;i<=1000;i++) {
            if (cnt[i] == n) res.add(i);
        }
        return res;
    }

    public static void main(String[] args) {
        IntersectionofMultipleArrays_2248_E solution = new IntersectionofMultipleArrays_2248_E();
        int[][] nums1 = {{3,1,2,4,5}, {1,2,3,4}, {3,4,5,6}};

        List<Integer> result = solution.intersection(nums1);
        System.out.println("result:" + result);
    }
}
