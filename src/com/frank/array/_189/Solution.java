package com.frank.array._189;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/3/7
 *     title  : 189. Rotate Array
 * </pre>
 */
public class Solution {
    
	/**
	Given an array, rotate the array to the right by k steps, where k is non-negative.

	Example 1:

	Input: [1,2,3,4,5,6,7] and k = 3
	Output: [5,6,7,1,2,3,4]
	Explanation:
	rotate 1 steps to the right: [7,1,2,3,4,5,6]
	rotate 2 steps to the right: [6,7,1,2,3,4,5]
	rotate 3 steps to the right: [5,6,7,1,2,3,4]
	Example 2:

	Input: [-1,-100,3,99] and k = 2
	Output: [3,99,-1,-100]
	Explanation: 
	rotate 1 steps to the right: [99,-1,-100,3]
	rotate 2 steps to the right: [3,99,-1,-100]
	Note:

	Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
	Could you do it in-place with O(1) extra space?
	*/
	// Solution: http://www.cnblogs.com/grandyang/p/4298711.html
	
	
	//思路是先把前n-k個數字翻轉一下，再把後k個數字翻轉一下，最後再把整個數組翻轉一下
	public static void rotate(int[] nums, int k) {
	    k %= nums.length;
	    System.out.println("k:" + k);
	    reverse(nums, 0, nums.length - 1);
	    reverse(nums, 0, k - 1);
	    reverse(nums, k, nums.length - 1);
	}

	public static void reverse(int[] nums, int start, int end) {
	    while (start < end) {
	        int temp = nums[start];
	        nums[start] = nums[end];
	        nums[end] = temp;
	        start++;
	        end--;
	    }
	}
	
	/*
	 * 思路：效率比較慢
	 * 原理就是使用氣泡排序法，但不比較大小，而把最後的數字，逐漸往前移動 
	 */
	public static void rotateBubble(int[] nums, int k) {
		if (nums == null || k < 0) {
		    throw new IllegalArgumentException("Illegal argument!");
		}
	 
		for (int i = 0; i < k; i++) {
			for (int j = nums.length - 1; j > 0; j--) {
				int temp = nums[j];
				nums[j] = nums[j - 1];
				nums[j - 1] = temp;
			}
		}
	}
	

    public static void main(String[] args) {
        int sample[] = {1,2,3,4,7,5,6};
        //solution.rotate(sample, k);
		rotate(sample, 2);
        System.out.println(Arrays.toString(sample));
    }
}
