package com.frank.array._414_E;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/15
 *     title : 414. Third Maximum Number
 * </pre>
 */
public class ThirdMaximumNumber_414_E {
    /***
     Given integer array nums, return the third maximum number in this array.
     If the third maximum does not exist, return the maximum number.

     Example 1:
     Input: nums = [3,2,1]
     Output: 1
     Explanation: The third maximum is 1.

     Example 2:
     Input: nums = [1,2]
     Output: 2
     Explanation: The third maximum does not exist, so the maximum (2)
     is returned instead.

     Example 3:
     Input: nums = [2,2,3,1]
     Output: 1
     Explanation: Note that the third maximum here means the third maximum distinct number.
     Both numbers with value 2 are both considered as second maximum.


     Constraints:
     1 <= nums.length <= 104
     -231 <= nums[i] <= 231 - 1

     Follow up: Can you find an O(n) solution?
     */

    //題意：找出陣列裡第三大的數字，若不存在，則回傳最大的數字

    /*
    思路：用三個變量first, second, third來分別保存第一大，第二大，和第三大的數，
     然後我們遍歷數組，如果遍歷到的數字大於當前第一大的數first，那麼三個變量各自錯位賦值，
     如果當前數字大於second，小於first，那麼就更新second和third，
     如果當前數字大於third，小於second，那就只更新third，注意這裡有個坑，
     就是初始化要用長整型long的最小值，否則當數組中有INT_MIN存在時
     程序就不知道該返回INT_MIN還是最大值first了
     此解法時間複雜度是O(n)，空間複雜度是O(1)。
     Time Complexity: O(n)
     Space Complexity: O(1)
     */

    public int thirdMax(int[] nums) {
        long first = Long.MIN_VALUE;
        long second = Long.MIN_VALUE;
        long third = Long.MIN_VALUE;

        for (int num : nums) {
            if (num > first) {
                third = second;
                second = first;
                first = num;
            } else if (num > second && num < first) {
                third = second;
                second = num;
            } else if (num > third && num < second) {
                third = num;
            }
        }
        return (third == Long.MIN_VALUE) ? (int)first : (int)third;
    }

    /*
      解法2:記憶體用最少,但速度非最快
      Time Complexity: O(nlogn)
      Space Complexity: O(n)
     */

    public int thirdMax2(int[] nums) {
        LinkedList<Integer> hs= new LinkedList<>();
        Arrays.sort(nums);
        //先去重
        for (int num : nums) {
            if (!hs.contains(num))
                hs.add(num);
        }
        if (hs.size() < 3) {
            return hs.get(hs.size()-1);
        }
        System.out.println("hs:" + hs);
        return hs.get(hs.size()-3);
    }



    static String decrypt(String encryptedMessage) {
        StringBuilder decryptedMessage = new StringBuilder();
        String[] words = encryptedMessage.split(" ");

        for (int i = words.length - 1; i >= 0; i--) {
            String word = words[i];
            StringBuilder decryptedWord = new StringBuilder();
            for (int j = 0; j < word.length(); j++) {
                char c = word.charAt(j);
                if (j == word.length() - 1 || c != word.charAt(j + 1)) {
                    decryptedWord.append(c);
                } else {
                    int count = 1;
                    while (j < word.length() - 1 && c == word.charAt(j + 1)) {
                        count++;
                        j++;
                    }
                    decryptedWord.append(c);
                    decryptedWord.append(count);
                }
            }
            decryptedMessage.append(decryptedWord.reverse());
            decryptedMessage.append(" ");
        }
        decryptedMessage.deleteCharAt(decryptedMessage.length() - 1); // remove last space
        return decryptedMessage.toString();
    }


    public static void typeCounter(String sentence) {
        // 將句子分割成單詞
        String[] words = sentence.split(" ");
        // 計算每種類型的出現次數
        int stringCount = 0, integerCount = 0, doubleCount = 0;
        for (String word : words) {
            try {
                // 嘗試將單詞轉換為整數
                int intValue = Integer.parseInt(word);
                integerCount++;
            } catch (NumberFormatException e) {
                try {
                    // 嘗試將單詞轉換為浮點數
                    double doubleValue = Double.parseDouble(word);
                    doubleCount++;
                } catch (NumberFormatException e2) {
                    // 如果無法轉換為數字，則視為字符串
                    stringCount++;
                }
            }
        }
        // 輸出結果
        System.out.println("string " + stringCount);
        System.out.println("integer " + integerCount);
        System.out.println("double " + doubleCount);
    }


    public static int countingValleys(int steps, String path) {
        int currentLevel = 0;
        int valleys = 0;

        for (char step : path.toCharArray()) {
            if (step == 'U') {
                currentLevel++;
            } else if (step == 'D') {
                currentLevel--;
            }

            // 如果當前海拔為 -1，並且上一步在海平面上，代表進入一個山谷
            if (currentLevel == -1 && step == 'D') {
                valleys++;
            }
        }

        return valleys;
    }


    public static void main(String[] args) {
        ThirdMaximumNumber_414_E solution = new ThirdMaximumNumber_414_E();
        int[] num1 = {1,2,Integer.MIN_VALUE, 4,5,7,8};

        int result = solution.thirdMax2(num1);
        //System.out.println("result:" + result);

        String decrypt = decrypt("seaside the to sent be to ne2ds army ten of team a");
        //System.out.println("decrypt:" + decrypt);

        int count = countingValleys(8, "UDDDUDUU");
        System.out.println("count:" + count);
    }
}
