package com.frank.array._121_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/9/2
 *     title : 121. Best Time to Buy and Sell Stock
 * </pre>
 */
public class Solution {
    
	/**
     Say you have an array for which the ith element is the price of a given stock on day i.

     If you were only permitted to complete at most one transaction
     (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.

     Note that you cannot sell a stock before you buy one.

     Example 1:

     Input: [7,1,5,3,6,4]
     Output: 5
     Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
     Not 7-1 = 6, as selling price needs to be larger than buying price.

     Example 2:
     Input: [7,6,4,3,1]
     Output: 0
     Explanation: In this case, no transaction is done, i.e. max profit = 0.
	*/
	// 題目:數組裡的數按照順序是每天的股票價格，你可以選擇在某一天購買，然後再之後的某一天銷售掉，
    // 銷售的日子當然必須在購買的日子之後，也就是數組的順序要靠後一些。要計算在哪兩天進行買賣收益最大。
    // 只有一次交易


    private int maxProfit(int[] prices) {
        int result = 0;
        int small = 0;
        if (prices.length == 0) return 0;
        else small = prices[0];
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] - small > result) result = prices[i] - small;
            if (prices[i] < small) small = prices[i];
        }
        return result;
    }

    /*
	  思路：只需要遍歷一次數組，用一個變量記錄遍尋過數中的最小值，然後每次計算當前值和這個最小值之間的差值最為利潤，
	  然後每次選較大的利潤來更新。當遍歷完成後當前利潤即為所求
	 */
    private int maxProfit2(int[] prices) {
        int result = 0, buy = Integer.MAX_VALUE;
        for (int price : prices) {
            buy = Math.min(buy, price);
            System.out.println("result:" + result + ", buy:"+ buy);
            result = Math.max(result, price - buy);
            System.out.println("After result:" + result + ", buy:"+ buy);
        }
        return result;
    }

    public static void main(String[] args) {
    	Solution solution = new Solution();
        int[] sample = {7,1,5,3,6,4};
        
        int result = solution.maxProfit2(sample);
        //System.out.println("result:" + result);
    }
}
