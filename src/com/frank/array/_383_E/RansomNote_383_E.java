package com.frank.array._383_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/1
 *     title  : 383. Ransom Note
 * </pre>
 */
public class RansomNote_383_E {
    
	/**
	 Given two strings ransomNote and magazine, return true
	 if ransomNote can be constructed by using the letters from magazine and false otherwise.

	 Each letter in magazine can only be used once in ransomNote.

	 Example 1:
	 Input: ransomNote = "a", magazine = "b"
	 Output: false

	 Example 2:
	 Input: ransomNote = "aa", magazine = "ab"
	 Output: false

	 Example 3:
	 Input: ransomNote = "aa", magazine = "aab"
	 Output: true


	 Constraints:

	 1 <= ransomNote.length, magazine.length <= 105
	 ransomNote and magazine consist of lowercase English letters.
	*/

	// 題意：給定一個隨意的勒索信字串，以及另一個包含所有雜誌的字母之字串。撰寫一個一個函式，
	// 當勒索信的內容可以用雜誌的字母組成時回傳真（True），反之回傳假（False）。


	/*
	 * 解法一：
	 *   就像在找兩個陣列的重複元素一樣。
	 *   將 magazine 中的字元丟進雜湊表並記錄出現次數，然後掃過 ransomNote 字串，
	 *   如果有字母在雜湊表裡出現而且目前出現次數 ≧ 1 ，則將出現次數減去 1；反之，
	 *   則可知 magazine 的字元無法組成 ransomNote ，即回傳假。如果沒有回傳過假，最後就回傳真。
	 * 	 Time: O(M+N); Space: O(1)
	 */
	public static boolean canConstruct(String ransomNote, String magazine) {
		int[] freq = new int[26];
		for (int i=0; i < magazine.length();i++) {
			freq[magazine.charAt(i) - 'a']++;
		}
		for (int i =0; i< ransomNote.length();i++) {
			if ((--freq[ransomNote.charAt(i) - 'a']) < 0) {
				return false;
			}
		}
		return true;
	}

	/*


	在最壞情況下，如果每個字母都在雜誌中的不同位置，則時間複雜度可能會比第一種方法高。
	 Time: O(M+N); Space: O(1)
	 */
	public static boolean canConstruct2(String ransomNote, String magazine) {
		// 建立一個長度為26的陣列，用於存儲每個字母出現的次數
		int[] alphabet = new int[26];
		for (char c:ransomNote.toCharArray()) {
			// 取得字母 c 在雜誌中上一次出現的位置
			int value = alphabet[c-'a'];
			int i = magazine.indexOf(c, value);
			// 如果找不到字母 c 或者字母 c 在雜誌中的位置小於上一次出現的位置，返回 false
			if (i==-1) {
				return false;
			}
			// 更新字母 c 在 alphabet 中的位置為下一次出現的位置
			alphabet[c-'a']= i+1;
		}
		// 如果遍歷完勒索信中的每個字母後都沒有問題，則返回 true
		return true;
	}

    public static void main(String[] args) {
        boolean result = canConstruct2("dd", "addb");
        System.out.println("result:" + result);
    }
}
