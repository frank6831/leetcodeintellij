package com.frank.array._011_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/16
 *     title : 11. Container With Most Water
 * </pre>
 */
public class ContainerWithMostWater_11_M {
    //複習日期: 2019.09.23
	/**
	 Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai).
	 n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
	 Find two lines, which together with x-axis forms a container,
	 such that the container contains the most water.

	 Note: You may not slant the container and n is at least 2.



	 The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7].
	 In this case, the max area of water (blue section) the container can contain is 49.

	 Example:
	 Input: [1,8,6,2,5,4,8,3,7]
	 Output: 49
	 */
	
	/*
	 題意: 給一個陣列，每一個元素分別代表高度，從陣列中找兩個元素，當成容器的左右兩側，
	 計算包圍起來的面積大小，找最大面積，面積計算方式不會是梯形!
	 */

	// 暴力解，兩個迴圈，但會超時, 直接遍歷任意兩根柱子，求出能存水的大小，用一個變量保存最大的。
	// Time Complexity : O（n²）, Space Complexity : O(1)
	private int maxArea(int[] height) {
		int max = 0;
		for (int i = 0; i < height.length; i++) {
			for (int j = i + 1; j < height.length; j++) {
				int h = Math.min(height[i], height[j]);
				if (h * (j - i) > max) {
					max = h * (j - i);
				}
			}
		}
		return max;
	}

	/*
	思路：
		1.用兩個指標從兩端開始向中間靠攏，如果左端線段短於右端，那麼左端右移，反之右端左移，
		2.知道左右兩端移到中間重合，記錄這個過程中每一次組成木桶的容積，返回其中最大的。
		合理性解釋：當左端線段L小於右端線段R時，我們把L右移，這時捨棄的是L與右端其他線段（R-1, R-2, …）組成的木桶，
				  這些木桶是沒必要判斷的，因為這些木桶的容積肯定都沒有L和R組成的木桶容積大。

		Time Complexity : O(n)
		Space Complexity : O(1)
	 */
	private int maxAreaTwoPointer(int[] height) {
		int maxArea = 0;
		if (height == null) {
			return maxArea;
		}
		int left = 0;
		int right = height.length - 1;

		while (left < right) {
			int area = (right - left) * Math.min(height[left], height[right]);
			maxArea = Math.max(maxArea, area);
			if (height[left] < height[right]) {
				left++;
			} else {
				right--;
			}
		}
		return maxArea;
	}


	public static void main(String[] args) {
		ContainerWithMostWater_11_M solution = new ContainerWithMostWater_11_M();
        int[] sample = {1,8,6,2,5,4,8,3,7};
        System.out.println("Max area :" + solution.maxAreaTwoPointer(sample));
    }
}
