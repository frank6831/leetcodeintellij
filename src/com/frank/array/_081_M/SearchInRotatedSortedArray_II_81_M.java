package com.frank.array._081_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/24
 *     title : 81. Search in Rotated Sorted Array II
 * </pre>
 */
public class SearchInRotatedSortedArray_II_81_M {
    //複習日期:
	/**
	 Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

	 (i.e., [0,0,1,2,2,5,6] might become [2,5,6,0,0,1,2]).

	 You are given a target value to search. If found in the array return true, otherwise return false.

	 Example 1:
	 Input: nums = [2,5,6,0,0,1,2], target = 0
	 Output: true

	 Example 2:
	 Input: nums = [2,5,6,0,0,1,2], target = 3
	 Output: false
	 Follow up:

	 This is a follow up problem to Search in Rotated Sorted Array, where nums may contain duplicates.
	 Would this affect the run-time complexity? How and why?
	 */
	
	/*
	 題意: 是33. Search in Rotated Sorted Array的拓展題目，變的是加了一個可能含有重複數字。

	 具體來說，假設數組是nums，每次左邊緣為left，右邊緣為right，還有中間位置是mid。在每次迭代中，分三種情況：
	（1）如果target==nums[mid]，那麼mid就是我們要的結果，直接返回；
	（2）如果nums[left]<=nums[mid]，那麼說明從low到mid一定是有序的，那麼我們只需要判斷target是不是在left到mid之間，
		如果是則把右邊緣移到mid-1，否則target就在另一半，即把左邊緣移到mid+1。
	（3）如果nums[left]>nums[mid]，那麼說明從mid到right一定是有序的，那麼我們只需要判斷target是不是在mid到right之間，
		如果是則把左邊緣移到mid +1，否則就target在另一半，即把右邊緣移到mid-1。
	 */
	// Time Complexity : O(logn), Space Complexity : O(1)
	private boolean search(int[] nums, int target) {
		if (nums == null || nums.length == 0) {
			return false;
		}

		int left = 0;
		int right = nums.length -1;
		int mid;
		while (left <= right) {
			mid = (left + right) /2;
			if (nums[mid]== target) return true;

			if (nums[mid] < nums[right]) {
				// 右邊已排序
				if (nums[mid] < target && nums[right] >= target) {
					//表示target在此一半
					left = mid + 1;
				} else {
					//否則target在另一半
					right = mid - 1;
				}
			} else if (nums[mid] > nums[right]) {
				// 左邊已排序
				if (nums[mid] > target && nums[left] <= target) {
					//表示target在此一半
					right = mid - 1;
				} else {
					left = mid + 1;
				}
			} else {
				right--;
			}
		}
		return false;
	}

    public static void main(String[] args) {
		SearchInRotatedSortedArray_II_81_M solution = new SearchInRotatedSortedArray_II_81_M();
		int[] sample = {2,5,6,0,0,1,2};
        System.out.println("Max area :" + solution.search(sample, 1));
    }
}
