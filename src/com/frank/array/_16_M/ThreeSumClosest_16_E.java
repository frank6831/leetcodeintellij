package com.frank.array._16_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/6
 *     title  : 16. 3Sum Closest
 * </pre>
 */
public class ThreeSumClosest_16_E {
    /**
     Given an integer array nums of length n and an integer target,
     find three integers in nums such that the sum is closest to target.

     Return the sum of the three integers.

     You may assume that each input would have exactly one solution.



     Example 1:
     Input: nums = [-1,2,1,-4], target = 1
     Output: 2
     Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).

     Example 2:
     Input: nums = [0,0,0], target = 1
     Output: 0
     Explanation: The sum that is closest to the target is 0. (0 + 0 + 0 = 0).


     Constraints:

     3 <= nums.length <= 500
     -1000 <= nums[i] <= 1000
     -104 <= target <= 104
     */
    /*
     題意：給定一個長度為n的整數數組nums和一個整數目標target，找出nums中三個整數的和最接近target，返回這三個整數的和。
     */


    /*
      解法一：Two pointer
      1.先將數組排序
      2.初始化最接近目標的和，指針left和right分別初始化為i+1和n-1
      3.計算當前三個數的和sum=nums[i]+nums[left]+nums[right]
       (1) sum > target,則需減小sum的值,right--
       (2) sum < target,則需增大sum的值,left++
       (3) sum == target, 找到最接近目標，返回

      Time Complexity: O(n^2)
      Space Complexity: O(logn)
     */
    public int threeSumClosest1(int[] nums, int target) {
        Arrays.sort(nums);
        int result = nums[0] + nums[1] + nums[2];
        for (int i = 0; i < nums.length - 2; i++) {
            int left = i + 1;
            int right = nums.length - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                if (Math.abs(sum - target) < Math.abs(result - target))
                    result = sum;
                if (sum > target) {
                    right--;
                } else {
                    left++;
                }
            }
        }
        return result;
    }


    public int threeSumClosest2(int[] nums, int target) {
        int result = 0;
        int minGap = Integer.MAX_VALUE;
        Arrays.sort(nums);

        for (int i=0; i< nums.length -2;i++) {
            int j = i+1;
            int k = nums.length -1;

            while (j<k) {
                int sum = nums[i] + nums[j] + nums[k]; // 當前三個數的和
                int gap = Math.abs(sum - target);

                if (gap < minGap) {// 若當前三數之和更接近目標值，則更新最接近的和
                    result = sum;
                    minGap = gap;
                }

                if (sum < target) {// 若當前和小於目標值，則左指針向內移動
                    j++;
                } else {// 若當前和大於目標值，則右指針向內移動
                    k--;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        ThreeSumClosest_16_E solution = new ThreeSumClosest_16_E();
        int[] nums = {-1,2,1,-4};
        int result = solution.threeSumClosest1(nums, 1);
        System.out.println("result:" + result);
    }
}
