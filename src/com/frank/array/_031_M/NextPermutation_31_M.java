package com.frank.array._031_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/24
 *     title : 31. Next Permutation
 * </pre>
 */
public class NextPermutation_31_M {
	/**
	 A permutation of an array of integers is an arrangement of its members into a sequence or linear order.

	 For example, for arr = [1,2,3], the following are all the permutations of arr:
	 [1,2,3], [1,3,2], [2, 1, 3], [2, 3, 1], [3,1,2], [3,2,1].
	 The next permutation of an array of integers is the next lexicographically greater permutation of its integer.
	 More formally, if all the permutations of the array are sorted in one container according to their lexicographical order,
	 then the next permutation of that array is the permutation that follows it in the sorted container.
	 If such arrangement is not possible, the array must be rearranged as the lowest possible order
	 (i.e., sorted in ascending order).

	 For example, the next permutation of arr = [1,2,3] is [1,3,2].
	 Similarly, the next permutation of arr = [2,3,1] is [3,1,2].
	 While the next permutation of arr = [3,2,1] is [1,2,3] because [3,2,1] does not have a lexicographical larger rearrangement.
	 Given an array of integers nums, find the next permutation of nums.

	 The replacement must be in place and use only constant extra memory.


	 Example 1:
	 Input: nums = [1,2,3]
	 Output: [1,3,2]

	 Example 2:
	 Input: nums = [3,2,1]
	 Output: [1,2,3]

	 Example 3:
	 Input: nums = [1,1,5]
	 Output: [1,5,1]


	 Constraints:

	 1 <= nums.length <= 100
	 0 <= nums[i] <= 100
	 */
	
	/*
	 題意:給定一個陣列，將這些數字的位置重新排列，得到一個剛好比原數字大的一種排列。如果沒有比原數字大的，就升序輸出。

	 （下一個排列）給定輸入一個陣列，我們要將陣列排列成『下一個較大的值』。比方說題目輸入了 [1,2,3]，
	 那麼我們就不能排列成 [3,1,2]、而是要排列成 [1,3,2] —— 因為 132 才下一個較大的值，而非 312。

	 簡單來說，在使用相同的數字情況下，找目前比這個稍微大一點的數值。另外題目有限制這題沒有回傳值，必須使用 in-place 實做。
	 另外如果輸入的數字已經是最大的了，那輸出應該輸最小的。
	 */


	/*
      解法一：Ｔwo Pointer
	  1.由後向前搜尋找到第一個降序的點，其index為i，若無則，將整個數列反轉後回傳
	  2.找到 index 為 i 降序點右側第一個大於降序點的值，並與降序點交換
	  3.將 index 為 i 的右側進行降序排列。

	  Time Complexity: O(n)
      Space Complexity: O(1)
	 */
	public void nextPermutation(int[] nums) {
		// 1.從後往前找到第一降序的值x
		int i = nums.length -2;
		while (i >=0 && nums[i+1] <= nums[i]) {
			i--;
		}

		//如果到了最左边，就直接倒置输出
		if (i < 0) {
			reverse(nums, 0);
			return;
		}

		// 2.從後往前找到第一個比x大的數y
		int j = nums.length -1;
		while (j>=0 && nums[j] <= nums[i]) {
			j--;
		}
		System.out.println("j :"+ j + ", value:" + nums[j]);
		// 3.交換x, y
		swap(nums, i, j);
		// 4.將x之後的數字重新排序
		System.out.println("before swap: " + Arrays.toString(nums));
		reverse(nums, i+1);
	}

	private void reverse(int[] nums, int i) {
		int left = i;
		int right = nums.length -1;
		while (left < right) {
			swap(nums, left, right);
			left++;
			right--;
		}
	}

	private void swap(int[]  nums, int i, int j) {
		int num = nums[i];
		nums[i] = nums[j];
		nums[j] = num;
	}

    public static void main(String[] args) {
		NextPermutation_31_M solution = new NextPermutation_31_M();
		int[] sample = {3,2,1};
		solution.nextPermutation(sample);
        System.out.println("Max area :" + Arrays.toString(sample));
    }
}
