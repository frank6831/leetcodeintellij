package com.frank.array._349_E;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/5/10
 *     title : 349. Intersection of Two Arrays
 * </pre>
 */
public class IntersectionofTwoArrays_349_E {
    
	/**
	Given two arrays, write a function to compute their intersection.

	Example 1:
	Input: nums1 = [1,2,2,1], nums2 = [2,2]
	Output: [2]
	
	Example 2:
	Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
	Output: [9,4]
	Note:
	
	Each element in the result must be unique.
	The result can be in any order.
	*/

	//題意：找兩個數組交集的部分(不包含重複數字)

	/*
	 * 解法一：暴力解，兩個迴圈一個一個比對，利用HashSet去重。
	 */
	public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> resultSet = new HashSet<>();
		for (int i = 0; i< nums1.length;i++) {
			for (int j =0; j < nums2.length; j++) {
				if (nums1[i] == nums2[j]) {
					resultSet.add(nums1[i]);
				}
			}
		}
		 int[] arr = new int[resultSet.size()];
		 int i = 0;
		 for (Integer val : resultSet) {
			 arr[i++] = val;
		 }
		return arr;
    }
	
	/*解答：
	 1.將nums1轉換為Set1，便於快速查找是否存在某個元素
	 2.將nums2轉換為Set2，加入時多判斷是否有在set1中，就可得到交集
	 3.將交集元素轉換為數組返回

	  Time Complexity: O(n)
      Space Complexity: O(n)
	 */
	public int[] intersection2(int[] nums1, int[] nums2) {
        Set<Integer> nums1Set = new HashSet<>();
        for (int num : nums1) {
            nums1Set.add(num);
        }
        Set<Integer> intersect = new HashSet<>();
        for (int num : nums2) {
            if (nums1Set.contains(num)) {
                intersect.add(num);
            }
        }
        int[] intersectArray = new int[intersect.size()];
        int i = 0;
        for (int num : intersect) {
            intersectArray[i++] = num;
        }
        return intersectArray;
    }
	
    public static void main(String[] args) {
		IntersectionofTwoArrays_349_E solution = new IntersectionofTwoArrays_349_E();
        int num1[] = {4,9,5};
        int num2[] = {9,4,9,8,4};
        int num3[] = {1,2,2,1};
        int num4[] = {2,2};
        
        int result[] = solution.intersection2(num3, num4);
        System.out.println(Arrays.toString(result));
    }
}
