package com.frank.array._229_M;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/3/12
 *     title  : 229. Majority Element II
 * </pre>
 */
public class MajorityElementII_229_M {
    /**
     Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.

     Example 1:
     Input: nums = [3,2,3]
     Output: [3]

     Example 2:
     Input: nums = [1]
     Output: [1]

     Example 3:
     Input: nums = [1,2]
     Output: [1,2]


     Constraints:

     1 <= nums.length <= 5 * 104
     -109 <= nums[i] <= 109


     Follow up: Could you solve the problem in linear time and in O(1) space?
     */
    /*
     題意：給定一個整數陣列 nums，找出出現超過 n/3 次的數字
     */

    /*
      解法:HashMap
      1.使用HashMap紀錄每個數字出現的次數


      Time Complexity:  O()
      Space Complexity: O(n)
     */
    public List<Integer> majorityElement(int[] nums) {
        List<Integer> resultList = new ArrayList<>();
        if (nums ==null || nums.length ==0) {
            return resultList;
        }

        int n = nums.length;
        int time = n/3;
        Map<Integer, Integer> counts = new HashMap<>();

        for (int num:nums) {
            counts.put(num, counts.getOrDefault(num, 0) + 1);
        }

        for (Map.Entry<Integer, Integer> entry:counts.entrySet()) {
            if (entry.getValue() > time) {
                resultList.add(entry.getKey());
            }
        }

        return resultList;
    }

    /*
      解法:Moore voting algorithm
      1.初始化兩個候選人和他們的票數計數。
      2.遍歷整個數組，對每個數字進行投票。
      3.根據投票結果更新兩個候選人和他們的票數計數。
      4.再次遍歷整個數組，重新計算兩個候選人的票數。
      5.確定兩個候選人的票數是否超過了n/3，將符合條件的候選人添加到結果列表中
     */
    public List<Integer> majorityElementMoore2(int[] nums) {
        List<Integer> result = new ArrayList<>();
        if (nums == null || nums.length ==0) {
            return result;
        }

        int candidate1 = nums[0], count1 = 0;
        int candidate2 = nums[0], count2 = 0;

        for (int num:nums) {
            if (num == candidate1) {
                //如果當前數字與候選人1相等，則候選人1的票數計數加1
                count1++;
            } else if (num == candidate2) {
                //如果當前數字與候選人2相等，則候選人2的票數計數加1
                count2++;
            } else if (count1 == 0) {
                // 如果當前候選人1的票數計數為0，將候選人1設置為當前數字
                candidate1 = num;
                count1 = 1;
            } else if (count2 == 0) {
                // 如果當前候選人2的票數計數為0，將候選人2設置為當前數字
                candidate2 = num;
                count2 = 1;
            } else {
                //如果當前數字與兩個候選人都不相等，則將兩個候選人的票數計數均減1
                count1--;
                count2--;
            }
        }
        System.out.println("candidate1:" + candidate1 + ", candidate2:" + candidate2);
        count1 =0;
        count2 =0;

        // 遍歷整個數組，重新計算兩個候選人的票數
        for(int num:nums) {
            if (num == candidate1) {
                count1++;
            } else if (num == candidate2) {
                count2++;
            }
        }

        // 如果候選人1的票數大於n/3，則將其添加到結果列表中
        if (count1 > nums.length/3) {
            result.add(candidate1);
        }

        // 如果候選人2的票數大於n/3，則將其添加到結果列表中
        if (count2 > nums.length/3) {
            result.add(candidate2);
        }
        return result;
    }

    public static void main(String[] args) {
        MajorityElementII_229_M solution = new MajorityElementII_229_M();
        int[] nums1 = {2,1,1,3,1,4,5,6};
        int[] nums2 = {1,2};
        List<Integer> resultList = solution.majorityElementMoore2(nums1);
        System.out.println("resultList:" + resultList);
    }
}
