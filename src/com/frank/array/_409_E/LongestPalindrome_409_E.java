package com.frank.array._409_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/3/2
 *     title  : 409. Longest Palindrome
 * </pre>
 */
public class LongestPalindrome_409_E {
    /**
     Given a string s which consists of lowercase or uppercase letters, return the length of the longest palindrome that can be built with those letters.

     Letters are case sensitive, for example, "Aa" is not considered a palindrome here.



     Example 1:

     Input: s = "abccccdd"
     Output: 7
     Explanation: One longest palindrome that can be built is "dccaccd", whose length is 7.
     Example 2:

     Input: s = "a"
     Output: 1
     Explanation: The longest palindrome that can be built is "a", whose length is 1.


     Constraints:

     1 <= s.length <= 2000
     s consists of lowercase and/or uppercase English letters only.
     */
    /*
     題意：給定一個包含大小寫字母的陣列，組成一個最長的回文串，回傳其長度。
     */




    /*
    解法：
      1.使用整數陣列統計字串中每個字元出現的次數。
      2.遍歷字元統計結果，計算每個字元可以貢獻到回文字串的長度。
      3.如果存在奇數次字符，將回文串的長度加1。
      Time Complexity: O(n)
      Space Complexity: O(1)
     */
    public static int longestPalindrome(String s) {
        // 統計每個字符出現的次數
        int[] count = new int[128];
        for (char c:s.toCharArray()) {
            count[c]++;
        }

        int longestLength = 0;
        boolean hasOdd = false;

        for (int feq:count) {
            // 對於每個字符出現的次數，將其可以貢獻到回文串的長度加入結果
            longestLength += feq/2*2;
            // 如果當前字符出現次數是奇數，且之前未出現過奇數次字符
            if (feq%2 ==1 && !hasOdd) {
                hasOdd = true;
            }
        }
        // 如果存在奇數次字符，將回文串的長度加1
        if (hasOdd) {
            longestLength++;
        }
        return longestLength;
    }

    public static void main(String[] args) {
        String str = "abccccdd";

        int length = longestPalindrome(str);
        System.out.println("length:" + length);
    }
}
