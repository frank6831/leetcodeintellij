package com.frank.array._1901_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/11/7
 *     title  : 1901. Find Peak Element II
 * </pre>
 */
public class FindPeakElement_II_1901_M {
    /**
     A peak element in a 2D grid is an element that is strictly greater than all of its adjacent neighbors to the left, right, top, and bottom.

     Given a 0-indexed m x n matrix mat where no two adjacent cells are equal, find any peak element mat[i][j] and return the length 2 array [i,j].

     You may assume that the entire matrix is surrounded by an outer perimeter with the value -1 in each cell.

     You must write an algorithm that runs in O(m log(n)) or O(n log(m)) time.



     Example 1:



     Input: mat = [[1,4],[3,2]]
     Output: [0,1]
     Explanation: Both 3 and 4 are peak elements so [1,0] and [0,1] are both acceptable answers.
     Example 2:



     Input: mat = [[10,20,15],[21,30,14],[7,16,32]]
     Output: [1,1]
     Explanation: Both 30 and 32 are peak elements so [1,1] and [2,2] are both acceptable answers.


     Constraints:

     m == mat.length
     n == mat[i].length
     1 <= m, n <= 500
     1 <= mat[i][j] <= 105
     No two adjacent cells are equal.
     */
    /*
     題意：給一個數組,找出任一個峰頂。峰頂的特點就是,值比它的左鄰居和右鄰居都大。
     */

    /*
      解法:Binary Search



      Time Complexity:  O(logN)
      Space Complexity: O(n)
     */
    public int[] findPeakGrid(int[][] mat) {
        return null;
    }



    public static void main(String[] args) {
        FindPeakElement_II_1901_M solution = new FindPeakElement_II_1901_M();
        int[] nums1 = {0,10,5,2};
        int[] nums2 = {1,2,3,1};
        //int peakElement = solution.findPeakElement(nums1);
        //System.out.println("peakElement:" + peakElement);
    }
}
