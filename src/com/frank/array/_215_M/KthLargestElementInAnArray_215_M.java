package com.frank.array._215_M;


import java.util.PriorityQueue;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/4/10
 *     title  : 215. Kth Largest Element in an Array
 * </pre>
 */
public class KthLargestElementInAnArray_215_M {
    /**
     Given an integer array nums and an integer k, return the kth largest element in the array.

     Note that it is the kth largest element in the sorted order, not the kth distinct element.

     Can you solve it without sorting?



     Example 1:

     Input: nums = [3,2,1,5,6,4], k = 2
     Output: 5
     Example 2:

     Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
     Output: 4


     Constraints:

     1 <= k <= nums.length <= 105
     -104 <= nums[i] <= 104
     */
    /*
     題意：找到數組中第k大的數字，並返回該數字的值。
     */

    /*
      解法:
      1.初始化一個容量為k的最小堆(PriorityQueue)
      2.將數組中的每個元素加入最小堆。
      3.如果最小堆的大小超過k，則移除堆頂元素，保持堆的大小為k。
      4.最終，最小堆的堆頂元素即為數組中的第k大元素。
      Time Complexity:  O(n log k)
      Space Complexity: O(k)
     */
    public int findKthLargest(int[] nums, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>(k);
        // 將數組中的元素依次加入最小堆
        for (int num:nums) {
            minHeap.offer(num);
            // 如果最小堆中的元素超過 k 個，則移除堆頂元素，保持堆的大小為 k
            if (minHeap.size() > k) {
                minHeap.poll();
            }
        }
        // 返回最小堆的堆頂元素，即為第 k 大的元素
        return minHeap.peek() != null ? minHeap.peek() : 0;
    }


    public static void main(String[] args) {
        KthLargestElementInAnArray_215_M solution = new KthLargestElementInAnArray_215_M();
        int[] nums = {3,2,1,5,6,4};
        int longestStreak = solution.findKthLargest(nums, 2);
        System.out.println("longestStreak:" + longestStreak);


        PriorityQueue<Integer> q = new PriorityQueue<>();
        q.add(8);
        q.add(5);
        q.add(13);
        q.add(2);
        while (!q.isEmpty()) {
            System.out.print(q.poll() + " ");
        }
    }
}
