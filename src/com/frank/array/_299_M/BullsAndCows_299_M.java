package com.frank.array._299_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/11/7
 *     title  : 852. Peak Index in a Mountain Array
 * </pre>
 */
public class BullsAndCows_299_M {
    /**
     You are playing the Bulls and Cows game with your friend.

     You write down a secret number and ask your friend to guess what the number is.
     When your friend makes a guess, you provide a hint with the following info:

     The number of "bulls", which are digits in the guess that are in the correct position.
     The number of "cows", which are digits in the guess that are in your secret number but are located in the wrong position.
     Specifically, the non-bull digits in the guess that could be rearranged such that they become bulls.
     Given the secret number secret and your friend's guess guess, return the hint for your friend's guess.

     The hint should be formatted as "xAyB", where x is the number of bulls and y is the number of cows.
     Note that both secret and guess may contain duplicate digits.



     Example 1:

     Input: secret = "1807", guess = "7810"
     Output: "1A3B"
     Explanation: Bulls are connected with a '|' and cows are underlined:
     "1807"
     |
     "7810"
     Example 2:

     Input: secret = "1123", guess = "0111"
     Output: "1A1B"
     Explanation: Bulls are connected with a '|' and cows are underlined:
     "1123"        "1123"
     |      or     |
     "0111"        "0111"
     Note that only one of the two unmatched 1s is counted as a cow since the non-bull digits can only be rearranged to allow one 1 to be a bull.


     Constraints:

     1 <= secret.length, guess.length <= 1000
     secret.length == guess.length
     secret and guess consist of digits only.
     */
    /*
     題意：
     */

    /*
      解法:Binary Search

      Time Complexity:  O(logN)
      Space Complexity: O(n)
     */
    public String getHint(String secret, String guess) {
        // 用來統計每個數字出現的次數
        int[] nums = new int[10];
        // 初始化兩個變量用來統計公牛（Bull）和奶牛（Cow）的數量
        int bull = 0;
        int cow = 0;

        // 遍歷秘密數字和猜測數字，統計每個數字出現的次數以及計算公牛數量
        for (int i=0; i<secret.length();i++) {
            char s = secret.charAt(i);
            char g = guess.charAt(i);
            System.out.println(" s:"+ s + ", g:" + g);
            if (s == g) {
                bull++;
            } else {
                // 如果 secret 中的數字出現次數小於 0，表示在 guess 中已經出現過，增加奶牛數量
                if (nums[s-'0']++ < 0) {
                    System.out.println("secret match s:"+ s);
                    cow++;
                }
                // 如果 guess 中的數字出現次數大於 0，表示在 secret 中已經出現過，增加奶牛數量
                if (nums[g-'0']-- > 0) {
                    cow++;
                }
            }
        }
        return bull+"A"+cow+"B";
    }


    public String getHint2(String secret, String guess) {
        // 用來統計秘密數字和猜測數字中每個數字的出現次數。
        int[] secdigs = new int[10];
        int[] guessdigs = new int[10];
        //分別統計公牛和奶牛的數量。
        int bull = 0, cow = 0;

        //使用一個迴圈遍歷秘密數字和猜測數字
        for(int i = 0; i < secret.length() ; i ++) {
            if (secret.charAt(i) == guess.charAt(i)) {
                bull++;
            } else {
                secdigs[secret.charAt(i) - '0']++;
                guessdigs[guess.charAt(i) - '0']++;
            }
        }

        //將每個位置的數字出現次數較小的那個加到cow中。
        for (int i = 0; i < 10; i ++) {
            cow += Math.min(secdigs[i], guessdigs[i]);
        }

        StringBuilder sb = new StringBuilder();
        sb.append(bull).append("A").append(cow).append("B");
        return sb.toString();
    }

    public static void main(String[] args) {
        BullsAndCows_299_M solution = new BullsAndCows_299_M();
        String hint = solution.getHint("1807", "7810");
        System.out.println("hint:" + hint);
    }
}
