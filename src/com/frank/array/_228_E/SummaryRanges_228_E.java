package com.frank.array._228_E;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/14
 *     desc  : 228. Summary Ranges
 * </pre>
 */
public class SummaryRanges_228_E {
    /***
     You are given a sorted unique integer array nums.

     Return the smallest sorted list of ranges that cover all the numbers in the array exactly.
     That is, each element of nums is covered by exactly one of the ranges, and there is no integer
     x such that x is in one of the ranges but not in nums.
     Each range [a,b] in the list should be output as:

     "a->b" if a != b
     "a" if a == b

     Example 1:
     Input: nums = [0,1,2,4,5,7]
     Output: ["0->2","4->5","7"]
     Explanation: The ranges are:
     [0,2] --> "0->2"
     [4,5] --> "4->5"
     [7,7] --> "7"

     Example 2:
     Input: nums = [0,2,3,4,6,8,9]
     Output: ["0","2->4","6","8->9"]
     Explanation: The ranges are:
     [0,0] --> "0"
     [2,4] --> "2->4"
     [6,6] --> "6"
     [8,9] --> "8->9"

     Example 3:
     Input: nums = []
     Output: []

     Example 4:
     Input: nums = [-1]
     Output: ["-1"]

     Example 5:
     Input: nums = [0]
     Output: ["0"]

     Constraints:

     0 <= nums.length <= 20
     -231 <= nums[i] <= 231 - 1
     All the values of nums are unique.
     nums is sorted in ascending order.
     */
    // 題意：若有連續的數，則要用箭頭表示區間，如2-4

    /*
    Algorithm
    1.Create a list of string to store the result.
    2.Start traversing the array from i=0 till i<N, (N is the size of array) in a while loop.
        #Mark the number at current index as start of the range.
        #Traverse the array starting from current index and find the last element whose difference
         from previous element is exactly 1, i.e. nums[i+1]==nums[i]+1
        #Mark this element as end of the range.
        #Now insert this formed range into the result list. And repeat for the remaining elements.
    3.Return the result list.
     */
    //思路：只需遍歷一遍數組即可，每次檢查下一個數是不是遞增的，如果是，則繼續往下遍歷，
    // 如果不是了，判斷此時是一個數還是一個序列，一個數直接存入結果，序列的話要存入首尾數字和箭頭"->"。

    //https://www.tutorialcup.com/leetcode-solutions/summary-ranges-leetcode-solution.htm
    public List<String> summaryRanges(int[] nums) {
        List<String> result = new ArrayList<>();

        int i=0, n=nums.length;
        while (i<n) {
            int start,end;
            start = nums[i];
            while (i+1<n && nums[i+1]== nums[i]+1) {
                i++;
            }
            end = nums[i];

            if (start==end) {
                result.add(start + "");
            } else {
                result.add(start + "->" + end);
            }
            i++;
        }
        return result;
    }

    public static void main(String[] args) {
        SummaryRanges_228_E solution = new SummaryRanges_228_E();
        int[] nums={0,1,2,4,5,7};
        List<String> resultList =  solution.summaryRanges(nums);
        System.out.println("resultList:" + resultList);
    }
}
