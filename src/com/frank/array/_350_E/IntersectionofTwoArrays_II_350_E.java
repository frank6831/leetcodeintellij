package com.frank.array._350_E;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/3
 *     title  : 350. Intersection of Two Arrays II
 * </pre>
 */
public class IntersectionofTwoArrays_II_350_E {
    /**
     Given two integer arrays nums1 and nums2, return an array of their intersection.
     Each element in the result must appear as many times as it shows in both arrays and
     you may return the result in any order.



     Example 1:
     Input: nums1 = [1,2,2,1], nums2 = [2,2]
     Output: [2,2]

     Example 2:
     Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
     Output: [4,9]
     Explanation: [9,4] is also accepted.


     Constraints:

     1 <= nums1.length, nums2.length <= 1000
     0 <= nums1[i], nums2[i] <= 1000


     Follow up:

     What if the given array is already sorted? How would you optimize your algorithm?
     What if nums1's size is small compared to nums2's size? Which algorithm is better?
     What if elements of nums2 are stored on disk, and the memory is limited such that
     you cannot load all elements into the memory at once?
     */
    /*
     題意：找出兩個整數數組的交集，在結果數組中每個元素都必須顯示與它在兩個數組中出現的次數相同
     */


    /*
      解法一：
      1.用HashMap來記錄nums1中元素出現次數
      2.遍歷nums2，將出現在nums1和nums2中的元素加入結果列表，同時更新HashMap中相應元素的出現次數
      3.將結果列表轉換為數組

      Time Complexity:  O(m + n)
      Space Complexity: O(min(m, n))
     */
    public int[] intersect(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num:nums1) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        List<Integer> res = new ArrayList();
        for (int num:nums2) {
            if (map.containsKey(num) && map.get(num) != 0) {
                res.add(num);
                map.put(num, map.get(num) -1);
            }
        }

        int[] arr = new int[res.size()];
        for (int i =0; i< res.size();i++) {
            arr[i] = res.get(i);
        }
        return arr;
    }


    /*
      解法二：
      1.先將nums1和nums2由小到大排好序
      2.使用兩個指針i和j分別指向nums1和nums2的開頭。
        若nums1[i]小於nums2[j]，則i++；
        若nums1[i]大於nums2[j]，則j++。
        否則，將nums1[i]（或 nums2[j]）添加到結果數組中，
        並同時將i和j增加1。
      3.重複此過程，直到任一指針到達其數組的末尾。

      Time Complexity:  O(m+n)
      Space Complexity: O(min(m,n))
     */
    public int[] intersectTwoPointer(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i=0, j=0, k=0;
        int[] arr = new int[Math.min(nums1.length, nums2.length)];
        while (i < nums1.length && j<nums2.length) {
            if (nums1[i] < nums2[j]) {
                i++;
            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                arr[k] = nums1[i];
                i++;
                j++;
                k++;
            }
        }
        return Arrays.copyOfRange(arr, 0, k);
    }


    public static void main(String[] args) {
        IntersectionofTwoArrays_II_350_E solution = new IntersectionofTwoArrays_II_350_E();
        int[] nums1 = {1,2,2,1};
        int[] nums2 = {2,2};

        int[] result = solution.intersectTwoPointer(nums1, nums2);
        System.out.println("result:" + Arrays.toString(result));
    }
}
