package com.frank.array._162_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/11/7
 *     title  : 162. Find Peak Element
 * </pre>
 */
public class FindPeakElement_162_M {
    /**
     A peak element is an element that is strictly greater than its neighbors.

     Given a 0-indexed integer array nums, find a peak element, and return its index. If the array contains multiple peaks, return the index to any of the peaks.

     You may imagine that nums[-1] = nums[n] = -∞. In other words, an element is always considered to be strictly greater than a neighbor that is outside the array.

     You must write an algorithm that runs in O(log n) time.



     Example 1:

     Input: nums = [1,2,3,1]
     Output: 2
     Explanation: 3 is a peak element and your function should return the index number 2.
     Example 2:

     Input: nums = [1,2,1,3,5,6,4]
     Output: 5
     Explanation: Your function can return either index number 1 where the peak element is 2, or index number 5 where the peak element is 6.


     Constraints:

     1 <= nums.length <= 1000
     -231 <= nums[i] <= 231 - 1
     nums[i] != nums[i + 1] for all valid i.
     */
    /*
     題意：給一個數組,找出任一個峰頂。峰頂的特點就是,值比它的左鄰居和右鄰居都大。
     */

    /*
      解法:Binary Search
     1.根據左右指針計算中間位置m，並比較m與m+1的值.
     2.如果m較大，則左側存在峰值,r=m，如果m+1較大，則右側存在峰值,l=m+1
     3.持續比較直到l與r重合，即找到答案


      Time Complexity:  O(logN)
      Space Complexity: O(n)
     */
    public int findPeakElement(int[] nums) {
        int left = 0;
        int right = nums.length -1;

        while(left < right) {
            int mid = left + (right - left) /2;
            if (nums[mid] > nums[mid+1]) {
                right = mid;
            } else {
                left = mid +1;
            }
        }
        return left;
    }



    public static void main(String[] args) {
        FindPeakElement_162_M solution = new FindPeakElement_162_M();
        int[] nums1 = {0,10,5,2};
        int[] nums2 = {1,2,3,1};
        int peakElement = solution.findPeakElement(nums1);
        System.out.println("peakElement:" + peakElement);
    }
}
