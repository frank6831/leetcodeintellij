package com.frank.array._442_M;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/9
 *     title  : 442. Find All Duplicates in an Array
 * </pre>
 */
public class FindAllDuplicatesInAnArray_442_M {
    /**
     Given an integer array nums of length n where all the integers of nums are in the range [1, n] and
     each integer appears once or twice, return an array of all the integers that appears twice.

     You must write an algorithm that runs in O(n) time and uses only constant extra space.



     Example 1:

     Input: nums = [4,3,2,7,8,2,3,1]
     Output: [2,3]
     Example 2:

     Input: nums = [1,1,2]
     Output: [1]
     Example 3:

     Input: nums = [1]
     Output: []


     Constraints:

     n == nums.length
     1 <= n <= 105
     1 <= nums[i] <= n
     Each element in nums appears once or twice.
     */
    /*
     題意：尋找旋轉有序數組(數組的值不重複)的最小值
     */


    /*
      解法一：Binary search

      Time Complexity: O(log n)
      Space Complexity: O(1)
     */
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> result = new ArrayList<>();
        Set<Integer> set = new HashSet<>();

        for (int i=0; i< nums.length;i++) {
            int value = nums[i];
            if (!set.add(value)) {
                result.add(value);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        FindAllDuplicatesInAnArray_442_M solution = new FindAllDuplicatesInAnArray_442_M();
        int[] nums = {1};
        List<Integer> result = solution.findDuplicates(nums);
        System.out.println("result:" + result);
    }
}
