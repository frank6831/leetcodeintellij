package com.frank.array._238_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/18
 *     title  : 238. Product of Array Except Self
 * </pre>
 */
public class ProductOfArrayExceptSelf_238_M {
    /**
     Given an integer array nums, return an array answer such that answer[i] is equal to
     the product of all the elements of nums except nums[i].

     The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

     You must write an algorithm that runs in O(n) time and without using the division operation.



     Example 1:

     Input: nums = [1,2,3,4]
     Output: [24,12,8,6]
     Example 2:

     Input: nums = [-1,1,0,-3,3]
     Output: [0,0,9,0,0]


     Constraints:

     2 <= nums.length <= 105
     -30 <= nums[i] <= 30
     The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.


     Follow up: Can you solve the problem in O(1) extra space complexity?
     (The output array does not count as extra space for space complexity analysis.)
     */
    /*
     題意：輸入1個陣列，需產生新的陣列，值output[i] 是原本陣列其他元素相乘但不包含相乘nums[i]的結果。
     */

    /*
      解法：https://www.youtube.com/watch?v=rpQhKorJRd8
      1.從左至右，只存左半邊的乘積
      2.從右至左，將右半邊乘積乘入res
      Time Complexity: O(n)
      Space Complexity: O(1)
     */
    public static int[] productExceptSelf(int[] nums) {
        int[] res = new int[nums.length];
        // left to right
        res[0] = 1;
        for (int i=1; i< nums.length;i++) {
            res[i] = res[i-1]* nums[i-1];
            //System.out.println("Frank loop res:" + Arrays.toString(res) + ", i:" + i);
        }
        System.out.println("Frank res:" + Arrays.toString(res));
        // right to left
        int right = 1;
        for (int i=nums.length -1; i>=0;i--) {
            res[i] = res[i] * right;
            right = right * nums[i];
        }
        return res;
    }

    public static void main(String[] args) {
        int[] array = {1, 2,3,4};

        int[] result = productExceptSelf(array);
        System.out.println("result:"+ Arrays.toString(result));
    }
}
