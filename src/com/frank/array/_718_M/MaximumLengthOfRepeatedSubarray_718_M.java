package com.frank.array._718_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/3/3
 *     title  : 718. Maximum Length of Repeated Subarray
 * </pre>
 */
public class MaximumLengthOfRepeatedSubarray_718_M {
    /**
     Given two integer arrays nums1 and nums2, return the maximum length of a subarray that appears in both arrays.


     Example 1:
     Input: nums1 = [1,2,3,2,1], nums2 = [3,2,1,4,7]
     Output: 3
     Explanation: The repeated subarray with maximum length is [3,2,1].

     Example 2:
     Input: nums1 = [0,0,0,0,0], nums2 = [0,0,0,0,0]
     Output: 5
     Explanation: The repeated subarray with maximum length is [0,0,0,0,0].


     Constraints:

     1 <= nums1.length, nums2.length <= 1000
     0 <= nums1[i], nums2[i] <= 100
     */
    /*
     題意：
     */

    /*
      解法:DP
      1.

      Time Complexity:  O()
      Space Complexity: O(n)
     */
    public int findLength(int[] nums1, int[] nums2) {
        return 0;
    }

    public static void main(String[] args) {
        MaximumLengthOfRepeatedSubarray_718_M solution = new MaximumLengthOfRepeatedSubarray_718_M();
        int[] nums1 = {0,10,5,2};
        int[] nums2 = {24,69,100,99,79,78,67,36,26,19};
        int peakElement = solution.findLength(nums1, nums2);
        System.out.println("peakElement:" + peakElement);
    }
}
