package com.frank.array._219_E;

import java.util.HashSet;
import java.util.Set;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/16
 *     title : 219. Contains Duplicate II
 * </pre>
 */
public class Solution {
    
	/**
	Given an array of integers and an integer k, find out whether there are 
	two distinct indices i and j in the array such that nums[i] = nums[j] and 
	the absolute difference between i and j is at most k.

	Example 1:
	Input: nums = [1,2,3,1], k = 3
	Output: true
	
	Example 2:
	Input: nums = [1,0,1,1], k = 1
	Output: true
	
	Example 3:
	Input: nums = [1,2,3,1,2,3], k = 2
	Output: false
	*/
	// 題目意思：在一個數組中是否存在兩個數字相等，並且他們的間距最多為k.
	/*
	  思路：
	  1.使用一個map存放出現過的數字以及位置
	  2.如果出現重複的數字，判斷目前位置與儲存的位置距離是否小於等於k，有的話直接回傳true
      3.陣列中沒有符合的配對，回傳false
	 */
	private static boolean containsNearbyDuplicate(int[] nums, int k) {
		Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i])) {
            	return true;
            }
           	set.add(nums[i]);	
            if (set.size() > k) set.remove(nums[i-k]);
        }
        return false;
    }

    public static void main(String[] args) {
        int num1[] = {1,2,3,1};
        int num2[] = {9,4,9,8,4};
        int num3[] = {1,2,2,1};
        int num4[] = {1,2,3,1,2,3};
        int k = 2;
        
        boolean result = containsNearbyDuplicate(num2, k);
        System.out.println("result:" + result);
    }
}
