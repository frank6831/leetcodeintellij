package com.frank.array._2215_E;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/3
 *     title  : 2215. Find the Difference of Two Arrays
 * </pre>
 */
public class FindtheDifferenceofTwoArrays_2215_E {
    /**
     Given two 0-indexed integer arrays nums1 and nums2, return a list answer of size 2 where:

     answer[0] is a list of all distinct integers in nums1 which are not present in nums2.
     answer[1] is a list of all distinct integers in nums2 which are not present in nums1.
     Note that the integers in the lists may be returned in any order.



     Example 1:

     Input: nums1 = [1,2,3], nums2 = [2,4,6]
     Output: [[1,3],[4,6]]
     Explanation:
     For nums1, nums1[1] = 2 is present at index 0 of nums2, whereas nums1[0] = 1 and nums1[2] = 3 are not present in nums2. Therefore, answer[0] = [1,3].
     For nums2, nums2[0] = 2 is present at index 1 of nums1, whereas nums2[1] = 4 and nums2[2] = 6 are not present in nums2. Therefore, answer[1] = [4,6].
     Example 2:

     Input: nums1 = [1,2,3,3], nums2 = [1,1,2,2]
     Output: [[3],[]]
     Explanation:
     For nums1, nums1[2] and nums1[3] are not present in nums2. Since nums1[2] == nums1[3], their value is only included once and answer[0] = [3].
     Every integer in nums2 is present in nums1. Therefore, answer[1] = [].


     Constraints:

     1 <= nums1.length, nums2.length <= 1000
     -1000 <= nums1[i], nums2[i] <= 1000
     */
    /*
     題意：給定兩個0索引的整數數組 nums1 和 nums2，返回一個大小為2的列表 answer
          answer[0] 是 nums1 中所有不在 nums2 中出現的不同整數的列表。
          answer[1] 是 nums2 中所有不在 nums1 中出現的不同整數的列表。
          請注意，列表中的整數可以按任何順序返回。
     */

    /*
      解法：自己寫
      1.num1和num2跑迴圈，加入到set1,set2去重
      2.num1跑迴圈找出不在num2的值，num2跑迴圈找出不在num1的值，各自加入到list1,list2中
      3.最後將list1,list加入到resultList，並回傳。

      Time Complexity:  O(n)
      Space Complexity: O(n)
     */
    public List<List<Integer>> findDifference2(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();

        List<List<Integer>> result = new ArrayList<>();
        List<Integer> onlyNum1List = new ArrayList<>();
        List<Integer> onlyNum2List = new ArrayList<>();

        for (int num:nums1) {
            set1.add(num);
        }

        for (int num:nums2) {
            set2.add(num);
        }

        for (int num:set1) {
            if (!set2.contains(num)) {
                onlyNum1List.add(num);
            }
        }

        for (int num:set2) {
            if (!set1.contains(num)) {
                onlyNum2List.add(num);
            }
        }
        result.add(onlyNum1List);
        result.add(onlyNum2List);

        return result;
    }

    public static void main(String[] args) {
        FindtheDifferenceofTwoArrays_2215_E solution = new FindtheDifferenceofTwoArrays_2215_E();
        int[] nums1 = {1,2,3};
        int[] nums2 = {2,4,6};
        int[] nums3 = {1,2,3,3};
        int[] nums4 = {1,1,2,2};

        List<List<Integer>> result = solution.findDifference2(nums3, nums4);
        System.out.println("result:" + result);
    }
}
