package com.frank.array._026_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/13
 *     title   : 26. Remove Duplicates from Sorted Array
 * </pre>
 */
public class Solution_2 {
    /**
     # [Remove Duplicates from Sorted Array]
     ## Description

     Given a sorted array, remove the duplicates in-place such that each element appear
     only once and return the new length.

     Do not allocate extra space for another array, you must do this by modifying
     the input array in-place with O(1) extra memory.

     **Examples:**
     Given nums = [1,1,2],

     Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
     It doesn't matter what you leave beyond the new length.

     **Tags:** Array, Two Pointers
     */
    //題意：移除重複的數字，並回傳陣列的長度

    //思路：因為陣列已排序，從index= 1開始跑回圈，目前位置與前一個位置的數字比較，若相同，表示重複，則arraySize減一
    //     依序比較完，就可以扣除掉重複的數字
    public int removeDuplicates(int[] sample) {
        if (sample.length == 0) {
            return 0;
        }
        int arraySize = sample.length;
        for (int i=1;i < sample.length;i++) {
            if (sample[i-1] == sample[i]) {
                --arraySize;
            }
        }
        return arraySize;
    }

    public static void main(String[] args) {
        Solution_2 solution = new Solution_2();
        int[] sample = {1,2,4,4,5,6,7,7};
        System.out.println(solution.removeDuplicates(sample));
    }
}
