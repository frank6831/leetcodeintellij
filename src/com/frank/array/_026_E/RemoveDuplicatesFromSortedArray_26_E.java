package com.frank.array._026_E;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/06
 *     title   : 26. Remove Duplicates from Sorted Array
 * </pre>
 */
public class RemoveDuplicatesFromSortedArray_26_E {
    
	/**
		 # [Remove Duplicates from Sorted Array]
		 ## Description
		
		Given a sorted array, remove the duplicates in-place such that each element appear 
		only once and return the new length.
			
		Do not allocate extra space for another array, you must do this by modifying 
		the input array in-place with O(1) extra memory.
		
		**Examples:**
		Given nums = [1,1,2],
		
		Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
		It doesn't matter what you leave beyond the new length.
		
		**Tags:** Array, Two Pointers 
	 */
	// 題意是讓你從一個有序的數組中移除重複的元素，並返回之後數組的長度
	
	/*
	 思路：由於給定的數組是已經完成排序的數組，所以我們只需要判斷第i個元素是否和第i+1個元素相等，就可以知道第i個元素是否是重複的。
		  另外需要注意的是我們需要將不重複的元素重新放置在數組的前面。
	 */
	public int removeDuplicates(int[] nums) {
		int len = nums.length;
		int tail = 1;
		for (int i = 1; i < len;i++) {
			if (nums[i-1] != nums[i]) {
				nums[tail++] = nums[i];
			}
		}
		return tail;
    }
	
	public int removeDuplicates2(int[] nums) {
		if(nums == null || nums.length == 0) return 0;
	    if(nums.length == 1) return 1;
	    int p = 0; // 指針
	    for(int i = 1 ; i < nums.length ; i++){
	        if (nums[p] != nums[i]){
	            p++;
	            nums[p] = nums[i];
	        }
	    }    
	    return p + 1;
	}

	public int removeDuplicateTwoPointer(int[] nums) {

		int i=0, j=0;
		while (j < nums.length) {
			// if not duplicate, keep it, otherwise skip it.
			if (i==0 || nums[j] != nums[i-1]) {
				nums[i++] = nums[j++];
			} else {
				j++;
			}
		}
		// i is now at the length of new array,, (0,i) is what we want return i;
		return i;
	}

	// 題解方法是雙指針，一個指針只記錄unique的並且幫助記錄長度，一個指針往前找
	public int removeDuplicates3(int[] nums) {
		if(nums == null || nums.length == 0) return 0;
		int len = 1;
		for(int i = 1; i < nums.length; i++){
			if (nums[i] != nums[i - 1]){
				nums[len] = nums[i];
				len++;
				System.out.println("len:" + len + ", i: " + i + ", nums[i]:" + nums[i]);
				System.out.println(Arrays.toString(nums));
			}
		}
		return len;
	}

    public static void main(String[] args) {
		RemoveDuplicatesFromSortedArray_26_E solution = new RemoveDuplicatesFromSortedArray_26_E();
        int[] sample = {1,2,4,4,4,5};
        
        System.out.println(solution.removeDuplicates3(sample));
		System.out.println(Arrays.toString(sample));
    }
}
