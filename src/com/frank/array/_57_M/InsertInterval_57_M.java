package com.frank.array._57_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/16
 *     title  : 57. Insert Interval
 * </pre>
 */
public class InsertInterval_57_M {
    /**
     You are given an array of non-overlapping intervals intervals where intervals[i] = [starti, endi]
     represent the start and the end of the ith interval and intervals is sorted in ascending order by starti.
     You are also given an interval newInterval = [start, end] that represents the start and end of another interval.

     Insert newInterval into intervals such that intervals is still sorted in ascending order by starti and
     intervals still does not have any overlapping intervals (merge overlapping intervals if necessary).

     Return intervals after the insertion.


     Example 1:

     Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
     Output: [[1,5],[6,9]]
     Example 2:

     Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
     Output: [[1,2],[3,10],[12,16]]
     Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].


     Constraints:

     0 <= intervals.length <= 104
     intervals[i].length == 2
     0 <= starti <= endi <= 105
     intervals is sorted by starti in ascending order.
     newInterval.length == 2
     0 <= start <= end <= 105
     */
    /*
     題意：給定一個不重疊的區間之集合 intervals，插入一個新的區間進入 intervals（如果需要的話請合併區間）
     */

    /*
      解法一：
        原來的intervals的數組已排序， 可分成下面三個步驟
        a. 把前面所有和newInterval沒有交集的都加到結果中
        b. 把和newInterval有交集的都合併到newInterval上
        c. 把最後和newInterval沒有交集的加到結果中
      Time Complexity: O(n)
      Space Complexity: O(1)
     */
    public static int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> list = new ArrayList<>();
        int i =0;
        // 1.若intervals的結束時間< new的起始時間，表示時間沒有重疊，直接加到result list裡
        while (i < intervals.length && intervals[i][1] < newInterval[0]) {
            list.add(intervals[i++]);
        }
        System.out.println("Front, Frank already judge no intersect i:" + i);

        // 2.若intervals的開始時間 < new的結束時間，表示兩者有交集，需要merge
        while (i < intervals.length && intervals[i][0] <= newInterval[1]) {
            //開始時間：取兩者開始時間的最小值 ; 結束時間：取兩者結束時間的最大值
            newInterval[0] = Math.min(intervals[i][0], newInterval[0]);
            newInterval[1] = Math.max(intervals[i][1], newInterval[1]);
            i++;
        }
        // 將合併後的時間區段加到result list裡
        list.add(newInterval);

        // 3. 將剩下interval裡不相交的，直接加入到result list裡，則完成
        while (i < intervals.length) {
            list.add(intervals[i++]);
        }

        return list.toArray(new int[list.size()][]);
    }

    public static void main(String[] args) {
        int[][] intervals = {{1, 2}, {3, 5}, {6, 7},{8, 10},{12, 16}, {18, 20}};
        int[] newInterval = {4, 8};

        int[][] result = insert(intervals, newInterval);
        for (int i=0; i<result.length; i++) {
            for(int j=0; j<result[i].length; j++) {//這此運用到取得第i列的元素個數
                System.out.println("result["+i+"]["+j+"]="+result[i][j]);
            }
        }
    }
}
