package com.frank.array._122_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/9/2
 *     title : 122. Best Time to Buy and Sell Stock II
 * </pre>
 */
public class Solution {
    
	/**
     Say you have an array for which the ith element is the price of a given stock on day i.

     Design an algorithm to find the maximum profit. You may complete as many transactions as you like
     (i.e., buy one and sell one share of the stock multiple times).

     Note: You may not engage in multiple transactions at the same time
     (i.e., you must sell the stock before you buy again).

     Example 1:
     Input: [7,1,5,3,6,4]
     Output: 7
     Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
     Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.

     Example 2:
     Input: [1,2,3,4,5]
     Output: 4
     Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
     Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
     engaging multiple transactions at the same time. You must sell before buying again.

     Example 3:
     Input: [7,6,4,3,1]
     Output: 0
     Explanation: In this case, no transaction is done, i.e. max profit = 0.
	*/
	// 題目:求買賣股票的最大值，不同於I, 可以無限次買入賣出

    /*
	  思路：這道題由於可以無限次買入和賣出。我們都知道炒股想掙錢當然是低價買入高價拋出，
	  那麼這裡我們只需要從第二天開始，如果當前價格比之前價格高，則把差值加入利潤中，
	  因為我們可以昨天買入，今日賣出，若明日價更高的話，還可以今日買入，明日再拋出。
	  以此類推，遍歷完整個數組後即可求得最大利潤
	 */

    private int maxProfit(int[] prices) {
        if (prices.length <= 1) return 0;
        int sum = 0;
        for (int i = 1; i < prices.length; ++i) {
            //若後面股價>前面股價
            if (prices[i] > prices[i - 1]) {
                sum += prices[i] - prices[i - 1];
            }
        }
        return sum;
    }

    public static void main(String[] args) {
    	Solution solution = new Solution();
        int[] sample = {7,1,5,3,6,4};
        
        int result = solution.maxProfit(sample);
        System.out.println("result:" + result);
    }
}
