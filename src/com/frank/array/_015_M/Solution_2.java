package com.frank.array._015_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/13
 *     title : 15. 3Sum
 * </pre>
 */
public class Solution_2 {
    /**
     Given an array nums of n integers, are there elements a, b, c in nums
     such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

     Note:
     The solution set must not contain duplicate triplets.

     Example:
     Given array nums = [-1, 0, 1, 2, -1, -4],

     A solution set is:
     [
     [-1, 0, 1],
     [-1, -1, 2]
     ]
     */
    //題意: 找出三個數相加為0的組合，並存成List回傳

    //思路：
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> resultList = new ArrayList<>();
        Arrays.sort(nums);

        for (int i=0;i< nums.length;i++) {
            // 去重
            if (i !=0 && nums[i] == nums[i-1]) {
                continue;
            }
            // -4,-1,-1,-1,0,1,1,2,2
            // 先找target
            int target = -nums[i];
            int left = i+1;
            int right = nums.length -1;

            while (left < right) {
                int sum = nums[left] + nums[right];
                if (target == sum) {
                    List<Integer> truple = Arrays.asList(nums[i], nums[left], nums[right]);
                    resultList.add(truple);
                    left++;
                    right--;
                    //去重複先前已掃過
                    while (left<right && nums[left] == nums[left-1]) {
                        // 表示下一個數跟前一個相同，那就無需再比較，直接再跳到下一個
                        left++;
                    }
                    while(left<right && nums[right] == nums[right+1]) {
                        right--;
                    }
                } else if (target > sum) {
                    left++;
                } else {
                    right--;
                }
            }
        }
        return resultList;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] sample = {-1,0,1,2,-1,-4}; // -4,-1,-1,-1,0,1,1,2,2
        System.out.println("Result :" + solution.threeSum(sample));
    }
}
