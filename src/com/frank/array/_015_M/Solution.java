package com.frank.array._015_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/16
 *     title : 15. 3Sum
 * </pre>
 */
public class Solution {
    
	/**
	 Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]]
	 such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

	 Notice that the solution set must not contain duplicate triplets.



	 Example 1:

	 Input: nums = [-1,0,1,2,-1,-4]
	 Output: [[-1,-1,2],[-1,0,1]]
	 Explanation:
	 nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0.
	 nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0.
	 nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0.
	 The distinct triplets are [-1,0,1] and [-1,-1,2].
	 Notice that the order of the output and the order of the triplets does not matter.
	 Example 2:

	 Input: nums = [0,1,1]
	 Output: []
	 Explanation: The only possible triplet does not sum up to 0.
	 Example 3:

	 Input: nums = [0,0,0]
	 Output: [[0,0,0]]
	 Explanation: The only possible triplet sums up to 0.
	 */
	
	/*
	 題意: 找到陣列中所有3個和為0的數，並且不能重複。
	 */

	/*
	 思路：該題可以轉化成Two Sum的思路去解決。先固定一個數，然後從陣列中剩下的數中查詢和為該數負值(target)得2個數，
	 則轉化成了Two Sum問題：先排序陣列，使兩個指標分別指向首尾的兩個數，如果這兩個數和等於target，則找到，
	 如果小於target則右移左指標，如果大於target則左移右指標。
	 */
	// Time : O(n^2), Space: O(n)
	// -1, 0, 1, 2, -1, -4 ->  -4, -1,-1,0,1,2
	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result = new ArrayList<>();
		Arrays.sort(nums);
		for (int i = 0; i < nums.length; i++) {
			// 用於去重, 第二個重複，不需要再跑一次
			if (i != 0 && nums[i] == nums[i - 1]) {
				continue;
			}

			// 轉化成Two Sum 問題
			int target = -nums[i];
			int left = i + 1;
			int right = nums.length - 1;
			// -4,-1,-1,0,1,2
			while (left < right) {
				int sum = nums[left] + nums[right];
				if (sum == target) {
					List<Integer> triplets = Arrays.asList(nums[i], nums[left], nums[right]);
					result.add(triplets);
					left++;
					right--;

					// 用於去重
					while (left < right && nums[left] == nums[left - 1]) {
						left++;
					}
					while (left < right && nums[right] == nums[right + 1]) {
						right--;
					}

				} else if (sum < target) {
					left++;
				} else {
					right--;
				}
			}
		}
		return result;
	}

	public List<List<Integer>> threeSumNew(int[] nums) {
		List<List<Integer>> result = new ArrayList<>();
		Arrays.sort(nums);
		int target = 0;

		if (nums.length < 3) return result;

		for (int i=0;i<nums.length;i++) {
			if (i > 0 && nums[i] == nums[i-1]) continue; // skip duplicates
			int j = i+1;
			int k = nums.length -1;

			while (j < k) {
				if (nums[i] + nums[j] + nums[k] < target) {
					j++;
					while (j<k && nums[j] == nums[j-1]) j++; // skip duplicates
				} else if (nums[i] + nums[j] + nums[k] > target) {
					k--;
					while (j<k && nums[k] == nums[k-1]) k--;// skip duplicates
				} else {
					result.add(Arrays.asList(nums[i], nums[j], nums[k]));
					j++;
					k--;
					while (j<k && nums[j] == nums[j-1]) j++; // skip duplicates
					while (j<k && nums[k] == nums[k-1]) k--;// skip duplicates
				}
			}
		}
		return result;
	}

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] sample = {-4, -4,-1, -1, 2, 3, 3}; // -4,-1,-1,-1,0,1,1,2,2
        System.out.println("Result :" + solution.threeSumNew(sample));
    }
}
