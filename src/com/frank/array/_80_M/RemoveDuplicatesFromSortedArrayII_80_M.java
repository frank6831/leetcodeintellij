package com.frank.array._80_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/10/27
 *     title  : 80. Remove Duplicates from Sorted Array II
 * </pre>
 */
public class RemoveDuplicatesFromSortedArrayII_80_M {
    /**
     Given an integer array nums sorted in non-decreasing order, remove some duplicates in-place such that each unique element appears at most twice.
     The relative order of the elements should be kept the same.

     Since it is impossible to change the length of the array in some languages, you must instead have the result be placed in the first part of the array nums.
     More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result.
     It does not matter what you leave beyond the first k elements.

     Return k after placing the final result in the first k slots of nums.

     Do not allocate extra space for another array. You must do this by modifying the input array in-place with O(1) extra memory.

     Custom Judge:

     The judge will test your solution with the following code:

     int[] nums = [...]; // Input array
     int[] expectedNums = [...]; // The expected answer with correct length

     int k = removeDuplicates(nums); // Calls your implementation

     assert k == expectedNums.length;
     for (int i = 0; i < k; i++) {
     assert nums[i] == expectedNums[i];
     }
     If all assertions pass, then your solution will be accepted.



     Example 1:

     Input: nums = [1,1,1,2,2,3]
     Output: 5, nums = [1,1,2,2,3,_]
     Explanation: Your function should return k = 5, with the first five elements of nums being 1, 1, 2, 2 and 3 respectively.
     It does not matter what you leave beyond the returned k (hence they are underscores).

     Example 2:
     Input: nums = [0,0,1,1,1,1,2,3,3]
     Output: 7, nums = [0,0,1,1,2,3,3,_,_]
     Explanation: Your function should return k = 7, with the first seven elements of nums being 0, 0, 1, 1, 2, 3 and 3 respectively.
     It does not matter what you leave beyond the returned k (hence they are underscores).


     Constraints:
     1 <= nums.length <= 3 * 104
     -104 <= nums[i] <= 104
     nums is sorted in non-decreasing order.
     */
    /*
     題意：給定一個已排序的int陣列,要求移除陣列內出現數超過兩次的元素,同時要求O(1)空間複雜度。
     對原始陣列進行原地修改，使得陣列中的每個元素最多只能出現兩次，同時要求傳回修改後的陣列的長度。
     */

    /*
      解法一：Two pointer
      只需要判斷目前元素是否和已歸置部分的倒數第二個元素相同進行取捨
      定義slow慢指針表示處理出的數組的長度，fast快指針表示已經檢查過的數組的長度，
      即nums[fast]表示待檢查的第一個元素 ，nums[slow−1] 為上一個應該被保留的元素所移動到的指定位置。
      1.對於長度不超過2的數組，無需進行任何處理，長度超過2的數組，直接將雙指針的初始值設為2即可。
      2.檢查上上個應該保留的元素nums[slow−2]是否和目前待檢查元素nums[fast] 相同。

      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    public static int removeDuplicates(int[] nums) {
        int n= nums.length;
        if (n <= 2) {
            return n;
        }

        int slow=2, fast=2;
        while (fast < n) {
            if (nums[slow-2] != nums[fast]) {
                nums[slow] = nums[fast];
                ++slow;
            }
            ++fast;
        }
        System.out.println("slow:" + slow + ",fast:" + fast);
        return slow;
    }

    /*
     解法：
      1.利用雙指標法遍歷數組，同時使用計數器統計重複元素的數量。
      2.如果重複元素的個數小於等於2，則將該元素複製到指標位置，並將指標向後移動。
      3.最後傳回非重複元素的個數。
     */
    public static int removeDuplicatesCount(int[] nums) {
        // 如果數組為空，則直接傳回0
        if (nums.length == 0) {
            return 0;
        }
        // 1,1,1,2,2,3
        // 初始化指標和計數器
        int p = 0; // 指向目前非重複元素的位置
        int count = 1; // 統計重複元素的數量

        // 遍歷數組，從第二個元素開始
        for (int j = 1; j < nums.length; j++) {
            // 如果目前元素與前一個元素相同
            if (nums[j] == nums[j - 1]) {
                // 增加重複元素的計數
                count++;
            } else {
                // 若目前元素與前一個元素不同，重置重複元素的計數為1
                count = 1;
            }

            // 如果重複元素的計數小於等於2，則將目前元素複製到指標位置，並將指標向後移動
            if (count <= 2) {
                nums[p] = nums[j];
                p++;
            }
            System.out.println("Arrays:" + Arrays.toString(nums) + ", j:" +j + ", count:" + count + ", p:" + p);
        }

        // 傳回非重複元素的數量
        return p + 1;
    }

    /*
    只需要和上一個元素作比對, 而不必對整個陣列作搜尋。
    1.發現同樣的元素時, 則把 flag 改為 true,
    2.若 flag 已經為 true, 則代表元素已經出現超過兩次。可以跳過該元素。
     */
    public static int removeDuplicatesCountCount2(int[] nums) {
        int size = nums.length;
        if (size <= 2) {
            return size;
        }

        int count = 1; // 統計重複元素的數量
        boolean hit = false;
        for(int i = 1; i < size; i++){
            if (nums[count-1] == nums[i]){
                if (hit) {
                    continue;
                }
                hit = true;
            } else {
                hit = false;
            }
            nums[count] = nums[i];
            count++;
            System.out.println("Arrays:" + Arrays.toString(nums) + ", i:" +i + ", hit:" + hit + ", count:" + count);
        }
        System.out.println("count:" + count);
        return count;
    }


    /*
        解法：
        1.使用了一個指針 p，它指向第一個無效的位置，即在這之前的元素都是有效的
        2.遍歷整個陣列，對於每個元素，如果它不等於nums[p-2]，將其存放到 nums[p] 的位置
          並將p往後移動一位。
        3.最後返回p的值，即修改後的陣列的長度。
        Time Complexity: O(n)
        Space Complexity: O(1)
     */
    private static int removeDuplicatesShort(int[] nums) {
        // p指向第一個無效的位置
        int p=2;
        // 從第三個元素開始遍歷
        for (int i=2; i< nums.length;i++) {
            // 如果當前元素不等於p-2位置的元素（表示該元素最多只能出現兩次）
            if (nums[i] != nums[p-2]) {
                // 將當前元素存放到p位置，並將p向後移動一位
                nums[p] = nums[i];
                p++;
            }
        }
        // 返回p的值，即修改後的陣列的長度
        return p;
    }

    public static void main(String[] args) {
        int[] nums = {1,1,1,2,2,3};

        int size = removeDuplicatesCount(nums);
        System.out.println("size:" + size);
        System.out.println("arrays:" + Arrays.toString(nums));
    }
}
