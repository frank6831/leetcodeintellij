package com.frank.array._283_E;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/18
 *     title : 283. Move Zeroes
 * </pre>
 */
public class Solution {
    
	/**
	Given an array nums, write a function to move all 0's to the end of it 
	while maintaining the relative order of the non-zero elements.

	Example:
	
	Input: [0,1,0,3,12]
	Output: [1,3,12,0,0]
	Note:
	
	You must do this in-place without making a copy of the array.
	Minimize the total number of operations.
	*/
	// 題目意思：輸入為一個陣列，要將該陣列中所有為0的元素移動至最後面進行存放
	/*
	  思路：
	  使用Two pointer進行實現，將兩個雙指標索引放在起始的位置，
	  並利用其中一個指標進行偵測，若遇到0則向右移一格，若不是則將兩個指標索引之值進行互換。
	 1.將兩個指標索引放置於頭的位置(int i = 0, j = 0;)。
	 2.利用其中一個指標索引進行偵測，若遇到0則向右移一格(if (nums[j] == 0))。
	 3.若偵測到不為0，則將兩指標索引之值進行互換，並且將兩個指標索引向右移一格(程式中else的部分)。
	 */
	public void moveZeroes(int[] nums) {
		int i = 0, j = 0;
		int tmp;

		while (j < nums.length) {
			if (nums[j] != 0) {
				tmp = nums[j];
				nums[j] = nums[i];
				nums[i] = tmp;
				i++;
			}
			j++;
		}
    }

	/*
	1.用一個 pointer 去記錄可以 swap 的第一個 0
	2.若遇到元素 nums[i] 不為 0 時
	  @判斷 pointer 及 i 是不是一樣
	   #如果 一樣 ，就不需要 swap (沒有自己在跟自己交換的)
	   #如果 不一樣，就需要 swap，把現在這個 int 跟 pointer上記住的 0 交換
	  @pointer++ ，再找尋下一個需要交換的 0
	 */

	public void moveZeroes2(int[] nums) {
		if (nums == null || nums.length == 0) return;

		int cur = 0;
		for (int i=0; i< nums.length;i++) {
			System.out.println("i= "+ i + ",cur:" + cur);
			if (nums[i] != 0) {
				System.out.println("i= "+ i + ",nums[i]:" + nums[i]);
				int tmp = nums[cur];
				nums[cur] = nums[i];
				nums[i] = tmp;
				cur++;
				System.out.println(Arrays.toString(nums));
			}
		}
	}


	public void moveZeroesNew(int[] nums) {
		int i = 0; // i 紀錄目前非零元素要存放的位置
		for (int num : nums) {
			if (num != 0) {
				// 如果當前元素不為零，則將其放到 i 指向的位置，並將 i 往後移動一位
				nums[i] = num;
				i++;
			}
		}
		// 將 i 之後的位置全部設為 0
		while (i < nums.length) {
			nums[i++] = 0;
		}
	}


	public static void main(String[] args) {
    	Solution solution = new Solution();
        int sample[] = {0,1,0,3,12};
        
        solution.moveZeroes2(sample);
        System.out.println(Arrays.toString(sample));
    }
}
