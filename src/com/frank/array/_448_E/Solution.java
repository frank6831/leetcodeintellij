package com.frank.array._448_E;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/18
 *     title : 448. Find All Numbers Disappeared in an Array
 * </pre>
 */
public class Solution {
    
	/**
	Given an array of integers where 1 ≤ a[i] ≤ n (n = size of array), some elements appear 
	twice and others appear once.

	Find all the elements of [1, n] inclusive that do not appear in this array.
	
	Could you do it without extra space and in O(n) runtime? 
	You may assume the returned list does not count as extra space.
	
	Example:
	Input:
	[4,3,2,7,8,2,3,1]
	
	Output:
	[5,6]
	*/
	// 題目意思：不使用額外的空間找出數組中缺少的數字，有幾個缺少的就有幾個重複的
	/*
	  思路：
	 */
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> ret = new ArrayList<>();
        // 不能使用list，有重复元素，容易超时
        Set<Integer> temp = new HashSet<>();
        for (int i : nums) {
            temp.add(i);
        }
        for (int i = 1; i <= nums.length; i++) {
            if (!temp.contains(i))
                ret.add(i);
        }
        return ret;
    }

	
	public List<Integer> findDisappearedNumbers2(int[] nums) {
        List<Integer> result = new ArrayList<Integer>();
        if(nums==null || nums.length==0) return result;
        for (int i=0; i<nums.length; i++) {
            int index = Math.abs(nums[i])-1;
            if(nums[index]<0) continue;
            nums[index] = -nums[index];
        }
        for (int i=0; i<nums.length; i++) {
            if (nums[i]>0) {
                result.add(i+1);
            }
        }
        return result;
    }
	
    public static void main(String[] args) {
    	Solution solution = new Solution();
        int sample[] = {4,3,2,7,8,2,3,1};
        
        List<Integer> resultList = solution.findDisappearedNumbers2(sample);
        System.out.println(resultList.toString());
    }
}
