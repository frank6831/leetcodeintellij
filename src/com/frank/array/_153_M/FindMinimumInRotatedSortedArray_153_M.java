package com.frank.array._153_M;


/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/9
 *     title  : 153. Find Minimum in Rotated Sorted Array
 * </pre>
 */
public class FindMinimumInRotatedSortedArray_153_M {
    /**
     Suppose an array of length n sorted in ascending order is rotated between 1 and n times.
     For example, the array nums = [0,1,2,4,5,6,7] might become:

     [4,5,6,7,0,1,2] if it was rotated 4 times.
     [0,1,2,4,5,6,7] if it was rotated 7 times.
     Notice that rotating an array [a[0], a[1], a[2], ..., a[n-1]] 1 time results in the array
     [a[n-1], a[0], a[1], a[2], ..., a[n-2]].

     Given the sorted rotated array nums of unique elements, return the minimum element of this array.

     You must write an algorithm that runs in O(log n) time.



     Example 1:
     Input: nums = [3,4,5,1,2]
     Output: 1
     Explanation: The original array was [1,2,3,4,5] rotated 3 times.

     Example 2:
     Input: nums = [4,5,6,7,0,1,2]
     Output: 0
     Explanation: The original array was [0,1,2,4,5,6,7] and it was rotated 4 times.

     Example 3:
     Input: nums = [11,13,15,17]
     Output: 11
     Explanation: The original array was [11,13,15,17] and it was rotated 4 times.


     Constraints:

     n == nums.length
     1 <= n <= 5000
     -5000 <= nums[i] <= 5000
     All the integers of nums are unique.
     nums is sorted and rotated between 1 and n times.
     */
    /*
     題意：尋找旋轉有序數組(數組的值不重複)的最小值
     */


    /*
      解法一：Binary search
      1.數組是被旋轉過的，所以最小值一定會在最大值的左邊
      2.先找到中間的數mid，若mid比最右邊的數還大，最小值一定在mid右邊，否則最小值在mid左邊或者mid本身
      3.不斷縮小搜索範圍，直到左右指針相遇，剩下的數即為最小值
      Time Complexity: O(log n)
      Space Complexity: O(1)
     */
    public int findMin(int[] nums) {
        int left = 0;
        int right = nums.length -1;

        while (left < right) {
            int mid = left + (right - left )/2;
            System.out.println("mid:" + mid);
            if (nums[mid] > nums[right]) {  // 如果中間的數比最右邊的數大，則最小值應該在mid右邊
                left = mid + 1;
            } else {  // 否則最小值應該在mid左邊或mid本身
                right = mid;
            }
        }
        return nums[left];
    }

    public static void main(String[] args) {
        FindMinimumInRotatedSortedArray_153_M solution = new FindMinimumInRotatedSortedArray_153_M();
        int[] nums = {2,4,5,6,7,0,1};
        //4,5,6,7,0,1,2
        // 5,6,7,0,1,2,4
        // 0,1,2,4,5,6,7
        // 6,7,0,1,2,4,5
        //2,4,5,6,7,0,1
        //1,2,4,5,6,7,0

        int minValue = solution.findMin(nums);
        System.out.println("minValue:" + minValue);
    }
}
