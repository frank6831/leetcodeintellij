package com.frank.array._49_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2024/1/16
 *     title  : 49. Group Anagrams
 * </pre>
 */
public class GroupAnagrams_49_M {
    /**
     Given an array of strings strs, group the anagrams together. You can return the answer in any order.

     An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
     typically using all the original letters exactly once.


     Example 1:
     Input: strs = ["eat","tea","tan","ate","nat","bat"]
     Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

     Example 2:
     Input: strs = [""]
     Output: [[""]]

     Example 3:
     Input: strs = ["a"]
     Output: [["a"]]


     Constraints:
     1 <= strs.length <= 104
     0 <= strs[i].length <= 100
     strs[i] consists of lowercase English letters.
     */
    /*
     題意：給定多個字串，並將其分類。只要字串所包含的字元完全一樣就算同個group，不考慮順序。
     */

    /*
      解法：使用HashMap
      1.將每個字串轉乘char陣列後排序，同個group字串，排序後必然會相同，以此做為map的key
      2.一一比對，相同的加到同個list裡
      Time Complexity: O(nlong(k)), n是str中字串的數量，k是strs中字串的最大長度
      需要遍歷 n個字串，對於每個字串，需要 O(klogk)的時間進行排序以及O(1)的時間更新map
      Space Complexity: O(nk)
     */
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();

        for (int i=0; i< strs.length;i++) {
            char[] curr = strs[i].toCharArray();
            Arrays.sort(curr);
            String key = new String(curr);
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }
            map.get(key).add(strs[i]);
        }
        return new ArrayList<>(map.values());
    }

    public static void main(String[] args) {
        GroupAnagrams_49_M solution = new GroupAnagrams_49_M();
        String[] array = {"eat","tea","tan","ate","nat","bat"};

        List<List<String>> result = solution.groupAnagrams(array);
        for (List<String> innerList : result) {
            for (String element : innerList) {
                System.out.print(element + " ");
            }
            System.out.println(); // 換行，區分每個內部列表
        }
    }
}
