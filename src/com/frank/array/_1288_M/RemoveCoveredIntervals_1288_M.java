package com.frank.array._1288_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/3
 *     title  : 1288. Remove Covered Intervals
 * </pre>
 */
public class RemoveCoveredIntervals_1288_M {
    /**
     Given an integer array nums sorted in non-decreasing order,
     return an array of the squares of each number sorted in non-decreasing order.


     Example 1:
     Input: nums = [-4,-1,0,3,10]
     Output: [0,1,9,16,100]
     Explanation: After squaring, the array becomes [16,1,0,9,100].
     After sorting, it becomes [0,1,9,16,100].

     Example 2:
     Input: nums = [-7,-3,2,3,11]
     Output: [4,9,9,49,121]

     Constraints:

     1 <= nums.length <= 104
     -104 <= nums[i] <= 104
     nums is sorted in non-decreasing order.

     Follow up: Squaring each element and sorting the new array is very trivial,
     could you find an O(n) solution using a different approach?
     */
    /*
     題意：給定一個二維整數陣列 intervals，其中 intervals[i] = [li, ri] 代表一個區間 [li, ri)，
          要求移除所有被其他區間完全包含的區間，最後返回剩餘區間的數量。
          一個區間 [a, b] 被另一個區間 [c, d] 完全包含當且僅當 c <= a 且 b <= d。
     */


    /*
      解法一：暴力解
      跑兩個迴圈，一一拿出來比較，依照題目給的條件去比較(if c <= a and b <= d)
      表示[a,b]被[c,d]包括
      Time Complexity: O(n^2)
      Space Complexity: O(n)
     */
    public int removeCoveredIntervals(int[][] intervals) {
        int delete=0;

        // {{1, 4}, {2, 8}, {3,6}}
        for (int i=0;i<intervals.length;i++) {
            for (int j=0;j<intervals.length;j++) {
                if (i==j) {continue;}
                // 前面的left >= 後面的left  && 前面的right <= 後面的right
                if (intervals[i][0] >= intervals[j][0] &&
                    intervals[i][1] <= intervals[j][1]){
                    delete++;
                    break;
                }
            }
        }
        return intervals.length - delete;
    }


    /*
      解法一：
      1.先將區間按照起點升序排列，若起點相同則結尾升序排列
      2.每次處理一個區間時，只需要檢查它的結尾值是否大於當前最右端的結尾值即可
      3.若是，更新最右端的結尾值，count加1。若不是，表示該區間已被其他區間完全包含，直接跳過即可。
      4.最後，回傳count值

      Time Complexity: O(nlogn)
      Space Complexity: O(n)
     */
    public int removeCoveredIntervalsBySort(int[][] intervals) {
        // 將區間按照起點升序排列，若起點相同則結尾升序排列
        Arrays.sort(intervals, (a, b) -> (a[0] == b[0] ? b[1] - a[1] : a[0] - b[0]));
        int count = 0, cur = 0;
        for (int[] interval : intervals) {
            System.out.println("interval[1]:" + interval[1] + ", cur:" + cur);
            if (cur < interval[1]) { // 如果當前區間的結尾值大於當前最右端的結尾值
                cur = interval[1];// 更新最右端的結尾值
                count++;// 計數器加 1
            }
        }
        return count;
    }

    public static void main(String[] args) {
        RemoveCoveredIntervals_1288_M solution = new RemoveCoveredIntervals_1288_M();
        int[][] nums = {{1, 4}, {2, 8}, {3,6}};
        int[][] nums2 = {{1, 6}, {4, 6}, {4,8}};

        int count = solution.removeCoveredIntervals(nums);
        System.out.println("count:" + count);
    }
}
