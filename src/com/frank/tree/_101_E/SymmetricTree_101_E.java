package com.frank.tree._101_E;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/07/05
 *     title  : 101. Symmetric Tree
 * </pre>
 */
public class SymmetricTree_101_E {
    /**
     Given the root of a binary tree, check whether it is a mirror of itself
     (i.e., symmetric around its center).

     Example 1:
     Input: root = [1,2,2,3,4,4,3]
     Output: true

     Example 2:
     Input: root = [1,2,2,null,3,null,3]
     Output: false

     Constraints:

     The number of nodes in the tree is in the range [1, 1000].
     -100 <= Node.val <= 100

     Follow up: Could you solve it both recursively and iteratively?
     */
    // 題意：給一個二元樹，判斷他是不是鏡像樹(從root為中心，左邊跟右邊為鏡像)。

    /*
    思路：
    遞迴的方法實現比較左右子樹的左右子樹是否對稱，大概的概念就是左節點的left會等於右節點的right，
    左節點的right會等於右節點的left
     */
    public boolean isSymmetric(TreeNode root) {
        return root == null || isSymmetricInternal(root.left, root.right);
    }

    private boolean isSymmetricInternal(TreeNode left, TreeNode right) {
        if (left == null || right == null) {
            return left == right;
        }
        if (left.val != right.val) {
            return false;
        }

        return isSymmetricInternal(left.left, right.right) &&
                isSymmetricInternal(left.right, right.left);
    }

    public static void main(String[] args) {
        SymmetricTree_101_E solution = new SymmetricTree_101_E();
        //root = [3,9,20,null,null,15,7]
        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(2);
        TreeNode node4 = new TreeNode(3);
        TreeNode node5 = new TreeNode(4);
        TreeNode node6 = new TreeNode(4);
        TreeNode node7 = new TreeNode(3);

        node1.left = node2;
        node1.right = node3;

        node2.left = node4;
        node2.right = node5;

        node3.left = node6;
        node3.right = node7;

        System.out.println("Result:" + solution.isSymmetric(node1));
    }
}
