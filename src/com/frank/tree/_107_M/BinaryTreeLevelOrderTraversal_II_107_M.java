package com.frank.tree._107_M;

import com.frank.tree.TreeNode;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/17
 *     title  : 107. Binary Tree Level Order Traversal II
 * </pre>
 */
public class BinaryTreeLevelOrderTraversal_II_107_M {
    /**
     Given the root of a binary tree, return the bottom-up level order traversal of its nodes' values.
     (i.e., from left to right, level by level from leaf to root).


     Example 1:


     Input: root = [3,9,20,null,null,15,7]
     Output: [[15,7],[9,20],[3]]
     Example 2:

     Input: root = [1]
     Output: [[1]]
     Example 3:

     Input: root = []
     Output: []


     Constraints:

     The number of nodes in the tree is in the range [0, 2000].
     -1000 <= Node.val <= 1000
     */

    // 題意：給一個二元樹，從底部往上地按照階層遍歷回傳它每個節點的值


    /*思路：
    1.建立一個queue，先把根節點放進去，找根節點的左右兩個子節點，這時候去掉根節點，
    2.此時queue裡的元素就是下一層的所有節點，用一個for 循環遍歷它們，
    3.然後存到一個一維向量裡，遍歷完之後再把這個一維向量存到二維向量裡，以此類推
    4.最後，使用collections.reverse反轉排序，就可得到答案
      Time Complexity: O(N^2)
      Space Complexity: O(N)
     */
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        if (root == null) return new ArrayList<>();

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        List<List<Integer>> res = new ArrayList<>();

        while(!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> level = new ArrayList<>();
            for (int i=0;i< size;i++) {
                TreeNode current = queue.poll();
                level.add(current.val);
                if (current.left != null) queue.add(current.left);
                if (current.right != null) queue.add(current.right);
            }
            res.add(0, level);
        }
        //Collections.reverse(res);
        return res;
    }



    public static void main(String[] args) {
        BinaryTreeLevelOrderTraversal_II_107_M solution = new BinaryTreeLevelOrderTraversal_II_107_M();
        //[1,2,3,4,5]

        TreeNode node3 = new TreeNode(3);
        TreeNode node7 = new TreeNode(7);
        TreeNode node9 = new TreeNode(9);
        TreeNode node20 = new TreeNode(20);
        TreeNode node15 = new TreeNode(15);


        node3.left = node9;
        node3.right = node20;

        node20.left = node15;
        node20.right = node7;
        List<List<Integer>> levelOrderBottom = solution.levelOrderBottom(node3);
        System.out.println("The result: " + levelOrderBottom);
    }
}
