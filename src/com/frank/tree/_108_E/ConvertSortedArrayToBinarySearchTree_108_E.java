package com.frank.tree._108_E;

import com.frank.linkedlist._21_E.MergeTwoSortedLists_21_E;
import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/01/12
 *     title  : 108. Convert Sorted Array to Binary Search Tree
 * </pre>
 */
public class ConvertSortedArrayToBinarySearchTree_108_E {
    /**
     Given an integer array nums where the elements are sorted in ascending order,
     convert it to a height-balanced binary search tree.

     Example 1:
     Input: nums = [-10,-3,0,5,9]
     Output: [0,-3,9,-10,null,5]
     Explanation: [0,-10,5,null,-3,null,9] is also accepted:

     Example 2:
     Input: nums = [1,3]
     Output: [3,1]
     Explanation: [1,null,3] and [3,1] are both height-balanced BSTs.

     Constraints:

     1 <= nums.length <= 104
     -104 <= nums[i] <= 104
     nums is sorted in a strictly increasing order.
     */
    // 題意：給定一個陣列，其元素以升冪排序(也就是後面的一定比前面大)，試將其轉成一個高度平衡的二元搜尋樹。
    // Time complexity : O(n) , Space:O(n)
    /*
    思路：將陣列的中間值設為該節點的值，然後用遞迴方式，剩下陣列的左半邊建立左子樹，右半邊建立右子樹。
    1. 檢查陣列是否是空的或者null，是的話直接回傳
    2. 呼叫一個用來遞迴建立BST的函式，這邊命名成getNode()
    3. getNode會輸入陣列以及當下的左右邊界(nums, l, r)
       (一開始l = 0, r = N-1, N是nums的總數)
    4. 每次先檢查左右邊界是否黃金交叉(表示已經完成了，可以回傳null)
    5. 讓mid = l加r的一半，直接建立一個root節點
    6. root節點的左節點就應該是呼叫getNode(nums, l, mid-1)得到的結果
    7. root節點的右節點則會是getNode(nums, mid+1, r)得到的結果
    8. 回傳root

     */

    public TreeNode sortedArrayToBST(int[] nums) {
        if (nums == null || nums.length == 0) return null;
        return getTreeNode(nums, 0, nums.length -1);
    }

    private TreeNode getTreeNode(int[] nums, int start, int end) {
        if (start > end) return null;

        int mid = start + (end - start) /2;
        TreeNode root = new TreeNode(nums[mid]);
        System.out.println("nums[mid]:" + nums[mid]);
        System.out.println("root val:" + root.val);
        //用遞迴分別建立root的左右子樹
        root.left = getTreeNode(nums, start, mid - 1);
        root.right = getTreeNode(nums, mid + 1, end);
        return root;
    }

    public static void main(String[] args) {
        ConvertSortedArrayToBinarySearchTree_108_E solution = new ConvertSortedArrayToBinarySearchTree_108_E();
        int[] array = {-10,-3,0,5,9};
        TreeNode result = solution.sortedArrayToBST(array);

        System.out.println("Result:" + result.val);
    }
}
