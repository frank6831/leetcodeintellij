package com.frank.tree._236_M;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/29
 *     title  : 235. Lowest Common Ancestor of a Binary Search Tree
 * </pre>
 */
public class LowestCommonAncestorOfABinaryTree_236_M {
    /**
     Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.

     According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q
     as the lowest node in T that has both p and q as descendants
     (where we allow a node to be a descendant of itself).”



     Example 1:
     Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
     Output: 3
     Explanation: The LCA of nodes 5 and 1 is 3.

     Example 2:
     Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
     Output: 5
     Explanation: The LCA of nodes 5 and 4 is 5, since a node can be a descendant of
     itself according to the LCA definition.

     Example 3:
     Input: root = [1,2], p = 1, q = 2
     Output: 1


     Constraints:

     The number of nodes in the tree is in the range [2, 105].
     -109 <= Node.val <= 109
     All Node.val are unique.
     p != q
     p and q will exist in the tree.
     */

    // 題意：找二元樹中兩個節點的最低共同祖先(LCA)，也就是要找出兩個點的共同上層。

    /*思路：
      1.只要當前根節點是p和q中的任意一個，就返回root
      2.如果left 和 right都不為空，說明此時root就是最近公共節點
      3.若left不為空，表示p和q在左子樹里面，反之亦然
      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // 只要當前根節點是p和q中的任意一個，就返回
        if (root == null || root == p || root == q) {
            return root;
        }

        TreeNode left = lowestCommonAncestor(root.left, p,q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);

        // 如果left 和 right都不為空，說明此時root就是最近公共節點
        if (left != null && right != null) {
            return root;
        }

        if (left !=null) {
            //說明p和q在左子樹里面
            return left;
        } else {
            //說明p和q在右子樹里面
            return right;
        }
    }


    public static void main(String[] args) {
        LowestCommonAncestorOfABinaryTree_236_M solution = new LowestCommonAncestorOfABinaryTree_236_M();
        //[6,2,8,0,4,7,9,null,null,3,5]
        //[3,5,1,6,2,0,8,null,null,7,4]

        TreeNode node0 = new TreeNode(0);
        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);
        TreeNode node6 = new TreeNode(6);
        TreeNode node7 = new TreeNode(7);
        TreeNode node8 = new TreeNode(8);
        TreeNode node9 = new TreeNode(9);


        node3.left = node5;
        node3.right = node1;

        node1.left = node0;
        node1.right = node8;

        node5.left = node6;
        node5.right = node2;

        node2.left = node7;
        node2.right = node4;

        TreeNode result = solution.lowestCommonAncestor(node3,node7,node0);
        System.out.println("The result val: " + result.val);
    }
}
