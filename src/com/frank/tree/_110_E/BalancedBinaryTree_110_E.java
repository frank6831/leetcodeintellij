package com.frank.tree._110_E;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/28
 *     title  : 110. Balanced Binary Tree
 * </pre>
 */
public class BalancedBinaryTree_110_E {
    /**
     Given a binary tree, determine if it is height-balanced.


     Example 1:


     Input: root = [3,9,20,null,null,15,7]
     Output: true
     Example 2:


     Input: root = [1,2,2,3,3,null,null,4,4]
     Output: false
     Example 3:

     Input: root = []
     Output: true


     Constraints:

     The number of nodes in the tree is in the range [0, 5000].
     -104 <= Node.val <= 104
     */

    //題意：給定一個binary tree，去determine是否高度平衡，兩個subtrees的高度相差沒有超過1
    //ref: https://medium.com/data-science-for-kindergarten/leetcode-110-balanced-binary-tree-86b66716ecc2

    /*解法一： Recursion O(nlogn)
    在 isBalanced 中，我們不僅要判斷 heightDiff <= 1，同時也要確認左右兩棵子樹也都是 balanced。
    执行耗时:1 ms,击败了99.19% 的Java用户
	内存消耗:44 MB,击败了71.83% 的Java用户
     */
    public boolean isBalanced(TreeNode root) {
        if (root == null) return true;

        int heightDiff = Math.abs(height(root.left) - height(root.right));
        return (heightDiff <=1) && isBalanced(root.left) && isBalanced(root.right);
    }

    private int height(TreeNode root) {
        if (root == null) return 0;
        return Math.max(height(root.left), height(root.right)) + 1;
    }

    /*
    只要有一不 balanced 就回傳 -1，將時間複雜度減為 O(N)
    执行耗时:1 ms,击败了99.19% 的Java用户
	内存消耗:44.9 MB,击败了14.56% 的Java用户
     */
    public boolean isBalanced2(TreeNode root) {
        return level(root) != -1;
    }

    private int level(TreeNode root) {
        if (root == null) return 0;

        int leftHeight = level(root.left);
        if (leftHeight == -1) return -1;
        int rightHeight = level(root.right);
        if (rightHeight == -1) return -1;

        if (Math.abs(leftHeight - rightHeight) > 1) return -1;
        return Math.max(leftHeight, rightHeight) + 1;
    }

    public static void main(String[] args) {
        BalancedBinaryTree_110_E solution = new BalancedBinaryTree_110_E();
        // [3,9,20,null,null,15,7]
        TreeNode nodeA = new TreeNode(3);
        TreeNode nodeB = new TreeNode(9);
        TreeNode nodeC = new TreeNode(20);

        TreeNode nodeD = new TreeNode(15);
        TreeNode nodeE = new TreeNode(7);

        nodeA.left = nodeB;
        nodeA.right = nodeC;

        nodeC.left = nodeD;
        nodeC.right = nodeE;

        System.out.println(solution.isBalanced(nodeA));
    }
}
