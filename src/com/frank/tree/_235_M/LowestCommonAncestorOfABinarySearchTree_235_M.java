package com.frank.tree._235_M;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/29
 *     title  : 235. Lowest Common Ancestor of a Binary Search Tree
 * </pre>
 */
public class LowestCommonAncestorOfABinarySearchTree_235_M {
    /**
     Given a binary search tree (BST), find the lowest common ancestor (LCA) node of two given nodes in the BST.

     According to the definition of LCA on Wikipedia: "The lowest common ancestor is defined between
     two nodes p and q as the lowest node in T that has both p and q as descendants
     (where we allow a node to be a descendant of itself)."


     Example 1:


     Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
     Output: 6
     Explanation: The LCA of nodes 2 and 8 is 6.
     Example 2:


     Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
     Output: 2
     Explanation: The LCA of nodes 2 and 4 is 2, since a node can be a descendant of
     itself according to the LCA definition.
     Example 3:

     Input: root = [2,1], p = 2, q = 1
     Output: 2


     Constraints:

     The number of nodes in the tree is in the range [2, 105].
     -109 <= Node.val <= 109
     All Node.val are unique.
     p != q
     p and q will exist in the BST.
     */

    // 題意：給定一個二元搜尋樹及其中兩個不同的節點，試找出它們的最近共同祖先(LCA)。

    /*思路：
      1.若root的值小於兩個目標節點(表示此節點非最近共同祖先),就向右搜索，
      2.若root的值大於兩個目標節點(表示此節點非最近共同祖先),就向左搜索，
      3.當root的值介於兩個目標節點之間,則代表我們找到了最近共同祖先。
      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return null;
        if ((root.val - p.val) * (root.val - q.val) < 1) return root;
        if (p.val < root.val) {
            return lowestCommonAncestor(root.left, p, q);
        } else {
            return lowestCommonAncestor(root.right, p , q);
        }
    }

    public TreeNode lowestCommonAncestor2(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return null;
        if (root.val > p.val && root.val > q.val) {
            return lowestCommonAncestor(root.left, p, q);
        } else if (root.val < p.val && root.val < q.val) {
            return lowestCommonAncestor(root.right, p, q);
        } else {
            return root;
        }
    }

    public static void main(String[] args) {
        LowestCommonAncestorOfABinarySearchTree_235_M solution = new LowestCommonAncestorOfABinarySearchTree_235_M();
        //[6,2,8,0,4,7,9,null,null,3,5]

        TreeNode root = new TreeNode(6);
        TreeNode node2 = new TreeNode(2);
        TreeNode node8 = new TreeNode(8);

        TreeNode node0 = new TreeNode(0);
        TreeNode node4 = new TreeNode(4);
        TreeNode node7 = new TreeNode(7);
        TreeNode node9 = new TreeNode(9);
        TreeNode node3 = new TreeNode(3);
        TreeNode node5 = new TreeNode(5);

        root.left = node2;
        root.right = node8;

        node2.left = node0;
        node2.right = node4;

        node8.left = node7;
        node8.right = node9;

        node4.left = node3;
        node4.right = node5;

        TreeNode result = solution.lowestCommonAncestor2(root,node2,node4);
        System.out.println("The result val: " + result.val);
    }
}
