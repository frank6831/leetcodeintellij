package com.frank.tree._103_M;

import com.frank.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/17
 *     title  : 103. Binary Tree Zigzag Level Order Traversal
 * </pre>
 */
public class BinaryTreeZigzagLevelOrderTraversal_103_M {
    /**
     Given the root of a binary tree, return the zigzag level order traversal of its nodes' values.
     (i.e., from left to right, then right to left for the next level and alternate between).


     Example 1:

     Input: root = [3,9,20,null,null,15,7]
     Output: [[3],[20,9],[15,7]]
     Example 2:

     Input: root = [1]
     Output: [[1]]
     Example 3:

     Input: root = []
     Output: []


     Constraints:

     The number of nodes in the tree is in the range [0, 2000].
     -100 <= Node.val <= 100
     */

    // 題意：給一個二元樹，搜集每一層的數值，每隔一層搜集順序就需要反過來
    // 可以用 Breadth-First Search 來解決問題，唯一需要注意的地方就是每隔一層就要把取得的結果反過來以符合他的題意


    /*思路：
    1.建立一個queue，先把根節點放進去，找根節點的左右兩個子節點，這時候去掉根節點
    2.此時queue裡的元素就是下一層的所有節點，已迴圈遍歷，並用一個boolean值決定該階層是由左至右還是由右至左
    3.然後存到一個一維向量裡，遍歷完之後再把這個一維向量存到二維向量裡，以此類推，即可完成

      Time Complexity: O(N^2)
      Space Complexity: O(N)
     */
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        if (root == null) return new ArrayList<>();

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        List<List<Integer>> res = new ArrayList<>();
        boolean left2Right = true;
        while (!queue.isEmpty()) {
            int size = queue.size();
            ArrayList<Integer> level = new ArrayList<>();
            for (int i=0; i< size;i++) {
                TreeNode curr = queue.poll();
                if (left2Right) {
                    level.add(curr.val);
                } else {
                    level.add(0, curr.val);
                }
                if (curr.left != null) queue.add(curr.left);
                if (curr.right != null) queue.add(curr.right);
            }
            res.add(level);
            left2Right = !left2Right;
        }
        return res;
    }


    public static void main(String[] args) {
        BinaryTreeZigzagLevelOrderTraversal_103_M solution = new BinaryTreeZigzagLevelOrderTraversal_103_M();
        //[3,9,20,null,null,15,7]

        TreeNode node3 = new TreeNode(3);
        TreeNode node7 = new TreeNode(7);
        TreeNode node9 = new TreeNode(9);
        TreeNode node20 = new TreeNode(20);
        TreeNode node15 = new TreeNode(15);

        node3.left = node9;
        node3.right = node20;

        node20.left = node15;
        node20.right = node7;

        List<List<Integer>> result = solution.zigzagLevelOrder(node3);
        System.out.println("The result: " + result);
    }
}
