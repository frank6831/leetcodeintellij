package com.frank.tree._572_E;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/13
 *     title  : 572. Subtree of Another Tree
 * </pre>
 */
public class SubtreeOfAnotherTree_572_E {
    /**
     Given the roots of two binary trees root and subRoot, return true if there is a subtree of root with the same structure
     and node values of subRoot and false otherwise.

     A subtree of a binary tree tree is a tree that consists of a node in tree and all of this node's descendants.
     The tree tree could also be considered as a subtree of itself.


     Example 1:


     Input: root = [3,4,5,1,2], subRoot = [4,1,2]
     Output: true
     Example 2:


     Input: root = [3,4,5,1,2,null,null,null,null,0], subRoot = [4,1,2]
     Output: false


     Constraints:

     The number of nodes in the root tree is in the range [1, 2000].
     The number of nodes in the subRoot tree is in the range [1, 1000].
     -104 <= root.val <= 104
     -104 <= subRoot.val <= 104
     */

    // 題意：實作演算法去察看 subRoot 是不是 root 的 subTree
    // 100 SameTree的變形題
    /* 困難點:
      1.要明白某一個樹是另一個樹的 subTree 的定義
      2.要理解怎麼去檢驗兩樹相等
    */

    /*思路：
      從s的某個結點開始，跟t的所有結構都一樣，那麼問題就轉換成了判斷兩棵樹是否相同，也就是Same Tree的問題了.
      先從s的根結點開始，跟t比較，如果兩棵樹完全相同，那麼返回true，否則就分別對s的左子結點和右子結點調用遞歸再次來判斷是否相同，
      只要有一個返回true了，就表示可以找得到。

      Time Complexity: O(M*N)
      Space Complexity: O(M+N)
     */
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s == null) {
            return false;
        }
        if (isSameTree(s, t)) {
            return true;
        }
        return isSubtree(s.left, t) || isSubtree(s.right, t);
    }


    private boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }

        return (p.val == q.val) &&isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    public static void main(String[] args) {
        SubtreeOfAnotherTree_572_E solution = new SubtreeOfAnotherTree_572_E();
        //[1,2,3,4,5]

        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);

        node3.left = node4;
        node3.right = node5;
        node4.left = node1;
        node4.right = node2;

        // root
        TreeNode subNode1 = new TreeNode(1);
        TreeNode subNode2 = new TreeNode(2);
        TreeNode subNode3 = new TreeNode(3);
        TreeNode subNode4 = new TreeNode(4);
        TreeNode subNode5 = new TreeNode(5);

        // subRoot
        subNode4.left = subNode1;
        subNode4.right = subNode2;

        boolean result = solution.isSubtree(node3, subNode4);
        System.out.println("The result: " + result);
    }
}
