package com.frank.tree._543_E;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/30
 *     title  : 543. Diameter of Binary Tree
 * </pre>
 */
public class DiameterOfBinaryTree_543_E {
    /**
     Given the root of a binary tree, return the length of the diameter of the tree.

     The diameter of a binary tree is the length of the longest path between any two nodes in a tree.
     This path may or may not pass through the root.

     The length of a path between two nodes is represented by the number of edges between them.



     Example 1:
     Input: root = [1,2,3,4,5]
     Output: 3
     Explanation: 3 is the length of the path [4,2,1,3] or [5,2,1,3].

     Example 2:
     Input: root = [1,2]
     Output: 1


     Constraints:

     The number of nodes in the tree is in the range [1, 104].
     -100 <= Node.val <= 100
     */

    // 題意：找出整個tree裡的最長路徑

    /*思路：找出所有節點的left MaxDepth 及所有節點的right MaxDepth相加最大值
      本來想得很簡單，只要找出root.left 的 MaxDepth 及 root.right 的 MaxDepth 相加就好，但案情絕對沒有這麼單純！
      概念是對的，但應該要改成找出 所有節點的 left MaxDepth 及 所有節點的 right MaxDepth 相加最大值 才對

      1.若 root 為 null，回傳 0
      2.用遞迴 Recursion 找出 left 節點的最大深度
      3.用遞迴 Recursion 找出 right 節點的最大深度
      4.比較 max 及 left 的最大深度 + right 的最大深度，並將較大的紀錄在 max
        這邊就是在紀錄除了root 外的最長路徑
      5.比較 left 的最大深度及 right 的最大深度，並將較大的值 + 1 後回傳
        此時的 +1 就是 往下多一階 的意思

      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    int max = 0;
    public int diameterOfBinaryTree(TreeNode root) {
        maxDepth(root);
        return max;
    }

    private int maxDepth(TreeNode root) {
        if (root == null) return 0;

        int left = maxDepth(root.left);// 遞歸計算左子樹的深度
        int right = maxDepth(root.right);// 遞歸計算右子樹的深度
        // 更新直徑，直徑為左子樹深度加上右子樹深度的最大值
        max = Math.max(max, left + right);
        System.out.println("root:" + root.val + ",left:" + left + " , right:" + right + " ,max:" + max);
        // 返回當前節點的深度，為左右子樹深度的最大值加上 1
        return Math.max(left, right) + 1;
    }
    /*
    1.使用深度優先搜索（DFS）遍歷二元樹。
    2.遞歸計算每個節點的深度，深度為左子樹深度和右子樹深度的最大值加 1。
    3.在遍歷的過程中，更新直徑，直徑為每個節點的左子樹深度和右子樹深度之和的最大值。
    4.返回直徑，即遍歷完整個二元樹後直徑的值。
     */

    public static void main(String[] args) {
        DiameterOfBinaryTree_543_E solution = new DiameterOfBinaryTree_543_E();
        //[1,2,3,4,5]

        TreeNode root = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);

        root.left = node2;
        root.right = node3;
        node2.left = node4;
        node2.right = node5;

        int result = solution.diameterOfBinaryTree(root);
        System.out.println("The result val: " + result);
    }
}
