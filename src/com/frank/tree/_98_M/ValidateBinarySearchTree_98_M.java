package com.frank.tree._98_M;

import com.frank.tree.TreeNode;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/18
 *     title  : 98. Validate Binary Search Tree
 * </pre>
 */
public class ValidateBinarySearchTree_98_M {
    /**
     Given the root of a binary tree, determine if it is a valid binary search tree (BST).

     A valid BST is defined as follows:

     The left
     subtree
     of a node contains only nodes with keys less than the node's key.
     The right subtree of a node contains only nodes with keys greater than the node's key.
     Both the left and right subtrees must also be binary search trees.


     Example 1:


     Input: root = [2,1,3]
     Output: true
     Example 2:


     Input: root = [5,1,4,null,null,3,6]
     Output: false
     Explanation: The root node's value is 5 but its right child's value is 4.


     Constraints:

     The number of nodes in the tree is in the range [1, 104].
     -231 <= Node.val <= 231 - 1
     */

    // 題意：給定一個二元樹，檢查它是否是二元搜尋樹
    /*
     二元搜尋樹如同題目說明，要符合以下三個特性：
      1.任一個節點的左子樹的所有節點值均小於該節點自身的值
      2.任一個節點的右子樹的所有節點值均大於該節點自身的值
      3.二元搜尋樹的左子樹和右子樹也都是二元搜尋樹
     */

    /*解法一：(遞迴)
     只要記住上一個節點，每次去比較上一個節點和現在走到的節點，
     當現在的節點跟上一個節點相比較小或相等時，表示這不是二元搜尋樹

      Time Complexity: O(N)
      Space Complexity: O(N)
     */
    public boolean isValidBST(TreeNode root) {
        return valid(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    private boolean valid(TreeNode root, long min, long max) {
        if (root == null) return true;
        System.out.println("The root.val: " + root.val);
        if (root.val <= min || root.val >= max) return false;
        return valid(root.left, min, root.val) && valid(root.right, root.val , max); // 左子樹 & 右子樹
    }

    /*
     解法二：stack
     */
    public boolean isValidBSTStack(TreeNode root) {
        Stack<TreeNode> s = new Stack<>();
        TreeNode p = root, pre = null;

        while (p != null || !s.empty()) {
            while (p != null) {
                s.push(p);
                p = p.left;
            }
            p = s.pop();
            if (pre != null && p.val <= pre.val) return false;
            pre = p;
            p = p.right;
        }
        return true;
    }

    public static void main(String[] args) {
        ValidateBinarySearchTree_98_M solution = new ValidateBinarySearchTree_98_M();
        //[1,2,3,4,5]
        TreeNode node0 = new TreeNode(0);

        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);
        TreeNode node6 = new TreeNode(6);

        node5.left = node1;
        node5.right = node4;

        node4.left = node3;
        node4.right = node6;

        boolean result = solution.isValidBST(node5);
        System.out.println("The result: " + result);
    }
}
