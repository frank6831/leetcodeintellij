package com.frank.tree._100_E;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/07/03
 *     title  : 100. Same Tree
 * </pre>
 */
public class SameTree_100_E {
    /**
     Given the roots of two binary trees p and q, write a function to
     check if they are the same or not.

     Two binary trees are considered the same if they are
     structurally identical, and the nodes have the same value.


     Example 1:
     Input: p = [1,2,3], q = [1,2,3]
     Output: true


     Example 2:
     Input: p = [1,2], q = [1,null,2]
     Output: false

     Example 3:
     Input: p = [1,2,1], q = [1,1,2]
     Output: false

     Constraints:

     The number of nodes in both trees is in the range [0, 100].
     -104 <= Node.val <= 104
     */

    // 題意：給定兩個二元樹，判斷這兩個樹的每一個節點中的值與節點位置是否都相同

    /*
     思路：用遞迴解會比迴圈容易理解很多。
     比較兩個節點的值(val)，如果val不同或是子節點數量不相同，都是false，子節點不同就是說其中一邊有左右節點，
     另外一邊就只有一個左節點或右節點這種情況。 當目前val與子節點數量都相同，再比較子節點是否相同，將子節點丟
     入判斷是否相同的function，直到比較完毎個節點都是一樣的，結果才會是true
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) return true;
        if (p == null || q == null) return false;

        if (p.val != q.val) {
            return false;
        }
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);

    }

    public static void main(String[] args) {
        SameTree_100_E solution = new SameTree_100_E();

        TreeNode nodeA = new TreeNode(1);
        TreeNode nodeB = new TreeNode(2);
        TreeNode nodeC = new TreeNode(3);

        TreeNode nodeD = new TreeNode(1);
        TreeNode nodeE = new TreeNode(2);
        TreeNode nodeF = new TreeNode(3);

        nodeA.left = nodeB;
        nodeA.right = nodeC;

        nodeD.left = nodeE;
        nodeD.right = nodeF;

        System.out.println(solution.isSameTree(nodeA, nodeD));
    }
}
