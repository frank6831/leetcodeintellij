package com.frank.tree._105_M;

import com.frank.tree.TreeNode;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/07/04
 *     title  : 105. Construct Binary Tree from Preorder and Inorder Traversal
 * </pre>
 */
public class ConstructBinaryTreeFromPreorderandInorderTraversal_105_M {
    /**
     Given two integer arrays preorder and inorder where preorder is the preorder traversal of
     a binary tree and inorder is the inorder traversal of the same tree, construct and return the binary tree.



     Example 1:


     Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
     Output: [3,9,20,null,null,15,7]
     Example 2:

     Input: preorder = [-1], inorder = [-1]
     Output: [-1]


     Constraints:

     1 <= preorder.length <= 3000
     inorder.length == preorder.length
     -3000 <= preorder[i], inorder[i] <= 3000
     preorder and inorder consist of unique values.
     Each value of inorder also appears in preorder.
     preorder is guaranteed to be the preorder traversal of the tree.
     inorder is guaranteed to be the inorder traversal of the tree.
     */
    //題目意思：給定到個二元樹的中序跟後序，請建出這個二元樹。


    /* 思路：遞迴，從根結點開始找最大深度

      Time Complexity: O(n^2)
      Space Complexity: O(n)
    */
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder == null || inorder == null || preorder.length != inorder.length) {
            // 如果 preorder 或 inorder 數組為空，或兩個數組的長度不相等，則返回 null
            return null;
        }

        Map<Integer, Integer> inorderMap = new HashMap<>();
        for (int i=0;i<inorder.length;i++) {
            inorderMap.put(inorder[i], i);
        }

        return buildTreeHelper(preorder, inorder, 0, 0,  inorder.length -1, inorderMap);
    }

    private TreeNode buildTreeHelper(int[] preorder,
                                     int[] inorder,
                                     int preStart,
                                     int inStart,
                                     int inEnd,
                                     Map<Integer, Integer> inorderMap) {
        if (preStart >= preorder.length || inStart > inEnd) {
            // 如果當前子樹的節點數為 0，則返回 null
            return null;
        }

        // 使用 preorder 數組中的第一個元素作為根節點
        int rootVal = preorder[preStart];
        TreeNode root = new TreeNode(rootVal);

        // 在 inorderMap 中查找根節點的位置
        int rootIndexInorder = inorderMap.get(rootVal);
        System.out.println("rootIndexInorder:" + rootIndexInorder + ", value:" + preorder[rootIndexInorder]);
        // 計算左子樹的節點數
        int leftSubtreeSize = rootIndexInorder - inStart;
        System.out.println("leftSubtreeSize:" + leftSubtreeSize + ", preStart:" + preStart);
        // 遞歸構建左子樹和右子樹
        /*
         在inorder裡，左子樹：[inStart...(rootIndexInorder-1)], 右子樹：[(rootIndexInorder+1)...inEnd]
         在preorder裡，左子樹：preStart+1,  右子樹: preStart＋透過中序計算出左子樹的節點數+1
         */
        root.left = buildTreeHelper(preorder, inorder,preStart +1, inStart, rootIndexInorder -1, inorderMap);
        //
        root.right = buildTreeHelper(preorder, inorder,
                preStart + leftSubtreeSize +1,
                rootIndexInorder + 1,
                inEnd, inorderMap);

        // 返回根節點
        return root;
    }

    public static void main(String[] args) {
        ConstructBinaryTreeFromPreorderandInorderTraversal_105_M solution = new ConstructBinaryTreeFromPreorderandInorderTraversal_105_M();
        int[] preorder = {3,9,20,15,7};
        // root:3, left: [3,9,20]
        int[] inorder = {9,3,15,20,7};

        TreeNode treeNode = solution.buildTree(preorder, inorder);

        System.out.println(treeNode.val);
    }

}

