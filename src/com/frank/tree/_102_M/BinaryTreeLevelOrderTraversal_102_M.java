package com.frank.tree._102_M;

import com.frank.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/16
 *     title  : 102. Binary Tree Level Order Traversal
 * </pre>
 */
public class BinaryTreeLevelOrderTraversal_102_M {
    /**
     Given the root of a binary tree, return the level order traversal of its nodes' values.
     (i.e., from left to right, level by level).


     Example 1:


     Input: root = [3,9,20,null,null,15,7]
     Output: [[3],[9,20],[15,7]]
     Example 2:

     Input: root = [1]
     Output: [[1]]
     Example 3:

     Input: root = []
     Output: []


     Constraints:

     The number of nodes in the tree is in the range [0, 2000].
     -1000 <= Node.val <= 1000
     */

    // 題意：所謂Traversal就是用某種順序來走訪Binary Search Tree中的每一個節點。Level Order Tree Traversal，又稱為 Breadth-First Search (廣度優先)


    /*思路：
    1.建立一個queue，先把根節點放進去，找根節點的左右兩個子節點，這時候去掉根節點，
    2.此時queue裡的元素就是下一層的所有節點，用一個for 循環遍歷它們，
    3.然後存到一個一維向量裡，遍歷完之後再把這個一維向量存到二維向量裡，以此類推，即可完成

      Time Complexity: O(N^2)
      Space Complexity: O(N)
     */
    public List<List<Integer>> levelOrder(TreeNode root) {
        if (root == null) return new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        List<List<Integer>> res = new ArrayList<>();
        while(!queue.isEmpty()) {
            int size = queue.size();

            List<Integer> level = new ArrayList<>();
            for (int i=0; i<size;i++) {
                TreeNode curr = queue.poll();
                level.add(curr.val);
                if (curr.left != null) queue.add(curr.left);
                if (curr.right != null) queue.add(curr.right);
            }
            res.add(level);
        }
        return res;
    }



    public static void main(String[] args) {
        BinaryTreeLevelOrderTraversal_102_M solution = new BinaryTreeLevelOrderTraversal_102_M();
        //[1,2,3,4,5]

        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);

        node1.left = node2;
        node1.right = node3;


        List<List<Integer>> result = solution.levelOrder(node1);
        System.out.println("The result: " + result);
    }
}
