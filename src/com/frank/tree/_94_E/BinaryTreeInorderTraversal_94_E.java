package com.frank.tree._94_E;

import com.frank.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/22
 *     title  : 94. Binary Tree Inorder Traversal
 * </pre>
 */
public class BinaryTreeInorderTraversal_94_E {
    /**
     Given the root of a binary tree, return the inorder traversal of its nodes' values.

     Example 1:
     Input: root = [1,null,2,3]
     Output: [1,3,2]

     Example 2:
     Input: root = []
     Output: []

     Example 3:
     Input: root = [1]
     Output: [1]

     Example 4:
     Input: root = [1,2]
     Output: [2,1]

     Example 5:
     Input: root = [1,null,2]
     Output: [1,2]


     Constraints:

     The number of nodes in the tree is in the range [0, 100].
     -100 <= Node.val <= 100

     Follow up: Recursive solution is trivial, could you do it iteratively?
     */

    /*
     題意：印出二元樹的中序走訪
     概念就是先走訪左子樹，再走訪父節點，再走訪右子樹。
     */

    /**
     * 遞迴方式
     */
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        inorder(root, list);
        return list;
    }

    void inorder(TreeNode node, List<Integer> list) {
        if(null == node) return;
        inorder(node.left, list);
        list.add(node.val);
        inorder(node.right, list);
    }

    /**
     * 疊代 (iterative) 的方法
     */
    public List<Integer> inorderTraversalIteratively(TreeNode root) {
        List<Integer> list = new ArrayList<>();

        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        System.out.println("Before while cur val:" + cur.val);
        while (null != cur || stack.size() > 0) {
            //先把左節點全加入stack
            while (null != cur) {// 走訪左子樹
                stack.push(cur);//紀錄父節點
                cur = cur.left;
            }
            cur = stack.pop(); // 回到父節點
            list.add(cur.val);
            cur = cur.right; // 走訪右子樹
        }
        return list;
    }

    public static void main(String[] args) {
        BinaryTreeInorderTraversal_94_E solution = new BinaryTreeInorderTraversal_94_E();

        TreeNode nodeA = new TreeNode(1);
        TreeNode nodeB = new TreeNode(2);
        TreeNode nodeC = new TreeNode(3);
        TreeNode nodeD = new TreeNode(4);
        TreeNode nodeE = new TreeNode(5);

        nodeA.left = nodeB;
        nodeA.right = nodeC;

        nodeB.right = nodeD;
        nodeC.right = nodeE;

        System.out.println(solution.inorderTraversalIteratively(nodeA));
    }
}
