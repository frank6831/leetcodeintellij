package com.frank.tree._226_E;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/29
 *     title  : 226. Invert Binary Tree
 * </pre>
 */
public class InvertBinaryTree_226_E {
    /**
     Given the root of a binary tree, invert the tree, and return its root.

     Example 1:


     Input: root = [4,2,7,1,3,6,9]
     Output: [4,7,2,9,6,3,1]
     Example 2:


     Input: root = [2,1,3]
     Output: [2,3,1]
     Example 3:

     Input: root = []
     Output: []


     Constraints:

     The number of nodes in the tree is in the range [0, 100].
     -100 <= Node.val <= 100
     */

    // 題意：反轉一個二元樹，將二元樹中每個節點的子節點左右對調

    /*思路：二元樹的題用遞迴的思想求解自然是最容易的

      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    public TreeNode invertTree(TreeNode root) {
        //節點為null或沒有子節點，不用反轉，終止遞迴
        if (root == null || (root.left == null && root.right == null)) {
            return root;
        }

        // 左節點為本來的右節點反轉，右節點為本來的左節點反轉
        TreeNode tmp = root.right;
        root.right = root.left;
        root.left = tmp;
        invertTree(root.left);
        invertTree(root.right);
        return root;
    }

    public static void main(String[] args) {
        InvertBinaryTree_226_E solution = new InvertBinaryTree_226_E();
        //[4,2,7,1,3,6,9]
        /*
         * 以[4][2,7][1,3,6,9]這個樹來說明
         * 執行時傳入節點4，發現有左節點[2]，右節點[7]，將[2][7]交換
         * 交換同時發現新的左節點[7]有子節點[9,6]，交換成為[6,9]，因為[6][9]都沒子節點，遞迴結束
         * 接著看新的右節點[2]，有子節點[1,3]，步驟同上
         * 上述步驟執行後就會得到反轉的tree [4][7,2][9,6,3,1]
         */

        TreeNode root = new TreeNode(4);
        TreeNode nodeB = new TreeNode(2);
        TreeNode nodeC = new TreeNode(7);

        TreeNode nodeD = new TreeNode(1);
        TreeNode nodeE = new TreeNode(3);
        TreeNode nodeF = new TreeNode(6);
        TreeNode nodeG = new TreeNode(9);

        root.left = nodeB;
        root.right = nodeC;

        nodeB.left = nodeD;
        nodeB.right = nodeE;

        nodeC.left = nodeF;
        nodeC.right = nodeG;

        System.out.println(solution.invertTree(root));
    }
}
