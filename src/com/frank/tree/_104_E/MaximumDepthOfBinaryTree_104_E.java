package com.frank.tree._104_E;

import com.frank.tree.TreeNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/07/04
 *     title  : 104. Maximum Depth of Binary Tree
 * </pre>
 */
public class MaximumDepthOfBinaryTree_104_E {
    /**
     Given the root of a binary tree, return its maximum depth.

     A binary tree's maximum depth is the number of nodes along the longest path
     from the root node down to the farthest leaf node.

     Example 1:
     Input: root = [3,9,20,null,null,15,7]
     Output: 3


     Example 2:
     Input: root = [1,null,2]
     Output: 2

     Example 3:
     Input: root = []
     Output: 0

     Example 4:
     Input: root = [0]
     Output: 1

     Constraints:

     The number of nodes in the tree is in the range [0, 104].
     -100 <= Node.val <= 100
     */
    //題目意思：找出二元樹中的最大高度


    // 思路：遞迴，從根結點開始找最大深度，
    // depth = 1+max(function(root.left), function(foot.right))，
    // 當root == None時，return 0。

    /*
     1.如果根節點為空，則返回0。
     2.遞迴計算左子樹的最大深度，將結果保存在變數leftDepth 中。
     3.遞迴計算右子樹的最大深度，將結果保存在變數rightDepth 中。
     4.返回左右子樹最大深度中的較大值加上根節點的深度（1）作為結果。
     */
    public int maxDepth(TreeNode root) {
        // 如果根節點為空，則返回0
        if (root == null) return 0;
        System.out.println("root val:" + maxDepth(root.left));
        // 遞歸計算左子樹的最大深度
        int leftDepth = maxDepth(root.left);
        // 遞歸計算右子樹的最大深度
        int rightDepth = maxDepth(root.right);

        // 返回左右子樹最大深度中的較大值加上根節點的深度（1）
        return Math.max(leftDepth, rightDepth) + 1;
    }

    public static void main(String[] args) {
        MaximumDepthOfBinaryTree_104_E solution = new MaximumDepthOfBinaryTree_104_E();
        //root = [3,9,20,null,null,15,7]
        TreeNode nodeA = new TreeNode(3);
        TreeNode nodeB = new TreeNode(9);
        TreeNode nodeC = new TreeNode(20);
        TreeNode nodeD = new TreeNode(15);
        TreeNode nodeE = new TreeNode(7);

        nodeA.left = nodeB;
        nodeA.right = nodeC;

        nodeC.left = nodeD;
        nodeC.right = nodeE;

        System.out.println(solution.maxDepth(nodeA));
    }

}

