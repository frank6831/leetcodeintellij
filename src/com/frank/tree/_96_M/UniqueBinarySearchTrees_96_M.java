package com.frank.tree._96_M;

import com.frank.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/02/17
 *     title  : 96. Unique Binary Search Trees
 * </pre>
 */
public class UniqueBinarySearchTrees_96_M {
    /**
     Given an integer n, return the number of structurally unique BST's (binary search trees)
     which has exactly n nodes of unique values from 1 to n.



     Example 1:


     Input: n = 3
     Output: 5
     Example 2:

     Input: n = 1
     Output: 1


     Constraints:

     1 <= n <= 19
     */

    /*
     題意：
     */

    /**
     * 遞迴方式
     */
    public int numTrees(int n) {
        int[] dp = new int[n+1];
        dp[0] = 1;
        for (int i=1;i<=n;i++) {
            for (int j=0;j <i;j++) {
                dp[i] += dp[j]* dp[i-j-1];
            }
        }
        return dp[n];
    }

    public int numTrees2(int n) {
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                int leftNum = dp[j - 1];
                int rightNum = dp[i - j];
                dp[i] += leftNum * rightNum;
            }
        }
        return dp[n];
    }

    public static void main(String[] args) {
        UniqueBinarySearchTrees_96_M solution = new UniqueBinarySearchTrees_96_M();
        System.out.println(solution.numTrees2(3));
    }
}
