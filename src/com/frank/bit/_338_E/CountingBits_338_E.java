package com.frank.bit._338_E;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/12/5
 *     title  : 338. Counting Bits
 * </pre>
 */
public class CountingBits_338_E {
    
	/**
	 Given an integer n, return an array ans of length n + 1 such that for each i (0 <= i <= n),
	 ans[i] is the number of 1's in the binary representation of i.


	 Example 1:

	 Input: n = 2
	 Output: [0,1,1]
	 Explanation:
	 0 --> 0
	 1 --> 1
	 2 --> 10

	 Example 2:

	 Input: n = 5
	 Output: [0,1,1,2,1,2]
	 Explanation:
	 0 --> 0
	 1 --> 1
	 2 --> 10
	 3 --> 11
	 4 --> 100
	 5 --> 101


	 Constraints:

	 0 <= n <= 105


	 Follow up:

	 It is very easy to come up with a solution with a runtime of O(n log n).
	 Can you do it in linear time O(n) and possibly in a single pass?
	 Can you do it without using any built-in function (i.e., like __builtin_popcount in C++)?
	*/

	//題意：將十進位數字轉為二進位表示，計算其中包含幾個1


	/*
	 * 解法一：Dynamic programming
	 */
	public static int[] countBits(int n) {
		int[] dp = new int[n + 1];
		dp[0] = 0;

		for (int i = 1; i <= n; i++) {
			if (i % 2 ==0) {
				dp[i] = dp[i/2];
			} else {
				dp[i] = dp[i/2] + 1;
			}
		}
		return dp;
	}


	public int[] countBitsDP(int num) {
		// 創建一個用於存儲每個數字的二進制表示中1的位數的結果數組
		int[] result = new int[num + 1];

		// 從1開始遍歷到給定數字num
		for (int i = 1; i <= num; i++) {
			// 根據動態規劃的思想，計算 i 數字的二進制表示中1的位數
			// 該數字的1的位數可以由其最後一位的1的位置 (i & (i - 1)) 的1的位數加1來得到
			result[i] = result[i & (i - 1)] + 1;
		}

		return result;
	}
	
    public static void main(String[] args) {
        int[] result = countBits(7);
		int value = 1%2;
		System.out.println("value:" + value);
        System.out.println("result:" + Arrays.toString(result));
    }
}
