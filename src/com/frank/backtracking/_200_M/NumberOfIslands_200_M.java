package com.frank.backtracking._200_M;


import java.util.LinkedList;
import java.util.Queue;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/10
 *     title  : 200. Number of Islands
 * </pre>
 */
public class NumberOfIslands_200_M {
    /**
     Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.

     An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
     You may assume all four edges of the grid are all surrounded by water.



     Example 1:

     Input: grid = [
     ['1','1','1','1','0'],
     ['1','1','0','1','0'],
     ['1','1','0','0','0'],
     ['0','0','0','0','0']
     ]
     Output: 1
     Example 2:

     Input: grid = [
     ['1','1','0','0','0'],
     ['1','1','0','0','0'],
     ['0','0','1','0','0'],
     ['0','0','0','1','1']
     ]
     Output: 3


     Constraints:

     m == grid.length
     n == grid[i].length
     1 <= m, n <= 300
     grid[i][j] is '0' or '1'.
     */
    /*
     題意：給一個二維的陣列，1代表陸地，0代表是水，島嶼四面環水，島嶼是由上下左右連接組合而成的，請問這個二維陣列中有幾個島嶼？
     */

    /*
      解法一：DFS
      目標是 找到矩陣中"島嶼的數量"，上下左右相連的 1 都被認為是連續島嶼。
      1.設目前指針指向一個島嶼中的某一點(i, j)，尋找包括此點的島嶼邊界
      2.從(i, j) 向此點的上下左右(i+1,j), (i-1,j), (i,j+1),(i,j-1)做深度搜索。
      終止條件：
      (1) (i, j)越過矩陣邊界;
      (2) grid[i][j]== '*'，代表此分支已越過島嶼邊界。
      3.搜索島嶼的同時，執行grid[i][j]='*'，即將島嶼所有節點刪除，以免之後重複搜索相同島嶼。
      4.遍歷整個矩陣，當遇到grid[i][j]=='1' 時，從此點開始做dfs，島嶼數count+1且在深度優先搜索中刪除此島嶼。
      5.最終返回島嶼數 count 即可。
      Time Complexity: O(MN) //M和N分別為行數和列數
      Space Complexity: O(MN)
     */
    public int numIslandsDfs(char[][] grid) {
        int count = 0;
        for (int i=0; i< grid.length;i++) {
            for (int j=0; j< grid[i].length;j++) {
                if (grid[i][j] == '1') {
                    count++;
                    dfs(grid, i, j);
                }
            }
        }
        return count;
    }

    private void dfs(char[][] grid, int i, int j) {
        if ((i< 0)
                || (j<0)
                || (i >= grid.length)
                || (j >= grid[0].length)
                || (grid[i][j] !='1')) {
            return;
        }
        grid[i][j] = '*';
        dfs(grid, i, j+1); //向右
        dfs(grid, i, j-1); //向左
        dfs(grid, i+1, j);//向下
        dfs(grid, i-1, j);//向上
    }


    public int numIslandsBfs(char[][] grid) {
        int count = 0;
        for (int i=0; i< grid.length;i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    bfs(grid, i, j);
                    count++;
                }
            }
        }

        return count;
    }

    private void bfs(char[][] grid, int i, int j) {
        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[]{i, j});

        while (!queue.isEmpty()) {
            int[] curr = queue.remove();
            i = curr[0];
            j = curr[1];

            if (0 <=i && i< grid.length && 0 <=j && j <= grid[0].length && grid[i][j] == '1') {
                grid[i][j] = '0';
                queue.add(new int[]{i+1, j});
                queue.add(new int[]{i-1, j});
                queue.add(new int[]{i, j+1});
                queue.add(new int[]{i, j-1});
            }

        }
    }


    public static void main(String[] args) {
        NumberOfIslands_200_M solution = new NumberOfIslands_200_M();
        char[][] grid = {
            {'1','1','1','1','0'},
            {'1','1','0','1','0'},
            {'1','1','0','0','0'},
            {'0','0','0','0','0'}
        };

        int count = solution.numIslandsBfs(grid);
        System.out.println("count:" + count);
    }
}
