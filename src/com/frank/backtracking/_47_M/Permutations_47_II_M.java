package com.frank.backtracking._47_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/30
 *     title  : 47. Permutations II
 * </pre>
 */
public class Permutations_47_II_M {
    /**
     Given a collection of numbers, nums, that might contain duplicates,
     return all possible unique permutations in any order.

     Example 1:
     Input: nums = [1,1,2]
     Output:
     [[1,1,2],
     [1,2,1],
     [2,1,1]]

     Example 2:
     Input: nums = [1,2,3]
     Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
     */
    /*
     題意：給定一個可包含重複數字的序列 nums ，按任意順序 返回所有不重複的全排列
     */

    /*
      解法：回溯法
      1.把每個元素不斷窮舉，來組成新的排列。
      2.使用一個 visited 數組記錄使用過的數字，使用過了就不再使用
      3.剪枝的條件：和前一個元素值相同，且前一個元素還沒有被使用過
      4.當集合大小等於數組長度，說明一個結果被找到了，可結束本次遞迴
      Time Complexity: O(n!)
      Space Complexity: O(n)
     */

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtracking(list, new ArrayList<>(), nums, new boolean[nums.length]);
        return list;
    }

    private void backtracking(List<List<Integer>> list,  List<Integer> tempList,
                           int[] nums, boolean[] visited) {

        if (tempList.size() == nums.length) {
            System.out.println("Add into result list:" + tempList);
            list.add(new ArrayList<>(tempList));
            return;
        }

        for (int i=0; i< nums.length;i++) {
            //使用一個 visited 數組記錄使用過的數字，使用過了就不再使用
            if (visited[i])
                continue;
            // 剪枝的條件即為：和前一個元素值相同，且前一個元素還沒有被使用過
            if (i !=0 && nums[i] == nums[i-1] && !visited[i-1])
                continue;

            tempList.add(nums[i]);
            System.out.println("  递归之前 => " + tempList + " , i:" + i);
            visited[i] = true;
            backtracking(list, tempList, nums, visited);
            tempList.remove(tempList.size()-1);
            System.out.println("递归之后 => " + tempList + " , i:" + i);
            visited[i] = false;
        }
    }

    public static void main(String[] args) {
        Permutations_47_II_M solution = new Permutations_47_II_M();
        int[] array = {1,1,1,2};
        List<List<Integer>> result = solution.permuteUnique(array);
        System.out.println("result:"+ result);
    }
}
