package com.frank.backtracking._46_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/30
 *     title  : 46. Permutations
 * </pre>
 */
public class Permutations_46_M {
    /**
     Given an array nums of distinct integers, return all the possible permutations.
     You can return the answer in any order.


     Example 1:

     Input: nums = [1,2,3]
     Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
     Example 2:

     Input: nums = [0,1]
     Output: [[0,1],[1,0]]
     Example 3:

     Input: nums = [1]
     Output: [[1]]


     Constraints:

     1 <= nums.length <= 6
     -10 <= nums[i] <= 10
     All the integers of nums are unique.
     */
    /*
     題意：給定一個不重複的整數陣列，然後輸出給他們所有排列的可能性
     */

    /*
      解法：回溯法
      1.把每個元素不斷窮舉，來組成新的排列。
      2.利用tempList存放使用過的值，每一次遞迴中，已使用過的在本次遞歸中不能再使用。
      3.當集合大小等於數組長度，說明一個結果被找到了，可結束本次遞迴
      Time Complexity: O(n!)
      Space Complexity: O(n)
     */

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        backtracking(list, new ArrayList<>(), nums);
        return list;
    }

    private void backtracking(List<List<Integer>> list,  List<Integer> tempList, int[] nums) {
        if (tempList.size() == nums.length) {
            System.out.println("Add into result list:" + tempList);
            list.add(new ArrayList<>(tempList));
            return;
        }

        for (int i=0; i< nums.length;i++) {
            if (tempList.contains(nums[i])) {
                continue;
            }

            tempList.add(nums[i]);
            System.out.println("  递归之前 => " + tempList + " , i:" + i);
            backtracking(list, tempList, nums);
            tempList.remove(tempList.size()-1);
            System.out.println("递归之后 => " + tempList + " , i:" + i);
        }
    }

    public static void main(String[] args) {
        Permutations_46_M solution = new Permutations_46_M();
        int[] array = {1,2,3};
        List<List<Integer>> result = solution.permute(array);
        System.out.println("result:"+ result);
    }
}
