package com.frank.backtracking._90_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/10
 *     title  : 90. Subsets II
 * </pre>
 */
public class Subsets_II_90_M {
    /**
     Given an integer array nums that may contain duplicates, return all possible
     subsets
     (the power set).

     The solution set must not contain duplicate subsets. Return the solution in any order.


     Example 1:
     Input: nums = [1,2,2]
     Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]

     Example 2:
     Input: nums = [0]
     Output: [[],[0]]


     Constraints:

     1 <= nums.length <= 10
     -10 <= nums[i] <= 10
     */
    /*
     題意：與78題差別是，輸入陣列允許有重複的數字，窮舉出所有可能的子組合並回傳，子集合不能有相同的。
     */

    /*
      解法一：回溯法
      1.先將陣列由小到大排序
      2.若不為0，從start位置開始遞迴，先將當前位置數值加入tempList中，並進入下一層，等待返回後將本層加入的數據移除
      3.多判斷當前數字和上一個數字是否相同，相同的話跳過即可
      Time Complexity: O(n!)
      Space Complexity: O(k)
     */
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtracking(list, new ArrayList<>(), nums, 0);
        return list;
    }

    private void backtracking(List<List<Integer>> list,  List<Integer> tempList,
                              int[] nums, int index) {
        list.add(new ArrayList<>(tempList));
        for (int i = index; i< nums.length;i++) {
            if (i != index && nums[i] == nums[i-1]) {
                continue;
            }
            tempList.add(nums[i]);
            backtracking(list, tempList, nums, i+1);
            tempList.remove(tempList.size()-1);
        }
    }

    public static void main(String[] args) {
        Subsets_II_90_M solution = new Subsets_II_90_M();
        int[] array = {1,2,2};
        List<List<Integer>> result = solution.subsetsWithDup(array);
        System.out.println("result:"+ result);
    }
}
