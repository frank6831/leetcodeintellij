package com.frank.backtracking._78_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/10
 *     title  : 78. Subsets
 * </pre>
 */
public class Subsets_78_M {
    /**
     Given an integer array nums of unique elements, return all possible subsets
     (the power set).

     The solution set must not contain duplicate subsets. Return the solution in any order.


     Example 1:
     Input: nums = [1,2,3]
     Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]

     Example 2:
     Input: nums = [0]
     Output: [[],[0]]

     Constraints:

     1 <= nums.length <= 10
     -10 <= nums[i] <= 10
     All the numbers of nums are unique.
     */
    /*
     題意：給定一組陣列，窮舉出所有可能的子組合並回傳。
     */

    /*
      解法一：回溯法
      1.每一個數字都有兩個狀態，選或者不選，考慮完所有數字就是所有的 subsets 了
      2.這邊注意空集合 [] 也是一個 subset
      3.取過的元素不會重複取，寫回溯算法時，for就要從startIndex開始，而不是從0開始
      Time Complexity: O(n!)
      Space Complexity: O(k)
     */
    //https://leetcode.cn/problems/subsets/solutions/420458/shou-hua-tu-jie-zi-ji-hui-su-fa-xiang-jie-wei-yun-/
    //                O
    //            /         \
    //           [1]             []
    //        /     \           /    \
    //     [1, 2]     [1]     [2]      []
    //    /   \       /   \      / \    / \
    // [1,2,3] [1,2] [1, 3] [1]  [2, 3] [2] [3] []

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        backtracking(list, new ArrayList<>(), nums, 0);
        return list;
    }

    private void backtracking(List<List<Integer>> list,  List<Integer> tempList,
                              int[] nums, int index) {
        list.add(new ArrayList<>(tempList));
        //终止条件, 不一定要加
        if (index >=nums.length) {
            System.out.println("index:"+ index + ", end");
            return;
        }
        //System.out.println("index:"+ index + ", tempList:" + tempList);
        for (int i = index; i< nums.length;i++) {
            tempList.add(nums[i]);
            System.out.println("  递归之前 => " + tempList + " , i:" + i);
            backtracking(list, tempList, nums, i+1);
            tempList.remove(tempList.size()-1);
            System.out.println("递归之后 => " + tempList + " , i:" + i);
        }
    }

    public static void main(String[] args) {
        Subsets_78_M solution = new Subsets_78_M();
        int[] array = {1,2,3};
        List<List<Integer>> result = solution.subsets(array);
        System.out.println("result:"+ result);
    }
}
