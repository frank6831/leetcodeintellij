package com.frank.backtracking._40_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/30
 *     title  : 40. Combination Sum II
 * </pre>
 */
public class CombinationSum_II_40_M {
    /**
     Given a collection of candidate numbers (candidates) and a target number (target),
     find all unique combinations in candidates where the candidate numbers sum to target.

     Each number in candidates may only be used once in the combination.

     Note: The solution set must not contain duplicate combinations.



     Example 1:

     Input: candidates = [10,1,2,7,6,1,5], target = 8
     Output:
     [
     [1,1,6],
     [1,2,5],
     [1,7],
     [2,6]
     ]
     Example 2:

     Input: candidates = [2,5,2,1,2], target = 5
     Output:
     [
     [1,2,2],
     [5]
     ]


     Constraints:

     1 <= candidates.length <= 100
     1 <= candidates[i] <= 50
     1 <= target <= 30
     */
    /*
     題意：給一個數列與目標數，請從數列找出相加的組合等於目標數，每個數字只能用一次

     與第 39 題（組合之和）的差別
     這道題與上一問的區別在於：

      第 39 題：candidates 中的數字可以無限制重複被選取；
      第 40 題：candidates 中的每個數字在每個組合中只能使用一次。
      相同點是：相同數字列表的不同排列視為一個結果。
     */

    /*
      解法：回溯法
      1.需先將原陣列由小至大排序
      2.關鍵：當回溯碰到相同的直接跳過, 防止重複項
      3.如果當前值 > 剩下要湊齊的數字，那此值及後面的值就不用進行了
      4.剩餘要湊的數字為0，說明target已經達到了，放進結果集合中

      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    List<List<Integer>> ans;
    List<Integer> list;
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        ans = new ArrayList<>();
        list = new ArrayList<>();
        Arrays.sort(candidates);
        System.out.println("After sort candidates:" + Arrays.toString(candidates));
        process(candidates, target, 0);
        return ans;
    }

    // target：剩下要湊齊的數字
    // start：從哪個下標開始，繼續拿值嘗試
    private void process(int[] candidates, int target, int start) {
        if (target < 0) return;
        //剩餘要湊的數字為0，說明target已經達到了，放進結果集合中
        if (target == 0) {
            ans.add(new ArrayList<>(list));
            return;
        }

        for (int i = start; i < candidates.length; ++i) {
            //關鍵：當回溯碰到相同的直接跳過, 防止重複項
            if (i > start && candidates[i] == candidates[i - 1]) {
                continue;
            }

            //如果當前值 > 剩下要湊齊的數字，那此值及後面的值就不用進行了
            if (candidates[i] > target) {
                break;
            }

            list.add(candidates[i]);
            process(candidates, target - candidates[i], i + 1);
            list.remove(list.size() - 1);
        }
    }

    public static void main(String[] args) {
        CombinationSum_II_40_M solution = new CombinationSum_II_40_M();
        int[] array = {1,1,2,5,6,7,10};
        List<List<Integer>> result = solution.combinationSum2(array, 8);
        System.out.println("result:"+ result);
    }
}
