package com.frank.backtracking._79_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/14
 *     title  : 79. Word Search
 * </pre>
 */
public class WordSearch_79_M {
    /**
     Given an m x n grid of characters board and a string word,
     return true if word exists in the grid.

     The word can be constructed from letters of sequentially adjacent cells,
     where adjacent cells are horizontally or vertically neighboring.
     The same letter cell may not be used more than once.



     Example 1:
     Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
     Output: true
     Example 2:


     Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
     Output: true
     Example 3:


     Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
     Output: false


     Constraints:
     m == board.length
     n = board[i].length
     1 <= m, n <= 6
     1 <= word.length <= 15
     board and word consists of only lowercase and uppercase English letters.


     Follow up: Could you use search pruning to make your solution faster with a larger board?
     */
    /*
     題意：給予一個m*n的二維陣列board 以及一個字串word，在陣列中, 可以從水平或垂直方向連成一個字串
     (也就是可以上下左右去traverse陣列), 且連成字串的途中不能再重複使用已連過的字元
     */

    /*
      解法一：回溯法
      1.使用DFS去跑網格。
      2.判斷當前遍歷元素是否對應word字串，若不匹配就結束當前的遍歷，返回上一次的元素，嘗試其他路徑。
      3.把當前訪問的元素置為"*"，也就是在board中不會出現的字符。
        當上下左右全部嘗試完之後，我們再把當前元素還原就可以了。
      Time Complexity: O(MN 3L)
      Space Complexity: O(MN)
     */

    public boolean exist(char[][] board, String word) {
        // row
        for (int i=0; i < board.length;i++) {
            // column
            for (int j=0; j < board[0].length;j++) {
                if (helper(board, i, j, word, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean helper(char[][] board, int row, int col,
                           String word, int index) {
        //已經匹配到了最後一個字母，返回 true
        if (index == word.length()) return true;
        //判斷是否越界
        if (row < 0 || row >= board.length ||
                col < 0 || col >= board[0].length) {
            return false;
        }

        //當前元素訪問過或者和當前 word 不匹配返回 false
        if (word.charAt(index) != board[row][col]) {
            return false;
        }

        // 暫時性的改掉我們經過的字母, 在找到 選擇這個位置後的所有可能性後, 再把字母放回去
        char temp = board[row][col];
        board[row][col] = '*';
        System.out.println("Frank temp:" + temp + " , row:" + row + " , col:" + col);
        boolean found = helper(board, row, col-1, word, index +1) //往左
                || helper(board, row+ 1, col, word, index +1) //往下
                || helper(board, row, col +1, word, index +1) // 往右
                || helper(board, row-1,col, word, index +1);//往上
        board[row][col] = temp;
        return found;
    }

    public static void main(String[] args) {
        WordSearch_79_M solution = new WordSearch_79_M();
        char[][] array = {{'A','B','C','E'}, {'S','F','C','S'}, {'A','D','E','E'}};
        boolean result = solution.exist(array, "ABCB");
        System.out.println("result:"+ result);
    }
}
