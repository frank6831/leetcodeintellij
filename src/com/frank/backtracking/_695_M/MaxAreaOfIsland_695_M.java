package com.frank.backtracking._695_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/10
 *     title  : 200. Number of Islands
 * </pre>
 */
public class MaxAreaOfIsland_695_M {
    /**
     You are given an m x n binary matrix grid. An island is a group of 1's (representing land)
     connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

     The area of an island is the number of cells with a value 1 in the island.

     Return the maximum area of an island in grid. If there is no island, return 0.



     Example 1:


     Input: grid = [
     [0,0,1,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],
     [0,1,1,0,1,0,0,0,0,0,0,0,0],[0,1,0,0,1,1,0,0,1,0,1,0,0],
     [0,1,0,0,1,1,0,0,1,1,1,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0],
     [0,0,0,0,0,0,0,1,1,1,0,0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0]
     ]
     Output: 6
     Explanation: The answer is not 11, because the island must be connected 4-directionally.

     Example 2:

     Input: grid = [[0,0,0,0,0,0,0,0]]
     Output: 0


     Constraints:

     m == grid.length
     n == grid[i].length
     1 <= m, n <= 50
     grid[i][j] is either 0 or 1.
     */
    /*
     題意：給一個二維的陣列，1代表陸地，0代表是水，島嶼四面環水，島嶼是由上下左右連接組合而成的，請找出二維陣列最大島嶼的面積？
     */

    /*
      解法一：DFS
      目標是 找到矩陣中"島嶼"時，順便求出該島嶼面積，上下左右相連的 1 都被認為是連續島嶼。
      1.設目前指針指向一個島嶼中的某一點(i, j)，尋找包括此點的島嶼邊界
      2.從(i, j) 向此點的上下左右(i+1,j), (i-1,j), (i,j+1),(i,j-1)做深度搜索。
      終止條件：
      (1) (i, j)越過矩陣邊界;
      (2) grid[i][j]== '*'，代表此分支已越過島嶼邊界。
      3.搜索島嶼的同時，執行grid[i][j]='*'，即將島嶼所有節點刪除，以免之後重複搜索相同島嶼。
      4.遍歷整個矩陣，當遇到grid[i][j]=='1' 時，從此點開始做dfs，島嶼數area+1且在深度優先搜索中刪除此島嶼。
      5.每一回合計算出當下島嶼面積，最後取最大的面積回傳。
      Time Complexity: O(MN) //M和N分別為行數和列數
      Space Complexity: O(MN)
     */

    int area = 0;
    public int maxAreaOfIsland(int[][] grid) {
        int res = 0;
        for (int i=0; i< grid.length;i++) {
            for (int j=0; j< grid[i].length;j++) {
                if (grid[i][j] == 1) {
                    area = 0;
                    dfs(grid, i, j);
                    res = Math.max(res, area);
                }
            }
        }
        return res;
    }

    private void dfs(int[][] grid, int i, int j) {
        if ((i< 0)
                || (j<0)
                || (i >= grid.length)
                || (j >= grid[0].length)
                || (grid[i][j] != 1)) {
            return;
        }
        grid[i][j] = 0;
        area++;

        dfs(grid, i, j+1); //向右
        dfs(grid, i, j-1); //向左
        dfs(grid, i+1, j);//向下
        dfs(grid, i-1, j);//向上
    }


    public static void main(String[] args) {
        MaxAreaOfIsland_695_M solution = new MaxAreaOfIsland_695_M();
        int[][] grid = {
            {0,0,1,0,0,0,0,1,0,0,0,0,0},
            {0,0,0,0,0,0,0,1,1,1,0,0,0},
            {0,1,1,0,1,0,0,0,0,0,0,0,0},
            {0,1,0,0,1,1,0,0,1,0,1,0,0},
            {0,1,0,0,1,1,0,0,1,1,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,1,0,0},
            {0,0,0,0,0,0,0,1,1,1,0,0,0},
            {0,0,0,0,0,0,0,1,1,0,0,0,0},
        };

        int count = solution.maxAreaOfIsland(grid);
        System.out.println("count:" + count);
    }
}
