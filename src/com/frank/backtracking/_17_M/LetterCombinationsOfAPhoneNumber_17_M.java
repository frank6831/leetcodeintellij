package com.frank.backtracking._17_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/4
 *     title  : 17. Letter Combinations of a Phone Number
 * </pre>
 */
public class LetterCombinationsOfAPhoneNumber_17_M {
    /**
     Given a string containing digits from 2-9 inclusive, return all possible letter combinations
     that the number could represent. Return the answer in any order.

     A mapping of digits to letters (just like on the telephone buttons) is given below.
     Note that 1 does not map to any letters.


     Example 1:

     Input: digits = "23"
     Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]

     Example 2:
     Input: digits = ""
     Output: []

     Example 3:
     Input: digits = "2"
     Output: ["a","b","c"]


     Constraints:

     0 <= digits.length <= 4
     digits[i] is a digit in the range ['2', '9'].
     */
    /*
     題意：給一串數字，每個數可以代表數字鍵下的幾個字母，返回這些數字下的字母的所有組成可能。
     */

    /*
      解法一：回溯法
      1.建立數字與字串對照的陣列
      2.遍尋字串，比如第一個數字是2, 對照字串是"abc"，依序遍歷，將其中的一個字母插入到已有的字母排列後面
      3.繼續處理後一位數字，直到處理完號碼中的所有數字，即得到一個完整的字母排列
      4.進行回退操作，遍歷其餘的字母排列
      Time Complexity: O(n!)
      Space Complexity: O(k)
     */
    String[] letter_map = {" ","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    List<String> res = new ArrayList<>();
    public List<String> letterCombinations(String digits) {
        if (digits.equals("")) return new ArrayList<>();

        backtracking(digits, new StringBuilder(), 0);
        return res;
    }

    private void backtracking(String str, StringBuilder letter, int index) {
        if (index == str.length()) {
            res.add(letter.toString());
            return;
        }

        char c = str.charAt(index);
        String mapString = letter_map[c - '0'];
        for(int i= 0; i< mapString.length();i++) {
            letter.append(mapString.charAt(i));
            System.out.println("  递归之前 => " + letter + " , i:" + i);
            backtracking(str, letter, index +1);
            letter.deleteCharAt(letter.length() -1);
            System.out.println("递归之后 => " + letter + " , i:" + i);
        }
    }

    /*
    解法二：使用Queue

    Time Complexity: O(n!)
    Space Complexity: O(k)
     */
    public List<String> letterCombinationQueue(String digits) {
        if (digits == null || digits.length() ==0) return new ArrayList<>();
        Queue<String> queue = new LinkedList<>();
        queue.offer("");
        String[] dict = {" ","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

        for (int i=0; i< digits.length();i++) {
            int num = digits.charAt(i) - '0';
            String str = dict[num];

            while (queue.peek().length() == i) {
                String curr = queue.poll();
                for(char c: str.toCharArray()) {
                    queue.offer(curr + c);
                }
            }
        }

        List<String> res = new ArrayList<>();
        while (!queue.isEmpty()) {
            res.add(queue.poll());
        }
        return res;
    }

    public static void main(String[] args) {
        LetterCombinationsOfAPhoneNumber_17_M solution = new LetterCombinationsOfAPhoneNumber_17_M();
        List<String> result = solution.letterCombinationQueue("23");
        System.out.println("result:"+ result);
    }
}
