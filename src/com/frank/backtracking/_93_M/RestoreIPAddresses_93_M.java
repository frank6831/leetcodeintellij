package com.frank.backtracking._93_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/12
 *     title  : 93. Restore IP Addresses
 * </pre>
 */
public class RestoreIPAddresses_93_M {
    /**
     A valid IP address consists of exactly four integers separated by single dots.
     Each integer is between 0 and 255 (inclusive) and cannot have leading zeros.

     For example, "0.1.2.201" and "192.168.1.1" are valid IP addresses,
     but "0.011.255.245", "192.168.1.312" and "192.168@1.1" are invalid IP addresses.
     Given a string s containing only digits, return all possible valid IP addresses
     that can be formed by inserting dots into s.
     You are not allowed to reorder or remove any digits in s.
     You may return the valid IP addresses in any order.



     Example 1:

     Input: s = "25525511135"
     Output: ["255.255.11.135","255.255.111.35"]
     Example 2:

     Input: s = "0000"
     Output: ["0.0.0.0"]
     Example 3:

     Input: s = "101023"
     Output: ["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]


     Constraints:

     1 <= s.length <= 20
     s consists of digits only.
     */
    /*
     題意：給一個字符串，輸出所有的可能的ip地址，注意一下，01.1.001.1 類似這種0 開頭的是非法字符串。
     */

    /*
      解法一：dfs 回溯法
      1.先將陣列由小到大排序
      2.若不為0，從start位置開始遞迴，先將當前位置數值加入tempList中，並進入下一層，等待返回後將本層加入的數據移除
      3.判斷字串是否合法
        (1) 段位以0為開頭的數字不合法
        (2) 段位裡有非正整數字符不合法
        (3) 段位如果大於255了不合法
      Time Complexity: O(n!)
      Space Complexity: O(k)
     */
    public List<String> restoreIpAddresses(String s) {
        List<String> list = new ArrayList<>();
        backtracking(list, s, 0, 4, "");
        return list;
    }

    /*
     s: input字串
     index: 從第幾個開始
     remain：分段，以此例，ip需有4段
     curr:之前結果
     */
    private void backtracking(List<String> list,  String s, int index,
                              int remain, String curr) {

        /*停止條件
         1. 當remain ==0, 表示已經湊齊4段
         2. 判斷index是不是已到最後
         */
        if (remain == 0) {
            if (index == s.length()) {
                list.add(curr);
            }
            return;
        }

        //ip需要四個片段
        for (int i = 1; i<=3;i++) {
            //超過字符串長度，須停止
            if (index +i > s.length()) break;
            //第一個數字為0，須停止
            if (i != 1 && s.charAt(index) == '0') break;

            String temp = s.substring(index, index +i);
            System.out.println("temp:"+ temp + " ,i: " + i  + ", index:" + index);
            int val = Integer.parseInt(temp);

            if (val <= 255) {
                backtracking(list, s, index + i, remain-1,
                        curr + temp + (remain ==1 ? "" : "."));
            }
        }
    }

    public static void main(String[] args) {
        RestoreIPAddresses_93_M solution = new RestoreIPAddresses_93_M();
        List<String> result = solution.restoreIpAddresses("101023");
        System.out.println("result:"+ result);
    }
}
