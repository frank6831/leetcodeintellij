package com.frank.backtracking._77_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/30
 *     title  : 77. Combinations
 * </pre>
 */
public class Combinations_77_M {
    /**
     Given two integers n and k, return all possible combinations of k numbers chosen from the range [1, n].

     You may return the answer in any order.


     Example 1:

     Input: n = 4, k = 2
     Output: [[1,2],[1,3],[1,4],[2,3],[2,4],[3,4]]
     Explanation: There are 4 choose 2 = 6 total combinations.
     Note that combinations are unordered, i.e., [1,2] and [2,1] are considered to be the same combination.
     Example 2:

     Input: n = 1, k = 1
     Output: [[1]]
     Explanation: There is 1 choose 1 = 1 total combination.


     Constraints:

     1 <= n <= 20
     1 <= k <= n
     */
    /*
     題意：給定兩個整數n和k，返回範圍[1, n]中所有可能的k個數的組合。
     */

    /*
      解法一：回溯法
      1.判斷count == 0，若為0表示list中已經存入k個數，存入結果ans中
      2.若不為0，從start位置開始遞迴，先將當前位置數值加入tempList中，並進入下一層，等待返回後將本層加入的數據移除
      3.循環結束條件默認為最大值到n，這裡可優化進行剪枝，
      比如n=4，k=3 時，若列表從start=3開始，該組合一定不存在，因為至少要k=3個數據，所以剪枝臨界點為 n-k+1
      Time Complexity: O(n!)
      Space Complexity: O(k)
     */

    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> list = new ArrayList<>();
        backtracking(list, new ArrayList<>(), n, 1, k);
        return list;
    }

    private void backtracking(List<List<Integer>> list,  List<Integer> tempList,
                           int n, int index, int count) {
        if (count == 0) {
            list.add(new ArrayList<>(tempList));
            return;
        }

        for(int i= index; i<= (n-count+1);i++) {
            tempList.add(i);
            System.out.println("  递归之前 => " + tempList + " , i:" + i + ", count:" + count);
            backtracking(list, tempList, n, i+1, count-1);
            tempList.remove(tempList.size()-1);
            System.out.println("递归之后 => " + tempList + " , i:" + i);
        }
    }

    //Sample 1 ms submission
    public List<List<Integer>> combineFast(int n, int k) {
        List<List<Integer>> list = new ArrayList<>();
        if (k<0 || k>n) {
            return list;
        }

        dfs(n, k, 1, list,new ArrayList<>());

        return list;
    }

    private void dfs(int n, int k, int start, List<List<Integer>> list, List<Integer> cur){
        if (k == 0){
            list.add(new ArrayList<>(cur));
            return;
        }

        for (int i = start; i<= n - (k - cur.size()) + 1; i++){
            cur.add(i);
            System.out.println("递归之前 => " + cur + " , i:" + i + ", k:" + k);
            dfs(n, k-1, i+1, list, cur);
            cur.remove(cur.size()-1);
            System.out.println("递归之后 => " + cur + " , i:" + i);
        }
    }

    public static void main(String[] args) {
        Combinations_77_M solution = new Combinations_77_M();
        List<List<Integer>> result = solution.combine(4,2);
        System.out.println("result:"+ result);
    }
}
