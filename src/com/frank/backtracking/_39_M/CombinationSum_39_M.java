package com.frank.backtracking._39_M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/30
 *     title  : 39. Combination Sum
 * </pre>
 */
public class CombinationSum_39_M {
    /**
     Given an array of distinct integers candidates and a target integer target,
     return a list of all unique combinations of candidates where the chosen numbers sum to target.
     You may return the combinations in any order.

     The same number may be chosen from candidates an unlimited number of times. Two combinations are unique if the
     frequency
     of at least one of the chosen numbers is different.

     The test cases are generated such that the number of unique combinations that sum up to
     target is less than 150 combinations for the given input.



     Example 1:
     Input: candidates = [2,3,6,7], target = 7
     Output: [[2,2,3],[7]]
     Explanation:
     2 and 3 are candidates, and 2 + 2 + 3 = 7. Note that 2 can be used multiple times.
     7 is a candidate, and 7 = 7.
     These are the only two combinations.

     Example 2:
     Input: candidates = [2,3,5], target = 8
     Output: [[2,2,2,2],[2,3,3],[3,5]]

     Example 3:
     Input: candidates = [2], target = 1
     Output: []


     Constraints:

     1 <= candidates.length <= 30
     2 <= candidates[i] <= 40
     All elements of candidates are distinct.
     1 <= target <= 40
     */
    /*
     題意：給定一組不重複的數值組成的陣列，以及一個正整數target。
     從陣列中取數值組成加總正好為target的新陣列（每個數值都可以重複取），並列出所有的組合並回傳。

     思路：
     從2開始
     2 < 7 -> 2+2=4 < 7 -> 2+2+2=6 < 7 -> 2+2+2+2=8  > 7，回到上一步 -> 2+2+3=7 = 7，是其中一個解，回到上一步
     2+2+6 > 7，回到上一步 -> 2+2+7 > 7，再往前回到上一步
     3 -> 3+3 = 6 < 7 -> 3+3+3 > 7，回到上一步->....
     6 -> 6+6 = 12 > 7 ->....
     7 -> 等於7，是其中一個解
     基本上是這樣，如果數列是排好序的話，這樣就可以全部解完了

     */

    /*
      解法：回溯法
      1.先定義回溯方法的入參
        入參參數有：數組，還需要湊齊的和，要從哪個下標開始窮舉
      2.定義好回溯方法後，方法里首先確定回溯結束的條件
        回溯結束條件就是：還需要湊齊的和為0了，說明該終止本次回溯了
      3.定義好終止條件，下面就是開始窮舉
      把每個元素不斷窮舉，判斷其和是否達到了target。

      void process(參數) {
        if (終止條件) {
            存放結果;
            return;
        }

        for (選擇：本次遞歸集合中元素（從開始下標到數組結尾）) {
            處理節點;
            process(參數); // 遞歸
            回溯，撤銷處理結果
        }
      }

      Time Complexity: O(n)
      Space Complexity: O(1)
     */
    List<List<Integer>> ans;
    List<Integer> list;
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        ans = new ArrayList<>();
        list = new ArrayList<>();
        Arrays.sort(candidates);
        process(candidates, target, 0);
        return ans;
    }

    // rest：剩下要湊齊的數字
    // startIndex：從哪個下標開始，繼續拿值嘗試
    private void process(int[] candidates, int rest, int startIndex) {
        if (rest < 0) {
            return;
        }
        //剩餘要湊的數字為0，說明target已經達到了，放進結果集合中
        if (rest == 0) {
            ans.add(new ArrayList<>(list));
            return;
        }
        //從startIndex下標開始取值嘗試
        for (int i = startIndex; i< candidates.length;i++) {
            // 如果當前值 > 剩下要湊齊的數字，那這個值就不用考慮，剪枝操作
            System.out.println("===[Start] candidates[" + i+ "]: " + candidates[i] + "，rest = " + rest);
            if (candidates[i] <= rest) {
                // 先將值放進數組
                list.add(candidates[i]);
                // 去遞歸找剩下要湊齊的rest - candidates[i]值
                // 因為每個數可以無限取，所以下次嘗試還是從 i 開始，而不是 i + 1
                System.out.println("递归之前 => " + list + "，剩余 = " + (rest - candidates[i]) + ", i:" + i);
                process(candidates, rest - candidates[i], i);
                // 將剛才放進去的值刪除，回溯
                list.remove(list.size() - 1);
                System.out.println("递归之后 => " + list);
            }
        }
    }


    public static void main(String[] args) {
        CombinationSum_39_M solution = new CombinationSum_39_M();
        int[] array = {2,3,6,7};

        List<List<Integer>> result = solution.combinationSum(array, 7);
        System.out.println("result:"+ result);
    }
}
