package com.frank.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/1/30
 *     title  : 排序演算法
 * </pre>
 */
public class Sort {

    /* 參考網站
     1.https://iter01.com/16751.html
     */

    /**
     * 氣泡排序
     原理：由左到右，相鄰兩兩比較，較大者往右挪，最後最大值會出現在陣列右端。遞迴處理尚未排序的 N-1 個數字。
     最佳情況：T(n) = O(n)   最差情況：T(n) = O(n2)   平均情況：T(n) = O(n2)
     * @param array
     * @return
     */
    public static void bubbleSort(int[] array) {
        if (array.length == 0) {
            return;
        }
        int n = array.length;
        for (int i=0; i<n;i++) {
            for (int j=0; j< n-1-i;j++) {
                //比大小,if 前>後 換位
                if (array[j+1] < array[j]) {
                    int temp = array[j+1];
                    array[j+1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    /**
     * 選擇排序
     原理：反覆從未排序數列中找出最小值，將它與左邊的數做交換。可以有兩種方式排序，一為由大到小排序時，將最小值放到末端;若由小到大排序時，則將最小值放到前端。

     最佳情況：T(n) = O(n2)  最差情況：T(n) = O(n2)  平均情況：T(n) = O(n2)
     * @param array
     * @return
     */
    public static int[] selectionSort(int[] array) {
        if (array.length == 0)
            return array;
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[minIndex]) //找到最小的數
                    minIndex = j; //將最小數的索引儲存
            }
            int temp = array[minIndex];
            array[minIndex] = array[i];
            array[i] = temp;
        }
        return array;
    }

    /**
     * 插入排序
     原理是逐一將原始資料加入已排序好資料中，並逐一與已排序好的資料作比較，找到對的位置插入。
     例如:已有2筆排序好資料，將第3筆資料與前面已排序好的2筆資料作比較，找到對的位置插入，再將第4筆資料與前面已排序好的3筆資料作比較，找到對的位置插入，以此類推。

     最佳情況：T(n) = O(n)   最壞情況：T(n) = O(n2)   平均情況：T(n) = O(n2)
     * @param array
     * @return
     */
    public static int[] insertionSort(int[] array) {
        if (array.length == 0)
            return array;
        int current;
        for (int i = 0; i < array.length - 1; i++) {
            current = array[i + 1];
            int preIndex = i;
            while (preIndex >= 0 && current < array[preIndex]) {
                array[preIndex + 1] = array[preIndex];
                preIndex--;
            }
            array[preIndex + 1] = current;
        }
        return array;
    }





    public static void main(String[] args) {
        int[] array = {7,3,6,1};
        bubbleSort(array);
        System.out.println("Bubble sort result:"+ Arrays.toString(array));
    }
}
