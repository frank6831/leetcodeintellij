package com.frank.linkedlist._876_E;

import com.frank.linkedlist.ListNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/29
 *     title  : 876. Middle of the Linked List
 * </pre>
 */
public class MiddleOfTheLinkedList_876_E {
    /**
     Given the head of a singly linked list, return the middle node of the linked list.

     If there are two middle nodes, return the second middle node.



     Example 1:


     Input: head = [1,2,3,4,5]
     Output: [3,4,5]
     Explanation: The middle node of the list is node 3.
     Example 2:


     Input: head = [1,2,3,4,5,6]
     Output: [4,5,6]
     Explanation: Since the list has two middle nodes with values 3 and 4, we return the second one.


     Constraints:

     The number of nodes in the list is in the range [1, 100].
     1 <= Node.val <= 100
     */

    // 題意：已經有一個不為空的singly-linked list，我們有此linked list的head node，要找出此linked list的middle node。
    //如果linked list的長度是偶數的話，在中間的有2個node，那就回傳第2個。


    /*
        第一個while迴圈加總出該鏈結串列共有幾個節點，並將全部節點數除2，
        第二個迴圈再移動counter之次數，即可得到後半段的鏈結串列。

        Time Complexity: O(n)
        Space Complexity: O(1)
     */
    public ListNode middleNode(ListNode head) {
        int len = 0;
        ListNode current = head;
        while(current != null) {
            len++;
            current = current.next;
        }
        current = head;
        System.out.println("len:" + len);
        int mid = len/2;
        for (int i =0; i< mid;i++) {
            current = current.next;
        }
        return current;
    }

    /*最優解：快慢指針
      就是兩個指針，慢指針一次走一步，快指針一次走兩步，當快指針走到末尾的時候，慢指針剛好走到中間
      Time Complexity: O(n)
      Space Complexity: O(1)
     */
    public ListNode middleNodeFast(ListNode head) {
        ListNode slow = head, fast = head;
        while(fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    public static void main(String[] args) {
        MiddleOfTheLinkedList_876_E solution = new MiddleOfTheLinkedList_876_E();
        //[1,2,3,4,5]

        ListNode root = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(6);
        root.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;

        ListNode result = solution.middleNode(root);
        System.out.println("The result val: " + result.val);
    }
}
