package com.frank.linkedlist._141_E;

import com.frank.linkedlist.ListNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/18
 *     title : 141. Linked List Cycle
 * </pre>
 */
public class LinkedListCycle_141_E {

    /***
     Given head, the head of a linked list, determine if the linked list has a cycle in it.
     There is a cycle in a linked list if there is some node in the list that can be reached
     again by continuously following the next pointer. Internally, pos is used to denote
     the index of the node that tail's next pointer is connected to. Note that pos is not
     passed as a parameter.

     Return true if there is a cycle in the linked list. Otherwise, return false.

     Example 1:
     Input: head = [3,2,0,-4], pos = 1
     Output: true
     Explanation: There is a cycle in the linked list, where the tail connects to
     the 1st node (0-indexed).

     Example 2:
     Input: head = [1,2], pos = 0
     Output: true
     Explanation: There is a cycle in the linked list, where the tail connects to the 0th node.

     Example 3:
     Input: head = [1], pos = -1
     Output: false
     Explanation: There is no cycle in the linked list.

     Constraints:

     The number of the nodes in the list is in the range [0, 104].
     -105 <= Node.val <= 105
     pos is -1 or a valid index in the linked-list.

     Follow up: Can you solve it using O(1) (i.e. constant) memory?
     */

    /*
    解題有兩種解法。

    #使用Set：歷遍 Linked List ，將每個Node的記憶體位置存到Set，當有Node出現在Set，
        代表該Linked List有環。否則執行到結尾點null，代表沒有環。
    #使用Floyd's Algorithm：
        1.起點設2個指標，分別為Slow與Fast。
        2.每次的循環，Slow往前1步、Fast往前2步
        3.當Fast走到結尾點null，代表此Linked List無環；否則當Slow和Fast的位置相同時，代表此Linked List有環。
        4.可參考這篇演算法筆記-Cycle Finding: Floyd's Algorithm （ Tortoise and Hare Algorithm ）

        步驟 1.
        快指針 fast 一次走兩步，
        慢指針 slow 一次走一步，
        所以快指針 fast 永遠會追擊著慢指針 slow，
        期待有 cycle 的話， fast 會追上 slow ，
        這裡我們先讓 fast = head.next; slow = head;。

        步驟2.
        這是主要的一步，
        如果 Linked List 沒有 cycle 的話，那 fast 會遇到 null， return false 。
        如果 Linked List 有 cycle 的話，那 fast 一定會遇到 slow，並且 fast 已經超過 slow 一整圈， return true。
        完成！
     */

    public boolean hasCycle(ListNode head) {
        if (head == null) {
            return false;
        }

        ListNode fast = head;
        ListNode slow = head;

        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                return true;
            } 
        }
        return false;
    }

    public static void main(String[] args) {
        LinkedListCycle_141_E solution = new LinkedListCycle_141_E();
        ListNode node1 = new ListNode(3);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(0);
        ListNode node4 = new ListNode(-4);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node2;

        boolean hasCycle = solution.hasCycle(node1);
        System.out.println("hasCycle:" + hasCycle);
    }
}
