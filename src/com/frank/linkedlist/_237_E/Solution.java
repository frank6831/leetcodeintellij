package com.frank.linkedlist._237_E;

import com.frank.linkedlist.ListNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/23
 *     title : 237. Delete Node in a Linked List
 * </pre>
 */
public class Solution {
    
	/**
	 Write a function to delete a node (except the tail) in a singly linked list,
	 given only access to that node.

	 Given linked list -- head = [4,5,1,9], which looks like following:


	 Example 1:
	 Input: head = [4,5,1,9], node = 5
	 Output: [4,1,9]
	 Explanation: You are given the second node with value 5,
	 the linked list should become 4 -> 1 -> 9 after calling your function.

	 Example 2:
	 Input: head = [4,5,1,9], node = 1
	 Output: [4,5,9]
	 Explanation: You are given the third node with value 1,
	 the linked list should become 4 -> 5 -> 9 after calling your function.


	 Note:

	 The linked list will have at least two elements.
	 All of the nodes' values will be unique.
	 The given node will not be the tail and it will always be a valid node of the linked list.
	 Do not return anything from your function.


	 /*
	 題意: 鍊結串列，刪除特定節點
	 */
	private ListNode head;
	/*
	 思路:刪除鍊結串列特定節點
		 這道題的處理方法是先把當前節點的值用下一個節點的值覆蓋了，然後我們刪除下一個節點即可
	 */
	// Time : O(n), Space: O(1)
	private void deleteNode(ListNode node) {
		if (node == null) return;
		node.val = node.next.val; // 將下一個節點之值複製給該節點。
		node.next = node.next.next; // 將下下節點與該節點串接，即達成刪除下個節點之功能。
	}


	// prints content of double linked list
	private void printList(ListNode node) {
		while (node != null) {
			System.out.print(node.val + " ");
			node = node.next;
		}
	}

	public static void main(String[] args) {
        Solution list = new Solution();
		list.head = new ListNode(1);
		list.head.next = new ListNode(2);
		list.head.next.next = new ListNode(3);
		list.head.next.next.next = new ListNode(4);

		System.out.println("Given Linked list");
		list.printList(list.head);

		list.deleteNode(list.head);
		System.out.println("");
		System.out.println("Delete linked list ");
		list.printList(list.head);
    }
}
