package com.frank.linkedlist._234_E;

import com.frank.linkedlist.ListNode;

import java.util.ArrayList;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/12/5
 *     title  : 234. Palindrome Linked List
 * </pre>
 */
public class PalindromeLinkedList_234_E {
    /**
     Given the head of a singly linked list, return true if it is a
     palindrome or false otherwise.



     Example 1:


     Input: head = [1,2,2,1]
     Output: true
     Example 2:


     Input: head = [1,2]
     Output: false


     Constraints:

     The number of nodes in the list is in the range [1, 105].
     0 <= Node.val <= 9


     Follow up: Could you do it in O(n) time and O(1) space?
     */

    // 題意：給一個單向的連結陣列，判斷他是不是一個回文連結陣列。
    /*
    重點:
    (1) 遇到 list 要想到 slow and fast pointer
    (2) 遇到 palindrome 要想到 reverse
     */


    /*解法一：Two pointer
      使用左右指針，依序比對值，相等則往下一個移動，直到兩個指針相遇
      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    public boolean isPalindrome(ListNode head) {
        if (head == null) return false;
        ArrayList<Integer> resultList = new ArrayList<>();
        ListNode current = head;
        while(current != null) {
            resultList.add(current.val);
            current = current.next;
        }
        int left = 0;
        int right = resultList.size() - 1;
        while (left <= right) {
            System.out.println("Frank left:" + left + ",right:" + right);
            if (!resultList.get(left).equals(resultList.get(right))) {
                return false;
            }
            ++left;
            --right;
        }
        return true;
    }


    /*
     解法二：使用遞迴方法，但時間跟空間複雜度並未節省
      Time Complexity: O(n)
      Space Complexity: O(n)
     */
    ListNode frontPointer;
    private boolean recursivelyCheck(ListNode currentNode) {
        if (currentNode != null) {
            if (!recursivelyCheck(currentNode.next)) return false;
            if (currentNode.val != frontPointer.val) return false;
            System.out.println("Frank currentNode.val:" +currentNode.val + ",frontPointer val:" + frontPointer.val);
            System.out.println("Frank frontPointer val:" + frontPointer.val);
            frontPointer = frontPointer.next;
        }
        System.out.println("Frank recursivelyCheck return true");
        return true;
    }

    private boolean isPalindromeRecursive(ListNode head) {
        frontPointer = head;
        System.out.println("Frank isPalindromeRecursive val:" + frontPointer.val);
        return recursivelyCheck(head);
    }
    /*
    解法三：Reverse Second Half In-place
    (1) 快慢指針找出串列的中點
    (2) 反轉後半段串列
    (3) 比較前後兩個串列的值
    */
    /*
     解法三：Reverse Second Half In-place
     為了不使用額外的O(n)空間，我們先用快慢指針找出linked list的中點，
     找到後從中點之後將linked list反轉再與本來的head前半段比較是否相等，
     這邊需要一個額外的空間儲存反轉後的linked list。
       1. Find the end of the first half.
       2. Reverse the second half.
       3. Determine whether or not there is a palindrome.
       4. Restore the list.
       5. Return the result.
      Time Complexity: O(n)
      Space Complexity: O(1)
     */
    public boolean isPalindromeHalfInPlace(ListNode head) {
        if (head == null) return false;

        //找出linked list中間點
        ListNode middleNode = endOfFirstHalf(head);
        System.out.println("Frank middleNode val:" + middleNode.val);
        //反轉後半段linked list
        ListNode secondHalfStart = reverseList(middleNode.next);
        System.out.println("Frank secondHalfStart val:" + secondHalfStart.val);

        //從頭一一比較兩個串列的值，若都相同，表示是回文串列
        ListNode p1 = head;
        ListNode p2 = secondHalfStart;
        boolean result = true;
        while (p2 !=null) {
            if (p1.val != p2.val) result = false;
            p1 = p1.next;
           p2 = p2.next;
        }
        return result;
    }

    private void printList(ListNode node) {
        while (node != null) {
            System.out.print(node.val + " ");
            node = node.next;
        }
    }

    private ListNode reverseList(ListNode head) {
        System.out.println("\n Frank reverseList val:" + head.val);
        ListNode prev = null;
        while(head != null) {
            ListNode nextTemp = head.next;
            head.next = prev;
            prev = head;
            head = nextTemp;
        }
        return prev;
    }

    private ListNode endOfFirstHalf(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }


    public static void main(String[] args) {
        PalindromeLinkedList_234_E solution = new PalindromeLinkedList_234_E();
        //[1,2,3,3,2,1]
        // 1->2->3->3->2->1->null
        ListNode root = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(3);
        ListNode node5 = new ListNode(2);
        ListNode node6 = new ListNode(1);
        root.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;

        boolean isPalindrome = solution.isPalindromeHalfInPlace(root);
        System.out.println("The result val: " + isPalindrome);
    }
}
