package com.frank.linkedlist._160_E;


import com.frank.linkedlist.ListNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/20
 *     title : 160. Intersection of Two Linked Lists
 * </pre>
 */
public class IntersectionOfTwoLinkedLists_160_E {
    /***
     Given the heads of two singly linked-lists headA and headB, return the node
     at which the two lists intersect. If the two linked lists have no intersection
     at all, return null.

     For example, the following two linked lists begin to intersect at node c1:
     It is guaranteed that there are no cycles anywhere in the entire linked structure.

     Note that the linked lists must retain their original structure after
     the function returns.

     Example 1:
     Input: intersectVal = 8, listA = [4,1,8,4,5], listB = [5,6,1,8,4,5], skipA = 2, skipB = 3
     Output: Intersected at '8'
     Explanation: The intersected node's value is 8 (note that this must not be 0 if the two lists intersect).
     From the head of A, it reads as [4,1,8,4,5]. From the head of B, it reads as [5,6,1,8,4,5].
     There are 2 nodes before the intersected node in A;
     There are 3 nodes before the intersected node in B.

     Example 2:
     Input: intersectVal = 2, listA = [1,9,1,2,4], listB = [3,2,4], skipA = 3, skipB = 1
     Output: Intersected at '2'
     Explanation: The intersected node's value is 2 (note that this must not be 0 if the two lists intersect).
     From the head of A, it reads as [1,9,1,2,4]. From the head of B, it reads as [3,2,4].
     There are 3 nodes before the intersected node in A;
     There are 1 node before the intersected node in B.

     Example 3:
     Input: intersectVal = 0, listA = [2,6,4], listB = [1,5], skipA = 3, skipB = 2
     Output: No intersection
     Explanation: From the head of A, it reads as [2,6,4]. From the head of B, it reads as [1,5].
     Since the two lists do not intersect, intersectVal must be 0,
     while skipA and skipB can be arbitrary values.
     Explanation: The two lists do not intersect, so return null.


     Constraints:

     The number of nodes of listA is in the m.
     The number of nodes of listB is in the n.
     0 <= m, n <= 3 * 104
     1 <= Node.val <= 105
     0 <= skipA <= m
     0 <= skipB <= n
     intersectVal is 0 if listA and listB do not intersect.
     intersectVal == listA[skipA + 1] == listB[skipB + 1] if listA and listB intersect.

     Follow up: Could you write a solution that runs in O(n) time and use only O(1) memory?
     */

    //題意：回傳當2個鏈結串列值相同的節點

    /*
     思路：當2個連結串列都不為空時，判斷2個節點是否相同，相同時則直接回傳，不同時節點則直接移到下一個節點，
          而當某一個節點為空時將回傳另一個的節點。

          A1 = x長, B1 = y長, C1(交接長度) = z長
          A從x開始拜訪，B從y開始拜訪，遇到相等的值，表示就是到達相交會的點
          x + z + y   = y + z + x
     */

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }
        ListNode a = headA;
        ListNode b = headB;

        while (a != b) {
            a = a == null ? headB : a.next;
            b = b == null ? headA : b.next;
        }
        return a;
    }

    public static void main(String[] args) {
        IntersectionOfTwoLinkedLists_160_E solution = new IntersectionOfTwoLinkedLists_160_E();
        ListNode nodeA1 = new ListNode(4);
        ListNode nodeA2 = new ListNode(1);
        ListNode headA = nodeA1;

        ListNode nodeB1 = new ListNode(5);
        ListNode nodeB2 = new ListNode(0);
        ListNode nodeB3 = new ListNode(1);
        ListNode headB = nodeB1;

        ListNode nodeC1 = new ListNode(8);
        ListNode nodeC2 = new ListNode(4);
        ListNode nodeC3 = new ListNode(5);


        nodeA1.next = nodeA2;
        nodeA2.next = nodeC1;
        nodeC1.next = nodeC2;
        nodeC2.next = nodeC3;
        nodeC3.next = null;

        nodeB1.next = nodeB2;
        nodeB2.next = nodeB3;
        nodeB3.next = nodeC1;

        ListNode resultNode = solution.getIntersectionNode(headA, headB);
        System.out.println("intersectVal:" + resultNode.val);
    }
    
}
