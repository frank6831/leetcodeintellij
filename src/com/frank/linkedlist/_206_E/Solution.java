package com.frank.linkedlist._206_E;

import com.frank.linkedlist.ListNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/23
 *     title : 206. Reverse Linked List
 * </pre>
 */
public class Solution {
    
	/**
	 Reverse a singly linked list.

	 Example:

	 Input: 1->2->3->4->5->NULL
	 Output: 5->4->3->2->1->NULL
	 Follow up:

	 A linked list can be reversed either iteratively or recursively. Could you implement both?
	
	/*
	 題意: 反轉鍊結串列，寫出遞迴跟非遞迴的方法
	 */
	private ListNode head;

	/*
	 思路:反轉鍊結串列
	 1.保存head下一節點:先用next指向head的下一個節點，current指向head
	 2.將head所指向的下一節點改為prev
	 3.將prev替換為head，波浪式前進
	 4.將第一步保存的下一節點替換為head，用於下一次循環
	 */
	// Time : O(n), Space: O(1)
	private ListNode reverseList(ListNode head) {
		ListNode prev = null;
		ListNode current = head;
		ListNode next;
		while (current != null) {
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		return prev;
	}


	// prints content of double linked list
	private void printList(ListNode node) {
		while (node != null) {
			System.out.print(node.val + " ");
			node = node.next;
		}
	}

	/*
	1.判斷 head 或 head.next 是否為 null，若是則回傳 head
	2.宣告 ListNode root 使用遞迴去找出反轉後的head，就是根據 1.去判斷，若找到 root 後開始回傳
	3.head.next.next 改串接為 head
	4.head.next 則指向 null 斷開 cycle 的情況
	5.回傳已經反轉的 LinkedList

	遞歸解法的思路是，不斷的進入遞迴函數，直到head指向倒數第二個節點，
	因為head指向空或者是最後一個結點都直接返回了，newHead則指向對head的下一個結點調用遞歸函數返回的頭結點，
	此時newHead指向最後一個結點，然後head的下一個結點的next指向head本身，
	這個相當於把head結點移動到末尾的操作，因為是回溯的操作，
	所以head的下一個結點總是在上一輪被移動到末尾了，但head之後的next還沒有斷開，
	所以可以順勢將head移動到末尾，再把next斷開，最後返回newHead即可
	 */
	public ListNode reverseListRecursive(ListNode head) {
		if (head == null || head.next == null) return head;
		System.out.println("head val:" + head.val);
		ListNode newHead = reverseListRecursive(head.next);
		System.out.println("newHead val:" + newHead.val);
		System.out.println("after head val:" + head.val);
		head.next.next = head;
		head.next = null;
		return newHead;
	}


	public static void main(String[] args) {
        Solution list = new Solution();
		list.head = new ListNode(1);
		list.head.next = new ListNode(2);
		list.head.next.next = new ListNode(3);
		//list.head.next.next.next = new ListNode(4);

		System.out.println("Given Linked list");
		//list.printList(list.head);
		list.head = list.reverseList(list.head);
		System.out.println("");
		System.out.println("Reversed linked list ");
		list.printList(list.head);
    }
}
