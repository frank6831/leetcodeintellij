package com.frank.linkedlist._83_E;

import com.frank.linkedlist.ListNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2021/06/18
 *     title : 83. Remove Duplicates from Sorted List
 * </pre>
 */
public class RemoveDuplicatesFromSortedList_83_E {
    /***
     Given the head of a sorted linked list, delete all duplicates such that
     each element appears only once. Return the linked list sorted as well.

     Example 1:
     Input: head = [1,1,2]
     Output: [1,2]

     Example 2:
     Input: head = [1,1,2,3,3]
     Output: [1,2,3]


     Constraints:
     The number of nodes in the list is in the range [0, 300].
     -100 <= Node.val <= 100
     The list is guaranteed to be sorted in ascending order.
     */
    // 題意：
    
    private ListNode head;

    public ListNode deleteDuplicates(ListNode head) {
        ListNode cur = head;
        while (cur != null && cur.next != null) {
            if (cur.val == cur.next.val) {
                cur.next = cur.next.next;
            } else {
                cur = cur.next;
            }
        }
        return head;
    }

    public ListNode deleteDuplicates2(ListNode head) {
        /*
                  f       s
        1 -> 2 -> 3 -> 3
        p         n
        */
        if (head == null)
            return null;

        ListNode first = head;
        ListNode second = head.next;

        while (second != null) {
            if (first.val == second.val) {
                second = second.next;
                first.next = second;
            } else {
                first = first.next;
                second = second.next;
            }
        }

        first.next = null;
        System.gc();
        return head;
    }


    // prints content of double linked list
    private void printList(ListNode node) {
        while (node != null) {
            System.out.print(node.val + " ");
            node = node.next;
        }
    }
    
    public static void main(String[] args) {
        RemoveDuplicatesFromSortedList_83_E solution = new RemoveDuplicatesFromSortedList_83_E();
        solution.head = new ListNode(1);
        solution.head.next = new ListNode(1);
        solution.head.next.next = new ListNode(2);
        solution.head.next.next.next = new ListNode(3);
        solution.head.next.next.next.next = new ListNode(3);

        System.out.println("Given Linked list");
        solution.printList(solution.head);
        solution.head = solution.deleteDuplicates(solution.head);

        System.out.println("");
        System.out.println("After remove,  linked list :");
        solution.printList(solution.head);
    }
}
