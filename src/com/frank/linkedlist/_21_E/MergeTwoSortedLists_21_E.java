package com.frank.linkedlist._21_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/10/11
 *     title : 21. Merge Two Sorted Lists
 * </pre>
 */
public class MergeTwoSortedLists_21_E {
    /***
     You are given the heads of two sorted linked lists list1 and list2.

     Merge the two lists in a one sorted list.
     The list should be made by splicing together the nodes of the first two lists.

     Return the head of the merged linked list.


     Example 1:

     Input: list1 = [1,2,4], list2 = [1,3,4]
     Output: [1,1,2,3,4,4]
     Example 2:

     Input: list1 = [], list2 = []
     Output: []
     Example 3:

     Input: list1 = [], list2 = [0]
     Output: [0]


     Constraints:

     The number of nodes in both lists is in the range [0, 50].
     -100 <= Node.val <= 100
     Both list1 and list2 are sorted in non-decreasing order.
     */
    // 題意：將兩個已排序的linked lists合併，且所產生的新list必須由原有的節點(node)所構成。
    
    private ListNode head;
    public static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    /*
    思路：
    具體思想就是新建一個鏈表，然後比較兩個鏈表中的元素值，把較小的那個連到新的鏈表中，
    由於兩個輸入鍊錶的長度可能不同，所以最終會有一個鍊錶先完成插入所有元素，
    則直接另一個未完成的鍊錶直接鏈入新鍊錶的末尾。
     */

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;

        ListNode result = new ListNode(-1);
        ListNode cur = result;

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                cur.next = l1;
                l1 = l1.next;//
            } else {
                cur.next = l2;
                l2 = l2.next;
            }
            cur = cur.next;
        }
        //curr去連接剩下來的，也就是比較長的list
        cur.next = l1 != null ? l1:l2;
        return result.next;
    }


    /*
    拿出最左邊的node比較，稱為l1跟l2。
    l1是全空的狀況下，那其實答案就是l2(因為後面就不用繼續排了)，
    反之l2是全空的話，答案就是l1。
    那麼如果l1的值小於l2的值的話，那麼l1應該要當頭，
    接下來我們要拿l1.next跟l2來做比較大小，比較小的，
    就會是新的l1連接的下一個節點。
    一直連接到最後，我們就可以得到合併好的list了！
    相等的狀況因為題目沒有特別要求要分辨是l1還是l2的節點，
    所以任取一個均可。
     */
    private ListNode mergeTwoListsRecursive(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }

        if (l1.val < l2.val) {
            l1.next = mergeTwoListsRecursive(l1.next, l2);
            System.out.println(l1.val + ", ");
            return l1;
        } else {
            l2.next = mergeTwoListsRecursive(l1, l2.next);
            return l2;
        }
    }

    // prints content of double linked list
    private void printList(ListNode node) {
        while (node != null) {
            System.out.print(node.val + ", ");
            node = node.next;
        }
    }
    
    public static void main(String[] args) {
        MergeTwoSortedLists_21_E solution = new MergeTwoSortedLists_21_E();
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(4);

        ListNode l2= new ListNode(1);
        l2.next = new ListNode(3);
        l2.next.next = new ListNode(4);

        ListNode result = solution.mergeTwoListsRecursive(l1,l2);

        System.out.println("After remove,  linked list :");
        solution.printList(result);
    }
}
