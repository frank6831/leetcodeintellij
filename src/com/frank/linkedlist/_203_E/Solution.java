package com.frank.linkedlist._203_E;

import com.frank.linkedlist.ListNode;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/09/23
 *     title : 203. Remove Linked List Elements
 * </pre>
 */
public class Solution {
    
	/**
	 Remove all elements from a linked list of integers that have value val.

	 Example:

	 Input:  1->2->6->3->4->5->6, val = 6
	 Output: 1->2->3->4->5
	
	/*
	 題意: 給一個值val，移除連結陣列中所有值為val的節點。
	 */
	private ListNode head;

	/*
	 思路:
	 1.走訪連結陣列，只要比對目前節點與val相等，就移除目前這點
	 2.移除一個節點，其實是讓前一個節點跳過目前節點直接指向下一個節點
	 3.[1,6,3,5] val=3 ，移除[3]，因此我們需要知道[3]的前一點是[6]讓[6].next -> [5]
	 */
	// Time : O(n), Space: O(1)
	private ListNode removeElements(ListNode head, int val) {
		//先将前面满足删除条件的元素全部删除掉
		while (head != null && head.val == val) {
			head = head.next;
		}
		ListNode curr = head;

		// 刪除當前元素的下一個元素
		while (curr != null) {
			if (curr.next != null && curr.next.val == val) {
				curr.next = curr.next.next;
			} else {
				curr = curr.next;
			}
		}
		return head;
	}

	// 遞迴解法
	// 15312
	private ListNode removeElements2(ListNode head, int val) {
		if (head == null) return null;
		printList(head);
		System.out.println(" ");
		head.next = removeElements2(head.next, val);
		if (head.val == val) {
			return head.next;
		} else {
			return head;
		}
		//return head.val == val ? head.next : head;
	}

	//https://www.youtube.com/watch?v=cgzw4sPRihg
	//要用指標prev, cur來操作

	/*
		dummy(-1) ->  1 -> 5 -> 3 -> 1 -> 2
          prev       cur
	 */

	public ListNode removeElements3(ListNode head, int val) {
		ListNode dummyHead = new ListNode(-1);
		dummyHead.next = head;
		ListNode curr = head, prev = dummyHead;
		printList(prev);
		while (curr != null) {
			if (curr.val == val) {
				prev.next = curr.next;
			} else {
				prev = prev.next;
			}
			curr = curr.next;
		}
		return dummyHead.next;
	}


	// prints content of double linked list
	private void printList(ListNode node) {
		while (node != null) {
			System.out.print(node.val + " ");
			node = node.next;
		}
	}

	public static void main(String[] args) {
        Solution list = new Solution();
		list.head = new ListNode(1);
		list.head.next = new ListNode(5);
		list.head.next.next = new ListNode(3);
		list.head.next.next.next = new ListNode(1);
		list.head.next.next.next.next = new ListNode(2);

		System.out.println("Given Linked list");
		list.printList(list.head);
		list.head = list.removeElements3(list.head, 1);
		System.out.println("");
		System.out.println("After remove,  linked list :");
		list.printList(list.head);
    }
}
