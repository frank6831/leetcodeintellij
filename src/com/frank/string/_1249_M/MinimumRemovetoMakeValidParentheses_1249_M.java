package com.frank.string._1249_M;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/20
 *     title : 1249. Minimum Remove to Make Valid Parentheses
 * </pre>
 */
public class MinimumRemovetoMakeValidParentheses_1249_M {
    
	/**
	 Given a string s of '(' , ')' and lowercase English characters.

	 Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that the resulting parentheses string is valid and return any valid string.

	 Formally, a parentheses string is valid if and only if:

	 It is the empty string, contains only lowercase characters, or
	 It can be written as AB (A concatenated with B), where A and B are valid strings, or
	 It can be written as (A), where A is a valid string.


	 Example 1:

	 Input: s = "lee(t(c)o)de)"
	 Output: "lee(t(c)o)de"
	 Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.
	 Example 2:

	 Input: s = "a)b(c)d"
	 Output: "ab(c)d"
	 Example 3:

	 Input: s = "))(("
	 Output: ""
	 Explanation: An empty string is also valid.


	 Constraints:

	 1 <= s.length <= 105
	 s[i] is either'(' , ')', or lowercase English letter.
	*/

	// 題意：給定一個字符串s包含了小寫字母和圓括號 '(',')' 時，請移除最少的括號，使得剩下的字串中所有圓括號都是匹配的，然後返回任何一個合法的字符串。
	/*
	 	一個合法的圓括號字符串定義為：
		空字符串、只包含小寫字母的字符串都是合法的。
		如果字符串 AB 是合法的，那麼字符串 A+B 也是合法的。
		如果字符串 A 是合法的，那麼字符串 (A) 也是合法的。
	 */

	/*
		思路：
		1.建立Stack，記錄開始括號'('的索引位置，和一個 Set，記錄需要移除的括號')'的索引位置
		2.遍歷字符串，遇到括號'(', 將其索引位置加入Stack。若遇到結束括號')',且Stack是空的，就將其索引位置加入Set中，表示需要移除。
		3.若遇到結束括號')',且Stack不是空的，就從Stack中pop一個開始括號'('的索引位置。
		4.遍歷字符串，檢查Stack是否為空。若不為空，就將其加入 Set 中，表示需要移除。
		5.建立一個 StringBuilder，遍尋一遍，將不需要移除的字符加入 StringBuilder

		Time Complexity: O(n)
      	Space Complexity: O(n)
	 */

	public String minRemoveToMakeValid(String s) {
		Stack<Integer> stack = new Stack<>(); // 建立一個 Stack，用於記錄開始括號 '(' 的索引位置
		Set<Integer> toRemove = new HashSet<>(); // 建立一個 Set，用於記錄需要移除的括號 ')' 的索引位置
		//lee(t(c)o)de)
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '(') {
				stack.push(i); // 如果遇到開始括號 '('，就將其索引位置壓入 Stack 中
			} else if (c == ')') {
				if (stack.isEmpty()) {
					toRemove.add(i); // 如果遇到結束括號 ')'，且 Stack是空的，就將其索引位置加入 Set 中，表示需要移除
				} else {
					stack.pop(); // 如果遇到結束括號 ')'，且 Stack 不是空的，就從 Stack 中彈出一個開始括號 '(' 的索引位置
				}
			}
		}

		while (!stack.isEmpty()) {
			toRemove.add(stack.pop()); // 將 Stack 中剩餘的開始括號 '(' 的索引位置加入 Set 中，表示需要移除
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (!toRemove.contains(i)) {
				sb.append(c); // 將不需要移除的字符加入 StringBuilder 中
			}
		}
		return sb.toString(); // 將 StringBuilder 轉換為 String，並返回
	}

	public static void main(String[] args) {
		MinimumRemovetoMakeValidParentheses_1249_M solution = new MinimumRemovetoMakeValidParentheses_1249_M();
        String result = solution.minRemoveToMakeValid("))((");
        System.out.println("result:" + result);
    }
}
