package com.frank.string._522_M;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/1
 *     title : 521. Longest Uncommon Subsequence I
 * </pre>
 */
public class LongestUncommonSubsequence_II_522_M {
    
	/**
	 Given an array of strings strs, return the length of the longest uncommon subsequence between them.
	 If the longest uncommon subsequence does not exist, return -1.

	 An uncommon subsequence between an array of strings is a string that is a subsequence of one string but not the others.

	 A subsequence of a string s is a string that can be obtained after deleting any number of characters from s.

	 For example, "abc" is a subsequence of "aebdc" because you can delete the underlined characters in "aebdc" to get "abc".
	 Other subsequences of "aebdc" include "aebdc", "aeb", and "" (empty string).


	 Example 1:
	 Input: strs = ["aba","cdc","eae"]
	 Output: 3

	 Example 2:
	 Input: strs = ["aaa","aaa","aa"]
	 Output: -1


	 Constraints:

	 2 <= strs.length <= 50
	 1 <= strs[i].length <= 10
	 strs[i] consists of lowercase English letters.
	*/

	// 題意：給定一個由多個字符串組成的字符串陣列，找出其中最長的、不常見的子序列的長度。若不存在不常見的子序列，請返回-1。
	// 一個不常見的子序列是指它是某個字符串的子序列，但是不是其他字符串的子序列。

	/*
		思路：
		1.對於每個字符串，我們檢查它是否是其他字符串的子序列
		2.若是，那它就不是非共同子序列。否則，就是所有字符串的非共同子序列之一
		3.將其與最長非共同子序列的長度進行比較，取其中最大的值
		4.使用雙指針方法來進行線性搜索。使用兩個指針i和j分別指向兩個字符串，
		5.若兩個指針所指的字符相等，我們就將i向右移動。
		6.最後，若指針i能夠完全遍歷字符串a，那麼它就是字符串b的子序列。

		Time Complexity: O(n^2)
      	Space Complexity: O(1)
	 */

	public int findLUSlength(String[] strs) {
		int n = strs.length;
		int ans = -1;
		for (int i=0; i<n;i++) {
			boolean check = true; // 假設當前字串為不常見子序列
			for (int j=0; j< n;j++) { // 檢查其他字串是否包含當前字串
				if (i!=j && isSubseq(strs[i], strs[j])) { // 如果當前字串是其他字串的子序列
					check = false;// 當前字串不是不常見子序列
					break;// 可以提前結束檢查
				}
			}
			if (check) { // 如果當前字串是不常見子序列
				ans = Math.max(ans, strs[i].length()); // 更新最長不常見子序列的長度
			}
		}
		return ans;
	}

	// 判斷字串a是否是字串b的子序列
	private boolean isSubseq(String s, String t) {
		int i=0, j=0;
		while (i<s.length() && j < t.length()) {
			if (s.charAt(i) == t.charAt(j)) {
				i++;
			}
			j++;
		}
		return i == s.length();// 如果s中的所有字符都在t中出現過，則s是t的子序列
	}

	public static void main(String[] args) {
		LongestUncommonSubsequence_II_522_M solution = new LongestUncommonSubsequence_II_522_M();
		String[] array = {"aba","cdc","eae"};
        int result = solution.findLUSlength(array);
        System.out.println("result:" + result);
    }
}
