package com.frank.string._6_M;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/1/28
 *     title : 6. Zigzag Conversion
 * </pre>
 */
public class ZigzagConversion_6_M {
    
	/**
	 The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
	 (you may want to display this pattern in a fixed font for better legibility)

	 P   A   H   N
	 A P L S I I G
	 Y   I   R
	 And then read line by line: "PAHNAPLSIIGYIR"

	 Write the code that will take a string and make this conversion given a number of rows:

	 string convert(string s, int numRows);


	 Example 1:

	 Input: s = "PAYPALISHIRING", numRows = 3
	 Output: "PAHNAPLSIIGYIR"
	 Example 2:

	 Input: s = "PAYPALISHIRING", numRows = 4
	 Output: "PINALSIGYAHRPI"
	 Explanation:
	 P     I    N
	 A   L S  I G
	 Y A   H R
	 P     I
	 Example 3:

	 Input: s = "A", numRows = 1
	 Output: "A"


	 Constraints:

	 1 <= s.length <= 1000
	 s consists of English letters (lower-case and upper-case), ',' and '.'.
	 1 <= numRows <= 1000
	*/

	// 題意：輸入一個字串以及行高 numRows，我們將其重新排序成 Z 字型，並再次按照一行行的順序重新回傳字串。

	/*
	   解法一：
	   按照寫Z的過程，遍歷每個字符，然後將字符存到對應的行中。用flag保存當前的遍歷方向，如果遍歷到兩端，就改變方向。

	   算法流程： 按順序遍尋字符串s
	   1.res[i] += c： 把每個字符 c 填入對應行
	   2.i += flag：更新當前字符c對應的行索引
	   3.flag =- flag： 在達到Z字形轉折點時，執行反向。

	   Time: O(n)
	   Space: O(n)
	 */
	public String convert(String s, int numRows) {
		if (numRows < 2) return s;
		List<StringBuilder> rows = new ArrayList<>();

		for (int i =0; i< numRows;i++) {
			rows.add(new StringBuilder());
		}
		int i = 0, flag = -1;
		for (char c: s.toCharArray()) {
			rows.get(i).append(c);
			if (i ==0 || i == numRows -1) flag = -flag;
			i+=flag;
		}
		StringBuilder res = new StringBuilder();
		for (StringBuilder row:rows) {
			res.append(row);
		}
		return res.toString();
	}

    public static void main(String[] args) {
		ZigzagConversion_6_M solution = new ZigzagConversion_6_M();
		String result = solution.convert("AB", 1);
        System.out.println("result:" + result);
    }
}
