package com.frank.string._020_E;

import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/8/29
 *     title : 20. Valid Parentheses
 * </pre>
 */
public class Solution {

    /**
     * Given a string containing just the characters '(', ')', '{', '}', '[' and ']',
     * determine if the input string is valid.
     * <p>
     * An input string is valid if:
     * <p>
     * Open brackets must be closed by the same type of brackets.
     * Open brackets must be closed in the correct order.
     * Note that an empty string is also considered valid.
     * <p>
     * Example 1:
     * Input: "()"
     * Output: true
     * <p>
     * Example 2:
     * Input: "()[]{}"
     * Output: true
     * <p>
     * Example 3:
     * Input: "(]"
     * Output: false
     * <p>
     * Example 4:
     * Input: "([)]"
     * Output: false
     * <p>
     * Example 5:
     * Input: "{[]}"
     * Output: true
     */
	/* 題目翻譯
	   給一串只包含有'(', ')', '{', '}', '[' 和']',的字符串，要求判定是否是合法的。
    */
	/*
	  思路：用stack先進後出的特性來解，遇到左括號就放入stack，遇到右括號就取出stack裡面的左括號比對是否合法。
	  1.
	 */
    private boolean isValid(String s) {
        if (s == null || s.equals("")) //空的回傳合法
            return true;

        Stack<Character> stack = new Stack<>(); //建立一個stack
        //stack規則:後進先出
        if (s.length() % 2 != 0)
            return false; //如果不是雙數，必不合法

        char[] ca = s.toCharArray();
        if (ca[0] == ')' || ca[0] == ']' || ca[0] == '}')
            return false; //如果第一個非左括號，必錯

        try {
            for (char temp : ca) {
                //遇到右括號，先pop出已存進stack的資料，若不相符則回傳錯誤
                if (temp == ')') {
                    char sc = stack.pop();
                    if (sc != '(')
                        return false;
                } else if (temp == ']') {
                    char sc = stack.pop();
                    if (sc != '[')
                        return false;
                } else if (temp == '}') {
                    char sc = stack.pop();
                    if (sc != '{')
                        return false;

                } else { //非右括號的全都存進stack
                    stack.push(temp);
                }
            }
        } catch (Exception e) { //若stack.pop不出來，會挑出例外，代表不成對回傳錯誤
            return false;
        }
        //如果已經結束了，stack還沒空代表錯誤
        return stack.empty();
    }


    public boolean isValid2(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c == '(')
                stack.push(')');
            else if (c == '{')
                stack.push('}');
            else if (c == '[')
                stack.push(']');
            else if (stack.isEmpty() || stack.pop() != c)
                return false;
        }
        return stack.isEmpty();
    }



    public boolean isValid3(String s) {
        if (s == null || s.length() ==0) {
            return false;
        }
        Stack<Character> stack = new Stack<>();

        for (char c: s.toCharArray()) {
            if (c == '(')
                stack.push(')');
            else if (c == '[')
                stack.push(']');
            else if (c == '{')
                stack.push('}');
            else if (stack.isEmpty() || stack.pop() != c)
                return false;
        }
        return stack.isEmpty();
    }
    // }])



    public static void main(String[] args) {
        Solution solution = new Solution();

        boolean result = solution.isValid3("([{}])");
        System.out.println("result:" + result);
    }
}
