package com.frank.string._003;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <pre>
 *     author: frank
 *     time  : 2017/10/11
 *     desc  : 3. Longest Substring Without Repeating Characters
 * </pre>
 */
public class Solution {
	/***
	 Given a string, find the length of the longest substring without repeating characters.

	Example 1:
	Input: "abcabcbb"
	Output: 3 
	Explanation: The answer is "abc", with the length of 3.
	 
	Example 2:
	Input: "bbbbb"
	Output: 1
	Explanation: The answer is "b", with the length of 1.
	
	Example 3:
	Input: "pwwkew"
	Output: 3
	Explanation: The answer is "wke", with the length of 3. 
	             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
	 */

    /*
     題意：給一個字符串，找出裡面最長的不重複的字符串
     */

    public int lengthOfLongestSubstring(String s) {
        int len;
        if (s == null || (len = s.length()) == 0) return 0;
        int preP = 0, max = 0;
        int[] hash = new int[128];
        for (int i = 0; i < len; ++i) {
            char c = s.charAt(i);
            System.out.println("c:"+ c + ", preP:" + preP);
            if (hash[c] > preP) {
                preP = hash[c];
            }
            int l = i - preP + 1;
            hash[c] = i + 1;
            System.out.println("l:"+ l + ", hash[c]:" + i+1);
            if (l > max) max = l;
        }
        return max;
    }
    
    public int lengthOfLongestSubstringSet(String s) {
        int n = s.length();
        Set<Character> set = new HashSet<>();
        int ans = 0, i = 0, j = 0;
        System.out.println("length:"+ n);
        while (j < n) {
            // try to extend the range [i, j]
            if (!set.contains(s.charAt(j))){
                System.out.println("charAt:"+ s.charAt(j));
                set.add(s.charAt(j++));
                ans = Math.max(ans, set.size());
            } else {
                set.remove(s.charAt(i++));
            }
        }
        return ans;
    }

    /*
         Sliding Window
         Time: O(2n)=O(n), Space: O(min(m,n))
     */
    public int lengthOfLongestSubstringSlidingWindow(String s) {
        Map<Character, Integer> chars = new HashMap<>();

        int left = 0;
        int right = 0;

        int res = 0;
        while (right < s.length()) {
            char r = s.charAt(right);
            chars.put(r, chars.getOrDefault(r,0) + 1);

            while (chars.get(r) > 1) {
                char l = s.charAt(left);
                chars.put(l, chars.get(l) - 1);
                left++;
            }

            res = Math.max(res, right - left + 1);

            right++;
        }
        return res;
    }

    /*
     Sliding Window Optimized
     方法：用 Map (較佳): 記錄 char 和 index，檢查到重複的話，比較看看重複的 index 有沒有比目前的 i 大，
     沒有的話代表不在這個範圍不用管，把新的 char , index 加入取代掉。
     Time: O(n), Space: O(min(m,n))
     */
    public int lengthOfLongestSubstringSlidingWindowOptimized(String s) {
        Map<Character, Integer> map = new HashMap<>(); // current index of character
        int n = s.length();
        int maxLen = 0;
        int left = 0;//左指針
        // try to extend the range [i, j]
        for (int j = 0; j < n; j++) {
            //若map裡已有該值，表示重複，則取出該值的index跟left比較，取最大值當作新的left起點
            char word = s.charAt(j);
            if (map.containsKey(word)) {
                left = Math.max(map.get(word), left);
            }
            //最大長度 = 右指針-左指針+1
            maxLen = Math.max(maxLen, j - left + 1);
            //將不重複的值塞入map裡，(key:該字母, value:字母的index)
            map.put(word, j + 1);
        }
        return maxLen;
    }
    
    
    public int lengthOfLongestSubstringTable(String s) {
        int n = s.length(), ans = 0;
        int[] index = new int[128]; // current index of character
        // try to extend the range [i, j]
        for (int j = 0, i = 0; j < n; j++) {
            i = Math.max(index[s.charAt(j)], i);
            ans = Math.max(ans, j - i + 1);
            index[s.charAt(j)] = j + 1;
        }
        return ans;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.lengthOfLongestSubstringSlidingWindowOptimized("abcabcbb"));
        //System.out.println(solution.lengthOfLongestSubstringSet("bbbbb"));
        //System.out.println(solution.lengthOfLongestSubstring("pwwkew"));
        //System.out.println(solution.lengthOfLongestSubstring("Abcabcbb"));
    }
}
