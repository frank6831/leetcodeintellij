package com.frank.string._521_E;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/1
 *     title : 521. Longest Uncommon Subsequence I
 * </pre>
 */
public class LongestUncommonSubsequence_I_521_E {
    
	/**
	 Given two strings a and b, return the length of the longest uncommon subsequence between a and b.
	 If the longest uncommon subsequence does not exist, return -1.

	 An uncommon subsequence between two strings is a string that is a subsequence of one but not the other.

	 A subsequence of a string s is a string that can be obtained after deleting any number of characters from s.

	 For example, "abc" is a subsequence of "aebdc" because you can delete the underlined characters in "aebdc" to get "abc".
	 Other subsequences of "aebdc" include "aebdc", "aeb", and "" (empty string).


	 Example 1:

	 Input: a = "aba", b = "cdc"
	 Output: 3
	 Explanation: One longest uncommon subsequence is "aba" because "aba" is a subsequence of "aba" but not "cdc".
	 Note that "cdc" is also a longest uncommon subsequence.
	 Example 2:

	 Input: a = "aaa", b = "bbb"
	 Output: 3
	 Explanation: The longest uncommon subsequences are "aaa" and "bbb".
	 Example 3:

	 Input: a = "aaa", b = "aaa"
	 Output: -1
	 Explanation: Every subsequence of string a is also a subsequence of string b. Similarly, every subsequence of string b is also a subsequence of string a.


	 Constraints:

	 1 <= a.length, b.length <= 100
	 a and b consist of lower-case English letters.
	*/

	// 題意：給定兩個字符串a和b，返回a和b之間最長的非共同子序列的長度。如果最長的非共同子序列不存在，則返回-1。


	/*
		思路：
		1.檢查兩個字符串是否相等，如果相等，則不存在非共同子序列，返回-1
		2.若不相等，那麼最長的非共同子序列就是兩個字符串中較長的那個。

		Time Complexity: O(1)
      	Space Complexity: O(1)
	 */

	public int findLUSlength(String a, String b) {
		if (a.equals(b)) {
			return -1;
		}
		return Math.max(a.length(), b.length());
	}

	public static void main(String[] args) {
		LongestUncommonSubsequence_I_521_E solution = new LongestUncommonSubsequence_I_521_E();
        int result = solution.findLUSlength("aba", "cdc");
        System.out.println("result:" + result);
    }
}
