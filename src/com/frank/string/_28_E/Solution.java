package com.frank.string._28_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/9/2
 *     title : 28. Implement strStr()
 * </pre>
 */
public class Solution {
    
	/**
     Implement strStr().

     Return the index of the first occurrence of needle in haystack,
     or -1 if needle is not part of haystack.

     Example 1:
     Input: haystack = "hello", needle = "ll"
     Output: 2

     Example 2:
     Input: haystack = "aaaaa", needle = "bba"
     Output: -1

     Clarification:
     What should we return when needle is an empty string?
     This is a great question to ask during an interview.
     For the purpose of this problem, we will return 0 when needle is an empty string.
     This is consistent to C's strstr() and Java's indexOf().
	*/
	// 題目:給一個指針needle跟字串堆疊haystack，回傳指針第一次在堆疊中出現的位置index。
	/*
	  思路：這道題讓我們在一個字符串中找另一個字符串第一次出現的位置，那我們首先要做一些判斷，
	  1.如果子字符串為空，則返回0，如果子字符串長度大於母字符串長度，則返回-1。
	  2.遍歷母字符串，並不需要遍歷整個母字符串，而是遍歷到剩下的長度和子字符串相等的位置即可，這樣可以提高運算效率。
	  3.對於每一個字符，我們都遍歷一遍子字符串，一個一個字符的對應比較，如果對應位置有不等的，則跳出循環，
	    如果一直都沒有跳出循環，則說明子字符串出現了，則返回起始位置即可。
	 */
    private int strStr(String haystack, String needle) {
        System.out.println("haystack:" + haystack+ ", needle:" + needle);
        if (needle.isEmpty()) return 0;
        int m = haystack.length(), n = needle.length();
        if (m < n) return -1;
        System.out.println("m:" + m+ ", n:" + n);
        for (int i = 0; i <= m - n; ++i) {
            int j;
            for (j = 0; j < n; ++j) {
                System.out.println("i + j: " + (i + j) + ", source item:" + haystack.charAt(i + j) + ", com item:" + needle.charAt(j));
                if (haystack.charAt(i + j) != needle.charAt(j)) {
                    System.out.println("break");
                    break;
                }
            }
            System.out.println("j:" + j + ", n:" + n + ", i:" + i);
            if (j == n) {
                return i;
            }
        }
        return -1;
    }

    // 暴力法，時間複雜度為O(nm)
    private int strStr2(String haystack, String needle) {
        int n = haystack.length(), m = needle.length();
        for (int i = 0; i < n - m + 1; i++) {
            boolean ok = true;
            for (int j = 0; j < m; j++)
                if (haystack.charAt(i + j) != needle.charAt(j)) {
                    ok = false;
                    break;
                }
            if (ok) {
                return i;
            }
        }
        return -1;
    }


    // Use substring
    public int strStr4(String haystack, String needle) {
        if (needle.length() == 0) {
            return 0;
        }

        if (needle.length() > haystack.length()) {
            return -1;
        }

        for (int i = 0, j = needle.length(); j <= haystack.length(); i++, j++) {
            if (haystack.substring(i,j).equals(needle)) {
                return i;
            }
        }
        return -1;
    }



    public int strStrNow(String haystack, String needle) {
        // 先判斷 needle 是否為空，若是則返回 0
        if (needle.isEmpty()) {
            return 0;
        }

        int n = haystack.length(), m = needle.length();
        // 判斷 haystack 長度是否小於 needle 長度，若是則返回 -1
        if (n < m) {
            return -1;
        }
        // 使用 indexOf() 方法來找到第一個匹配的子字符串
        return haystack.indexOf(needle);
    }


    public static void main(String[] args) {
    	Solution solution = new Solution();

        int result = solution.strStrNow("leetcode", "leeto");
        System.out.println("result:" + result);
    }
}
