package com.frank.string._844_E;

import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/30
 *     title : 844. Backspace String Compare
 * </pre>
 */
public class BackspaceStringCompare_844_E {
    
	/**
	 Given two strings s and t, return true if they are equal
	 when both are typed into empty text editors. '#' means a backspace character.

	 Note that after backspacing an empty text, the text will continue empty.


	 Example 1:
	 Input: s = "ab#c", t = "ad#c"
	 Output: true
	 Explanation: Both s and t become "ac".

	 Example 2:
	 Input: s = "ab##", t = "c#d#"
	 Output: true
	 Explanation: Both s and t become "".

	 Example 3:
	 Input: s = "a#c", t = "b"
	 Output: false
	 Explanation: s becomes "c" while t becomes "b".


	 Constraints:
	 1 <= s.length, t.length <= 200
	 s and t only contain lowercase letters and '#' characters.

	 Follow up: Can you solve it in O(n) time and O(1) space?
	*/

	// 題意：題目輸入兩組字串，字串中的字元依序為輸入的順序，但如果字元為 # ，則表示在此輸入了刪除鍵（backspace）；
	// 如果前一個字元存在，則刪除它，最後，我們需要判斷兩組字串的結果是否相同。

	/*
	 * 解法一：Stack 解法
	 *   建立 stack，依序將字元推入，在遇到刪除符號 # 時則將 stack 中的字元彈出。
	 *   最後，則直接比較兩 stack 間是否相同。
	 *
	 * 	 Time: O(M+N), where M,N are the lengths of S and T respectively.
	 *   Space: O(M+N).
	 */

	public boolean backspaceCompareStack(String s, String t) {
		return helper(s).equals(helper(t));
	}

	private String helper(String str) {
		Stack<Character> stack = new Stack<>();
		for (int i=0; i< str.length();i++) {
			char c = str.charAt(i);
			if (c == '#') {
				if (!stack.isEmpty()) {
					stack.pop();
				}
			} else {
				stack.push(c);
			}
		}
		return String.valueOf(stack);
	}

	/*
	 解法二：StringBuilder 解法
	 使用sb, 速度會更快
	 */
	public boolean backspaceCompareSb(String s, String t) {
		return sb(s).equals(sb(t));
	}

	private String sb(String str) {
		StringBuilder sbr = new StringBuilder();

		for (char c:str.toCharArray()) {
			if (c != '#') {
				sbr.append(c);
			} else if (sbr.length() != 0) {
				sbr.deleteCharAt(sbr.length() -1);
			}
		}
		return sbr.toString();
	}


    public static void main(String[] args) {
		BackspaceStringCompare_844_E solution = new BackspaceStringCompare_844_E();
        boolean result = solution.backspaceCompareStack("ab#c","ad#c" );
        System.out.println("result:" + result);
    }
}
