package com.frank.string._22_M;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/30
 *     title : 22. Generate Parentheses
 * </pre>
 */
public class GenerateParentheses_22_E {
    
	/**
	 Given n pairs of parentheses, write a function to generate all combinations of
	 well-formed parentheses.



	 Example 1:

	 Input: n = 3
	 Output: ["((()))","(()())","(())()","()(())","()()()"]
	 Example 2:

	 Input: n = 1
	 Output: ["()"]


	 Constraints:

	 1 <= n <= 8
	*/

	// 題意：數字n代表生成括號的對數，請設計一個函數，用於能夠生成所有可能的並且有效的括號組合。
	// 重點在於, 如何做出有效括號-只要保持左括號數量不小於右括號即可。

	/*
	   解法一： 回溯法
	   1.右括號數量大於左括號數量, 失敗-直接返回
	   2.左右括號數量皆等於要求數量, 此為成功條件, 將字串存入答案數組中, 然後返回
       3.左或右括號的數量超過給定數, 失敗-直接返回
	   Time: O(4^n/ 根號n)
	   Space: O(n)
	 */
	List<String> res;
	public List<String> generateParenthesis(int n) {
		res = new ArrayList<>();
		helper("", n, n);
		return res;
	}

	private void helper(String curr, int left, int right) {
		if (left ==0 && right ==0) {
			res.add(curr);
			return;
		}
		if (left > 0) {
			helper(curr + "(", left -1, right);
		}

		// 可用的括號 右括號大於左括號時，說明有 左括號先放置，才會是有效的括號組合
		if (right > left) {
			helper(curr + ")", left, right -1);
		}
	}

	/*
	 1.當前左右括號都有大於0個可以使用的時候，才產生分支；
	 2.產生左分支的時候，只看當前是否還有左括號可以使用；
	 3.產生右分支的時候，還受到左分支的限制，右邊剩餘可以使用的括號數量一定得在嚴格大於左邊剩餘的數量的時候，才可以產生分支；
	 4.在左邊和右邊剩餘的括號數都等於0的時候結算。
	 */
	private void dfs(String curStr, int left, int right, List<String> res) {
		// 因為每一次嘗試，都使用新的字符串變量，所以無需回溯
		// 在遞歸終止的時候，直接把它添加到結果集即可，注意與「力扣」第 46 題、第 39 題區分
		if (left == 0 && right == 0) {
			res.add(curStr);
			return;
		}

		// 剪枝（如圖，左括號可以使用的個數嚴格大於右括號可以使用的個數，才剪枝，注意這個細節）
		if (left > right) {
			return;
		}

		if (left > 0) {
			dfs(curStr + "(", left - 1, right, res);
		}

		if (right > 0) {
			dfs(curStr + ")", left, right - 1, res);
		}
	}

    public static void main(String[] args) {
		GenerateParentheses_22_E solution = new GenerateParentheses_22_E();
		List<String> result = solution.generateParenthesis(2);
        System.out.println("result:" + result);
    }
}
