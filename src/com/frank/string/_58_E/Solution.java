package com.frank.string._58_E;


/**
 * <pre>
 *     author: frank
 *     time  : 2019/9/16
 *     title : 58. Length of Last Word
 * </pre>
 */
public class Solution {
    
	/**
     Given a string s consists of upper/lower-case alphabets and empty space characters ' ',
     return the length of last word in the string.

     If the last word does not exist, return 0.

     Note: A word is defined as a character sequence consists of non-space characters only.

     Example:

     Input: "Hello World"
     Output: 5
	*/
	// 題目:
	/*
	  思路：先用空白符號分割字串，變成字串陣列，取最後一個字串的長度
	 */
    private int lengthOfLastWord(String s) {
        if (s == null || s.length() == 0) return 0;
        int lastWordLength = 0;
        String[] result = s.split(" ");
        int size = result.length;
        if (size > 0) {
            lastWordLength = result[size - 1].length();
        }
        return lastWordLength;
    }

    public static void main(String[] args) {
    	Solution solution = new Solution();

        int result = solution.lengthOfLastWord(" ");
        System.out.println("result:" + result);
    }
}
