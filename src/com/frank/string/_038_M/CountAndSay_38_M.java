package com.frank.string._038_M;

/**
 * <pre>
 *     author: Frank
 *     blog  : http://blankj.com
 *     time  : 2018/02/05
 *     desc  :
 * </pre>
 */
public class CountAndSay_38_M {

	/***
	 The count-and-say sequence is a sequence of digit strings defined by the recursive formula:

	 countAndSay(1) = "1"
	 countAndSay(n) is the way you would "say" the digit string from countAndSay(n-1),
	 which is then converted into a different digit string.
	 To determine how you "say" a digit string, split it into the minimal number of substrings
	 such that each substring contains exactly one unique digit. Then for each substring,
	 say the number of digits, then say the digit. Finally, concatenate every said digit.

	 For example, the saying and conversion for digit string "3322251":


	 Given a positive integer n, return the nth term of the count-and-say sequence.



	 Example 1:

	 Input: n = 1
	 Output: "1"
	 Explanation: This is the base case.
	 Example 2:

	 Input: n = 4
	 Output: "1211"
	 Explanation:
	 countAndSay(1) = "1"
	 countAndSay(2) = say "1" = one 1 = "11"
	 countAndSay(3) = say "11" = two 1's = "21"
	 countAndSay(4) = say "21" = one 2 + one 1 = "12" + "11" = "1211"


	 Constraints:

	 1 <= n <= 30
	 */

	/*
     題意：找第n個數(字符串表示)，規則則是對於連續字符串，表示為重複次數+數本身。
     */


	/*
      解法：
      1.初始化第一個狀態s= "1"
      2.每次循環多次模擬求解，統計每個數字的個數，然後按照規定格式添加即可，
      3.通過對字符串s的不斷更新求解，用新的字符串去更新原本的字符串s時，使用StringBuilder的append方法即可
      Time Complexity: O(N*M) , M為字符串長度，N為給定的正整數n。
      Space Complexity: O(M)
	 */
	public String countAndSay(int n) {
		if (n == 1){
			return "1";
		}
		String s = "1";  
		int count = 1;
		for (int j = 0; j < n - 1; j++) {  
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < s.length(); i++) {
				// 數字相同，count++
		        if (i < s.length() - 1 && s.charAt(i) == s.charAt(i + 1)) {
					count++;
		        } else {
					// 數字不同，將目前的count與num加到字串，更新num，新的num從1開始數起
		            sb.append(count).append(s.charAt(i));
					count = 1;
		        }
		    }
		    s = sb.toString();
			System.out.println("s:" + s + ", j:" + j);
		}  
		return s;  
	}
	
	public static void main(String[] args) {
		CountAndSay_38_M solution = new CountAndSay_38_M();
		System.out.println("result:" + solution.countAndSay(4));
	}
}
