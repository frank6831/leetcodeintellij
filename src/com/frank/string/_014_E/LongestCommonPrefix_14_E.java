package com.frank.string._014_E;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/05
 *     title : 14. Longest Common Prefix
 * </pre>
 */
public class LongestCommonPrefix_14_E {
	/***
	 Write a function to find the longest common prefix string amongst an array of strings.

	 If there is no common prefix, return an empty string "".



	 Example 1:

	 Input: strs = ["flower","flow","flight"]
	 Output: "fl"
	 Example 2:

	 Input: strs = ["dog","racecar","car"]
	 Output: ""
	 Explanation: There is no common prefix among the input strings.


	 Constraints:

	 1 <= strs.length <= 200
	 0 <= strs[i].length <= 200
	 strs[i] consists of only lowercase English letters.
	 */

	//題意：從一組字串陣列中找出其最長的共用前綴，字串只會由a到z所構成，若沒有的話則回傳空字串。

	/*
	  思路：

	 */

	/**
	  Method: Vertical scanning
	  Time: O(S), Space:O(1)
	 */
	public String longestCommonPrefixVertical(String[] strs) {
		if (strs == null || strs.length == 0) return "";

		for (int i=0; i< strs[0].length();i++) {
			char c = strs[0].charAt(i);
			for (int j=1; j< strs.length;j++) {
				System.out.println("i:"+ i+ ",c:"+ c +  ", j:" + j + ", str[j]:" + strs[j]);
				if (i == strs[j].length() || strs[j].charAt(i) != c) {
					System.out.println("i:" + i);
					return strs[0].substring(0,i);
				}
			}
		}
		return strs[0];
	}

	/**
	 * Horizontal scanning
	 * 依序遍歷陣列中每個字串，對於每個遍歷到的字串，更新最長公共前綴，當遍歷完所有字串後，即可得到字符數組中的最長公共前綴
	 * Time: O(S), Space:O(1)
	 */
	public String longestCommonPrefixHorizontal(String[] strs) {
		if (strs == null || strs.length == 0) return "";

		// 將第一個字符串設置為基準，初始化最長公共前綴為該字符串
		String prefix = strs[0];

		// 從第二個字符串開始遍歷
		for (int i=1; i< strs.length;i++) {
			System.out.println("strs[i]:" + strs[i]);
			//若當下字串並沒有包括prefix內容, 就從尾巴依序刪除，直到比對出有包含prefix字串，即找到
			while(!strs[i].startsWith(prefix)) {
				prefix = prefix.substring(0, prefix.length() -1);
				System.out.println("prefix:" + prefix);
			}
		}
		return prefix;
	}

	public static void main(String[] args) {
		LongestCommonPrefix_14_E solution = new LongestCommonPrefix_14_E();
		String[] sample = {"ab", "abc", "abcde", "aeg"};
		String[] sample2 = {"leets", "lecode", "ldet", "leeds"};
		System.out.println("result:" + solution.longestCommonPrefixHorizontal(sample2));
	}


}

