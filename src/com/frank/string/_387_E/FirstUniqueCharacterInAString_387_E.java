package com.frank.string._387_E;

import java.util.*;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/3/1
 *     title : 387. First Unique Character in a String
 * </pre>
 */
public class FirstUniqueCharacterInAString_387_E {
    
	/**
	 Given a string s, find the first non-repeating character in it and return its index. If it does not exist, return -1.



	 Example 1:

	 Input: s = "leetcode"
	 Output: 0
	 Example 2:

	 Input: s = "loveleetcode"
	 Output: 2
	 Example 3:

	 Input: s = "aabb"
	 Output: -1


	 Constraints:

	 1 <= s.length <= 105
	 s consists of only lowercase English letters.
	*/

	// 題意：找到一個給定字串中第一個不重複的字符，並返回該字符的索引。如果該字串中所有字符都是重複的，則返回 -1。


	/*
		思路：
		1.使用 HashMap 來儲存每個字符的出現次數，將其出現次數放入對應的值中。若該字符已存在，將其對應的值加 1
		2.遍歷字符串，檢查其出現次數是否為1。如果是，則返回該字符的索引，這是第一個不重複出現的字符
		3.在迴圈中沒有找到任何一個不重複出現的字符，則返回-1。

		Time Complexity: O(n)
      	Space Complexity: O(n)
	 */

	public int firstUniqChar(String s) {
		Map<Character, Integer> charCount = new HashMap<>();

		// 計算字串中每個字符的出現次數
		for (int i=0; i< s.length();i++) {
			char ch = s.charAt(i);
			charCount.put(ch, charCount.getOrDefault(ch, 0) + 1);
		}

		// 找到第一個出現次數為 1 的字符的索引
		for (int i=0; i< s.length();i++) {
			if (charCount.get(s.charAt(i)) == 1) {
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		FirstUniqueCharacterInAString_387_E solution = new FirstUniqueCharacterInAString_387_E();
        int result = solution.firstUniqChar("leetcode");
        System.out.println("result:" + result);
    }
}
