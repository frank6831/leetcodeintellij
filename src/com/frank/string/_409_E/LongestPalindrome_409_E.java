package com.frank.string._409_E;

import java.util.HashSet;

/**
 * <pre>
 *     author: frank
 *     time  : 2022/11/1
 *     title : 409. Longest Palindrome
 * </pre>
 */
public class LongestPalindrome_409_E {
    
	/**
	 Given a string s which consists of lowercase or uppercase letters, return the length of
	 the longest palindrome that can be built with those letters.

	 Letters are case-sensitive, for example, "Aa" is not considered a palindrome here.

	 Example 1:
	 Input: s = "abccccdd"
	 Output: 7
	 Explanation: One longest palindrome that can be built is "dccaccd", whose length is 7.

	 Example 2:
	 Input: s = "a"
	 Output: 1
	 Explanation: The longest palindrome that can be built is "a", whose length is 1.


	 Constraints:
	 1 <= s.length <= 2000
	 s consists of lowercase and/or uppercase English letters only.
	*/

	// 題意：輸入1個字串，裡面有不同的字元，且大小寫也視為不同，求從這些字元組成最大回文的長度。

	// https://zihengcat.github.io/2021/06/30/leetcode-409-longest-palindrome/
	/*
	 * 解法一：分析每個字元出現的頻率，假如X字元出現偶數次，則它一定能全部拿去組回文。
	 * 遍歷字符串，使用一枚哈希表記錄下等待配對的字符，發現配對成功，將配對成功的字符從哈希表中移除，遍歷完成就找全了所有可配對的回文串。
	 * 	 Time: O(n); Space: O(n)
	 */

	//"abccccdd"
	public static int longestPalindromeMap(String s) {
		if (s==null || s.length()==0) return 0;
		HashSet<Character> hs = new HashSet<>();
		int count = 0;
		for (int i=0; i<s.length(); i++) {
			if (hs.contains(s.charAt(i))) {
				hs.remove(s.charAt(i));
				count++;
			} else {
				hs.add(s.charAt(i));
			}
		}
		System.out.println("map size:" + hs.size());
		if (!hs.isEmpty()) return count*2+1;
		return count*2;
	}

	public static int longPalindrome(String str) {
		if (str == null || str.length() == 0) return 0;

		HashSet<Character> resultSet = new HashSet<>();

		int count = 0;

		for (int i=0; i< str.length();i++) {
			char value = str.charAt(i);
			if (resultSet.contains(value)) {
				resultSet.remove(value);
				count++;
			} else {
				resultSet.add(value);
			}
		}

		if (resultSet.size() > 0) {
			return count*2 +1;
		}
		return count*2;
	}


    public static void main(String[] args) {
        int result = longPalindrome("abccccdd");
        System.out.println("result:" + result);
    }
}
