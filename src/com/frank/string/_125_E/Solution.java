package com.frank.string._125_E;


/**
 * <pre>
 *     author: frank
 *     time  : 2019/9/24, 2022/11/10
 *     title : 125. Valid Palindrome
 * </pre>
 */
public class Solution {

    /**
     Given a string, determine if it is a palindrome, considering only
     alphanumeric characters and ignoring cases.

     Note: For the purpose of this problem, we define empty string as valid palindrome.

     Example 1:

     Input: "A man, a plan, a canal: Panama"
     Output: true

     Example 2:
     Input: "race a car"
     Output: false
     */
	/* 題目翻譯
	   給一個字串，不考慮大小寫與非字母數字的情況下，判斷這個字串是不是迴文。
    */

	/*
	  思路：使用兩個指針
	  用兩個指針，一個從頭開始遍歷，一個從末尾開始遍歷，當頭尾都找到字母或者數字後，就進行對比是否是相同的，
	  有不同說明不是回文，否則就是回文，在比較時我們將大寫字母都轉化成小寫來對比，當然也可以反過來。
	 */
    public boolean isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return false;
        }

        s = s.toLowerCase();
        s = s.replaceAll("[^0-9a-z]", "");
        System.out.println("s:" + s);
        int i = 0;
        int j = s.length() - 1;

        while (i < j) {
            System.out.println("s.charAt(i):" + s.charAt(i) + ",  s.charAt(j):" +  s.charAt(j));
            if (s.charAt(i) == s.charAt(j)) {
                i++;
                j--;
            } else {
                return false;
            }
        }
        return true;
    }

    // Time Complexity: O(N), Space Complexity: O(1)
    private boolean isPalindrome2(String s) {
        if (s == null || s.length() == 0) {
            return false;
        }

        int left = 0;
        int right = s.length() -1;
        s = s.toLowerCase();
        while (left < right) {
            while (left < right && !Character.isLetterOrDigit(s.charAt(left))) {
                left++;
            }
            while (left < right && !Character.isLetterOrDigit(s.charAt(right))) {
                right--;
            }

            if (s.charAt(left) != s.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }



    public static void main(String[] args) {
        Solution solution = new Solution();

        boolean result = solution.isPalindrome("A man, a plan, a canal: Panama");
        System.out.println("result:" + result);
    }
}
