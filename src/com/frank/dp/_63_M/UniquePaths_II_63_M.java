package com.frank.dp._63_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/8
 *     title  : 63.Unique Paths II
 * </pre>
 */
public class UniquePaths_II_63_M {
    /**
     You are given an m x n integer array grid. There is a robot initially located at the top-left corner (i.e., grid[0][0]).
     The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]).
     The robot can only move either down or right at any point in time.

     An obstacle and space are marked as 1 or 0 respectively in grid.
     A path that the robot takes cannot include any square that is an obstacle.

     Return the number of possible unique paths that the robot can take to reach the bottom-right corner.

     The testcases are generated so that the answer will be less than or equal to 2 * 109.



     Example 1:


     Input: obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
     Output: 2
     Explanation: There is one obstacle in the middle of the 3x3 grid above.
     There are two ways to reach the bottom-right corner:
     1. Right -> Right -> Down -> Down
     2. Down -> Down -> Right -> Right

     Example 2:
     Input: obstacleGrid = [[0,1],[0,0]]
     Output: 1


     Constraints:

     m == obstacleGrid.length
     n == obstacleGrid[i].length
     1 <= m, n <= 100
     obstacleGrid[i][j] is 0 or 1.
     */
    /*
     題意：這是 62.Unique Paths的變化題，在一個只能向下向右走且有障礙物的網格中，計算出所有可能路徑的數量

     思路：
        1.obstacleGrid[i][j]若本身為障礙物，則不可能到達該點，因此 dp[i][j]必為 0。
        2.排除規律(1)，跟 Unique Paths 那題一樣，dp[i][j] 會是 dp[i - 1][j] 和 dp[i][j - 1]的加總，
          簡單的說就是下一個網格的狀態會取決於其左面和上面的狀態加總
     */

    /*
      解法：動態規劃,用2維陣列
      1.定義好儲存path的二維陣列dp並將1放到原點
      2.依序對每個方格檢查，只要確定它不是障礙物，就依照判斷將該加上去的部分加上去
      3.全部遍歷過一次以後，回傳右下角的值即可。
    */
    public int uniquePathsWithObstaclesDp(int[][] obs) {
        if (obs == null || obs[0][0] ==1) return 0;
        int m = obs.length; //column
        int n = obs[0].length; // row
        int[][] dp = new int[m][n];
        dp[0][0] = 1;
        for (int i=0; i< m;i++) {
            for (int j=0;j<n;j++) {
                if (obs[i][j] !=1) {
                    if (i-1 >=0) dp[i][j] += dp[i-1][j];
                    if (j-1 >=0) dp[i][j] += dp[i][j-1];
                }
            }
        }
        return dp[m-1][n-1];
    }

    /*
      解法：動態規劃+空間優化
      因為求第i行時，只需要i-1行已求解過的值，不需要i-2行的了，可以用滾動數組進行優化，將二維數組改為一維數組
      1.初始化起點，設成1
      2.從左向右一列一列更新
      3.找出狀態方程式: 計算當前值 = 以求出的左邊值 + 上一次迭代同位置的值
        dp[j] = dp[j - 1] + dp[j]
      4.返回數組右下角的值 dp[m−1]
      Time Complexity: O(m*n)
      Space Complexity: O(n)
     */
    public int uniquePathsWithObstaclesOptimize(int[][] obstacleGrid) {
        int m = obstacleGrid.length;// column
        int n = obstacleGrid[0].length; // row
        System.out.println("Frank cloumn:"+ m + ", row:"+ n);
        int[] dp = new int[m];
        dp[0] = 1;

        for (int j=0; j<n;j++) {//列
            for (int i=0;i<m;i++) { //行
                //遇到障礙物直接給0
                if (obstacleGrid[i][j] == 1) {
                    dp[i] = 0;
                } else if (i > 0) {
                    //dp[i]的值由左方和上一次迭代的dp[i]累加而來
                    dp[i] += dp[i-1];
                }
            }
        }
        return dp[m-1];
    }

    public static void main(String[] args) {
        UniquePaths_II_63_M solution = new UniquePaths_II_63_M();
        int[][] array = {{0,0,0}, {0,1,0}, {0,0,0}};

        int result = solution.uniquePathsWithObstaclesDp(array);
        System.out.println("result: " + result);
    }
}
