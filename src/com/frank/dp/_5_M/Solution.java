package com.frank.dp._5_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2019/8/28
 *     title : 5. Longest Palindromic Substring
 * </pre>
 */
public class Solution {
    
	/**
     Given a string s, find the longest palindromic substring in s.
     You may assume that the maximum length of s is 1000.

     Example 1:
     Input: "babad"
     Output: "bab"
     Note: "aba" is also a valid answer.

     Example 2:
     Input: "cbbd"
     Output: "bb"
	*/
	/* 題目：給定一個String，並找出最長的回文子字串，如有多個相同長度的最長子字串可只回傳一個
    首先要了解 palindromic 是什麼:
    palindromic 是回文，也就是一個字串從前面念過去和從後面念過來是相同的，也就是左右對稱。Ex: abba, abcba
    */

    private boolean isPalindromic(String s) {
        int len = s.length();
        for (int i = 0; i < len / 2; i++) {
            if (s.charAt(i) != s.charAt(len - i - 1)) {
                return false;
            }
        }
        return true;
    }

	/*
	  解法1：暴力法
	  列舉所有的子串，判斷是否為回文串，保存最長的回文串。此方法要取出每個子字串出來比對。
	  時間複雜度：兩層 for 循環 O（n²），for 循環裡邊判斷是否為回文，O（n），所以時間複雜度為 O（n³）。
	 */
    private String longestPalindrome(String s) {
        if (s == null || s.length() == 0) return "";

        String ans = "";
        int max = 0;
        int len = s.length();
        for (int i = 0; i < len; i++)
            for (int j = i + 1; j <= len; j++) {
                String test = s.substring(i, j);
                System.out.println("test:" +test);
                if (isPalindromic(test) && test.length() > max) {
                    ans = s.substring(i, j);
                    max = Math.max(max, ans.length());
                    System.out.println("ans:" +ans + ", max:" + max);
                }
            }
        return ans;
    }

    /*

        解法2：Dynamic programming
        我們維護一個二維數組dp，其中dp[i][j] 表示字符串區間[i, j] 是否為回文串，
        當i = j 時，只有一個字符，肯定是回文串，如果i = j + 1，說明是相鄰字符，
        此時需要判斷s[i] 是否等於s[j]，如果i和j不相鄰，即i - j >= 2 時，
        除了判斷s[i] 和s [j] 相等之外，dp[i + 1][j - 1] 若為真，就是回文串，
        通過以上分析，可以寫出遞推式如下：

        dp[i, j] = 1 if i == j
        time:O(n²), space: O(n²)

     */
    private String longestPalindromeDP(String s) {
        int n = s.length();
        String res = "";

        boolean[][] dp = new boolean[n][n];

        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                dp[i][j] = s.charAt(i) == s.charAt(j) && (j - i < 3 || dp[i + 1][j - 1]);

                if (dp[i][j] && (res.isEmpty() || j - i + 1 > res.length())) {
                    res = s.substring(i, j + 1);
                }
            }
        }
        return res;
    }


    public static void main(String[] args) {
    	Solution solution = new Solution();
        int[] sample = {1, 4, 3, 2};

        String result = solution.longestPalindromeDP("a");
        System.out.println("result:" + result);
    }
}
