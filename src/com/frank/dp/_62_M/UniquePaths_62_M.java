package com.frank.dp._62_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/8
 *     title  : 62.Unique Paths
 * </pre>
 */
public class UniquePaths_62_M {
    /**
     There is a robot on an m x n grid. The robot is initially located
     at the top-left corner (i.e., grid[0][0]).
     The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]).
     The robot can only move either down or right at any point in time.

     Given the two integers m and n, return the number of possible unique paths
     that the robot can take to reach the bottom-right corner.

     The test cases are generated so that the answer will be less than or equal to 2 * 109.



     Example 1:
     Input: m = 3, n = 7
     Output: 28

     Example 2:
     Input: m = 3, n = 2
     Output: 3
     Explanation: From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
     1. Right -> Down -> Down
     2. Down -> Down -> Right
     3. Down -> Right -> Down


     Constraints:

     1 <= m, n <= 100
     */
    /*
     題意：機器人從左上角走到右下角，只能向右或者向下走。輸出總共有多少種走法。

     思路：
     */

    /*
      解法：使用DP
      1.初始化第一列，都設成1
      2.從左向右一列一列更新
      3.找出狀態方程式 dp[i] = dp[i] + dp[i-1];
      4.返回數組右下角的值 dp[m−1]
      Time Complexity: O(m*n)
      Space Complexity: O(n)
     */

    public int uniquePaths(int m, int n) {
        // ex, m=3(row), n=4(column)
        int[] dp = new int[m];
        //初始化第一列，都設成1
        for (int i=0; i<m; i++) {
            dp[i] = 1;
        }
        //從左向右一列一列更新
        /*
             1 1 1 1
             1 2 3 4
             1 3 6 10
         */
        // j: column, i: row
        for (int j=1;j<n;j++) {
            for (int i=1; i<m;i++) {
                dp[i] = dp[i] + dp[i-1];
            }
            System.out.println("for dp: " + Arrays.toString(dp));
        }
        System.out.println("final dp: " + Arrays.toString(dp));
        return dp[m-1];
    }



    public int uniquePathsTwo(int m, int n) {
        int[][] dp = new int[m][n];
        for(int i=0;i<m;i++) {
            dp[i][0] = 1;
        }
        for (int j=0; j<n;j++) {
            dp[0][j] = 1;
        }

        for (int i=1;i<m;i++) {
            for (int j=1;j<n;j++) {
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }

    public static void main(String[] args) {
        UniquePaths_62_M solution = new UniquePaths_62_M();

        int result = solution.uniquePaths(3,4);
        System.out.println("result: " + result);
    }
}
