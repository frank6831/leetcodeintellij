package com.frank.dp._64_M;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/7
 *     title  : 64.Minimum Path Sum
 * </pre>
 */
public class MinimumPathSum_64_M {
    /**
     Given a m x n grid filled with non-negative numbers, find a path from top left to
     bottom right, which minimizes the sum of all numbers along its path.

     Note: You can only move either down or right at any point in time.

     Example 1:
     Input: grid = [[1,3,1],[1,5,1],[4,2,1]]
     Output: 7
     Explanation: Because the path 1 → 3 → 1 → 1 → 1 minimizes the sum.

     Example 2:
     Input: grid = [[1,2,3],[4,5,6]]
     Output: 12


     Constraints:

     m == grid.length
     n == grid[i].length
     1 <= m, n <= 200
     0 <= grid[i][j] <= 100
     */
    /*
     題意：給定一個包含非負整數的 m*n 網格，請找出一條從左上角到右下角的路徑，使得路徑上的數字總和為最小。

     思路：由於路徑的方向只能是向下或向右，因此網格的第一行的每個元素只能從左上角元素開始向右移動到達，
          網格的第一列的每個元素只能從左上角元素開始向下移動到達，此時的路徑是唯一的，
          因此每個元素對應的最小路徑和即為對應的路徑上的數字總和。
          dp[i][j]紀錄到點grid[i][j]需要的最短步數
     */

    /*
      解法：使用DP
      1.元素的最小路徑和等於其上方相鄰元素與其左方相鄰元素兩者的最小路徑和中的最小值加上當前元素的值
      2.最左一列和第一行的所有位置都必須作為初始值，防止轉移方程越界。
      3.找出狀態方程式 grid[i][j] += min(grid[i - 1][j], grid[i][j - 1])
      4.返回数组右下角的值 g(m−1,n−1)
      Time Complexity: O(m*n) 遍歷整個 grid 矩陣元素
      Space Complexity: O(1)
     */

    public int minPathSum(int[][] grid) {
        // column
        int m = grid.length;
        // row
        int n = grid[0].length;

        // initial row
        for(int j=1; j< n;j++) {
            grid[0][j] += grid[0][j-1];
        }
        // initial col
        for (int i=1; i< m;i++) {
            grid[i][0] += grid[i-1][0];
        }

        // 利用递推式更新其它的
        for (int i=1;i< m;i++) {
            for (int j=1; j<n; j++) {
                grid[i][j] += Math.min(grid[i-1][j], grid[i][j-1]);
            }
        }
        //回傳最右下角結果
        return grid[m-1][n-1];
    }

    public static void main(String[] args) {
        MinimumPathSum_64_M solution = new MinimumPathSum_64_M();
        int[][] array = {{1,3,1}, {1,5,1}, {4,2,1}};
        int[][] array2 = {{1,2,3}, {4,5,6}};

        int result = solution.minPathSum(array2);
        System.out.println("result: " + result);
    }
}
