package com.frank.dp._980_M;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2023/2/10
 *     title  : 980.Unique Paths III
 * </pre>
 */
public class UniquePaths_III_980_H {
    /**
     You are given an m x n integer array grid where grid[i][j] could be:

     1 representing the starting square. There is exactly one starting square.
     2 representing the ending square. There is exactly one ending square.
     0 representing empty squares we can walk over.
     -1 representing obstacles that we cannot walk over.
     Return the number of 4-directional walks from the starting square to the ending square,
     that walk over every non-obstacle square exactly once.



     Example 1:


     Input: grid = [[1,0,0,0],[0,0,0,0],[0,0,2,-1]]
     Output: 2
     Explanation: We have the following two paths:
     1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
     2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)
     Example 2:


     Input: grid = [[1,0,0,0],[0,0,0,0],[0,0,0,2]]
     Output: 4
     Explanation: We have the following four paths:
     1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
     2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
     3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
     4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)
     Example 3:


     Input: grid = [[0,1],[2,0]]
     Output: 0
     Explanation: There is no path that walks over every empty square exactly once.
     Note that the starting and ending square can be anywhere in the grid.


     Constraints:

     m == grid.length
     n == grid[i].length
     1 <= m, n <= 20
     1 <= m * n <= 20
     -1 <= grid[i][j] <= 2
     There is exactly one starting cell and one ending cell.
    /*
     題意：機器人從左上角走到右下角，只能向右或者向下走。輸出總共有多少種走法。

     思路：
     */

    /*
      解法：使用DP
      1.初始化第一列，都設成1
      2.從左向右一列一列更新
      3.找出狀態方程式 dp[i] = dp[i] + dp[i-1];
      4.返回數組右下角的值 dp[m−1]
      Time Complexity: O(m*n)
      Space Complexity: O(n)
     */

    public int uniquePaths(int m, int n) {
        // ex, m=3(row), n=4(column)
        int[] dp = new int[m];
        //初始化第一列，都設成1
        for (int i=0; i<m; i++) {
            dp[i] = 1;
        }
        //從左向右一列一列更新
        /*
             1 1 1 1
             1 2 3 4
             1 3 6 10
         */
        // j: column, i: row
        for (int j=1;j<n;j++) {
            for (int i=1; i<m;i++) {
                dp[i] = dp[i] + dp[i-1];
            }
            System.out.println("for dp: " + Arrays.toString(dp));
        }
        System.out.println("final dp: " + Arrays.toString(dp));
        return dp[m-1];
    }


    public int uniquePathsTwo(int m, int n) {
        int[][] dp = new int[m][n];
        for(int i=0;i<m;i++) {
            dp[i][0] = 1;
        }
        for (int j=0; j<n;j++) {
            dp[0][j] = 1;
        }

        for (int i=1;i<m;i++) {
            for (int j=1;j<n;j++) {
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }

    public int uniquePathsIII(int[][] grid) {
        return 0;
    }

    public static void main(String[] args) {
        UniquePaths_III_980_H solution = new UniquePaths_III_980_H();

        int result = solution.uniquePaths(3,4);
        System.out.println("result: " + result);
    }
}
